<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AdsDeviceShare extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'device_id',
        'campaign_id',
        'campaign_banner_id',
    ];
}
