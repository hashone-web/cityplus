<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Kyslik\ColumnSortable\Sortable;

class Campaign extends Model
{
    use SoftDeletes, Sortable;

    public $sortable = [
        'id',
        'name',
        'start_date',
        'end_date',
    ];

    public function advertiser()
    {
        return $this->belongsTo('App\Advertiser', 'advertiser_id');
    }

    public function banners()
    {
        return $this->hasMany('App\CampaignBanner', 'campaign_id');
    }

    public function impressions()
    {
        return $this->hasMany('App\CampaignDeviceImpression', 'campaign_id');
    }

    public function clicks()
    {
        return $this->hasMany('App\CampaignDevice', 'campaign_id');
    }

    public function cities()
    {
        return $this->belongsToMany('App\City', 'campaign_cities', 'campaign_id', 'city_id');
    }

    public function reactions()
    {
        return $this->hasMany('App\AdsDeviceReaction', 'campaign_id');
    }

    public function shares()
    {
        return $this->hasMany('App\AdsDeviceShare', 'campaign_id');
    }
}
