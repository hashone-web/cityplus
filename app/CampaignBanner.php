<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CampaignBanner extends Model
{
    use SoftDeletes;

	public function getMediaAttribute($value)
    {
        return asset('/storage/campaigns/' . $this->campaign_id . '/banners/' . $this->id . '/' . $value);
    }

    public function getActionImageAttribute($value)
    {
    	if($value !== null) {
    		return asset('/storage/campaigns/' . $this->campaign_id . '/banners/' . $this->id . '/action_image/' . $value);
    	}
    	return $value;
    }

    public function campaign()
    {
        return $this->belongsTo('App\Campaign', 'campaign_id');
    }

    public function reactions()
    {
        return $this->hasMany('App\AdsDeviceReaction', 'campaign_banner_id');
    }

    public function shares()
    {
        return $this->hasMany('App\AdsDeviceShare', 'campaign_banner_id');
    }
}
