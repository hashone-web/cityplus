<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CampaignDevice extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'campaign_id',
        'campaign_banner_id',
        'device_id',
    ];
}
