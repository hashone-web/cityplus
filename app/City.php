<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Kyslik\ColumnSortable\Sortable;

class City extends Model
{
    use SoftDeletes, Sortable;

    public function district()
    {
        return $this->belongsTo('App\District', 'district_id');
    }

    public function news()
    {
        return $this->hasMany('App\News', 'city_id');
    }

    public function news_published_to()
    {
        return $this->belongsToMany('App\News', 'news_cities', 'city_id', 'news_id');
    }

    public function devices()
    {
        return $this->belongsToMany('App\Device', 'device_cities', 'city_id', 'device_id');
    }
}
