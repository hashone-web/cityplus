<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Kyslik\ColumnSortable\Sortable;

class CityGroup extends Model
{
    use SoftDeletes, Sortable;

    public function cities()
    {
        return $this->belongsToMany('App\City', 'city_group_cities', 'city_group_id', 'city_id');
    }
}
