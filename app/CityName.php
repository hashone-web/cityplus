<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CityName extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'city_id', 'language_id',
    ];
}
