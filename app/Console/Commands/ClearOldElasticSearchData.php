<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\News;

class ClearOldElasticSearchData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'clear:old_elastic_search_data';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Clear Old Elastic Search Data';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $before_date = \Carbon::now()->subDays(15)->format('Y-m-d H:i:s');

        $response = \Elasticsearch::deleteByQuery([
            'index' => 'news',
            'body' => [
                'query' => [
                    'bool' => [
                        'must' => [
                            'range' => [
                                'datetime' => [
                                    'lte' => $before_date
                                ]
                            ]
                        ]
                    ]
                ]
            ]
        ]);

        echo json_encode($response);

        $fb_before_date = \Carbon::now()->subDays(3)->format('Y-m-d H:i:s');

        $breaking_news_remove_time = \Carbon::now()->subHours(12);
        $breaking_news_ids = News::where('is_breaking_datetime', '>', $fb_before_date)->where('is_breaking_datetime', '<', $breaking_news_remove_time)->get()->pluck('id')->toArray();

        $featured_news_remove_time = \Carbon::now()->subHours(10 * 24);
        $featured_news_ids = News::where('is_featured_datetime', '>', $fb_before_date)->where('is_featured_datetime', '<', $featured_news_remove_time)->get()->pluck('id')->toArray();
    
        $ids_for_update = array_merge($breaking_news_ids, $featured_news_ids);
        $ids_for_update = array_unique($ids_for_update);

        foreach ($ids_for_update as $id_for_update) {
            $news = News::with(['poll.options', 'media', 'thumbs', 'downloads', 'city' => function($q) {
                    $q->leftJoin(\DB::raw('(SELECT city_id, name FROM city_names where language_id = 2 GROUP BY city_id, name) as cn'), 'cn.city_id', '=', 'cities.id');
                }])
                ->withCount('attachments')
                ->leftJoin(\DB::raw('(SELECT news_id, IFNULL(count(id), 0) AS reactions_claps_count FROM news_reactions where type ="Claps" GROUP BY news_id) as nrc'), 'nrc.news_id', '=', 'news.id')
                ->leftJoin(\DB::raw('(SELECT news_id, IFNULL(count(id), 0) AS reactions_sad_count FROM news_reactions where type ="Sad" GROUP BY news_id) as nrs'), 'nrs.news_id', '=', 'news.id')
                ->addSelect('news.*')
                ->addSelect('nrc.reactions_claps_count as reactions_claps_count')
                ->addSelect('nrc.reactions_claps_count as reactions_happy_count')
                ->addSelect('nrs.reactions_sad_count as reactions_sad_count')
                ->addSelect('nrc.reactions_claps_count as views')
                ->withCount('shares as shares_whatsapp_count');

            $news = $news->find($id_for_update);

            if($news) {
                $news_cities_ids = $news->cities()->pluck('cities.id')->toArray();
                $news_likes_device_ids = $news->likes()->pluck('device_id')->toArray();
                $news_dislikes_device_ids = $news->dislikes()->pluck('device_id')->toArray();
                $news_sensitivities_device_ids = $news->sensitivities()->pluck('device_id')->toArray();

                $news = $news->toArray();
                $news['cities'] = $news_cities_ids;
                $news['likes'] = $news_likes_device_ids;
                $news['dislikes'] = $news_dislikes_device_ids;
                $news['sensitivities'] = $news_sensitivities_device_ids;

                \Elasticsearch::update([
                    'id' => $news['id'],
                    'index' => 'news',
                    'body' => [
                        'doc' => $news,
                        'doc_as_upsert' => true,
                    ]
                ]);

                echo $news['id'];
            }
        }
    }
}
