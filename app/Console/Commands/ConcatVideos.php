<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

use App\News;
use App\NewsMedia;

class ConcatVideos extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'concat:videos {data}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Concat Videos for News';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $all_arguments = $this->arguments();
        $all_data = $all_arguments['data'];

        $news = $all_data['news'];
        $videos_for_concat = $all_data['videos_for_concat'];
        $media_data = $all_data['media_data'];

        $videos_for_concat_paths = [];

        $public_storage_path = 'app/public/';
        $path = 'news/' . $news->id . '/media/concat_original/';
        $app_path = storage_path($public_storage_path . $path);

        if (!file_exists($app_path)) {
            \File::makeDirectory($app_path, 0777, true);
        }

        for ($i=0; $i < count($videos_for_concat); $i++) {
            $video_for_concat_filename = $videos_for_concat[$i];
            $video_for_concat_resized_filename = 'resized_' . $video_for_concat_filename;

            $video_resize = 'ffmpeg -i "' . $app_path . $video_for_concat_filename . '" -c:v libx264 -pix_fmt yuv420p -crf 23 -framerate 24 -vf "scale=1280:720:force_original_aspect_ratio=decrease,pad=1280:720:(ow-iw)/2:(oh-ih)/2,setsar=1" "' . $app_path . $video_for_concat_resized_filename . '"';

            exec($video_resize);

            array_push($videos_for_concat_paths, $app_path . $video_for_concat_resized_filename);
        }

        $dynamic_id = Str::random(10);
        $file_name = Str::random(10)  . '.mp4';
        $path = 'news/' . $news->id . '/media/' . $dynamic_id . '/';
        $app_path = storage_path($public_storage_path . $path);

        if (!file_exists($app_path)) {
            \File::makeDirectory($app_path, 0777, true);
        }

        $video_combine_string = '';
        $video_combine_command = 'ffmpeg -y -threads 12 -vsync 2';
        foreach ($videos_for_concat_paths as $videos_for_concat_path_key => $videos_for_concat_path) {
            $video_combine_command = $video_combine_command . ' -i "' . $videos_for_concat_path . '"';
            $video_combine_string = $video_combine_string . '[' . $videos_for_concat_path_key . ':v]';
            $video_combine_string = $video_combine_string . '[' . $videos_for_concat_path_key . ':a]';
        }
        $video_combine_command = $video_combine_command . ' -filter_complex "' . $video_combine_string . 'concat=n='. count($videos_for_concat_paths) .':v=1:a=1[concat]" -map [concat] -vcodec libx264 -b:v 800k -acodec aac -b:a 128k';
        $video_combine_command = $video_combine_command . ' ' . $app_path . $file_name;

        echo $video_combine_command;

        exec($video_combine_command);

        $news_media = $this->storeNewsOriginalMedia($news, 'public/' . $path, $file_name, $dynamic_id, $media_data);

        News::where('id', $news->id)->update([ 'status' => $news->status ]);  

        $this->info('Done');
    }

    protected function storeNewsOriginalMedia($news, $app_path, $file_name, $dynamic_id, $media_data)
    {
        $news_media = new NewsMedia();
        $news_media->name = $file_name;
        $news_media->original_name = 'concat.mp4';
        $news_media->type = 'original';
        $news_media->extension = 'mp4';
        $news_media->dynamic_id = $dynamic_id;
        $news_media->mimetype = Storage::mimeType($app_path . $file_name);
        $news_media->size = Storage::size($app_path . $file_name);
        if($media_data !== null) {
            $news_media->sort = $media_data['sort'];
            $news_media->is_sensitive = $media_data['is_sensitive'];
            $news_media->is_file_photo = $media_data['is_file_photo'];
            $news_media->courtesy = $media_data['courtesy'];
            if(isset($media_data['watermark_position']) && $media_data['watermark_position'] !== null&& $media_data['watermark_position'] != 'none') {
                $news_media->watermark_position = $media_data['watermark_position'];
            }
        } else {
            $news_media->sort = $media_index;
        }
        $news_media->news_id = $news->id;
        $news_media->save();

        $news_media->full_path = $news_media->full_path_dynamic;
        $news_media->thumb = $news_media->thumb_dynamic;
        $news_media->update();

        $o_news_media = $news_media;

        $public_storage_path = 'app/public/';
        $path = 'news/' . $news->id . '/media/' . $news_media->dynamic_id;
        $app_path = storage_path($public_storage_path . $path);

        // Update thumb size in config gile when change 515px size
        $sizes = ['128px', '256px', '515px', '1080px'];

        $thumb_name = Str::random(10) . '.jpg';

        \FFMpeg::fromDisk('public')
            ->open($path . '/' . $news_media->name)
            ->getFrameFromSeconds(2)
            ->export()
            ->toDisk('public')
            ->save($path . '/thumb/' . $thumb_name);

        $thumb_media = \Image::make($app_path . '/thumb/' . $thumb_name);
        $news_media = $this->storeNewsMediaThumb($news, $news_media, $thumb_media, $thumb_name);

        if($media_data !== null && isset($media_data) && isset($media_data['watermark_position']) && $media_data['watermark_position'] !== null&& $media_data['watermark_position'] != 'none') {
            $original_media_width = $thumb_media->width();
            $convert_media_width = $original_media_width;

            $resize_percentage = 50;

            $watermark_width = (int) round($convert_media_width * ((100 - $resize_percentage) / 100), 2);

            $original_watermark_path = public_path('assets/img/watermark.png');

            $watermark_path = storage_path($public_storage_path . 'watermarks');
            if (!file_exists($watermark_path)) {
                \File::makeDirectory($watermark_path, 0777, true);
            }

            $watermark_path = $watermark_path . '/' . $watermark_width . '.png';

            \Image::make($original_watermark_path)
                    ->resize($watermark_width, null, function ($constraint) {
                        $constraint->aspectRatio();
                    })->save($watermark_path, 90);
            
            $format = new \FFMpeg\Format\Video\X264('aac', 'libx264');
            $format->setAdditionalParameters(['-vsync', 2]);
            $format->setKiloBitrate(800);

            if (!file_exists($path . '/watermarked')) {
                \File::makeDirectory($path . '/watermarked', 0777, true);
            }

            $watermark_position_code = "overlay=(main_w-overlay_w)/2:(main_h-overlay_h)/2";
            
            if($media_data['watermark_position'] == 'left') {
                $watermark_position_code = "overlay=0:(main_h-overlay_h)/2";
            }
            if($media_data['watermark_position'] == 'right') {
                $watermark_position_code = "overlay=main_w-overlay_w-0:(main_h-overlay_h)/2";
            }
            if($media_data['watermark_position'] == 'top-left') {
                $watermark_position_code = "overlay=0:0";
            }
            if($media_data['watermark_position'] == 'top') {
                $watermark_position_code = "overlay=(main_w-overlay_w)/2:0";
            }
            if($media_data['watermark_position'] == 'top-right') {
                $watermark_position_code = "overlay=main_w-overlay_w-0:0";
            }
            if($media_data['watermark_position'] == 'bottom-left') {
                $watermark_position_code = "overlay=0:main_h-overlay_h-0";
            }
            if($media_data['watermark_position'] == 'bottom') {
                $watermark_position_code = "overlay=(main_w-overlay_w)/2:main_h-overlay_h-0";
            }
            if($media_data['watermark_position'] == 'bottom-right') {
                $watermark_position_code = "overlay=main_w-overlay_w-0:main_h-overlay_h-0";
            }

            \FFMpeg::fromDisk('public')
                ->open($path . '/' . $o_news_media->name)
                ->addFilter(['-i', $watermark_path])
                ->addFilter(['-filter_complex', $watermark_position_code])
                ->export()
                ->toDisk('public')
                ->inFormat($format)
                ->save($path . '/watermarked/' . $o_news_media->name);
        }

        foreach ($sizes as $size) {
            $height = str_replace('px', '', $size);

            if(strpos($size, 'pc') !== false) {
                $height = str_replace('pc', '', $size);
                $height = ($thumb_media->height() * $height) / 100;
            }

            if (!file_exists($app_path . '/thumb/' . $size)) {
                \File::makeDirectory($app_path . '/thumb/' . $size, 0777, true);
            }

            $resized_media = \Image::make($app_path . '/thumb/' . $news_media->name)
                ->resize(null, $height, function ($constraint) {
                    $constraint->aspectRatio();
                });

            if($media_data !== null && isset($media_data) && isset($media_data['watermark_position']) && $media_data['watermark_position'] !== null&& $media_data['watermark_position'] != 'none') {
                $thumb_media_width = $thumb_media->width();
                $thumb_media_height = $thumb_media->height();

                $convert_media_ratio = $height / $thumb_media_height;
                $convert_media_width = $convert_media_ratio * $thumb_media_width;

                $resize_percentage = 50;
                $x_offset = round(25 * $convert_media_ratio);

                $watermark_width = round($convert_media_width * ((100 - $resize_percentage) / 100), 2); //watermark will be $resizePercentage less then the actual width of the image

                // resize watermark width keep height auto
                $watermark = \Image::make(public_path('assets/img/watermark.png'));
                $watermark->resize($watermark_width, null, function ($constraint) {
                    $constraint->aspectRatio();
                });

                $resized_media = $resized_media->insert($watermark, $media_data['watermark_position'], 0, 0); 
            }    

            $resized_media = $resized_media->save($app_path . '/thumb/' . $size . '/' . $news_media->name, 90);

            $this->storeNewsMedia($news, $news_media, $resized_media, $size);
        }

        return $news_media;
    }

    protected function storeNewsMediaThumb($news, $news_media, $thumb_media, $thumb_name)
    {
        $news_resized_media = new NewsMedia();
        $news_resized_media->name = $thumb_name;
        $news_resized_media->original_name = $thumb_name;
        $news_resized_media->type = 'thumb';
        $news_resized_media->details = json_encode([
            'width' => $thumb_media->width(),
            'height' => $thumb_media->height()
        ]);
        $news_resized_media->extension = 'jpg';
        $news_resized_media->dynamic_id = $news_media->dynamic_id;
        $news_resized_media->mimetype = $thumb_media->mime();
        $news_resized_media->size = $thumb_media->filesize();
        $news_resized_media->sort = $news_media->sort;
        if($news_media->is_sensitive) $news_resized_media->is_sensitive = $news_media->is_sensitive;
        if($news_media->is_file_photo) $news_resized_media->is_file_photo = $news_media->is_file_photo;
        if($news_media->courtesy) $news_resized_media->courtesy = $news_media->courtesy;
        if($news_media->watermark_position) $news_resized_media->watermark_position = $news_media->watermark_position;
        $news_resized_media->parent_id = $news_media->id;
        $news_resized_media->news_id = $news->id;
        $news_resized_media->save();

        $news_resized_media->full_path = $news_resized_media->full_path_dynamic;
        $news_resized_media->thumb = $news_resized_media->thumb_dynamic;
        $news_resized_media->update();

        if(\Config::get('constants.thumb_size') == $news_resized_media->type) {
            $original_news_media = NewsMedia::find($news_resized_media->parent_id);
            $original_news_media->full_path = $original_news_media->full_path_dynamic;
            $original_news_media->thumb = $original_news_media->thumb_dynamic;
            $original_news_media->update();
        }

        return $news_resized_media;
    }

    protected function storeNewsMedia($news, $news_media, $resized_media, $size)
    {
        $news_resized_media = new NewsMedia();
        $news_resized_media->name = $news_media->name;
        $news_resized_media->original_name = $news_media->original_name;
        $news_resized_media->type = $size;
        $news_resized_media->details = json_encode([
            'width' => $resized_media->width(),
            'height' => $resized_media->height()
        ]);
        $news_resized_media->extension = $news_media->extension;
        $news_resized_media->dynamic_id = $news_media->dynamic_id;
        $news_resized_media->mimetype = $news_media->mimetype;
        $news_resized_media->size = $resized_media->filesize();
        $news_resized_media->sort = $news_media->sort;
        if($news_media->is_sensitive) $news_resized_media->is_sensitive = $news_media->is_sensitive;
        if($news_media->is_file_photo) $news_resized_media->is_file_photo = $news_media->is_file_photo;
        if($news_media->courtesy) $news_resized_media->courtesy = $news_media->courtesy;
        if($news_media->watermark_position) $news_resized_media->watermark_position = $news_media->watermark_position;
        $news_resized_media->parent_id = $news_media->id;
        $news_resized_media->news_id = $news->id;
        $news_resized_media->save();

        $news_resized_media->full_path = $news_resized_media->full_path_dynamic;
        $news_resized_media->thumb = $news_resized_media->thumb_dynamic;
        $news_resized_media->update();

        if(\Config::get('constants.thumb_size') == $news_resized_media->type) {
            $original_thumb_news_media = NewsMedia::find($news_resized_media->parent_id);
            $original_thumb_news_media->full_path = $original_thumb_news_media->full_path_dynamic;
            $original_thumb_news_media->thumb = $original_thumb_news_media->thumb_dynamic;
            $original_thumb_news_media->update();

            if($original_thumb_news_media->type == 'thumb') {
                $original_news_media = NewsMedia::find($original_thumb_news_media->parent_id);
                $original_news_media->full_path = $original_news_media->full_path_dynamic;
                $original_news_media->thumb = $original_news_media->thumb_dynamic;
                $original_news_media->update();
            }
        }

        return $news_media;
    }
}
