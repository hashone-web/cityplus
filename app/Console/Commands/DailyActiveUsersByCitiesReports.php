<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\City;
use App\Device;
use App\DailyActiveUserByCityReport;

class DailyActiveUsersByCitiesReports extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'generate:daily_active_users_by_cities_report {--past=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate Daily Active Users by Cities Reports';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $past = (bool) $this->option('past');

        // Start date
        $date = date("Y-m-d");
        // End date
        // $end_date = date("Y-m-d");

        // while (strtotime($date) <= strtotime($end_date)) {
        //     $this->generateData($date);

        //     $date = date("Y-m-d", strtotime("+1 day", strtotime($date)));
        // }

        $this->generateData($date);

        $this->info('done');
    }

    public function generateData($date)
    {
        $date_obj = \Carbon::createFromFormat('Y-m-d', $date);

        $cities = City::withCount([
            'devices as devices_active_24' => function ($query) use($date) {
                $query->where('devices.last_active_at', '>', \Carbon::createFromFormat('Y-m-d', $date)->subHours(24)->format('Y-m-d H:i:s'))
                        ->where('devices.last_active_at', '<', \Carbon::createFromFormat('Y-m-d', $date)->format('Y-m-d H:i:s'));
            },
            'devices as devices_active_168' => function ($query) use($date) {
                $query->where('devices.last_active_at', '>', \Carbon::createFromFormat('Y-m-d', $date)->subHours(168)->format('Y-m-d H:i:s'))
                        ->where('devices.last_active_at', '<', \Carbon::createFromFormat('Y-m-d', $date)->format('Y-m-d H:i:s'));
            },
            'devices as devices_active_720' => function ($query) use($date) {
                $query->where('devices.last_active_at', '>', \Carbon::createFromFormat('Y-m-d', $date)->subHours(720)->format('Y-m-d H:i:s'))
                        ->where('devices.last_active_at', '<', \Carbon::createFromFormat('Y-m-d', $date)->format('Y-m-d H:i:s'));
            },
            'devices as devices_registered_24' => function ($query) use($date) {
                $query->where('devices.created_at', '>', \Carbon::createFromFormat('Y-m-d', $date)->subHours(24)->format('Y-m-d H:i:s'))
                        ->where('devices.created_at', '<', \Carbon::createFromFormat('Y-m-d', $date)->format('Y-m-d H:i:s'));
            }
        ])->get();

        foreach ($cities as $city) {
            if($city->devices_active_24 && $city->devices_registered_24) {
                $devices_active_24 = new DailyActiveUserByCityReport();
                $devices_active_24->date = $date;
                $devices_active_24->data = $city->devices_active_24;
                $devices_active_24->type = 'devices_active_24';
                $devices_active_24->city_id = $city->id;
                $devices_active_24->save();

                $devices_active_168 = new DailyActiveUserByCityReport();
                $devices_active_168->date = $date;
                $devices_active_168->data = $city->devices_active_168;
                $devices_active_168->type = 'devices_active_168';
                $devices_active_168->city_id = $city->id;
                $devices_active_168->save();

                $devices_active_720 = new DailyActiveUserByCityReport();
                $devices_active_720->date = $date;
                $devices_active_720->data = $city->devices_active_720;
                $devices_active_720->type = 'devices_active_720';
                $devices_active_720->city_id = $city->id;
                $devices_active_720->save();

                $devices_registered_24 = new DailyActiveUserByCityReport();
                $devices_registered_24->date = $date;
                $devices_registered_24->data = $city->devices_registered_24;
                $devices_registered_24->type = 'devices_registered_24';
                $devices_registered_24->city_id = $city->id;
                $devices_registered_24->save();
            }
        }

        echo "Done $date\n";
    }
}
