<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Redis;
use App\Device;

class DeviceDataCache extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cache:device_data';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Device Data Cache';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $latest_device = Device::orderByDesc('id')->first();
        
        if($latest_device) {
            $i = $latest_device->id;

            while($i > 0) {
                $device = Device::find($i);

                if($device) {
                    $device->load(['cities']);

                    $user_cities = $device->cities->pluck('id')->toArray();

                    $data = [
                        'device' => [ 
                            'id' => $device->id,
                            'unique_id' => $device->unique_id,
                        ],
                        'user_cities' => $user_cities
                    ];

                    Redis::set('device_data:' . $device->unique_id, json_encode($data));

                    echo $device->id . ', ';
                }

                $i--;
            }
        }
    }
}
