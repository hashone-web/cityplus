<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\CoronaVirusData;
use Carbon\Carbon;

class FetchCoronaVirusData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'corona:data';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Fetch Corona Virus Data';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->fetchWorldData();
        $this->info('Corona World Data Fetched');

        $this->fetchIndiaData();
        $this->info('Corona India Data Fetched');

        // $this->fetchGujaratData();
        // $this->info('Corona Gujarat Data Fetched');
    }

    public function fetchGujaratData() {
        try {
            $body = "[{\"DistrictName\":\"Ahmedabad\",\"ConfirmedCount\":1298,\"PatientTestedCount\":12611,\"PatientCuredCount\":49,\"PeopleQuarantineCount\":6745,\"ViolationOfQuarantineCount\":0,\"TotalDath\":43,\"Latitude\":\"23.018723960533\",\"Longitude\":\"72.5860194518048\",\"DistrictId\":1,\"IsConfirmCase\":1,\"PrevDayConfirmedCount\":1248,\"PrevDayPatientTestedCount\":12611,\"PrevDayPatientCuredCount\":49,\"PrevDayPeopleQuarantineCount\":6108,\"PrevDayViolationOfQuarantineCount\":0,\"PrevDayTotalDath\":38,\"DiffConfirmedCount\":50,\"DiffPatientTestedCount\":0,\"DiffPatientCuredCount\":0,\"DiffPeopleQuarantineCount\":637,\"DiffViolationOfQuarantineCount\":0,\"DiffTotalDath\":5,\"LastUpdatedDate\":\"\\/Date(1587450529390)\\/\"},{\"DistrictName\":\"Amreli\",\"ConfirmedCount\":0,\"PatientTestedCount\":371,\"PatientCuredCount\":0,\"PeopleQuarantineCount\":3033,\"ViolationOfQuarantineCount\":0,\"TotalDath\":0,\"Latitude\":\"21.6004607153301\",\"Longitude\":\"71.2135900377137\",\"DistrictId\":2,\"IsConfirmCase\":0,\"PrevDayConfirmedCount\":0,\"PrevDayPatientTestedCount\":371,\"PrevDayPatientCuredCount\":0,\"PrevDayPeopleQuarantineCount\":2869,\"PrevDayViolationOfQuarantineCount\":0,\"PrevDayTotalDath\":0,\"DiffConfirmedCount\":0,\"DiffPatientTestedCount\":0,\"DiffPatientCuredCount\":0,\"DiffPeopleQuarantineCount\":164,\"DiffViolationOfQuarantineCount\":0,\"DiffTotalDath\":0,\"LastUpdatedDate\":\"\\/Date(1587450529390)\\/\"},{\"DistrictName\":\"Anand\",\"ConfirmedCount\":28,\"PatientTestedCount\":310,\"PatientCuredCount\":3,\"PeopleQuarantineCount\":1556,\"ViolationOfQuarantineCount\":0,\"TotalDath\":2,\"Latitude\":\"22.5523436047385\",\"Longitude\":\"72.9565472474796\",\"DistrictId\":3,\"IsConfirmCase\":1,\"PrevDayConfirmedCount\":28,\"PrevDayPatientTestedCount\":310,\"PrevDayPatientCuredCount\":3,\"PrevDayPeopleQuarantineCount\":1558,\"PrevDayViolationOfQuarantineCount\":0,\"PrevDayTotalDath\":2,\"DiffConfirmedCount\":0,\"DiffPatientTestedCount\":0,\"DiffPatientCuredCount\":0,\"DiffPeopleQuarantineCount\":-2,\"DiffViolationOfQuarantineCount\":0,\"DiffTotalDath\":0,\"LastUpdatedDate\":\"\\/Date(1587450529390)\\/\"},{\"DistrictName\":\"Aravalli\",\"ConfirmedCount\":8,\"PatientTestedCount\":244,\"PatientCuredCount\":0,\"PeopleQuarantineCount\":962,\"ViolationOfQuarantineCount\":0,\"TotalDath\":1,\"Latitude\":\"23.46474737719\\r\\n\",\"Longitude\":\"73.2928172137946\\r\\n\",\"DistrictId\":4,\"IsConfirmCase\":1,\"PrevDayConfirmedCount\":7,\"PrevDayPatientTestedCount\":244,\"PrevDayPatientCuredCount\":0,\"PrevDayPeopleQuarantineCount\":857,\"PrevDayViolationOfQuarantineCount\":0,\"PrevDayTotalDath\":1,\"DiffConfirmedCount\":1,\"DiffPatientTestedCount\":0,\"DiffPatientCuredCount\":0,\"DiffPeopleQuarantineCount\":105,\"DiffViolationOfQuarantineCount\":0,\"DiffTotalDath\":0,\"LastUpdatedDate\":\"\\/Date(1587450529390)\\/\"},{\"DistrictName\":\"Banaskantha\",\"ConfirmedCount\":10,\"PatientTestedCount\":252,\"PatientCuredCount\":1,\"PeopleQuarantineCount\":4912,\"ViolationOfQuarantineCount\":0,\"TotalDath\":0,\"Latitude\":\"24.1708676782801\",\"Longitude\":\"72.4417140296196\",\"DistrictId\":5,\"IsConfirmCase\":1,\"PrevDayConfirmedCount\":10,\"PrevDayPatientTestedCount\":252,\"PrevDayPatientCuredCount\":1,\"PrevDayPeopleQuarantineCount\":38,\"PrevDayViolationOfQuarantineCount\":0,\"PrevDayTotalDath\":0,\"DiffConfirmedCount\":0,\"DiffPatientTestedCount\":0,\"DiffPatientCuredCount\":0,\"DiffPeopleQuarantineCount\":4874,\"DiffViolationOfQuarantineCount\":0,\"DiffTotalDath\":0,\"LastUpdatedDate\":\"\\/Date(1587450529390)\\/\"},{\"DistrictName\":\"Bharuch\",\"ConfirmedCount\":23,\"PatientTestedCount\":514,\"PatientCuredCount\":2,\"PeopleQuarantineCount\":443,\"ViolationOfQuarantineCount\":0,\"TotalDath\":1,\"Latitude\":\"21.6935584258995\",\"Longitude\":\"72.9852169994023\",\"DistrictId\":6,\"IsConfirmCase\":1,\"PrevDayConfirmedCount\":23,\"PrevDayPatientTestedCount\":514,\"PrevDayPatientCuredCount\":2,\"PrevDayPeopleQuarantineCount\":345,\"PrevDayViolationOfQuarantineCount\":0,\"PrevDayTotalDath\":1,\"DiffConfirmedCount\":0,\"DiffPatientTestedCount\":0,\"DiffPatientCuredCount\":0,\"DiffPeopleQuarantineCount\":98,\"DiffViolationOfQuarantineCount\":0,\"DiffTotalDath\":0,\"LastUpdatedDate\":\"\\/Date(1587450529390)\\/\"},{\"DistrictName\":\"Bhavnagar\",\"ConfirmedCount\":32,\"PatientTestedCount\":1471,\"PatientCuredCount\":16,\"PeopleQuarantineCount\":250,\"ViolationOfQuarantineCount\":0,\"TotalDath\":5,\"Latitude\":\"21.7772864617379\",\"Longitude\":\"72.1461201074812\",\"DistrictId\":7,\"IsConfirmCase\":1,\"PrevDayConfirmedCount\":32,\"PrevDayPatientTestedCount\":1471,\"PrevDayPatientCuredCount\":16,\"PrevDayPeopleQuarantineCount\":263,\"PrevDayViolationOfQuarantineCount\":0,\"PrevDayTotalDath\":4,\"DiffConfirmedCount\":0,\"DiffPatientTestedCount\":0,\"DiffPatientCuredCount\":0,\"DiffPeopleQuarantineCount\":-13,\"DiffViolationOfQuarantineCount\":0,\"DiffTotalDath\":1,\"LastUpdatedDate\":\"\\/Date(1587450529390)\\/\"},{\"DistrictName\":\"Botad\",\"ConfirmedCount\":5,\"PatientTestedCount\":194,\"PatientCuredCount\":0,\"PeopleQuarantineCount\":123,\"ViolationOfQuarantineCount\":0,\"TotalDath\":1,\"Latitude\":\"22.1669110907742\",\"Longitude\":\"71.6668725802862\",\"DistrictId\":8,\"IsConfirmCase\":1,\"PrevDayConfirmedCount\":5,\"PrevDayPatientTestedCount\":194,\"PrevDayPatientCuredCount\":0,\"PrevDayPeopleQuarantineCount\":113,\"PrevDayViolationOfQuarantineCount\":0,\"PrevDayTotalDath\":1,\"DiffConfirmedCount\":0,\"DiffPatientTestedCount\":0,\"DiffPatientCuredCount\":0,\"DiffPeopleQuarantineCount\":10,\"DiffViolationOfQuarantineCount\":0,\"DiffTotalDath\":0,\"LastUpdatedDate\":\"\\/Date(1587450529390)\\/\"},{\"DistrictName\":\"Chhota Udaipur\",\"ConfirmedCount\":7,\"PatientTestedCount\":93,\"PatientCuredCount\":1,\"PeopleQuarantineCount\":14,\"ViolationOfQuarantineCount\":0,\"TotalDath\":0,\"Latitude\":\"22.3025269973207\\r\\n\",\"Longitude\":\"74.011177672762\\r\\n\",\"DistrictId\":9,\"IsConfirmCase\":1,\"PrevDayConfirmedCount\":7,\"PrevDayPatientTestedCount\":93,\"PrevDayPatientCuredCount\":1,\"PrevDayPeopleQuarantineCount\":12,\"PrevDayViolationOfQuarantineCount\":0,\"PrevDayTotalDath\":0,\"DiffConfirmedCount\":0,\"DiffPatientTestedCount\":0,\"DiffPatientCuredCount\":0,\"DiffPeopleQuarantineCount\":2,\"DiffViolationOfQuarantineCount\":0,\"DiffTotalDath\":0,\"LastUpdatedDate\":\"\\/Date(1587450529390)\\/\"},{\"DistrictName\":\"Dahod\",\"ConfirmedCount\":3,\"PatientTestedCount\":178,\"PatientCuredCount\":0,\"PeopleQuarantineCount\":356,\"ViolationOfQuarantineCount\":0,\"TotalDath\":0,\"Latitude\":\"22.8299762317198\",\"Longitude\":\"74.2578773408475\",\"DistrictId\":10,\"IsConfirmCase\":1,\"PrevDayConfirmedCount\":3,\"PrevDayPatientTestedCount\":178,\"PrevDayPatientCuredCount\":0,\"PrevDayPeopleQuarantineCount\":322,\"PrevDayViolationOfQuarantineCount\":0,\"PrevDayTotalDath\":0,\"DiffConfirmedCount\":0,\"DiffPatientTestedCount\":0,\"DiffPatientCuredCount\":0,\"DiffPeopleQuarantineCount\":34,\"DiffViolationOfQuarantineCount\":0,\"DiffTotalDath\":0,\"LastUpdatedDate\":\"\\/Date(1587450529390)\\/\"},{\"DistrictName\":\"Dang\",\"ConfirmedCount\":0,\"PatientTestedCount\":51,\"PatientCuredCount\":0,\"PeopleQuarantineCount\":181,\"ViolationOfQuarantineCount\":0,\"TotalDath\":0,\"Latitude\":\"20.7582535387483\",\"Longitude\":\"73.6884653269463\",\"DistrictId\":11,\"IsConfirmCase\":0,\"PrevDayConfirmedCount\":0,\"PrevDayPatientTestedCount\":51,\"PrevDayPatientCuredCount\":0,\"PrevDayPeopleQuarantineCount\":179,\"PrevDayViolationOfQuarantineCount\":0,\"PrevDayTotalDath\":0,\"DiffConfirmedCount\":0,\"DiffPatientTestedCount\":0,\"DiffPatientCuredCount\":0,\"DiffPeopleQuarantineCount\":2,\"DiffViolationOfQuarantineCount\":0,\"DiffTotalDath\":0,\"LastUpdatedDate\":\"\\/Date(1587450529390)\\/\"},{\"DistrictName\":\"Devbhoomi Dwarka\",\"ConfirmedCount\":0,\"PatientTestedCount\":154,\"PatientCuredCount\":0,\"PeopleQuarantineCount\":565,\"ViolationOfQuarantineCount\":0,\"TotalDath\":0,\"Latitude\":\"22.2056188639704\\r\\n\",\"Longitude\":\"69.6487645730444\\r\\n\",\"DistrictId\":12,\"IsConfirmCase\":0,\"PrevDayConfirmedCount\":0,\"PrevDayPatientTestedCount\":154,\"PrevDayPatientCuredCount\":0,\"PrevDayPeopleQuarantineCount\":553,\"PrevDayViolationOfQuarantineCount\":0,\"PrevDayTotalDath\":0,\"DiffConfirmedCount\":0,\"DiffPatientTestedCount\":0,\"DiffPatientCuredCount\":0,\"DiffPeopleQuarantineCount\":12,\"DiffViolationOfQuarantineCount\":0,\"DiffTotalDath\":0,\"LastUpdatedDate\":\"\\/Date(1587450529390)\\/\"},{\"DistrictName\":\"Gandhinagar\",\"ConfirmedCount\":17,\"PatientTestedCount\":403,\"PatientCuredCount\":11,\"PeopleQuarantineCount\":261,\"ViolationOfQuarantineCount\":0,\"TotalDath\":2,\"Latitude\":\"23.2112738848939\",\"Longitude\":\"72.6431454058677\",\"DistrictId\":13,\"IsConfirmCase\":1,\"PrevDayConfirmedCount\":17,\"PrevDayPatientTestedCount\":403,\"PrevDayPatientCuredCount\":11,\"PrevDayPeopleQuarantineCount\":231,\"PrevDayViolationOfQuarantineCount\":0,\"PrevDayTotalDath\":2,\"DiffConfirmedCount\":0,\"DiffPatientTestedCount\":0,\"DiffPatientCuredCount\":0,\"DiffPeopleQuarantineCount\":30,\"DiffViolationOfQuarantineCount\":0,\"DiffTotalDath\":0,\"LastUpdatedDate\":\"\\/Date(1587450529390)\\/\"},{\"DistrictName\":\"Gir Somnath\",\"ConfirmedCount\":3,\"PatientTestedCount\":72,\"PatientCuredCount\":1,\"PeopleQuarantineCount\":32,\"ViolationOfQuarantineCount\":0,\"TotalDath\":0,\"Latitude\":\"20.9049995004973\",\"Longitude\":\"70.3670177564914\",\"DistrictId\":14,\"IsConfirmCase\":1,\"PrevDayConfirmedCount\":2,\"PrevDayPatientTestedCount\":72,\"PrevDayPatientCuredCount\":1,\"PrevDayPeopleQuarantineCount\":28,\"PrevDayViolationOfQuarantineCount\":0,\"PrevDayTotalDath\":0,\"DiffConfirmedCount\":1,\"DiffPatientTestedCount\":0,\"DiffPatientCuredCount\":0,\"DiffPeopleQuarantineCount\":4,\"DiffViolationOfQuarantineCount\":0,\"DiffTotalDath\":0,\"LastUpdatedDate\":\"\\/Date(1587450529390)\\/\"},{\"DistrictName\":\"Jamnagar\",\"ConfirmedCount\":1,\"PatientTestedCount\":599,\"PatientCuredCount\":0,\"PeopleQuarantineCount\":852,\"ViolationOfQuarantineCount\":0,\"TotalDath\":1,\"Latitude\":\"22.4640220292663\",\"Longitude\":\"70.0774816800224\",\"DistrictId\":15,\"IsConfirmCase\":1,\"PrevDayConfirmedCount\":1,\"PrevDayPatientTestedCount\":599,\"PrevDayPatientCuredCount\":0,\"PrevDayPeopleQuarantineCount\":854,\"PrevDayViolationOfQuarantineCount\":0,\"PrevDayTotalDath\":1,\"DiffConfirmedCount\":0,\"DiffPatientTestedCount\":0,\"DiffPatientCuredCount\":0,\"DiffPeopleQuarantineCount\":-2,\"DiffViolationOfQuarantineCount\":0,\"DiffTotalDath\":0,\"LastUpdatedDate\":\"\\/Date(1587450529390)\\/\"},{\"DistrictName\":\"Junagadh\",\"ConfirmedCount\":0,\"PatientTestedCount\":294,\"PatientCuredCount\":0,\"PeopleQuarantineCount\":1050,\"ViolationOfQuarantineCount\":0,\"TotalDath\":0,\"Latitude\":\"21.5235832084201\",\"Longitude\":\"70.4653743314948\",\"DistrictId\":16,\"IsConfirmCase\":0,\"PrevDayConfirmedCount\":0,\"PrevDayPatientTestedCount\":294,\"PrevDayPatientCuredCount\":0,\"PrevDayPeopleQuarantineCount\":969,\"PrevDayViolationOfQuarantineCount\":0,\"PrevDayTotalDath\":0,\"DiffConfirmedCount\":0,\"DiffPatientTestedCount\":0,\"DiffPatientCuredCount\":0,\"DiffPeopleQuarantineCount\":81,\"DiffViolationOfQuarantineCount\":0,\"DiffTotalDath\":0,\"LastUpdatedDate\":\"\\/Date(1587450529390)\\/\"},{\"DistrictName\":\"Kutch\",\"ConfirmedCount\":6,\"PatientTestedCount\":353,\"PatientCuredCount\":0,\"PeopleQuarantineCount\":1459,\"ViolationOfQuarantineCount\":0,\"TotalDath\":1,\"Latitude\":\"23.253767720836\\r\\n\",\"Longitude\":\"69.67039551566\\r\\n\",\"DistrictId\":17,\"IsConfirmCase\":1,\"PrevDayConfirmedCount\":6,\"PrevDayPatientTestedCount\":353,\"PrevDayPatientCuredCount\":0,\"PrevDayPeopleQuarantineCount\":1411,\"PrevDayViolationOfQuarantineCount\":0,\"PrevDayTotalDath\":1,\"DiffConfirmedCount\":0,\"DiffPatientTestedCount\":0,\"DiffPatientCuredCount\":0,\"DiffPeopleQuarantineCount\":48,\"DiffViolationOfQuarantineCount\":0,\"DiffTotalDath\":0,\"LastUpdatedDate\":\"\\/Date(1587450529390)\\/\"},{\"DistrictName\":\"Kheda\",\"ConfirmedCount\":3,\"PatientTestedCount\":249,\"PatientCuredCount\":0,\"PeopleQuarantineCount\":134,\"ViolationOfQuarantineCount\":0,\"TotalDath\":0,\"Latitude\":\"22.6959997697355\",\"Longitude\":\"72.8662719908244\",\"DistrictId\":18,\"IsConfirmCase\":1,\"PrevDayConfirmedCount\":2,\"PrevDayPatientTestedCount\":249,\"PrevDayPatientCuredCount\":0,\"PrevDayPeopleQuarantineCount\":140,\"PrevDayViolationOfQuarantineCount\":0,\"PrevDayTotalDath\":0,\"DiffConfirmedCount\":1,\"DiffPatientTestedCount\":0,\"DiffPatientCuredCount\":0,\"DiffPeopleQuarantineCount\":-6,\"DiffViolationOfQuarantineCount\":0,\"DiffTotalDath\":0,\"LastUpdatedDate\":\"\\/Date(1587450529390)\\/\"},{\"DistrictName\":\"Mahisagar\",\"ConfirmedCount\":3,\"PatientTestedCount\":120,\"PatientCuredCount\":0,\"PeopleQuarantineCount\":1122,\"ViolationOfQuarantineCount\":0,\"TotalDath\":0,\"Latitude\":\"23.1248679521486\",\"Longitude\":\"73.6113776427237\",\"DistrictId\":19,\"IsConfirmCase\":1,\"PrevDayConfirmedCount\":3,\"PrevDayPatientTestedCount\":120,\"PrevDayPatientCuredCount\":0,\"PrevDayPeopleQuarantineCount\":1242,\"PrevDayViolationOfQuarantineCount\":0,\"PrevDayTotalDath\":0,\"DiffConfirmedCount\":0,\"DiffPatientTestedCount\":0,\"DiffPatientCuredCount\":0,\"DiffPeopleQuarantineCount\":-120,\"DiffViolationOfQuarantineCount\":0,\"DiffTotalDath\":0,\"LastUpdatedDate\":\"\\/Date(1587450529390)\\/\"},{\"DistrictName\":\"Mehsana\",\"ConfirmedCount\":6,\"PatientTestedCount\":128,\"PatientCuredCount\":0,\"PeopleQuarantineCount\":143,\"ViolationOfQuarantineCount\":0,\"TotalDath\":0,\"Latitude\":\"23.6011614699681\\r\\n\",\"Longitude\":\"72.3948631092104\\r\\n\",\"DistrictId\":20,\"IsConfirmCase\":1,\"PrevDayConfirmedCount\":6,\"PrevDayPatientTestedCount\":128,\"PrevDayPatientCuredCount\":0,\"PrevDayPeopleQuarantineCount\":111,\"PrevDayViolationOfQuarantineCount\":0,\"PrevDayTotalDath\":0,\"DiffConfirmedCount\":0,\"DiffPatientTestedCount\":0,\"DiffPatientCuredCount\":0,\"DiffPeopleQuarantineCount\":32,\"DiffViolationOfQuarantineCount\":0,\"DiffTotalDath\":0,\"LastUpdatedDate\":\"\\/Date(1587450529390)\\/\"},{\"DistrictName\":\"Morbi\",\"ConfirmedCount\":1,\"PatientTestedCount\":93,\"PatientCuredCount\":0,\"PeopleQuarantineCount\":97,\"ViolationOfQuarantineCount\":0,\"TotalDath\":0,\"Latitude\":\"22.8140616602745\",\"Longitude\":\"70.8328588524747\",\"DistrictId\":21,\"IsConfirmCase\":1,\"PrevDayConfirmedCount\":1,\"PrevDayPatientTestedCount\":93,\"PrevDayPatientCuredCount\":0,\"PrevDayPeopleQuarantineCount\":0,\"PrevDayViolationOfQuarantineCount\":0,\"PrevDayTotalDath\":0,\"DiffConfirmedCount\":0,\"DiffPatientTestedCount\":0,\"DiffPatientCuredCount\":0,\"DiffPeopleQuarantineCount\":97,\"DiffViolationOfQuarantineCount\":0,\"DiffTotalDath\":0,\"LastUpdatedDate\":\"\\/Date(1587450529390)\\/\"},{\"DistrictName\":\"Narmada\",\"ConfirmedCount\":12,\"PatientTestedCount\":523,\"PatientCuredCount\":0,\"PeopleQuarantineCount\":782,\"ViolationOfQuarantineCount\":0,\"TotalDath\":0,\"Latitude\":\"21.8679617101035\",\"Longitude\":\"73.5021053463554\",\"DistrictId\":22,\"IsConfirmCase\":1,\"PrevDayConfirmedCount\":12,\"PrevDayPatientTestedCount\":523,\"PrevDayPatientCuredCount\":0,\"PrevDayPeopleQuarantineCount\":732,\"PrevDayViolationOfQuarantineCount\":0,\"PrevDayTotalDath\":0,\"DiffConfirmedCount\":0,\"DiffPatientTestedCount\":0,\"DiffPatientCuredCount\":0,\"DiffPeopleQuarantineCount\":50,\"DiffViolationOfQuarantineCount\":0,\"DiffTotalDath\":0,\"LastUpdatedDate\":\"\\/Date(1587450529390)\\/\"},{\"DistrictName\":\"Navsari\",\"ConfirmedCount\":0,\"PatientTestedCount\":398,\"PatientCuredCount\":0,\"PeopleQuarantineCount\":5,\"ViolationOfQuarantineCount\":0,\"TotalDath\":0,\"Latitude\":\"20.9523630400929\",\"Longitude\":\"72.9309102319874\",\"DistrictId\":23,\"IsConfirmCase\":0,\"PrevDayConfirmedCount\":0,\"PrevDayPatientTestedCount\":398,\"PrevDayPatientCuredCount\":0,\"PrevDayPeopleQuarantineCount\":5,\"PrevDayViolationOfQuarantineCount\":0,\"PrevDayTotalDath\":0,\"DiffConfirmedCount\":0,\"DiffPatientTestedCount\":0,\"DiffPatientCuredCount\":0,\"DiffPeopleQuarantineCount\":0,\"DiffViolationOfQuarantineCount\":0,\"DiffTotalDath\":0,\"LastUpdatedDate\":\"\\/Date(1587450529390)\\/\"},{\"DistrictName\":\"Panchmahal\",\"ConfirmedCount\":11,\"PatientTestedCount\":93,\"PatientCuredCount\":0,\"PeopleQuarantineCount\":473,\"ViolationOfQuarantineCount\":0,\"TotalDath\":2,\"Latitude\":\"22.7746525560328\",\"Longitude\":\"73.6140416862791\",\"DistrictId\":24,\"IsConfirmCase\":1,\"PrevDayConfirmedCount\":11,\"PrevDayPatientTestedCount\":93,\"PrevDayPatientCuredCount\":0,\"PrevDayPeopleQuarantineCount\":410,\"PrevDayViolationOfQuarantineCount\":0,\"PrevDayTotalDath\":2,\"DiffConfirmedCount\":0,\"DiffPatientTestedCount\":0,\"DiffPatientCuredCount\":0,\"DiffPeopleQuarantineCount\":63,\"DiffViolationOfQuarantineCount\":0,\"DiffTotalDath\":0,\"LastUpdatedDate\":\"\\/Date(1587450529390)\\/\"},{\"DistrictName\":\"Patan\",\"ConfirmedCount\":15,\"PatientTestedCount\":212,\"PatientCuredCount\":11,\"PeopleQuarantineCount\":83,\"ViolationOfQuarantineCount\":0,\"TotalDath\":1,\"Latitude\":\"23.8483770637658\",\"Longitude\":\"72.1169104538126\",\"DistrictId\":25,\"IsConfirmCase\":1,\"PrevDayConfirmedCount\":15,\"PrevDayPatientTestedCount\":212,\"PrevDayPatientCuredCount\":11,\"PrevDayPeopleQuarantineCount\":88,\"PrevDayViolationOfQuarantineCount\":0,\"PrevDayTotalDath\":1,\"DiffConfirmedCount\":0,\"DiffPatientTestedCount\":0,\"DiffPatientCuredCount\":0,\"DiffPeopleQuarantineCount\":-5,\"DiffViolationOfQuarantineCount\":0,\"DiffTotalDath\":0,\"LastUpdatedDate\":\"\\/Date(1587450529390)\\/\"},{\"DistrictName\":\"Porbandar\",\"ConfirmedCount\":3,\"PatientTestedCount\":211,\"PatientCuredCount\":3,\"PeopleQuarantineCount\":35,\"ViolationOfQuarantineCount\":0,\"TotalDath\":0,\"Latitude\":\"21.6397192020473\",\"Longitude\":\"69.600457705833\",\"DistrictId\":26,\"IsConfirmCase\":1,\"PrevDayConfirmedCount\":3,\"PrevDayPatientTestedCount\":211,\"PrevDayPatientCuredCount\":3,\"PrevDayPeopleQuarantineCount\":7,\"PrevDayViolationOfQuarantineCount\":0,\"PrevDayTotalDath\":0,\"DiffConfirmedCount\":0,\"DiffPatientTestedCount\":0,\"DiffPatientCuredCount\":0,\"DiffPeopleQuarantineCount\":28,\"DiffViolationOfQuarantineCount\":0,\"DiffTotalDath\":0,\"LastUpdatedDate\":\"\\/Date(1587450529390)\\/\"},{\"DistrictName\":\"Rajkot\",\"ConfirmedCount\":40,\"PatientTestedCount\":1326,\"PatientCuredCount\":12,\"PeopleQuarantineCount\":511,\"ViolationOfQuarantineCount\":0,\"TotalDath\":0,\"Latitude\":\"22.3068004656872\",\"Longitude\":\"70.7983801602294\",\"DistrictId\":27,\"IsConfirmCase\":1,\"PrevDayConfirmedCount\":38,\"PrevDayPatientTestedCount\":1326,\"PrevDayPatientCuredCount\":12,\"PrevDayPeopleQuarantineCount\":180,\"PrevDayViolationOfQuarantineCount\":0,\"PrevDayTotalDath\":0,\"DiffConfirmedCount\":2,\"DiffPatientTestedCount\":0,\"DiffPatientCuredCount\":0,\"DiffPeopleQuarantineCount\":331,\"DiffViolationOfQuarantineCount\":0,\"DiffTotalDath\":0,\"LastUpdatedDate\":\"\\/Date(1587450529390)\\/\"},{\"DistrictName\":\"Sabarkantha\",\"ConfirmedCount\":2,\"PatientTestedCount\":229,\"PatientCuredCount\":2,\"PeopleQuarantineCount\":316,\"ViolationOfQuarantineCount\":0,\"TotalDath\":0,\"Latitude\":\"23.5896284492593\",\"Longitude\":\"72.9673290298342\",\"DistrictId\":28,\"IsConfirmCase\":1,\"PrevDayConfirmedCount\":2,\"PrevDayPatientTestedCount\":229,\"PrevDayPatientCuredCount\":2,\"PrevDayPeopleQuarantineCount\":323,\"PrevDayViolationOfQuarantineCount\":0,\"PrevDayTotalDath\":0,\"DiffConfirmedCount\":0,\"DiffPatientTestedCount\":0,\"DiffPatientCuredCount\":0,\"DiffPeopleQuarantineCount\":-7,\"DiffViolationOfQuarantineCount\":0,\"DiffTotalDath\":0,\"LastUpdatedDate\":\"\\/Date(1587450529390)\\/\"},{\"DistrictName\":\"Surat\",\"ConfirmedCount\":338,\"PatientTestedCount\":9117,\"PatientCuredCount\":11,\"PeopleQuarantineCount\":3488,\"ViolationOfQuarantineCount\":0,\"TotalDath\":10,\"Latitude\":\"21.1940878263477\",\"Longitude\":\"72.8282856728081\",\"DistrictId\":29,\"IsConfirmCase\":1,\"PrevDayConfirmedCount\":269,\"PrevDayPatientTestedCount\":9117,\"PrevDayPatientCuredCount\":11,\"PrevDayPeopleQuarantineCount\":3368,\"PrevDayViolationOfQuarantineCount\":0,\"PrevDayTotalDath\":10,\"DiffConfirmedCount\":69,\"DiffPatientTestedCount\":0,\"DiffPatientCuredCount\":0,\"DiffPeopleQuarantineCount\":120,\"DiffViolationOfQuarantineCount\":0,\"DiffTotalDath\":0,\"LastUpdatedDate\":\"\\/Date(1587450529390)\\/\"},{\"DistrictName\":\"Surendranagar\",\"ConfirmedCount\":0,\"PatientTestedCount\":154,\"PatientCuredCount\":0,\"PeopleQuarantineCount\":0,\"ViolationOfQuarantineCount\":0,\"TotalDath\":0,\"Latitude\":\"22.7079429671536\",\"Longitude\":\"71.6765436883286\",\"DistrictId\":30,\"IsConfirmCase\":0,\"PrevDayConfirmedCount\":0,\"PrevDayPatientTestedCount\":154,\"PrevDayPatientCuredCount\":0,\"PrevDayPeopleQuarantineCount\":0,\"PrevDayViolationOfQuarantineCount\":0,\"PrevDayTotalDath\":0,\"DiffConfirmedCount\":0,\"DiffPatientTestedCount\":0,\"DiffPatientCuredCount\":0,\"DiffPeopleQuarantineCount\":0,\"DiffViolationOfQuarantineCount\":0,\"DiffTotalDath\":0,\"LastUpdatedDate\":\"\\/Date(1587450529390)\\/\"},{\"DistrictName\":\"Tapi\",\"ConfirmedCount\":1,\"PatientTestedCount\":167,\"PatientCuredCount\":0,\"PeopleQuarantineCount\":53,\"ViolationOfQuarantineCount\":0,\"TotalDath\":0,\"Latitude\":\"21.1104407961683\",\"Longitude\":\"73.394228528114\",\"DistrictId\":31,\"IsConfirmCase\":1,\"PrevDayConfirmedCount\":0,\"PrevDayPatientTestedCount\":167,\"PrevDayPatientCuredCount\":0,\"PrevDayPeopleQuarantineCount\":68,\"PrevDayViolationOfQuarantineCount\":0,\"PrevDayTotalDath\":0,\"DiffConfirmedCount\":1,\"DiffPatientTestedCount\":0,\"DiffPatientCuredCount\":0,\"DiffPeopleQuarantineCount\":-15,\"DiffViolationOfQuarantineCount\":0,\"DiffTotalDath\":0,\"LastUpdatedDate\":\"\\/Date(1587450529390)\\/\"},{\"DistrictName\":\"Vadodara\",\"ConfirmedCount\":188,\"PatientTestedCount\":1810,\"PatientCuredCount\":8,\"PeopleQuarantineCount\":313,\"ViolationOfQuarantineCount\":0,\"TotalDath\":7,\"Latitude\":\"22.3039660985918\",\"Longitude\":\"73.1959994575652\",\"DistrictId\":32,\"IsConfirmCase\":1,\"PrevDayConfirmedCount\":188,\"PrevDayPatientTestedCount\":1810,\"PrevDayPatientCuredCount\":8,\"PrevDayPeopleQuarantineCount\":361,\"PrevDayViolationOfQuarantineCount\":0,\"PrevDayTotalDath\":7,\"DiffConfirmedCount\":0,\"DiffPatientTestedCount\":0,\"DiffPatientCuredCount\":0,\"DiffPeopleQuarantineCount\":-48,\"DiffViolationOfQuarantineCount\":0,\"DiffTotalDath\":0,\"LastUpdatedDate\":\"\\/Date(1587450529390)\\/\"},{\"DistrictName\":\"Valsad\",\"ConfirmedCount\":2,\"PatientTestedCount\":322,\"PatientCuredCount\":0,\"PeopleQuarantineCount\":5,\"ViolationOfQuarantineCount\":0,\"TotalDath\":0,\"Latitude\":\"20.615154841022\",\"Longitude\":\"72.928839756257\",\"DistrictId\":33,\"IsConfirmCase\":1,\"PrevDayConfirmedCount\":0,\"PrevDayPatientTestedCount\":322,\"PrevDayPatientCuredCount\":0,\"PrevDayPeopleQuarantineCount\":4,\"PrevDayViolationOfQuarantineCount\":0,\"PrevDayTotalDath\":0,\"DiffConfirmedCount\":2,\"DiffPatientTestedCount\":0,\"DiffPatientCuredCount\":0,\"DiffPeopleQuarantineCount\":1,\"DiffViolationOfQuarantineCount\":0,\"DiffTotalDath\":0,\"LastUpdatedDate\":\"\\/Date(1587450529390)\\/\"}]";
            $data = json_decode($body, true);

            foreach ($data as $key => $data1) {
                $data2 = [
                    'confirmed' => $data1['ConfirmedCount'],
                    'active' => $data1['ConfirmedCount'] - ($data1['PatientCuredCount'] + $data1['TotalDath']),
                    'recovered' => $data1['PatientCuredCount'],
                    'deaths' => $data1['TotalDath'],
                    'deltaconfirmed' => $data1['DiffConfirmedCount'],
                    'deltadeaths' => $data1['DiffTotalDath'],
                    'deltarecovered' => $data1['DiffPatientCuredCount'],
                ];
                $this->storeData($data1['DistrictName'], $data2);
            }

            return [ 'status' => true ];
        } catch(\Exception $e) {
            echo $e;
        }

        return [ 'status' => false ];
    }

    public function fetchWorldData() {
        try {
            $client = new \Goutte\Client();
            $crawler = $client->request('GET', 'https://www.worldometers.info/coronavirus/');

            $data = null;
            $i = 0;
            $crawler->filter('#main_table_countries_today tbody')->each(function ($node) use(&$data, &$i) {
                if($i == 2) {
                    $node->filter('tr')->each(function ($node1) use(&$data) {
                        $data = [
                            'total' => 0,
                            'confirmed' => 0,
                            'active' => 0,
                            'recovered' => 0,
                            'deaths' => 0,
                            'deltaconfirmed' => 0,
                            'deltadeaths' => 0,
                            'deltarecovered' => 0,
                        ];
                        
                        $flag = false;
                        $j = 0;
                        
                        $node1->filter('td')->each(function ($node2) use(&$data, &$flag, &$j) {
                            if($j == 1 && $node2->text() == 'Total:') {
                                $flag = true;
                            }
                            if($flag) {
                                if($j == 2) {
                                    $data['confirmed'] = str_replace(',', '', $node2->text());
                                }
                                if($j == 3) {
                                    $data['deltaconfirmed'] = str_replace(',', '', $node2->text());
                                }
                                if($j == 4) {
                                    $data['deaths'] = str_replace(',', '', $node2->text());
                                }
                                if($j == 5) {
                                    $data['deltadeaths'] = str_replace(',', '', $node2->text());
                                }
                                if($j == 6) {
                                    $data['recovered'] = str_replace(',', '', $node2->text());
                                }
                                if($j == 8) {
                                    $data['active'] = str_replace(',', '', $node2->text());
                                }
                            }
                            $j = $j + 1;
                        });
                    });
                }
                $i = $i + 1;
            });

            if($data !== null) {
                $this->storeData('world', $data);
            }

            return [ 'status' => true ];
        } catch(\Exception $e) {
            echo $e;
        }

        return [ 'status' => false ];
    }

    public function fetchIndiaData() {
        $client = new \GuzzleHttp\Client();
        $response = $client->request('GET', 'https://api.covid19india.org/data.json');

        if($response->getStatusCode() == 200) {
            try {
                $body = $response->getBody();
                $body = json_decode($body, true);

                if(isset($body['statewise'])) {
                    foreach ($body['statewise'] as $key => $state) {
                        if($state['state'] == 'Total') $state['state'] = 'India';

                        if($state['state'] == 'India' || $state['state'] == 'Gujarat') {
                            $this->storeData(strtolower($state['state']), $state);
                        }
                    }
                }

                return [ 'status' => true ];
            } catch(\Exception $e) {
                echo $e;
            }
        }

        return [ 'status' => false ];
    }

    public function storeData($name, $data)
    {
        $corona_virus_data = CoronaVirusData::firstOrNew([ 'name' => $name ]);
        $corona_virus_data->total = $data['confirmed'];
        $corona_virus_data->active = $data['active'];
        $corona_virus_data->recovered = $data['recovered'];
        $corona_virus_data->deaths = $data['deaths'];
        $corona_virus_data->delta_total = str_replace('+', '', $data['deltaconfirmed']);
        $corona_virus_data->delta_active = str_replace('+', '', $data['deltaconfirmed']);
        $corona_virus_data->delta_recovered = str_replace('+', '', $data['deltarecovered']);
        $corona_virus_data->delta_deaths = str_replace('+', '', $data['deltadeaths']);
        if($corona_virus_data->exists) {
            if(!$corona_virus_data->manual) {
                $corona_virus_data->last_updated_at = Carbon::now();
                $corona_virus_data->update();
            }
        } else {
            $corona_virus_data->save();
        }

        if($corona_virus_data && $corona_virus_data->name == 'world') {
            CoronaVirusData::where('name', 'gujarat')->update(['updated_at' => Carbon::now() ]);
        }

        return [ 'status' => true ];
    }
}