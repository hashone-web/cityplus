<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\MarketValue;

class FetchMarketValue extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'market:value';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Fetch Market Values';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $current_hour = (int) date('H');

        if($current_hour == 0 || ($current_hour >= 9 && $current_hour < 23)) {
            $commodity_data = $this->fetchCommodity();
            if($commodity_data['status']) {
                foreach ($commodity_data['data'] as $commodity => $value) {
                    $this->storeData($commodity, $value);
                }

                $this->info('Commodity Data Done');
            }
        }

        if($current_hour == 0 || ($current_hour >= 9 && $current_hour < 16)) {
            $sensex_data = $this->fetchSensex();
            if($sensex_data['status']) {
                $this->storeData('sensex', $sensex_data['value']);

                $this->info('Sensex Data Done');
            }
        }
    }

    public function fetchSensex() {
        $client = new \GuzzleHttp\Client();
        $response = $client->request('GET', 'https://appfeeds.moneycontrol.com/jsonapi/market/indices&format=&ind_id=4');

        if($response->getStatusCode() == 200) {
            try {
                $body = $response->getBody();
                $body = json_decode($body, true);

                $value = floatval(str_replace(',', '', $body['indices']['lastprice']));

                return [ 'status' => true, 'value' => $value ];
            } catch(\Exception $e) {
                echo $e;
            }
        }

        return [ 'status' => false ];
    }

    public function fetchCommodity() {
        try {
            $client = new \Goutte\Client();
            $crawler = $client->request('GET', 'https://www.moneycontrol.com/commodity/');

            $values = [];
            $crawler->filter('#comm_tab1 tr')->each(function ($node) use(&$values) {
                if(strpos($node->text(), 'GOLD') !== false || strpos($node->text(), 'SILVER') !== false) {
                    $key = 'gold';
                    if(strpos($node->text(), 'SILVER') !== false) {
                        $key = 'silver';
                    }
                    $i = 0;
                    $node->filter('td')->each(function ($node1) use(&$values, $key, &$i) {
                        if($i == 1) {
                            $values[$key] = floatval(str_replace(',', '', $node1->text()));
                        }
                        $i = $i + 1;
                    });
                }
            });

            return [ 'status' => true, 'data' => $values ];
        } catch(\Exception $e) {
            echo $e;
        }

        return [ 'status' => false ];
    }

    public function storeData($type, $value)
    {
        $date = \Carbon::now()->format('Y-m-d');

        $market_value = MarketValue::firstOrNew([ 'type' => $type ]);
        $old_market_value_current = $market_value->current;

        if($market_value->current_date !== $date) {
            $market_value->current_date = $date;
            if($market_value->current == null) {
                $market_value->current = $value;
            }
            $market_value->previous = $market_value->current;
            $market_value->current = $value;
        } else {
            $market_value->current = $value;
        }

        if(($old_market_value_current !== $value) && ($market_value->type == 'gold' || $market_value->type == 'silver' || $market_value->type == 'sensex')) {
            $market_value->is_increased = $market_value->current > $market_value->previous;
        }
        $market_value->save();

        return [ 'status' => true ];
    }
}
