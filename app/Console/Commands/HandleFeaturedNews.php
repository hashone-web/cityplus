<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\News;
use App\AdminPanelSetting;

use App\Events\NewsFeatured;

class HandleFeaturedNews extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'handle:featured_news';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Handle Featured News';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $admin_panel_settings = AdminPanelSetting::first();

        if($admin_panel_settings) {
            $datetime_period = \Carbon::now()->subHours(24);

            $all_news_ids_for_featured = [];

            $news_ids = News::where('is_featured', 0)
                ->where('datetime', '>', $datetime_period)
                ->where('status', 'Published')
                ->whereNull('is_featured_datetime')
                ->whereNotNull('published_datetime')
                ->get()->pluck('id')->toArray();
            
            // IsFeatured by Shares
            if($admin_panel_settings->is_featured_shares_scan_period_1 !== null &&
                $admin_panel_settings->is_featured_shares_count_1 !== null)
            {
                $shares_datetime_period = \Carbon::now()->subMinutes($admin_panel_settings->is_featured_shares_scan_period_1);

                $news_ids_for_featured = News::whereIn('id', $news_ids)
                    ->has('shares', '>=' , $admin_panel_settings->is_featured_shares_count_1)
                    ->where('published_datetime', '>', $shares_datetime_period)
                    ->get()->pluck('id')->toArray();
                $all_news_ids_for_featured = array_merge($all_news_ids_for_featured, $news_ids_for_featured);
            }

            if($admin_panel_settings->is_featured_shares_scan_period_2 !== null &&
                $admin_panel_settings->is_featured_shares_count_2 !== null)
            {
                $shares_datetime_period = \Carbon::now()->subMinutes($admin_panel_settings->is_featured_shares_scan_period_2);

                $news_ids_for_featured = News::whereIn('id', $news_ids)
                    ->has('shares', '>=' , $admin_panel_settings->is_featured_shares_count_2)
                    ->where('published_datetime', '>', $shares_datetime_period)
                    ->get()->pluck('id')->toArray();
                $all_news_ids_for_featured = array_merge($all_news_ids_for_featured, $news_ids_for_featured);
            }

            if($admin_panel_settings->is_featured_shares_scan_period_3 !== null &&
                $admin_panel_settings->is_featured_shares_count_3 !== null)
            {
                $shares_datetime_period = \Carbon::now()->subMinutes($admin_panel_settings->is_featured_shares_scan_period_3);

                $news_ids_for_featured = News::whereIn('id', $news_ids)
                    ->has('shares', '>=' , $admin_panel_settings->is_featured_shares_count_3)
                    ->where('published_datetime', '>', $shares_datetime_period)
                    ->get()->pluck('id')->toArray();
                $all_news_ids_for_featured = array_merge($all_news_ids_for_featured, $news_ids_for_featured);
            }

            // IsFeatured by Reactions
            if($admin_panel_settings->is_featured_reactions_scan_period_1 !== null &&
                $admin_panel_settings->is_featured_reactions_count_1 !== null)
            {
                $reactions_datetime_period = \Carbon::now()->subMinutes($admin_panel_settings->is_featured_reactions_scan_period_1);

                $news_ids_for_featured = News::whereIn('id', $news_ids)
                    ->has('reactions', '>=' , $admin_panel_settings->is_featured_reactions_count_1)
                    ->where('published_datetime', '>', $reactions_datetime_period)
                    ->get()->pluck('id')->toArray();
                $all_news_ids_for_featured = array_merge($all_news_ids_for_featured, $news_ids_for_featured);
            }

            if($admin_panel_settings->is_featured_reactions_scan_period_2 !== null &&
                $admin_panel_settings->is_featured_reactions_count_2 !== null)
            {
                $reactions_datetime_period = \Carbon::now()->subMinutes($admin_panel_settings->is_featured_reactions_scan_period_2);

                $news_ids_for_featured = News::whereIn('id', $news_ids)
                    ->has('reactions', '>=' , $admin_panel_settings->is_featured_reactions_count_2)
                    ->where('published_datetime', '>', $reactions_datetime_period)
                    ->get()->pluck('id')->toArray();
                $all_news_ids_for_featured = array_merge($all_news_ids_for_featured, $news_ids_for_featured);
            }

            if($admin_panel_settings->is_featured_reactions_scan_period_3 !== null &&
                $admin_panel_settings->is_featured_reactions_count_3 !== null)
            {
                $reactions_datetime_period = \Carbon::now()->subMinutes($admin_panel_settings->is_featured_reactions_scan_period_3);

                $news_ids_for_featured = News::whereIn('id', $news_ids)
                    ->has('reactions', '>=' , $admin_panel_settings->is_featured_reactions_count_3)
                    ->where('published_datetime', '>', $reactions_datetime_period)
                    ->get()->pluck('id')->toArray();
                $all_news_ids_for_featured = array_merge($all_news_ids_for_featured, $news_ids_for_featured);
            }

            $all_news_ids_for_featured = array_unique($all_news_ids_for_featured);

            if(count($all_news_ids_for_featured)) {
                echo "---";
                echo \Carbon::now();
                echo ": ";
                echo implode(', ', $all_news_ids_for_featured);
                echo " ";
            }

            if(count($all_news_ids_for_featured)) {
                event(new NewsFeatured([ 'ids' => $all_news_ids_for_featured ]));
            }

            News::whereIn('id', $all_news_ids_for_featured)
                ->update([
                    'is_featured' => 1,
                    'is_featured_datetime' => \Carbon::now(),
                ]);
        }

        $this->info('Done');
    }
}