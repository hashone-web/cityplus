<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\CoronaVirusData;

class ResetCoronaVirusData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'corona:reset';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Reset Corona Virus Data';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $gujarat_corona_virus_data = CoronaVirusData::where('name', 'gujarat')->first();
        if($gujarat_corona_virus_data) {
            $gujarat_corona_virus_data->delta_total = 0;
            $gujarat_corona_virus_data->delta_active = 0;
            $gujarat_corona_virus_data->delta_recovered = 0;
            $gujarat_corona_virus_data->delta_deaths = 0;

            $gujarat_corona_virus_data->update();


            CoronaVirusData::where('parent_id', $gujarat_corona_virus_data->id)->update([
                'delta_total' => 0,
                'delta_active' => 0,
                'delta_recovered' => 0,
                'delta_deaths' => 0
            ]);

            $this->info('Corona Data Reseted');
        } else {
            $this->info('Corona Gujarat Data not found');
        }
    }
}
