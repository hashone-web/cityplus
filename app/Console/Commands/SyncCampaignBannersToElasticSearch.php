<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Campaign;

class SyncCampaignBannersToElasticSearch extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sync:campaign_banners_to_elastic_search';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sync Campaign Banners To Elastic Search';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $campaigns = Campaign::with('banners', 'cities')->get();
        foreach ($campaigns as $campaign) {
            foreach ($campaign->banners as $banner) {
                $banner_data = $banner->toArray();
                $banner_data['cities'] = $campaign->cities->pluck('id')->toArray();
                $banner_data['campaign_start_date'] = $campaign->start_date;
                $banner_data['campaign_end_date'] = $campaign->end_date;
                $banner_data['campaign_status'] = $campaign->status;
                $banner_data['campaign_deleted_at'] = $campaign->deleted_at === null? 0: 1;
                
                \Elasticsearch::update([
                    'id' => $banner_data['id'],
                    'index' => 'ads',
                    'body' => [
                        'doc' => $banner_data,
                        'doc_as_upsert' => true,
                    ]
                ]);

                echo $banner_data['id'];
            }
        }
    }
}
