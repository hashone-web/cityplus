<?php

namespace App\Console\Commands;

use Illuminate\Support\Facades\DB;
use Illuminate\Console\Command;
use App\Campaign;

class SyncCampaignStats extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sync:campaign_stats';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sync Campaign Stats';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $campaigns = Campaign::with('advertiser')->withCount(['reactions as o_reactions_count', 'shares as o_shares_count', 'clicks as o_clicks_count', 'impressions as o_impressions_count' => function ($query) {
            $query->select(DB::raw("SUM(impressions) as impressions_count"))->where('campaign_device_impressions.impression_date', '!=', '0000-00-00');
        }])->get();

        foreach ($campaigns as $campaign_key => $campaign) {
            $campaign->reactions_count = $campaign->o_reactions_count;
            $campaign->shares_count = $campaign->o_shares_count;
            $campaign->clicks_count = $campaign->o_clicks_count;
            $campaign->impressions_count = $campaign->o_impressions_count;
            $campaign->save();
        }

        echo "Done";
    }
}
