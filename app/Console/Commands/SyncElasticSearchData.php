<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\News;

class SyncElasticSearchData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sync:elastic_search_data {--size=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sync ElasticSearch Data';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $size = (int) $this->option('size');

        $latest_news = News::orderByDesc('created_at')->first();
        
        if($latest_news) {
            $y = 0;
            $i = $latest_news->id;

            while($i > 0) {
                $y++;

                if($size !== 0 && $y > $size) {
                    $i = 0;
                }

                $news = News::with(['poll.options', 'media', 'thumbs', 'downloads', 'categories', 'created_by_user', 'updated_by_user', 'notifications', 'city' => function($q) {
                        $q->leftJoin(\DB::raw('(SELECT city_id, name FROM city_names where language_id = 2 GROUP BY city_id, name) as cn'), 'cn.city_id', '=', 'cities.id');
                    }])
                    ->withCount('attachments')
                    ->leftJoin(\DB::raw('(SELECT news_id, IFNULL(count(id), 0) AS reactions_claps_count FROM news_reactions where type ="Claps" GROUP BY news_id) as nrc'), 'nrc.news_id', '=', 'news.id')
                    ->leftJoin(\DB::raw('(SELECT news_id, IFNULL(count(id), 0) AS reactions_sad_count FROM news_reactions where type ="Sad" GROUP BY news_id) as nrs'), 'nrs.news_id', '=', 'news.id')
                    ->addSelect('news.*')
                    ->addSelect('nrc.reactions_claps_count as reactions_claps_count')
                    ->addSelect('nrc.reactions_claps_count as reactions_happy_count')
                    ->addSelect('nrs.reactions_sad_count as reactions_sad_count')
                    ->addSelect('nrc.reactions_claps_count as views')
                    ->withCount('shares as shares_whatsapp_count');

                $news = $news->find($i);

                if($news) {
                    $news_categories = $news->categories();
                    $news_categories_ids = $news->categories()->pluck('category_id')->toArray();
                    $news_categories = $news->categories()->get()->toArray();
                    $news_cities = $news->cities();
                    $news_cities_ids = $news_cities->pluck('cities.id')->toArray();
                    $news_cities = $news_cities->get()->toArray();
                    $news_likes_device_ids = $news->likes()->pluck('device_id')->toArray();
                    $news_dislikes_device_ids = $news->dislikes()->pluck('device_id')->toArray();
                    $news_sensitivities_device_ids = $news->sensitivities()->pluck('device_id')->toArray();

                    $media_views = $news->media_views !== null? $news->media_views: 0;
                    $media_downloads = $news->media_downloads !== null? $news->media_downloads: 0;

                    $news = $news->toArray();
                    $news['categories_ids'] = $news_categories_ids;
                    $news['cities'] = $news_cities_ids;
                    $news['cities_with_details'] = $news_cities;
                    $news['categories_with_details'] = $news_categories;
                    $news['media_views'] = $media_views;
                    $news['media_downloads'] = $media_downloads;
                    if($news['v2_cities']) {
                        $news['v2_cities_ids'] = $news_cities_ids;
                    } else {
                        $news['v2_cities_ids'] = [];
                    }
                    $news['likes'] = $news_likes_device_ids;
                    $news['dislikes'] = $news_dislikes_device_ids;
                    $news['sensitivities'] = $news_sensitivities_device_ids;

                    \Elasticsearch::update([
                        'id' => $news['id'],
                        'index' => 'news',
                        'body' => [
                            'doc' => $news,
                            'doc_as_upsert' => true,
                        ]
                    ]);

                    echo $news['id'];
                }

                $i--;
            }
        }

        $this->info('Done');
    }
}
