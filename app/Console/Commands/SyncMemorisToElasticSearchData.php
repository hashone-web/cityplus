<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Memory;

class SyncMemorisToElasticSearchData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sync:memories_to_elastic_search';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sync Memories To Elastic Search';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $memories = Memory::with('from_names')->get();

        foreach ($memories as $key => $memory) {
            $memory_cities_ids = $memory->cities()->pluck('cities.id')->toArray();
            $memory->cities = $memory_cities_ids;
            $memory = $memory->toArray();
            $memory['reactions_count'] = 0;
            $memory['shares_count'] = 0;
            
            \Elasticsearch::update([
                'id' => $memory['id'],
                'index' => 'memories',
                'body' => [
                    'doc' => $memory,
                    'doc_as_upsert' => true,
                ]
            ]);

            echo $memory['id'];
        }
    }
}
