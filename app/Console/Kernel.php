<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        Commands\ResizeNewsMedia::class,
        Commands\DeleteSizedNewsMedia::class,
        Commands\HandleBreakingAndFeaturedNews::class,
        Commands\FetchMarketValue::class,
        Commands\FetchCoronaVirusData::class,
        Commands\ResetCoronaVirusData::class,
        Commands\MakeNewsMediaPath::class,
        Commands\HandleFeaturedNews::class,
        Commands\GenerateDailyStatisticsReport::class,
        Commands\NewsNotificationOneSignalData::class,
        Commands\DailyActiveUsersByCitiesReports::class,
        Commands\DeviceDataCache::class,
        Commands\ProcessNews::class,
        Commands\ProcessNewsStatus::class,
        Commands\DailyNotificationDataReports::class,
        Commands\SyncMemorisToElasticSearchData::class,
        Commands\SyncCampaignStats::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')
        //          ->hourly();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
