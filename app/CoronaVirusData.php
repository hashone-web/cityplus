<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CoronaVirusData extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
    ];

    public function parent()
    {
        return $this->belongsTo('App\CoronaVirusData', 'parent_id');
    }

    public function children()
    {
        return $this->hasMany('App\CoronaVirusData', 'parent_id');
    }
}
