<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Kyslik\ColumnSortable\Sortable;

class Country extends Model
{
    use SoftDeletes, Sortable;

    public $sortable = [
        'name',
        'created_at'
    ];
}
