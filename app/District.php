<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Kyslik\ColumnSortable\Sortable;

class District extends Model
{
    use SoftDeletes, Sortable;

    public $sortable = [
        'name',
        'created_at'
    ];

    public function state()
    {
        return $this->belongsTo('App\State', 'state_id');
    }

    public function cities()
    {
        return $this->hasMany('App\City', 'district_id');
    }
}
