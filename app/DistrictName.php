<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DistrictName extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'district_id', 'language_id',
    ];
}
