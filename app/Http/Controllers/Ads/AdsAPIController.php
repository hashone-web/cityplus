<?php

namespace App\Http\Controllers\Ads;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

use App\Device;
use App\CampaignBanner;
use App\AdsDeviceReaction;
use App\AdsDeviceShare;

class AdsAPIController extends Controller
{
    public function storeReactions(Request $request) {
        $validator = Validator::make($request->all(), [
            'campaign_id' => 'required',
            'campaign_banner_id' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 401);
        }

        $data = ['status' => true];

        $device = Device::firstOrNew(['unique_id' => $request->input('device_id')]);
        $device->save();

        try {
            $device_reaction = AdsDeviceReaction::firstOrNew(['device_id' => $device->id, 'campaign_id' => $request->input('campaign_id'), 'campaign_banner_id' => $request->input('campaign_banner_id')]);
            $device_reaction->save();

            $campaign_banner = CampaignBanner::find($request->input('campaign_banner_id'));

            if($campaign_banner) {
                $device_reactions_device_ids = $campaign_banner->reactions()->pluck('device_id')->toArray();
            
                $campaign_banner = $campaign_banner->toArray();
                $device_for_elastic_search = [];
                $device_for_elastic_search['reactions'] = $device_reactions_device_ids;
                $device_for_elastic_search['reactions_count'] = count($device_reactions_device_ids);

                if(env('ELASTICSEARCH')) {
                    \Elasticsearch::update([
                        'id' => $campaign_banner['id'],
                        'index' => 'ads',
                        'body' => [
                            'doc' => $device_for_elastic_search,
                            'doc_as_upsert' => true
                        ]
                    ]);
                }

                $data['data'] = $campaign_banner;
            } else {
                $data['status'] = false;
            }
        } catch (\Exception $e) {
            $data['status'] = false;
        }

        return $data;
    }

    public function storeShares(Request $request) {
        $validator = Validator::make($request->all(), [
            'campaign_id' => 'required',
            'campaign_banner_id' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 401);
        }

        $data = ['status' => true];

        $device = Device::firstOrNew(['unique_id' => $request->input('device_id')]);
        $device->save();

        try {
            $device_share = AdsDeviceShare::firstOrNew(['device_id' => $device->id, 'campaign_id' => $request->input('campaign_id'), 'campaign_banner_id' => $request->input('campaign_banner_id')]);
            $device_share->save();
            
            $campaign_banner = CampaignBanner::find($request->input('campaign_banner_id'));

            if($campaign_banner) {
                $device_shares_device_ids = $campaign_banner->shares()->pluck('device_id')->toArray();

                $campaign_banner = $campaign_banner->toArray();
                $device_for_elastic_search = [];

                $device_for_elastic_search['shares'] = $device_shares_device_ids;
                $device_for_elastic_search['shares_count'] = count($device_shares_device_ids);

                if(env('ELASTICSEARCH')) {
                    \Elasticsearch::update([
                        'id' => $campaign_banner['id'],
                        'index' => 'ads',
                        'body' => [
                            'doc' => $device_for_elastic_search,
                            'doc_as_upsert' => true
                        ]
                    ]);
                }

                $data['data'] = $campaign_banner;
            } else {
                $data['status'] = false;    
            }
        } catch (\Exception $e) {
            $data['status'] = false;
        }

        return $data;
    }
}
