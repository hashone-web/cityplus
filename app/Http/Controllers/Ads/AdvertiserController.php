<?php

namespace App\Http\Controllers\Ads;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

use App\Advertiser;

class AdvertiserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $advertisers = Advertiser::sortable(['id' => 'desc'])->paginate(\Config::get('constants.pagination_size'));

        return view('pages.ads.advertisers.index', compact('advertisers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.ads.advertisers.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => ['required', 'string', 'max:255', 'unique:advertisers'],
        ]);

        if ($validator->fails()) {
            return redirect()
                ->route('advertisers.create')
                ->withErrors($validator)
                ->withInput();
        }

        try {
            DB::beginTransaction();

            $advertiser = new Advertiser();
            $advertiser->name = $request->input('name');
            $advertiser->business_name = $request->input('business_name');
            $advertiser->contact_number = $request->input('contact_number');
            $advertiser->save();

            DB::commit();

            \Toastr::success('Advertiser Created successfully', 'Success', []);
        } catch (\Exception $e) {
            DB::rollback();
            \Toastr::error('Error occured during creating advertiser', 'Error', []);
        }

        return redirect()->route('advertisers.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $advertiser = Advertiser::find($id);

        if($advertiser) {
            return view('pages.ads.advertisers.create', compact('advertiser'));
        }

        abort(404);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $advertiser = Advertiser::find($id);

        if($advertiser) {
            $validator = Validator::make($request->all(), [
                'name' => ['required', 'string', 'max:255', 'unique:advertisers,name,' . $id],
            ]);

            if ($validator->fails()) {
                return redirect()
                    ->route('advertisers.edit', ['id' => $id])
                    ->withErrors($validator)
                    ->withInput();
            }

            try {
                DB::beginTransaction();

                $advertiser->name = $request->input('name');
                $advertiser->business_name = $request->input('business_name');
                $advertiser->contact_number = $request->input('contact_number');
                $advertiser->update();

                DB::commit();

                \Toastr::success('Advertiser Updated successfully', 'Success', []);
            } catch (\Exception $e) {
                DB::rollback();
                \Toastr::error('Error occured during updating advertiser', 'Error', []);
            }

            return redirect()->route('advertisers.index');
        }

        abort(404);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $advertiser = Advertiser::find($id);

        if($advertiser) {
            $advertiser->delete();

            \Toastr::success('Advertiser Deleted successfully', 'Success', []);

            return redirect()->route('advertisers.index');
        }

        abort(404);
    }
}
