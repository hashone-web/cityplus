<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\User;
use App\UserDevice;
use App\Device;
use App\UserOtp;
use Carbon\Carbon;
use Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    public $successStatus = 200;

    public function loginRequest(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'mobile_number' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 401);
        }

        $otp = mt_rand(100000, 999999);

        $user = null;

        if($request->route()->getPrefix() == 'api/reporters-app') {
            $user = User::where('mobile_number', $request->input('mobile_number'))->where('status', 'Active')->where('type', 'Admin')->first();
        } else {
            $user = User::firstOrNew(['mobile_number' => $request->input('mobile_number')]);
            if (!$user->exists) $user->save();
        }

        if($user) {
            $user_otp = $this->getOtp($user->id);

            if ($user_otp === NULL) {
                $user_otp = new UserOtp();
                $user_otp->user_id = $user->id;
                $user_otp->otp = $otp;
                $user_otp->save();
            }

            $sms = $this->sendSMS($user->mobile_number, $user_otp->otp);

            return ['status' => true, 'sms_status' => $sms['status']];
        }

        return ['status' => false, 'message' => 'You are not allowed to login'];
    }

    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'mobile_number' => 'required',
            'otp' => 'required'
        ]);
        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 401);
        }

        $user = null;

        if($request->route()->getPrefix() == 'api/reporters-app') {
            $user = User::withCount('news')
                        ->with('reporting_cities')
                        ->where('status', 'Active')
                        ->where('mobile_number', $request->input('mobile_number'))
                        ->where('type', 'Admin')
                        ->first();
        } else {
            $user = User::where('mobile_number', $request->input('mobile_number'))->first();
        }

        if ($user) {
            $user_otp = $this->getOtp($user->id);

            if ($user_otp) {
                if ($user_otp->otp == $request->input('otp')) {
                    $user_otp->status = 'Verified';
                    $user_otp->update();

                    Auth::loginUsingId($user->id, TRUE);

                    $auth_user = Auth::user();
                    $data['token'] = $auth_user->createToken('MyApp')->accessToken;
                    $data['user'] = $user;

                    if($request->input('device_id') !== null) {
                        $device = Device::firstOrNew(['unique_id' => $request->input('device_id')]);
                        $device->save();

                        $user_device = UserDevice::firstOrNew([ 'device_id' => $device->id, 'user_id' => $user->id ]);
                        $user_device->save();
                    }

                    return response()->json(['status' => true, 'data' => $data], $this->successStatus);
                }
                return response()->json(['status' => false, 'error' => 'Wrong OTP'], 401);
            }
            return response()->json(['status' => false, 'error' => 'Unauthorised'], 401);
        }
        return response()->json(['status' => false, 'error' => 'User Not Found or You are not allowed to login'], 404);
    }

    public function update(Request $request) {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 401);
        }

        $user = $request->user();
        $user->name = $request->input('name');
        $user->update();

        return response()->json(['status' => true, 'data' => $user], $this->successStatus);
    }

    public function logout(Request $request)
    {
        $request->user()->token()->revoke();

        return response()->json([
            'status' => true
        ]);
    }

    private function getOtp($user_id)
    {
        UserOtp::where('user_id', $user_id)
            ->where('status', 'Active')
            ->where('created_at', '<', Carbon::now()->subMinutes(3)->toDateTimeString())
            ->update(['status' => 'Expired']);

        return UserOtp::where('user_id', $user_id)->where('status', 'Active')->first();
    }

    public function user(Request $request)
    {
        $user = User::withCount('news')
                    ->with('reporting_cities')
                    ->find($request->user()->id);

        if($user) {
            return [
                'status' => true,
                'data' => $user
            ];
        }

        return [
            'status' => false
        ];
    }
}
