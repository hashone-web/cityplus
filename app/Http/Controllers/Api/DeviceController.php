<?php

namespace App\Http\Controllers\Api;

use App\Device;
use App\DeviceBookmark;
use App\Http\Controllers\Controller;
use App\News;
use App\NewsDeviceSensitivity;
use App\NewsReaction;
use App\NewsShare;
use App\NewsView;
use App\DeviceLocation;
use App\CoronaVirusData;
use App\CampaignDevice;
use Illuminate\Http\Request;
use Validator;

class DeviceController extends Controller
{
    public function getBookmarks(Request $request) {
        $pagination_size = $request->query('limit') !== null? $request->query('limit'): \Config::get('constants.pagination_size');

        $device = Device::firstOrNew(['unique_id' => $request->input('device_id')]);
        $device->save();

        $news = News::with(['category', 'language', 'media', 'thumbs', 'downloads', 'city' => function($q) {
                $q->leftJoin(\DB::raw('(SELECT city_id, name FROM city_names where language_id = 2 GROUP BY city_id, name) as cn'), 'cn.city_id', '=', 'cities.id');
            }])
            ->has('category')->has('city')
            ->leftJoin(\DB::raw('(SELECT news_id, type FROM news_reactions where device_id = ' . $device->id . ' GROUP BY news_id, type) as nr'), 'nr.news_id', '=', 'news.id')
            ->leftJoin(\DB::raw('(SELECT news_id, IFNULL(count(id), 0) AS reactions_claps_count FROM news_reactions where type ="Claps" GROUP BY news_id) as nrc'), 'nrc.news_id', '=', 'news.id')
            ->leftJoin(\DB::raw('(SELECT news_id, IFNULL(count(id), 0) AS reactions_happy_count FROM news_reactions where type ="Happy" GROUP BY news_id) as nrh'), 'nrh.news_id', '=', 'news.id')
            ->leftJoin(\DB::raw('(SELECT news_id, IFNULL(count(id), 0) AS reactions_sad_count FROM news_reactions where type ="Sad" GROUP BY news_id) as nrs'), 'nrs.news_id', '=', 'news.id')
            ->leftJoin(\DB::raw('(SELECT news_id, IFNULL(count(id), 0) AS views FROM news_views GROUP BY news_id) as nv'), 'nv.news_id', '=', 'news.id')
            ->leftJoin(\DB::raw('(SELECT news_id, IFNULL(count(id), 0) AS shares_whatsapp_count FROM news_shares where type ="Whatsapp" GROUP BY news_id) as ns'), 'ns.news_id', '=', 'news.id')
            ->leftJoin(\DB::raw('(SELECT news_id, IFNULL(count(id), 0) AS is_sensitive_viewed FROM news_device_sensitivities where device_id = ' . $device->id . ' GROUP BY news_id) as nds'), 'nds.news_id', '=', 'news.id')
            ->addSelect('news.*')
            ->addSelect('nr.type as reaction')
            ->addSelect('nrc.reactions_claps_count as reactions_claps_count')
            ->addSelect('nrh.reactions_happy_count as reactions_happy_count')
            ->addSelect('nrs.reactions_sad_count as reactions_sad_count')
            ->addSelect('nv.views as views')
            ->addSelect('ns.shares_whatsapp_count as shares_whatsapp_count');
        
        if($request->input('platform') !== null && $request->input('platform') == 'iOS') {
            $news = $news->addSelect(\DB::raw('IFNULL(nds.is_sensitive_viewed, 0) as is_sensitive_viewed'));
        } else {
            $news = $news->addSelect('nds.is_sensitive_viewed as is_sensitive_viewed');
        }

        $news = $news->join('device_bookmarks', function ($j) use($device)
        {
            $j->on('news.id', '=', 'device_bookmarks.news_id');
            $j->where('device_bookmarks.device_id', $device->id);
        })->distinct()->addSelect('news.*');

        $news = $news->paginate($pagination_size);
        $news_data = $news->toArray();

        $data = [
            'server_time' => time() * 1000,
            'status' => true,
            'data' => $news_data['data'],
        ];

        $data['pagination'] = [
            'show_pagination' => ($news->total() / $pagination_size) > 1,
            'total' => $news->total(),
            'per_page' => $news->perPage(),
            'current_page' => $news->currentPage(),
            'last_page' => $news->lastPage(),
            'from' => $news->firstItem(),
            'to' => $news->lastItem()
        ];

        return $data;
    }

    public function storeBookmarks(Request $request) {
        $validator = Validator::make($request->all(), [
            'news_id' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 401);
        }

        $data = ['status' => true];

        $device = Device::firstOrNew(['unique_id' => $request->input('device_id')]);
        $device->save();

        try {
            if($request->input('remove') !== null) {
                DeviceBookmark::where([
                    'device_id' => $device->id,
                    'news_id' => $request->input('news_id'),
                ])->delete();
            } else {
                DeviceBookmark::firstOrCreate([
                    'device_id' => $device->id,
                    'news_id' => $request->input('news_id'),
                ]);
            }

            $news = $this->findNews($request->input('news_id'), $device, $request);
            $data['data'] = $news->toArray();
            if($request->input('platform') == null || ($request->input('platform') != null && $request->input('platform') == 'iOS')) {
                $data['data']['article'] = isset($data['data']['article_text'])? $data['data']['article_text']: $data['data']['article'];
            }
        } catch (\Exception $e) {
            $data['status'] = false;
        }

        return $data;
    }

    public function storeViews(Request $request) {
        $validator = Validator::make($request->all(), [
            'news_id' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 401);
        }

        $data = ['status' => true];

        $device = Device::firstOrNew(['unique_id' => $request->input('device_id')]);
        $device->save();

        try {
            NewsView::firstOrCreate([
                'device_id' => $device->id,
                'news_id' => $request->input('news_id'),
            ]);

            $news = $this->findNews($request->input('news_id'), $device, $request);
            $data['data'] = $news->toArray();
            if($request->input('platform') == null || ($request->input('platform') != null && $request->input('platform') == 'iOS')) {
                $data['data']['article'] = isset($data['data']['article_text'])? $data['data']['article_text']: $data['data']['article'];
            }
        } catch (\Exception $e) {
            $data['status'] = false;
        }

        return $data;
    }

    public function storeReactions(Request $request) {
        $validator = Validator::make($request->all(), [
            'news_id' => 'required',
            'type' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 401);
        }

        $data = ['status' => true];

        $device = Device::firstOrNew(['unique_id' => $request->input('device_id')]);
        $device->save();

        try {
            $news_reaction = NewsReaction::firstOrNew(['device_id' => $device->id, 'news_id' => $request->input('news_id')]);

            if($request->input('remove') !== null) {
                if($news_reaction) {
                    $news_reaction->delete();
                }
            }
            
            if($news_reaction->type !== $request->input('type')) {
                $news_reaction->type = $request->input('type');
                $news_reaction->save();
            } else {
                $news_reaction->delete();
            }

            $news = $this->findNews($request->input('news_id'), $device, $request);

            $news_likes_device_ids = $news->likes()->pluck('device_id')->toArray();
            $news_dislikes_device_ids = $news->dislikes()->pluck('device_id')->toArray();

            $news = $news->toArray();
            $news_for_elastic_search = [];
            $news_for_elastic_search['likes'] = $news_likes_device_ids;
            $news_for_elastic_search['dislikes'] = $news_dislikes_device_ids;

            $news_for_elastic_search['reactions_claps_count'] = count($news_for_elastic_search['likes']);
            $news_for_elastic_search['reactions_happy_count'] = count($news_for_elastic_search['likes']);
            $news_for_elastic_search['reactions_sad_count'] = count($news_for_elastic_search['dislikes']);

            if(env('ELASTICSEARCH')) {
                \Elasticsearch::update([
                    'id' => $news['id'],
                    'index' => 'news',
                    'body' => [
                        'doc' => $news_for_elastic_search,
                        'doc_as_upsert' => true
                    ]
                ]);
            }

            $data['data'] = $news;

            if($request->input('platform') == null || ($request->input('platform') != null && $request->input('platform') == 'iOS')) {
                $data['data']['article'] = isset($data['data']['article_text'])? $data['data']['article_text']: $data['data']['article'];
            }
        } catch (\Exception $e) {
            $data['status'] = false;
        }

        return $data;
    }

    public function storeShares(Request $request) {
        $validator = Validator::make($request->all(), [
            'news_id' => 'required',
            'type' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 401);
        }

        $data = ['status' => true];

        $device = Device::firstOrNew(['unique_id' => $request->input('device_id')]);
        $device->save();

        try {
            $news_share = NewsShare::firstOrNew(['device_id' => $device->id, 'news_id' => $request->input('news_id'), 'type' => $request->input('type')]);
            if(!$news_share->exists) {
                $news_share->link = \Str::random(10);
            }
            $news_share->save();
            $news = $this->findNews($request->input('news_id'), $device, $request);

            $news_shares_device_ids = $news->shares()->count();

            $news = $news->toArray();
            $news_for_elastic_search = [];

            $news_for_elastic_search['shares_whatsapp_count'] = $news_shares_device_ids;

            if(env('ELASTICSEARCH')) {
                \Elasticsearch::update([
                    'id' => $news['id'],
                    'index' => 'news',
                    'body' => [
                        'doc' => $news_for_elastic_search,
                        'doc_as_upsert' => true
                    ]
                ]);
            }

            $data['data'] = $news;

            if($request->input('platform') == null || ($request->input('platform') != null && $request->input('platform') == 'iOS')) {
                $data['data']['article'] = isset($data['data']['article_text'])? $data['data']['article_text']: $data['data']['article'];
            }
        } catch (\Exception $e) {
            $data['status'] = false;
        }

        return $data;
    }

    public function storeCoronaVirusDataShares(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'type' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 401);
        }

        $data = ['status' => true];

        $device = Device::firstOrNew(['unique_id' => $request->input('device_id')]);
        $device->save();

        try {
            $corona_virus_data = CoronaVirusData::where('name', $request->input('type'))->first();
            if($corona_virus_data) {
                $corona_virus_data->shares = $corona_virus_data->shares + 1;
                $corona_virus_data->update();
                $data['data'] = $corona_virus_data;
            } else {
                $data['status'] = false;
                $data['message'] = 'Type Data not found';
            }
        } catch (\Exception $e) {
            $data['status'] = false;
            $data['message'] = 'Exception';
        }

        return $data;
    }

    public function storeSensitivity(Request $request) {
        $validator = Validator::make($request->all(), [
            'news_id' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 401);
        }

        $data = ['status' => true];

        $device = Device::firstOrNew(['unique_id' => $request->input('device_id')]);
        $device->save();

        try {
            NewsDeviceSensitivity::firstOrCreate([
                'device_id' => $device->id,
                'news_id' => $request->input('news_id'),
            ]);

            $news = $this->findNews($request->input('news_id'), $device, $request);

            $news_sensitivities_device_ids = $news->sensitivities()->pluck('device_id')->toArray();

            $news = $news->toArray();
            $news_for_elastic_search = [];
            $news_for_elastic_search['sensitivities'] = $news_sensitivities_device_ids;

            if(env('ELASTICSEARCH')) {
                \Elasticsearch::update([
                    'id' => $news['id'],
                    'index' => 'news',
                    'body' => [
                        'doc' => $news_for_elastic_search,
                        'doc_as_upsert' => true
                    ]
                ]);
            }
        } catch (\Exception $e) {
            $data['status'] = false;
        }

        return $data;
    }

    public function notifications(Request $request) {
        $validator = Validator::make($request->all(), [
            'notification_level' => 'required',
            'onesignal_player_id' => 'required',
            'platform' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 401);
        }

        $data = ['status' => true];

        try {
            $device = Device::firstOrNew(['unique_id' => $request->input('device_id')]);
            $device->notification_level = $request->input('notification_level');
            $device->onesignal_player_id = $request->input('onesignal_player_id');
            $device->platform = $request->input('platform');
            if($request->input('notification_level_update') !== null) {
                $device->notification_level_update = $request->input('notification_level_update');
            }
            $device->save();
        } catch (\Exception $e) {
            $data['status'] = false;
        }

        return $data;
    }

    public function locations(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'address' => 'required',
            'cityName' => 'required',
            'latitude' => 'required',
            'longitute' => 'required',
            'stateName' => 'required',
            'postalcode' => 'required',
            'countryName' => 'required',
            'fulladdress' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 401);
        }

        $data = ['status' => true];

        $device = Device::firstOrNew(['unique_id' => $request->input('device_id')]);
        $device->save();

        try {
            $all_requests_data = $request->except(['device_id', 'token']);

            if(count($all_requests_data)) {
                $location_details = [];
                $location_details['address'] = $request->input('address');
                $location_details['city_name'] = $request->input('cityName');
                $location_details['latitude'] = $request->input('latitude');
                $location_details['longitute'] = $request->input('longitute');
                $location_details['state_name'] = $request->input('stateName');
                $location_details['postal_code'] = $request->input('postalcode');
                $location_details['country_name'] = $request->input('countryName');
                $location_details['full_address'] = $request->input('fulladdress');

                DeviceLocation::create([
                    'device_id' => $device->id,
                    'location_details' => json_encode($location_details)
                ]);
            }
        } catch (\Exception $e) {
            $data['status'] = false;
        }

        return $data;
    }

    public function locationPermissions(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'value' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 401);
        }

        $data = ['status' => true];

        $device = Device::firstOrNew(['unique_id' => $request->input('device_id')]);
        $device->save();

        try {
            $device->location_permission = $request->input('value');
            $device->update();
        } catch (\Exception $e) {
            $data['status'] = false;
        }

        return $data;
    }

    public function campaignDevices(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'campaign_id' => 'required',
            'campaign_banner_id' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 401);
        }

        $data = ['status' => true];

        $device = Device::firstOrNew(['unique_id' => $request->input('device_id')]);
        $device->save();

        try {
            $campaign_device = new CampaignDevice();
            $campaign_device->campaign_id = $request->input('campaign_id');
            $campaign_device->campaign_banner_id = $request->input('campaign_banner_id');
            $campaign_device->device_id = $device->id;
            $campaign_device->save();
        } catch (\Exception $e) {
            $data['status'] = false;
        }

        return $data;
    }
}
