<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Device;
use App\DeviceStaticFeedback;

class StaticFeedbackController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = [
            'id' => 1,
            'title' => 'શું તમે સિટીપ્લસ એપથી સંતુષ્ટ છો?',
            'options' => [
                [
                    'id' => 1,
                    'title' => 'હા'
                ],
                [
                    'id' => 2,
                    'title' => 'ના'
                ],
            ],
            'next' => [
                '1' => [
                    'title' => 'તમે એકથી વધુ વિકલ્પ પસંદ કરી શકો છો',
                    'options' => [
                        [
                            'id' => 1,
                            'title' => 'એપ વાપરવાની સરળતા'
                        ],
                        [
                            'id' => 2,
                            'title' => 'સમાચાર સચોટ હોઈ છે'
                        ],
                        [
                            'id' => 3,
                            'title' => 'સૌથી પેલા સમાચાર મળે છે'
                        ],
                        [
                            'id' => 4,
                            'title' => 'એપમાં કોઈ એડ્સ નથી'
                        ],
                    ],
                    'box_text' => 'તમારું કોઈ સુચન હોઈ તો આ બોક્સમાં લખો',
                    'on_submit_message' => 'તમારા પ્રતિભાવ બદલ આભાર',
                ],
                '2' => [
                    'title' => 'તમે એકથી વધુ વિકલ્પ પસંદ કરી શકો છો',
                    'options' => [
                        [
                            'id' => 5,
                            'title' => 'સમાચાર ઝડપી નથી આવતા'
                        ],
                        [
                            'id' => 6,
                            'title' => 'વધુ નોટિફિકેશન આવે છે'
                        ],
                        [
                            'id' => 7,
                            'title' => 'એપમાં ખામી છે'
                        ],
                        [
                            'id' => 8,
                            'title' => 'અન્ય કેટેગરીના સમાચાર નથી'
                        ],
                    ],
                    'box_text' => 'તમારું કોઈ સુચન હોઈ તો આ બોક્સમાં લખો',
                    'on_submit_message' => 'તમારા પ્રતિભાવ બદલ આભાર',
                ],
            ],
        ];

        return [
            'status' => true,
            'data' => $data
        ];
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function stepOne(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'value' => 'required|in:Yes,No',
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 401);
        }

        $device = Device::firstOrNew(['unique_id' => $request->input('device_id')]);
        $device->save();

        $data = ['status' => true];

        try {
            $device_static_feedback = DeviceStaticFeedback::firstOrNew(['device_id' => $device->id]);
            $device_static_feedback->value = $request->input('value');
            $device_static_feedback->save();

            $data['data'] = $device_static_feedback;
        } catch (\Exception $e) {
            $data['status'] = false;
        }

        return $data;
    }

    public function stepTwo(Request $request)
    {
        $data = ['status' => true];

        $device = Device::firstOrNew(['unique_id' => $request->input('device_id')]);
        $device->save();

        $device_static_feedback = DeviceStaticFeedback::where('device_id', $device->id)->first();

        if($device_static_feedback) {
            try {
                $device_static_feedback->options = $request->input('options');
                $device_static_feedback->message = $request->input('message');
                $device_static_feedback->update();

                $data['data'] = $device_static_feedback;
            } catch (\Exception $e) {
                $data['status'] = false;
            }
        } else {
            $data['status'] = false;
            $data['message'] = 'Feedback not found';
        }     

        return $data;
    }

    public function close(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'close_on' => 'required|in:Feedback,Yes,No',
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 401);
        }

        $device = Device::firstOrNew(['unique_id' => $request->input('device_id')]);
        $device->save();

        $data = ['status' => true];

        try {
            $device_static_feedback = DeviceStaticFeedback::firstOrNew(['device_id' => $device->id]);
            $device_static_feedback->close_on = $request->input('close_on');
            $device_static_feedback->save();

            $data['data'] = $device_static_feedback;
        } catch (\Exception $e) {
            $data['status'] = false;
        }

        return $data;
    }
}
