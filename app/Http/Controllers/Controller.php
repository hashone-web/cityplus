<?php

namespace App\Http\Controllers;

use App\News;
use App\NewsMedia;
use App\NewsAttachment;
use Carbon\Carbon;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Str;
use App\Device;
use App\AdminPanelActivity;
use App\NewsNotification;
use App\DevicePollOption;
use App\City;
use App\Poll;
use App\Memory;
use App\CampaignBanner;
use App\CampaignDeviceImpression;
use App\Quote;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function __construct()
    {
        //
    }

    public function convertHTMLtoTextHelper($html)
    {
        $options = array(
            'ignore_errors' => true,
            // 'drop_links' => true
        );

        $text = \Soundasleep\Html2Text::convert($html, $options);
    
        return $text;
    }

    public function getAdsBanner($type, $user_cities, $now, $device_id)
    {
        $size = 1;

        $elastic_search_params = '{
            "index":"ads",
            "body":{
                "query":{
                    "bool":{
                        "filter":{
                            "terms":{
                                "cities":'. json_encode($user_cities) .'
                            }
                        },
                        "must": [ 
                            {
                                "match": {
                                    "campaign_status":"Active"
                                }
                            },
                            {
                                "match": {
                                    "type":"'.$type.'"
                                }
                            },
                            {
                                "match": {
                                    "campaign_deleted_at":0
                                }
                            },
                            {
                                "range":{
                                    "campaign_start_date":{
                                        "lte":"'. $now .'"
                                    }
                                }
                            },
                            {
                                "range":{
                                    "campaign_end_date":{
                                        "gte":"'. $now .'"
                                    }
                                }
                            }
                        ]
                    }
                },
                "sort" : {
                    "_script" : { 
                        "script" : "Math.random()",
                        "type" : "number",
                        "order" : "asc"
                    }
                }
            },
            "size":'. $size .'
        }';

        $elastic_search_params = json_decode($elastic_search_params, true);
        $raw_banners = \Elasticsearch::search($elastic_search_params);

        $banner = null;

        if($raw_banners['hits']['total']['value']) {
            $banner = $raw_banners['hits']['hits'][0]['_source'];
            
            if(!isset($banner['reactions'])) {
                $banner['reactions'] = [];
                $banner['reactions_count'] = 0;
                $banner['reacted'] = 0;
            } else {
                $banner['reacted'] = in_array($device_id, $banner['reactions'])? 1: 0;
            }
            
            if(!isset($banner['shares'])) {
                $banner['shares'] = [];
                $banner['shares_count'] = 0;
                $banner['shared'] = 0;
            } else {
                $banner['shared'] = in_array($device_id, $banner['shares'])? 1: 0;
            }

            $campaign_device_impression = CampaignDeviceImpression::firstOrCreate(['device_id' => $device_id, 'campaign_id' => $banner['campaign_id'], 'campaign_banner_id' => $banner['id'], 'impression_date' => date('Y-m-d')]);
            $campaign_device_impression->increment('impressions', 1);
        }

        return $banner;
    }

    public function generateBannerData($request)
    {
        $banner_data = [];

        $all_requests = $request->all();
        $all_requests = array_keys($all_requests);

        // Type 
        $type_banner_keys = array_filter($all_requests, function($all_request) {
            return strpos($all_request, 'type__') === 0;
        });

        foreach ($type_banner_keys as $type_banner_key) {
            $banner_unique_id = str_replace('type__', '', $type_banner_key);
            if(!isset($banner_data[$banner_unique_id])) $banner_data[$banner_unique_id] = [];
            $banner_data[$banner_unique_id]['type'] = $request->input($type_banner_key);
        }

        // Action 
        $action_banner_keys = array_filter($all_requests, function($all_request) {
            return strpos($all_request, 'action__') === 0;
        });

        foreach ($action_banner_keys as $action_banner_key) {
            $banner_unique_id = str_replace('action__', '', $action_banner_key);
            if(!isset($banner_data[$banner_unique_id])) $banner_data[$banner_unique_id] = [];
            $banner_data[$banner_unique_id]['action'] = $request->input($action_banner_key);
        }

        // Action Data
        $action_data_banner_keys = array_filter($all_requests, function($all_request) {
            return strpos($all_request, 'action_data__') === 0;
        });

        foreach ($action_data_banner_keys as $action_data_banner_key) {
            $banner_unique_id = str_replace('action_data__', '', $action_data_banner_key);
            if(!isset($banner_data[$banner_unique_id])) $banner_data[$banner_unique_id] = [];
            $banner_data[$banner_unique_id]['action_data'] = $request->input($action_data_banner_key);
        }

        // Action Text
        $action_text_banner_keys = array_filter($all_requests, function($all_request) {
            return strpos($all_request, 'action_text__') === 0;
        });

        foreach ($action_text_banner_keys as $action_text_banner_key) {
            $banner_unique_id = str_replace('action_text__', '', $action_text_banner_key);
            if(!isset($banner_data[$banner_unique_id])) $banner_data[$banner_unique_id] = [];
            $banner_data[$banner_unique_id]['action_text'] = $request->input($action_text_banner_key);
        }

        // Action Button Colour
        $action_button_colour_banner_keys = array_filter($all_requests, function($all_request) {
            return strpos($all_request, 'action_button_colour__') === 0;
        });

        foreach ($action_button_colour_banner_keys as $action_button_colour_banner_key) {
            $banner_unique_id = str_replace('action_button_colour__', '', $action_button_colour_banner_key);
            if(!isset($banner_data[$banner_unique_id])) $banner_data[$banner_unique_id] = [];
            $banner_data[$banner_unique_id]['action_button_colour'] = $request->input($action_button_colour_banner_key);
        }

        // Action Text Colour
        $action_text_colour_banner_keys = array_filter($all_requests, function($all_request) {
            return strpos($all_request, 'action_text_colour__') === 0;
        });

        foreach ($action_text_colour_banner_keys as $action_text_colour_banner_key) {
            $banner_unique_id = str_replace('action_text_colour__', '', $action_text_colour_banner_key);
            if(!isset($banner_data[$banner_unique_id])) $banner_data[$banner_unique_id] = [];
            $banner_data[$banner_unique_id]['action_text_colour'] = $request->input($action_text_colour_banner_key);
        }

        // Action Image
        $action_image_banner_keys = array_filter($all_requests, function($all_request) {
            return strpos($all_request, 'action_image__') === 0;
        });

        foreach ($action_image_banner_keys as $action_image_banner_key) {
            $banner_unique_id = str_replace('action_image__', '', $action_image_banner_key);
            if(!isset($banner_data[$banner_unique_id])) $banner_data[$banner_unique_id] = [];
            $banner_data[$banner_unique_id]['action_image'] = $request->file($action_image_banner_key);
        }

        return $banner_data;
    }

    public function generateMediaData($request)
    {
        $media_data = [];

        $media_sorting = $request->input('media_sorting');
        $media_sorting = json_decode($media_sorting, true);

        foreach ($media_sorting as $media_sorting_single_key => $media_sorting_single) {
            $media_data[$media_sorting_single] = [
                'sort' => $media_sorting_single_key,
                'is_sensitive' => false,
                'is_file_photo' => false,
                'courtesy' => null,
                'watermark_position' => null,
                'concat_video' => false,
                'is_blur' => false,
            ]; 
        }

        $all_requests = $request->all();
        $all_requests = array_keys($all_requests);

        // Is Sensitive 
        $is_sensitive_media_keys = array_filter($all_requests, function($all_request) {
            return strpos($all_request, 'is_sensitive__') === 0;
        });

        foreach ($is_sensitive_media_keys as $is_sensitive_media_key) {
            $media_unique_id = str_replace('is_sensitive__', '', $is_sensitive_media_key);
            if(isset($media_data[$media_unique_id])) {
                $media_data[$media_unique_id]['is_sensitive'] = $request->input($is_sensitive_media_key) == 'on'? true: false;
            }
        }

        // Is Blur 
        $is_blur_media_keys = array_filter($all_requests, function($all_request) {
            return strpos($all_request, 'is_blur__') === 0;
        });

        foreach ($is_blur_media_keys as $is_blur_media_key) {
            $media_unique_id = str_replace('is_blur__', '', $is_blur_media_key);
            if(isset($media_data[$media_unique_id])) {
                $media_data[$media_unique_id]['is_blur'] = $request->input($is_blur_media_key) == 'on'? true: false;
            }
        }

        // Is File Photo   
        $is_file_photo_media_keys = array_filter($all_requests, function($all_request) {
            return strpos($all_request, 'is_file_photo__') === 0;
        });

        foreach ($is_file_photo_media_keys as $is_file_photo_media_key) {
            $media_unique_id = str_replace('is_file_photo__', '', $is_file_photo_media_key);
            if(isset($media_data[$media_unique_id])) {
                $media_data[$media_unique_id]['is_file_photo'] = $request->input($is_file_photo_media_key) == 'on'? true: false;
            }
        }


        // Courtesy
        $courtesy_media_keys = array_filter($all_requests, function($all_request) {
            return strpos($all_request, 'courtesy__') === 0;
        });

        foreach ($courtesy_media_keys as $courtesy_media_key) {
            $media_unique_id = str_replace('courtesy__', '', $courtesy_media_key);
            if(isset($media_data[$media_unique_id])) {
                $media_data[$media_unique_id]['courtesy'] = $request->input($courtesy_media_key);
            }
        }


        // Watermark
        $watermark_position_media_keys = array_filter($all_requests, function($all_request) {
            return strpos($all_request, 'watermark_position__') === 0;
        });

        foreach ($watermark_position_media_keys as $watermark_position_media_key) {
            $media_unique_id = str_replace('watermark_position__', '', $watermark_position_media_key);
            if(isset($media_data[$media_unique_id])) {
                $media_data[$media_unique_id]['watermark_position'] = $request->input($watermark_position_media_key);
            }
        }

        // Concat Video
        $concat_video_media_keys = array_filter($all_requests, function($all_request) {
            return strpos($all_request, 'concat_video__') === 0;
        });

        foreach ($concat_video_media_keys as $concat_video_media_key) {
            $media_unique_id = str_replace('concat_video__', '', $concat_video_media_key);
            if(isset($media_data[$media_unique_id])) {
                $media_data[$media_unique_id]['concat_video'] = $request->input($concat_video_media_key) !== null;
            }
        }

        return $media_data;
    }

    public function updateExistingMedia($news_id, $media_id, $media_data)
    {
        $is_sensitive_all_media_ids_1 = NewsMedia::where('parent_id', $media_id)->get()->pluck('id')->toArray();
        $is_sensitive_all_media_ids_2 = NewsMedia::whereIn('parent_id', $is_sensitive_all_media_ids_1)->get()->pluck('id')->toArray();
        $is_sensitive_media_ids = array_merge([$media_id], $is_sensitive_all_media_ids_1, $is_sensitive_all_media_ids_2);

        NewsMedia::whereIn('id', $is_sensitive_media_ids)->update([
            'sort' => $media_data['sort'],
            'is_sensitive' => $media_data['is_sensitive'],
            'is_file_photo' => $media_data['is_file_photo'],
            'courtesy' => $media_data['courtesy'],
        ]);
    }

    public function updateExistingBanner($campaign_id, $banner_id, $banner_data)
    {
        $data = [
            'type' => $banner_data['type'],
            'action' => $banner_data['action'],
            'action_data' => $banner_data['action_data'],
            'action_text' => $banner_data['action_text'],
            'action_text_colour' => $banner_data['action_text_colour'],
            'action_button_colour' => $banner_data['action_button_colour'],
        ];

        if(isset($banner_data['action_image'])) {
            $public_storage_path = 'app/public/';
            $path = 'campaigns/' . $campaign_id . '/banners/' . $banner_id . '/action_image';
            $app_path = storage_path($public_storage_path . $path);

            if (!file_exists($app_path)) {
                \File::makeDirectory($app_path, 0777, true);
            }

            $banner_action_image = $banner_data['action_image'];
            $banner_action_image_extension = $banner_action_image->getClientOriginalExtension();
            $banner_action_image_filename = \Str::random(16) . '.' . $banner_action_image_extension;
            $banner_action_image->move($app_path, $banner_action_image_filename);

            $data['action_image'] = $banner_action_image_filename;
        }

        CampaignBanner::where('id', $banner_id)->update($data);
    }

    public function sendSMS($mobile, $otp) {
        $authkey = env('SMS_AUTHKEY', '306139AfNRnzr5pu5de0ad0d');
        $template_id = env('TEMPLATE_ID', '5de0aee0d6fc052bd37d63ec');

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://api.msg91.com/api/v5/otp?invisible=1&otp={$otp}&authkey={$authkey}&mobile={$mobile}&template_id={$template_id}",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => "",
            CURLOPT_SSL_VERIFYHOST => 0,
            CURLOPT_SSL_VERIFYPEER => 0,
            CURLOPT_HTTPHEADER => array(
                "content-type: application/json"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if($err) {
            return [
                'status' => false,
                'err' => $err
            ];
        }

        return [
            'status' => true
        ];
    }

    protected function createResizedImage($app_path, $image, $filename, $original_file = true)
    {
        $final_filename = $filename;

        if ($original_file) {
            if (!file_exists($app_path)) {
                \File::makeDirectory($app_path, 0777, true);
            } else {
                \File::deleteDirectory($app_path, 0777, true);
                if (!file_exists($app_path)) {
                    \File::makeDirectory($app_path, 0777, true);
                }
            }

            $final_filename = $filename . '.' . $image->getClientOriginalExtension();
            $image->move($app_path, $final_filename);
        }

        $md_path = $app_path . '/256';
        \File::makeDirectory($md_path, 0777, true);
        $md_icon = \Image::make($app_path . '/' . $final_filename);
        $md_icon->resize(null, 256, function ($constraint) {
            $constraint->aspectRatio();
        })->save($md_path . '/' . $final_filename);

        $sm_path = $app_path . '/128';
        \File::makeDirectory($sm_path, 0777, true);
        $sm_icon = \Image::make($app_path . '/' . $final_filename);
        $sm_icon->resize(null, 128, function ($constraint) {
            $constraint->aspectRatio();
        })->save($sm_path . '/' . $final_filename);

        $xs_path = $app_path . '/64';
        \File::makeDirectory($xs_path, 0777, true);
        $xs_icon = \Image::make($app_path . '/' . $final_filename);
        $xs_icon->resize(null, 64, function ($constraint) {
            $constraint->aspectRatio();
        })->save($xs_path . '/' . $final_filename);

        return $final_filename;
    }

    protected function storeNewsMediaThumb($news, $news_media, $thumb_media, $thumb_name)
    {
        $news_resized_media = new NewsMedia();
        $news_resized_media->name = $thumb_name;
        $news_resized_media->original_name = $thumb_name;
        $news_resized_media->type = 'thumb';
        $news_resized_media->details = json_encode([
            'width' => $thumb_media->width(),
            'height' => $thumb_media->height()
        ]);
        $news_resized_media->extension = 'jpg';
        $news_resized_media->dynamic_id = $news_media->dynamic_id;
        $news_resized_media->mimetype = $thumb_media->mime();
        $news_resized_media->size = $thumb_media->filesize();
        $news_resized_media->sort = $news_media->sort;
        if($news_media->is_sensitive) $news_resized_media->is_sensitive = $news_media->is_sensitive;
        if($news_media->is_file_photo) $news_resized_media->is_file_photo = $news_media->is_file_photo;
        if($news_media->courtesy) $news_resized_media->courtesy = $news_media->courtesy;
        if($news_media->watermark_position) $news_resized_media->watermark_position = $news_media->watermark_position;
        $news_resized_media->parent_id = $news_media->id;
        $news_resized_media->news_id = $news->id;
        $news_resized_media->save();

        $news_resized_media->full_path = $news_resized_media->full_path_dynamic;
        $news_resized_media->thumb = $news_resized_media->thumb_dynamic;
        $news_resized_media->update();

        if(\Config::get('constants.thumb_size') == $news_resized_media->type) {
            $original_news_media = NewsMedia::find($news_resized_media->parent_id);
            $original_news_media->full_path = $original_news_media->full_path_dynamic;
            $original_news_media->thumb = $original_news_media->thumb_dynamic;
            $original_news_media->update();
        }

        return $news_resized_media;
    }

    protected function storeNewsAttachment($news, $attachment, $attachment_index)
    {
        $attachment_original_name = $attachment->getClientOriginalName();
        $attachment_extension = $attachment->getClientOriginalExtension();

        $news_attachment = new NewsAttachment();
        $news_attachment->name = Str::random(10) . '.' . $attachment_extension;
        $news_attachment->original_name = $attachment_original_name;
        $news_attachment->extension = $attachment_extension;
        $news_attachment->dynamic_id = Str::random(10);
        $news_attachment->mimetype = $attachment->getClientMimeType();
        $news_attachment->size = $attachment->getSize();
        $news_attachment->news_id = $news->id;
        $news_attachment->save();

        return $news_attachment;
    }

    protected function storeNewsOriginalMedia($news, $media, $media_index, $media_data = null)
    {
        $media_original_name = $media->getClientOriginalName();
        $media_extension = $media->getClientOriginalExtension();

        $media_unique_id = preg_replace('/[\W_]+/u', '', $media_original_name);
        $media_unique_id = substr($media_unique_id, 0, 10);
        $media_unique_id = $media->getSize() . '' . $media_unique_id;

        if(!isset($media_data[$media_unique_id]['concat_video']) || isset($media_data[$media_unique_id]['concat_video']) && $media_data[$media_unique_id]['concat_video'] !== true) {
            $news_media = new NewsMedia();
            $news_media->name = Str::random(10) . '.' . $media_extension;
            $news_media->original_name = $media_original_name;
            $news_media->type = 'original';
            $news_media->extension = $media_extension;
            $news_media->dynamic_id = Str::random(10);
            $news_media->mimetype = $media->getClientMimeType();
            $news_media->size = $media->getSize();
            if($media_data !== null && isset($media_data[$media_unique_id])) {
                $news_media->sort = $media_data[$media_unique_id]['sort'];
                $news_media->is_sensitive = $media_data[$media_unique_id]['is_sensitive'];
                $news_media->is_file_photo = $media_data[$media_unique_id]['is_file_photo'];
                $news_media->courtesy = $media_data[$media_unique_id]['courtesy'];
                if(isset($media_data[$media_unique_id]['watermark_position']) && $media_data[$media_unique_id]['watermark_position'] !== null&& $media_data[$media_unique_id]['watermark_position'] != 'none') {
                    $news_media->watermark_position = $media_data[$media_unique_id]['watermark_position'];
                }
            } else {
                $news_media->sort = $media_index;
            }
            $news_media->news_id = $news->id;
            $news_media->save();

            $news_media->full_path = $news_media->full_path_dynamic;
            $news_media->thumb = $news_media->thumb_dynamic;
            $news_media->update();

            return $news_media;
        }

        return null;
    }

    protected function storeNewsMedia($news, $news_media, $resized_media, $size)
    {
        $news_resized_media = new NewsMedia();
        $news_resized_media->name = $news_media->name;
        $news_resized_media->original_name = $news_media->original_name;
        $news_resized_media->type = $size;
        $news_resized_media->details = json_encode([
            'width' => $resized_media->width(),
            'height' => $resized_media->height()
        ]);
        $news_resized_media->extension = $news_media->extension;
        $news_resized_media->dynamic_id = $news_media->dynamic_id;
        $news_resized_media->mimetype = $news_media->mimetype;
        $news_resized_media->size = $resized_media->filesize();
        $news_resized_media->sort = $news_media->sort;
        if($news_media->is_sensitive) $news_resized_media->is_sensitive = $news_media->is_sensitive;
        if($news_media->is_file_photo) $news_resized_media->is_file_photo = $news_media->is_file_photo;
        if($news_media->courtesy) $news_resized_media->courtesy = $news_media->courtesy;
        if($news_media->watermark_position) $news_resized_media->watermark_position = $news_media->watermark_position;
        $news_resized_media->parent_id = $news_media->id;
        $news_resized_media->news_id = $news->id;
        $news_resized_media->save();

        $news_resized_media->full_path = $news_resized_media->full_path_dynamic;
        $news_resized_media->thumb = $news_resized_media->thumb_dynamic;
        $news_resized_media->update();

        if(\Config::get('constants.thumb_size') == $news_resized_media->type) {
            $original_thumb_news_media = NewsMedia::find($news_resized_media->parent_id);
            $original_thumb_news_media->full_path = $original_thumb_news_media->full_path_dynamic;
            $original_thumb_news_media->thumb = $original_thumb_news_media->thumb_dynamic;
            $original_thumb_news_media->update();

            if($original_thumb_news_media->type == 'thumb') {
                $original_news_media = NewsMedia::find($original_thumb_news_media->parent_id);
                $original_news_media->full_path = $original_news_media->full_path_dynamic;
                $original_news_media->thumb = $original_news_media->thumb_dynamic;
                $original_news_media->update();
            }
        }

        return $news_media;
    }

    protected function attachmentUpload($news, $news_attachment, $attachment)
    {
        $public_storage_path = 'app/public/';
        $path = 'news/' . $news->id . '/attachments/' . $news_attachment->dynamic_id;
        $app_path = storage_path($public_storage_path . $path);

        if (!file_exists($app_path)) {
            \File::makeDirectory($app_path, 0777, true);
        } else {
            if ($delete_directory) {
                \File::deleteDirectory($app_path, true);
            }
        }

        $attachment->move($app_path, $news_attachment->name);

        return [
            'status' => true
        ];
    }

    protected function mediaUpload($news, $news_media, $media, $media_data = null)
    {
        $o_news_media = $news_media;

        $media_unique_id = preg_replace('/[\W_]+/u', '', $news_media->original_name);
        $media_unique_id = substr($media_unique_id, 0, 10);
        $media_unique_id = $news_media->size . '' . $media_unique_id;

        $public_storage_path = 'app/public/';
        $path = 'news/' . $news->id . '/media/' . $news_media->dynamic_id;
        $app_path = storage_path($public_storage_path . $path);

        if (!file_exists($app_path)) {
            \File::makeDirectory($app_path, 0777, true);
        } else {
            if ($delete_directory) {
                \File::deleteDirectory($app_path, true);
            }
        }

        if($media_data !== null && isset($media_data[$media_unique_id]) && $media_data[$media_unique_id]['is_blur']) {
            $source_file_name = 'original__' . $news_media->name;
            $source_file_path = $app_path . '/' . $source_file_name;
            $output_file_path = $app_path . '/' . $news_media->name;

            $media->move($app_path, $source_file_name);

            $origianl_file = \Image::make($source_file_path);

            $output_file_height = $origianl_file->height();
            $output_file_width = ceil(1.56 * $output_file_height);
            
            $image_blur_command = env('IMAGEMAGICK_COMMAND', 'convert') . " " . $source_file_path . " \( -clone 0 -blur 0x45 -resize " . $output_file_width . "x" . $output_file_height . "\! \) \( -clone 0 -resize " . $output_file_width . "x" . $output_file_height . " \) -delete 0 -gravity center -compose over -composite " . $output_file_path;   
        
            exec($image_blur_command);
        } else {
            $media->move($app_path, $news_media->name);
        }
        
        $media_type = getMediaType($news_media->extension);

        // Update thumb size in config gile when change 515px size
        $sizes = ['128px', '256px', '515px', '1080px'];

        if($media_type == 'video') {
            $thumb_name = Str::random(10) . '.jpg';

            \FFMpeg::fromDisk('public')
                ->open($path . '/' . $news_media->name)
                ->getFrameFromSeconds(2)
                ->export()
                ->toDisk('public')
                ->save($path . '/thumb/' . $thumb_name);

            $thumb_media = \Image::make($app_path . '/thumb/' . $thumb_name);
            $news_media = $this->storeNewsMediaThumb($news, $news_media, $thumb_media, $thumb_name);

            if($media_data !== null && isset($media_data[$media_unique_id]) && isset($media_data[$media_unique_id]['watermark_position']) && $media_data[$media_unique_id]['watermark_position'] !== null&& $media_data[$media_unique_id]['watermark_position'] != 'none') {
                $original_media_width = $thumb_media->width();
                $convert_media_width = $original_media_width;

                $resize_percentage = 50;

                $watermark_width = (int) round($convert_media_width * ((100 - $resize_percentage) / 100), 2);

                $original_watermark_path = public_path('assets/img/watermark.png');

                $watermark_path = storage_path($public_storage_path . 'watermarks');
                if (!file_exists($watermark_path)) {
                    \File::makeDirectory($watermark_path, 0777, true);
                }

                $watermark_path = $watermark_path . '/' . $watermark_width . '.png';

                \Image::make($original_watermark_path)
                        ->resize($watermark_width, null, function ($constraint) {
                            $constraint->aspectRatio();
                        })->save($watermark_path, 90);
                
                $format = new \FFMpeg\Format\Video\X264('aac', 'libx264');
                $format->setKiloBitrate(800);

                if (!file_exists($path . '/watermarked')) {
                    \File::makeDirectory($path . '/watermarked', 0777, true);
                }

                $watermark_position_code = "overlay=(main_w-overlay_w)/2:(main_h-overlay_h)/2";
                
                if($media_data[$media_unique_id]['watermark_position'] == 'left') {
                    $watermark_position_code = "overlay=0:(main_h-overlay_h)/2";
                }
                if($media_data[$media_unique_id]['watermark_position'] == 'right') {
                    $watermark_position_code = "overlay=main_w-overlay_w-0:(main_h-overlay_h)/2";
                }
                if($media_data[$media_unique_id]['watermark_position'] == 'top-left') {
                    $watermark_position_code = "overlay=0:0";
                }
                if($media_data[$media_unique_id]['watermark_position'] == 'top') {
                    $watermark_position_code = "overlay=(main_w-overlay_w)/2:0";
                }
                if($media_data[$media_unique_id]['watermark_position'] == 'top-right') {
                    $watermark_position_code = "overlay=main_w-overlay_w-0:0";
                }
                if($media_data[$media_unique_id]['watermark_position'] == 'bottom-left') {
                    $watermark_position_code = "overlay=0:main_h-overlay_h-0";
                }
                if($media_data[$media_unique_id]['watermark_position'] == 'bottom') {
                    $watermark_position_code = "overlay=(main_w-overlay_w)/2:main_h-overlay_h-0";
                }
                if($media_data[$media_unique_id]['watermark_position'] == 'bottom-right') {
                    $watermark_position_code = "overlay=main_w-overlay_w-0:main_h-overlay_h-0";
                }

                \FFMpeg::fromDisk('public')
                    ->open($path . '/' . $o_news_media->name)
                    ->addFilter(['-i', $watermark_path])
                    ->addFilter(['-filter_complex', $watermark_position_code])
                    ->export()
                    ->toDisk('public')
                    ->inFormat($format)
                    ->save($path . '/watermarked/' . $o_news_media->name);
            }

            foreach ($sizes as $size) {
                $height = str_replace('px', '', $size);

                if(strpos($size, 'pc') !== false) {
                    $height = str_replace('pc', '', $size);
                    $height = ($thumb_media->height() * $height) / 100;
                }

                if (!file_exists($app_path . '/thumb/' . $size)) {
                    \File::makeDirectory($app_path . '/thumb/' . $size, 0777, true);
                }

                $resized_media = \Image::make($app_path . '/thumb/' . $news_media->name)
                    ->resize(null, $height, function ($constraint) {
                        $constraint->aspectRatio();
                    });

                if($media_data !== null && isset($media_data[$media_unique_id]) && isset($media_data[$media_unique_id]['watermark_position']) && $media_data[$media_unique_id]['watermark_position'] !== null&& $media_data[$media_unique_id]['watermark_position'] != 'none') {
                    $thumb_media_width = $thumb_media->width();
                    $thumb_media_height = $thumb_media->height();

                    $convert_media_ratio = $height / $thumb_media_height;
                    $convert_media_width = $convert_media_ratio * $thumb_media_width;

                    $resize_percentage = 50;
                    $x_offset = round(25 * $convert_media_ratio);

                    $watermark_width = round($convert_media_width * ((100 - $resize_percentage) / 100), 2); //watermark will be $resizePercentage less then the actual width of the image

                    // resize watermark width keep height auto
                    $watermark = \Image::make(public_path('assets/img/watermark.png'));
                    $watermark->resize($watermark_width, null, function ($constraint) {
                        $constraint->aspectRatio();
                    });

                    $resized_media = $resized_media->insert($watermark, $media_data[$media_unique_id]['watermark_position'], 0, 0); 
                }    

                $resized_media = $resized_media->save($app_path . '/thumb/' . $size . '/' . $news_media->name, 90);

                $this->storeNewsMedia($news, $news_media, $resized_media, $size);
            }
        } else {
            $original_media = \Image::make($app_path . '/' . $news_media->name);

            $news_media->details = json_encode([
                'width' => $original_media->width(),
                'height' => $original_media->height()
            ]);
            $news_media->update();

            foreach ($sizes as $size) {
                $height = str_replace('px', '', $size);

                if(strpos($size, 'pc') !== false) {
                    $height = str_replace('pc', '', $size);
                    $height = ($original_media->height() * $height) / 100;
                }

                if (!file_exists($app_path . '/' . $size)) {
                    \File::makeDirectory($app_path . '/' . $size, 0777, true);
                }

                $resized_media = \Image::make($app_path . '/' . $news_media->name)
                    ->resize(null, $height, function ($constraint) {
                        $constraint->aspectRatio();
                    });

                if($media_data !== null && isset($media_data[$media_unique_id]) && isset($media_data[$media_unique_id]['watermark_position']) && $media_data[$media_unique_id]['watermark_position'] !== null&& $media_data[$media_unique_id]['watermark_position'] != 'none') {
                    $original_media_width = $original_media->width();
                    $original_media_height = $original_media->height();

                    $convert_media_ratio = $height / $original_media_height;
                    $convert_media_width = $convert_media_ratio * $original_media_width;

                    $resize_percentage = 50;
                    $x_offset = round(25 * $convert_media_ratio);

                    $watermark_width = round($convert_media_width * ((100 - $resize_percentage) / 100), 2); //watermark will be $resizePercentage less then the actual width of the image

                    // resize watermark width keep height auto
                    $watermark = \Image::make(public_path('assets/img/watermark.png'));
                    $watermark->resize($watermark_width, null, function ($constraint) {
                        $constraint->aspectRatio();
                    });

                    $resized_media = $resized_media->insert($watermark, $media_data[$media_unique_id]['watermark_position'], 0, 0); 
                }
    
                $resized_media = $resized_media->save($app_path . '/' . $size . '/' . $news_media->name, 90);

                $this->storeNewsMedia($news, $news_media, $resized_media, $size);
            }
        }

        return [
            'status' => true
        ];
    }

    public function onesignalNotification($data, $included_segments) {
        if(isset($data['additional_data']) && isset($data['additional_data']['allowed_cities'])) {
            $data['additional_data']['allowed_cities'] = City::where('status', 'Active')->pluck('id')->toArray();
        }

        \Artisan::queue('news:process', [
                'data' => [
                    'data' => $data,
                    'included_segments' => $included_segments,
                ]
            ]
        );

        return [
            'status' => true
        ];
    }

    public function webPushlNotification($data) {
        \Artisan::queue('news:process_status', [
                'data' => $data
            ]
        );

        return [
            'status' => true
        ];
    }

    public function concatVideos($data) {
        \Artisan::queue('concat:videos', [
                'data' => $data
            ]
        );

        return [
            'status' => true
        ];
    }

    public function findNews($id, $device, $request = null) {
        $news = News::with(['poll.options', 'media', 'thumbs', 'downloads', 'categories', 'city' => function($q) {
                $q->leftJoin(\DB::raw('(SELECT city_id, name FROM city_names where language_id = 2 GROUP BY city_id, name) as cn'), 'cn.city_id', '=', 'cities.id');
            }])
            ->leftJoin(\DB::raw('(SELECT news_id, type FROM news_reactions where device_id = ' . $device->id . ' GROUP BY news_id, type) as nr'), 'nr.news_id', '=', 'news.id')
            ->leftJoin(\DB::raw('(SELECT news_id, IFNULL(count(id), 0) AS reactions_claps_count FROM news_reactions where type ="Claps" GROUP BY news_id) as nrc'), 'nrc.news_id', '=', 'news.id')
            // ->leftJoin(\DB::raw('(SELECT news_id, IFNULL(count(id), 0) AS reactions_happy_count FROM news_reactions where type ="Happy" GROUP BY news_id) as nrh'), 'nrh.news_id', '=', 'news.id')
            ->leftJoin(\DB::raw('(SELECT news_id, IFNULL(count(id), 0) AS reactions_sad_count FROM news_reactions where type ="Sad" GROUP BY news_id) as nrs'), 'nrs.news_id', '=', 'news.id')
            // ->leftJoin(\DB::raw('(SELECT news_id, IFNULL(count(id), 0) AS views FROM news_views GROUP BY news_id) as nv'), 'nv.news_id', '=', 'news.id')
            // ->leftJoin(\DB::raw('(SELECT news_id, IFNULL(count(id), 0) AS shares_whatsapp_count FROM news_shares where type ="Whatsapp" GROUP BY news_id) as ns'), 'ns.news_id', '=', 'news.id')
            ->leftJoin(\DB::raw('(SELECT news_id, IFNULL(count(id), 0) AS is_sensitive_viewed FROM news_device_sensitivities where device_id = ' . $device->id . ' GROUP BY news_id) as nds'), 'nds.news_id', '=', 'news.id')
            ->addSelect('news.*')
            ->addSelect('nr.type as reaction')
            ->addSelect('nrc.reactions_claps_count as reactions_claps_count')
            ->addSelect('nrc.reactions_claps_count as reactions_happy_count')
            ->addSelect('nrs.reactions_sad_count as reactions_sad_count')
            ->addSelect('nrc.reactions_claps_count as views')
            ->withCount('shares as shares_whatsapp_count');
            // ->addSelect('ns.shares_whatsapp_count as shares_whatsapp_count');

        if($request !== null && $request->input('platform') !== null && $request->input('platform') == 'iOS') {
            $news = $news->addSelect(\DB::raw('IFNULL(nds.is_sensitive_viewed, 0) as is_sensitive_viewed'));
        } else {
            $news = $news->addSelect('nds.is_sensitive_viewed as is_sensitive_viewed');
        }

        $news = $news->find($id);

        return $news;
    }

    public function findNewsForElasticSearch($id) {
        $news = News::with(['poll.options', 'media', 'thumbs', 'downloads', 'categories', 'created_by_user', 'updated_by_user', 'notifications', 'city' => function($q) {
                $q->leftJoin(\DB::raw('(SELECT city_id, name FROM city_names where language_id = 2 GROUP BY city_id, name) as cn'), 'cn.city_id', '=', 'cities.id');
            }])
            ->withCount('attachments')
            ->leftJoin(\DB::raw('(SELECT news_id, IFNULL(count(id), 0) AS reactions_claps_count FROM news_reactions where type ="Claps" GROUP BY news_id) as nrc'), 'nrc.news_id', '=', 'news.id')
            ->leftJoin(\DB::raw('(SELECT news_id, IFNULL(count(id), 0) AS reactions_sad_count FROM news_reactions where type ="Sad" GROUP BY news_id) as nrs'), 'nrs.news_id', '=', 'news.id')
            ->addSelect('news.*')
            ->addSelect('nrc.reactions_claps_count as reactions_claps_count')
            ->addSelect('nrc.reactions_claps_count as reactions_happy_count')
            ->addSelect('nrs.reactions_sad_count as reactions_sad_count')
            ->addSelect('nrc.reactions_claps_count as views')
            ->withCount('shares as shares_whatsapp_count');

        $news = $news->find($id);

        $news_categories = $news->categories();
        $news_categories_ids = $news->categories()->pluck('category_id')->toArray();
        $news_categories = $news->categories()->get()->toArray();
        $news_cities = $news->cities();
        $news_cities_ids = $news_cities->pluck('cities.id')->toArray();
        $news_cities = $news_cities->get()->toArray();
        $news_likes_device_ids = $news->likes()->pluck('device_id')->toArray();
        $news_dislikes_device_ids = $news->dislikes()->pluck('device_id')->toArray();
        $news_sensitivities_device_ids = $news->sensitivities()->pluck('device_id')->toArray();

        $media_views = $news->media_views !== null? $news->media_views: 0;
        $media_downloads = $news->media_downloads !== null? $news->media_downloads: 0;

        $news = $news->toArray();
        $news['categories_ids'] = $news_categories_ids;
        $news['cities'] = $news_cities_ids;
        $news['cities_with_details'] = $news_cities;
        $news['categories_with_details'] = $news_categories;
        $news['media_views'] = $media_views;
        $news['media_downloads'] = $media_downloads;
        
        $news['v2_cities_ids'] = $news_cities_ids;
        $news['likes'] = $news_likes_device_ids;
        $news['dislikes'] = $news_dislikes_device_ids;
        $news['sensitivities'] = $news_sensitivities_device_ids;

        return $news;
    }

    public function indexMemoryForElasticSearch($id)
    {
        $memory = Memory::with('from_names')->find($id);
        if($memory) {
            $memory_cities_ids = $memory->cities()->pluck('cities.id')->toArray();
            $memory->cities = $memory_cities_ids;
            $memory = $memory->toArray();
            
            \Elasticsearch::update([
                'id' => $memory['id'],
                'index' => 'memories',
                'body' => [
                    'doc' => $memory,
                    'doc_as_upsert' => true,
                ]
            ]);
        }

        return $memory;
    }

    public function indexQuoteForElasticSearch($id)
    {
        $quote = Quote::find($id);

        if($quote) {
            $quote = $quote->toArray();
            
            \Elasticsearch::update([
                'id' => $quote['id'],
                'index' => 'quotes',
                'body' => [
                    'doc' => $quote,
                    'doc_as_upsert' => true,
                ]
            ]);
        }

        return $quote;
    }

    public function pollResponce($device, $poll)
    {
        $now = \Carbon::now();

        $poll_option_ids = [];
        if(is_array($poll)) {
            $poll_option_ids = \Arr::pluck($poll['options'], 'id');
        } else {
            $poll_option_ids = $poll->options->pluck('id')->toArray();
        }
        $device_poll_options = DevicePollOption::whereIn('poll_option_id', $poll_option_ids)->where('device_id', $device->id)->get()->pluck('poll_option_id')->toArray();

        if(!is_array($poll)) {
            $poll = $poll->toArray();
        }

        $poll = Poll::with('options')
            ->leftJoin(\DB::raw('(SELECT poll_id, IFNULL(count(id), 0) AS is_result_viewed FROM poll_result_device_views where device_id = ' . $device->id . ' GROUP BY poll_id) as prdv'), 'prdv.poll_id', '=', 'polls.id')
            ->find($poll['id'])->toArray();

        if($poll['start_datetime'] < $now && ($poll['end_datetime'] > $now || ($poll['result_end_datetime'] !== null && $poll['result_end_datetime'] > $now))) {
            $poll['is_voted'] = 0;
            if(count($device_poll_options)) {
                $poll['is_voted'] = 1;
            }

            foreach ($poll['options'] as $key => $poll_option) {
                $poll['options'][$key]['is_selected'] = 0;
                if(count($device_poll_options) && in_array($poll['options'][$key]['id'], $device_poll_options)) {
                    $poll['options'][$key]['is_selected'] = 1;   
                }
            }

            return $poll;
        }

        return null;
    }
}
