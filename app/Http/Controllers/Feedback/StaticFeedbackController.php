<?php

namespace App\Http\Controllers\Feedback;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\DeviceStaticFeedback;

class StaticFeedbackController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $feedback_options_list = [
            'Yes' => [
                [
                    'id' => 1,
                    'title' => 'એપ વાપરવાની સરળતા',
                ],
                [
                    'id' => 2,
                    'title' => 'સમાચાર સચોટ હોઈ છે'
                ],
                [
                    'id' => 3,
                    'title' => 'સૌથી પેલા સમાચાર મળે છે'
                ],
                [
                    'id' => 4,
                    'title' => 'એપમાં કોઈ એડ્સ નથી'
                ],
            ],
            'No' => [
                [
                    'id' => 5,
                    'title' => 'સમાચાર ઝડપી નથી આવતા'
                ],
                [
                    'id' => 6,
                    'title' => 'વધુ નોટિફિકેશન આવે છે'
                ],
                [
                    'id' => 7,
                    'title' => 'એપમાં ખામી છે'
                ],
                [
                    'id' => 8,
                    'title' => 'અન્ય કેટેગરીના સમાચાર નથી'
                ],
            ]
        ];

        $feedback_options = ['Yes' => [], 'No' => []];

        foreach ($feedback_options_list as $feedback_option_type => $feedback_option_type_options) {
            foreach ($feedback_option_type_options as $feedback_option) {
                $feedback_options[$feedback_option_type][$feedback_option['id']] = ['title' => $feedback_option['title'], 'count' => 0, 'percentage' => 0];
            }
        }

        $static_feedbacks = DeviceStaticFeedback::all();
        $static_feedbacks_total = count($static_feedbacks);
        $static_feedbacks_options_total = 0;

        foreach ($static_feedbacks as $static_feedback) {
            if($static_feedback->value !== null && $static_feedback->options !== null) {
                foreach ($static_feedback->options as $static_feedback_option) {
                    if($feedback_options[$static_feedback->value] && isset($feedback_options[$static_feedback->value][$static_feedback_option])) {
                        $feedback_options[$static_feedback->value][$static_feedback_option]['count']++;
                        $static_feedbacks_options_total++;
                    }
                }
            }
        }

        foreach ($static_feedbacks as $static_feedback) {
            if($static_feedback->value !== null && $static_feedback->options !== null) {
                foreach ($static_feedback->options as $static_feedback_option) {
                    if(isset($feedback_options[$static_feedback->value]) && isset($feedback_options[$static_feedback->value][$static_feedback_option])) {
                        $feedback_options[$static_feedback->value][$static_feedback_option]['percentage'] = round(($feedback_options[$static_feedback->value][$static_feedback_option]['count'] / $static_feedbacks_options_total) * 100, 2);
                    }
                }
            }
        }

        $yes_count = DeviceStaticFeedback::where('value', 'Yes')->count();
        $no_count = DeviceStaticFeedback::where('value', 'No')->count();
        $no_feedback_count = DeviceStaticFeedback::where('value', NULL)->count();

        $yes_percentage = round(($yes_count / $static_feedbacks_total) * 100, 2);
        $no_percentage = round(($no_count / $static_feedbacks_total) * 100, 2);
        $no_feedback_percentage = round(($no_feedback_count / $static_feedbacks_total) * 100, 2);

        $yes_messages = DeviceStaticFeedback::where('value', 'Yes')->where('message', '!=', null)->orderBy('id', 'desc')->get();
        $no_messages = DeviceStaticFeedback::where('value', 'No')->where('message', '!=', null)->orderBy('id', 'desc')->get();

        return view('pages.devices.feedbacks.static', compact('static_feedbacks', 'static_feedbacks_total', 'yes_percentage', 'no_percentage', 'no_feedback_percentage', 'feedback_options', 'yes_messages', 'no_messages'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
