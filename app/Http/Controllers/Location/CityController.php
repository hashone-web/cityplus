<?php

namespace App\Http\Controllers\Location;

use App\City;
use App\CityName;
use App\District;
use App\Http\Controllers\Controller;
use App\Language;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class CityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $statuses = ['Active', 'Inactive'];

        $query_parameters = $request->query();

        $cities = City::with('district')->withCount('devices as devices_count')->has('district')->sortable()
            ->leftJoin(\DB::raw('(SELECT city_id, name FROM city_names where language_id = 2 GROUP BY city_id, name) as cn'), 'cn.city_id', '=', 'cities.id')
            ->addSelect('cities.*')
            ->addSelect('cn.name as gujarati_name');

        if($request->input('status') !== null) {
            $cities = $cities->where('cities.status', $request->input('status') == 'Active'? 1:0);
        }

        if($request->input('search_value') !== null) {
            $cities = $cities->where('cities.name', 'like', '%'. $request->input('search_value') . '%');
        }

        $cities = $cities->paginate(\Config::get('constants.pagination_size'));

        return view('pages.cities.index', compact('cities', 'statuses', 'query_parameters'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $districts = District::all();

        return view('pages.cities.create', compact('districts'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => ['required', 'max:255', 'unique:cities'],
            'district' => ['required']
        ]);

        if ($validator->fails()) {
            return redirect()
                ->route('cities.create')
                ->withErrors($validator)
                ->withInput();
        }

        try {
            DB::beginTransaction();

            $city = new City();
            $city->name = $request->input('name');
            $city->district_id = $request->input('district');
            $city->save();

            $gujarati_name = CityName::firstOrNew([ 'city_id' => $city->id, 'language_id' => 2 ]);
            $gujarati_name->name = $request->input('gujarati_name');
            $gujarati_name->save();

            DB::commit();

            \Toastr::success('City Created successfully', 'Success', []);
        } catch (\Exception $e) {
            DB::rollback();
            \Toastr::error('Error occured during creating city', 'Error', []);
        }

        return redirect()->route('cities.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $city = City::leftJoin(\DB::raw('(SELECT city_id, name FROM city_names where language_id = 2 GROUP BY city_id, name) as cn'), 'cn.city_id', '=', 'cities.id')
            ->addSelect('cities.*')
            ->addSelect('cn.name as gujarati_name')
            ->find($id);

        if($city) {
            $districts = District::all();

            return view('pages.cities.create', compact('districts', 'city'));
        }

        abort(404);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $city = City::find($id);

        if($city) {
            $validator = Validator::make($request->all(), [
                'name' => ['required', 'string', 'max:255', 'unique:cities,name,' . $id],
                'district' => ['required']
            ]);

            if ($validator->fails()) {
                return redirect()
                    ->route('cities.edit', ['id' => $id])
                    ->withErrors($validator)
                    ->withInput();
            }

            try {
                DB::beginTransaction();

                $city->name = $request->input('name');
                $city->district_id = $request->input('district');
                $city->update();

                $gujarati_name = CityName::firstOrNew([ 'city_id' => $city->id, 'language_id' => 2 ]);
                $gujarati_name->name = $request->input('gujarati_name');
                $gujarati_name->save();

                DB::commit();

                \Toastr::success('City Updated successfully', 'Success', []);
            } catch (\Exception $e) {
                DB::rollback();
                \Toastr::error('Error occured during updating city', 'Error', []);
            }

            return redirect()->route('cities.index');
        }

        abort(404);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $city = City::find($id);

        if($city) {
            $city->delete();

            \Toastr::success('City Deleted successfully', 'Success', []);

            return redirect()->route('cities.index');
        }

        abort(404);
    }

    public function toggleStatus(Request $request, $id)
    {
        $city = City::find($id);

        if($city) {
            $city->status = $request->input('status') == "true"? 1: 0;
            $city->update();

            return [
                'status' => true,
                'data' => $city
            ];
        }

        abort(404);
    }
}
