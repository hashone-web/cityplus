<?php

namespace App\Http\Controllers\Location;

use App\Http\Controllers\Controller;
use App\District;
use App\City;
use App\CityGroup;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class CityGroupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $city_groups = CityGroup::sortable()->paginate(\Config::get('constants.pagination_size'));

        return view('pages.city_groups.index', compact('city_groups'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $cities = City::where('status', 1)->get();
        $districts = District::with('cities')->where('status', 1)->get();

        return view('pages.city_groups.create', compact('districts', 'cities'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => ['required', 'string', 'max:255', 'unique:city_groups'],
            'cities' => ['required'],
        ]);

        if ($validator->fails()) {
            return redirect()
                ->route('city.groups.create')
                ->withErrors($validator)
                ->withInput();
        }

        try {
            DB::beginTransaction();

            $city_group = new CityGroup();
            $city_group->name = $request->input('name');
            $city_group->save();

            $city_group->cities()->sync($request->input('cities'));

            DB::commit();

            \Toastr::success('City Group Created successfully', 'Success', []);
        } catch (\Exception $e) {
            DB::rollback();
            \Toastr::error('Error occured during creating city group', 'Error', []);
        }

        return redirect()->route('city.groups.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\CityGroup  $cityGroup
     * @return \Illuminate\Http\Response
     */
    public function show(CityGroup $cityGroup)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\CityGroup  $cityGroup
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $city_group = CityGroup::with('cities')->find($id);
        $cities = City::where('status', 1)->get();
        $districts = District::with('cities')->where('status', 1)->get();

        if($city_group) {
            return view('pages.city_groups.create', compact('districts', 'cities', 'city_group'));
        }

        abort(404);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\CityGroup  $cityGroup
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $city_group = CityGroup::find($id);

        if($city_group) {
            $validator = Validator::make($request->all(), [
                'name' => ['required', 'string', 'max:255', 'unique:city_groups,name,' . $id],
                'cities' => ['required'],
            ]);

            if ($validator->fails()) {
                return redirect()
                    ->route('city.groups.edit', ['id' => $id])
                    ->withErrors($validator)
                    ->withInput();
            }

            try {
                DB::beginTransaction();

                $city_group->name = $request->input('name');
                $city_group->update();

                $city_group->cities()->sync($request->input('cities'));

                DB::commit();

                \Toastr::success('City Group Updated successfully', 'Success', []);
            } catch (\Exception $e) {
                DB::rollback();
                \Toastr::error('Error occured during updating city group', 'Error', []);
            }

            return redirect()->route('city.groups.index');
        }

        abort(404);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\CityGroup  $cityGroup
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $city_group = CityGroup::find($id);

        if($city_group) {
            $city_group->delete();

            \Toastr::success('City Group Deleted successfully', 'Success', []);

            return redirect()->route('city.groups.index');
        }

        abort(404);
    }
}
