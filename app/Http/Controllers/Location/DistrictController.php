<?php

namespace App\Http\Controllers\Location;

use App\District;
use App\DistrictName;
use App\Http\Controllers\Controller;
use App\State;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class DistrictController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $statuses = ['Active', 'Inactive'];

        $query_parameters = $request->query();

        $districts = District::with('state')->has('state')->sortable()
            ->leftJoin(\DB::raw('(SELECT district_id, name FROM district_names where language_id = 2 GROUP BY district_id, name) as dn'), 'dn.district_id', '=', 'districts.id')
            ->addSelect('districts.*')
            ->addSelect('dn.name as gujarati_name');

        if($request->input('status') !== null) {
            $districts = $districts->where('districts.status', $request->input('status') == 'Active'? 1:0);
        }

        if($request->input('search_value') !== null) {
            $districts = $districts->where('districts.name', 'like', '%'. $request->input('search_value') . '%');
        }

        $districts = $districts->paginate(\Config::get('constants.pagination_size'));

        return view('pages.districts.index', compact('districts', 'statuses', 'query_parameters'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $states = State::all();

        return view('pages.districts.create', compact('states'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => ['required', 'max:255', 'unique:districts'],
            'state' => ['required']
        ]);

        if ($validator->fails()) {
            return redirect()
                ->route('districts.create')
                ->withErrors($validator)
                ->withInput();
        }

        try {
            DB::beginTransaction();

            $district = new District();
            $district->name = $request->input('name');
            $district->state_id = $request->input('state');
            $district->save();

            $gujarati_name = DistrictName::firstOrNew([ 'district_id' => $district->id, 'language_id' => 2 ]);
            $gujarati_name->name = $request->input('gujarati_name');
            $gujarati_name->save();

            DB::commit();

            \Toastr::success('District Created successfully', 'Success', []);
        } catch (\Exception $e) {
            DB::rollback();
            \Toastr::error('Error occured during creating district', 'Error', []);
        }

        return redirect()->route('districts.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $district = District::find($id);

        if($district) {
            $states = State::all();

            return view('pages.districts.create', compact('states', 'district'));
        }

        abort(404);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $district = District::find($id);

        if($district) {
            $validator = Validator::make($request->all(), [
                'name' => ['required', 'string', 'max:255', 'unique:districts,name,' . $id],
                'state' => ['required']
            ]);

            if ($validator->fails()) {
                return redirect()
                    ->route('districts.edit', ['id' => $id])
                    ->withErrors($validator)
                    ->withInput();
            }

            try {
                DB::beginTransaction();

                $district->name = $request->input('name');
                $district->state_id = $request->input('state');
                $district->update();

                $gujarati_name = DistrictName::firstOrNew([ 'district_id' => $district->id, 'language_id' => 2 ]);
                $gujarati_name->name = $request->input('gujarati_name');
                $gujarati_name->save();

                DB::commit();

                \Toastr::success('District Updated successfully', 'Success', []);
            } catch (\Exception $e) {
                DB::rollback();
                \Toastr::error('Error occured during updating district', 'Error', []);
            }

            return redirect()->route('districts.index');
        }

        abort(404);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $district = District::find($id);

        if($district) {
            $district->delete();

            \Toastr::success('District Deleted successfully', 'Success', []);

            return redirect()->route('districts.index');
        }

        abort(404);
    }

    public function toggleStatus(Request $request, $id)
    {
        $district = District::find($id);

        if($district) {
            $district->status = $request->input('status') == "true"? 1: 0;
            $district->update();

            return [
                'status' => true,
                'data' => $district
            ];
        }

        abort(404);
    }
}
