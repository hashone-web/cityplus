<?php

namespace App\Http\Controllers\Location;

use App\City;
use App\Http\Controllers\Controller;
use App\Locality;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class LocalityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $localities = Locality::with('city')->has('city')->sortable()->paginate(\Config::get('constants.pagination_size'));

        return view('pages.localities.index', compact('localities'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $cities = City::all();

        return view('pages.localities.create', compact('cities'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => ['required', 'string', 'max:255', 'unique:localities'],
            'city' => ['required']
        ]);

        if ($validator->fails()) {
            return redirect()
                ->route('localities.create')
                ->withErrors($validator)
                ->withInput();
        }

        try {
            DB::beginTransaction();

            $locality = new Locality();
            $locality->name = $request->input('name');
            $locality->city_id = $request->input('city');
            $locality->save();

            DB::commit();

            \Toastr::success('Locality Created successfully', 'Success', []);
        } catch (\Exception $e) {
            DB::rollback();
            \Toastr::error('Error occured during creating locality', 'Error', []);
        }

        return redirect()->route('localities.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $locality = Locality::find($id);

        if($locality) {
            $cities = City::all();

            return view('pages.localities.create', compact('cities', 'locality'));
        }

        abort(404);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $locality = Locality::find($id);

        if($locality) {
            $validator = Validator::make($request->all(), [
                'name' => ['required', 'string', 'max:255', 'unique:localities,name,' . $id],
                'city' => ['required']
            ]);

            if ($validator->fails()) {
                return redirect()
                    ->route('localities.edit', ['id' => $id])
                    ->withErrors($validator)
                    ->withInput();
            }

            try {
                DB::beginTransaction();

                $locality->name = $request->input('name');
                $locality->city_id = $request->input('city');
                $locality->update();

                DB::commit();

                \Toastr::success('Locality Updated successfully', 'Success', []);
            } catch (\Exception $e) {
                DB::rollback();
                \Toastr::error('Error occured during updating locality', 'Error', []);
            }

            return redirect()->route('localities.index');
        }

        abort(404);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $locality = Locality::find($id);

        if($locality) {
            $locality->delete();

            \Toastr::success('Locality Deleted successfully', 'Success', []);

            return redirect()->route('localities.index');
        }

        abort(404);
    }
}
