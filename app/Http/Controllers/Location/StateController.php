<?php

namespace App\Http\Controllers\Location;

use App\Country;
use App\Http\Controllers\Controller;
use App\State;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class StateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $states = State::with('country')->has('country')->sortable()->paginate(\Config::get('constants.pagination_size'));

        return view('pages.states.index', compact('states'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $countries = Country::all();

        return view('pages.states.create', compact('countries'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => ['required', 'string', 'max:255', 'unique:states'],
            'country' => ['required']
        ]);

        if ($validator->fails()) {
            return redirect()
                ->route('states.create')
                ->withErrors($validator)
                ->withInput();
        }

        try {
            DB::beginTransaction();

            $state = new State();
            $state->name = $request->input('name');
            $state->country_id = $request->input('country');
            $state->save();

            DB::commit();

            \Toastr::success('State Created successfully', 'Success', []);
        } catch (\Exception $e) {
            DB::rollback();
            \Toastr::error('Error occured during creating state', 'Error', []);
        }

        return redirect()->route('states.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $state = State::find($id);

        if($state) {
            $countries = Country::all();

            return view('pages.states.create', compact('countries', 'state'));
        }

        abort(404);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $state = State::find($id);

        if($state) {
            $validator = Validator::make($request->all(), [
                'name' => ['required', 'string', 'max:255', 'unique:states,name,' . $id],
                'country' => ['required']
            ]);

            if ($validator->fails()) {
                return redirect()
                    ->route('states.edit', ['id' => $id])
                    ->withErrors($validator)
                    ->withInput();
            }

            try {
                DB::beginTransaction();

                $state->name = $request->input('name');
                $state->country_id = $request->input('country');
                $state->update();

                DB::commit();

                \Toastr::success('State Updated successfully', 'Success', []);
            } catch (\Exception $e) {
                DB::rollback();
                \Toastr::error('Error occured during updating state', 'Error', []);
            }

            return redirect()->route('states.index');
        }

        abort(404);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $state = State::find($id);

        if($state) {
            $state->delete();

            \Toastr::success('State Deleted successfully', 'Success', []);

            return redirect()->route('states.index');
        }

        abort(404);
    }
}
