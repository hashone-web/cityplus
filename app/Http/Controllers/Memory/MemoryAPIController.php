<?php

namespace App\Http\Controllers\Memory;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

use App\Device;
use App\Memory;
use App\MemoryDeviceReaction;
use App\MemoryDeviceShare;

class MemoryAPIController extends Controller
{
    public function storeReactions(Request $request) {
        $validator = Validator::make($request->all(), [
            'memory_id' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 401);
        }

        $data = ['status' => true];

        $device = Device::firstOrNew(['unique_id' => $request->input('device_id')]);
        $device->save();

        try {
            $memory_device_reaction = MemoryDeviceReaction::firstOrNew(['device_id' => $device->id, 'memory_id' => $request->input('memory_id')]);
            $memory_device_reaction->save();

            $memory = Memory::find($request->input('memory_id'));
            $memory_device_reactions_device_ids = [];

            if($memory) {
                $memory_device_reactions_device_ids = $memory->reactions()->pluck('device_id')->toArray();
            
                $memory = $memory->toArray();
                $memory_device_for_elastic_search = [];
                $memory_device_for_elastic_search['reactions'] = $memory_device_reactions_device_ids;
                $memory_device_for_elastic_search['reactions_count'] = count($memory_device_reactions_device_ids);

                if(env('ELASTICSEARCH')) {
                    \Elasticsearch::update([
                        'id' => $memory['id'],
                        'index' => 'memories',
                        'body' => [
                            'doc' => $memory_device_for_elastic_search,
                            'doc_as_upsert' => true
                        ]
                    ]);
                }

                $data['data'] = $memory;
            } else {
                $data['status'] = false;
            }
        } catch (\Exception $e) {
            $data['status'] = false;
        }

        return $data;
    }

    public function storeShares(Request $request) {
        $validator = Validator::make($request->all(), [
            'memory_id' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 401);
        }

        $data = ['status' => true];

        $device = Device::firstOrNew(['unique_id' => $request->input('device_id')]);
        $device->save();

        try {
            $memory_device_share = MemoryDeviceShare::firstOrNew(['device_id' => $device->id, 'memory_id' => $request->input('memory_id')]);
            $memory_device_share->save();
            
            $memory = Memory::find($request->input('memory_id'));

            if($memory) {
                $memory_device_shares_device_ids = $memory->shares()->pluck('device_id')->toArray();

                $memory = $memory->toArray();
                $memory_device_for_elastic_search = [];

                $memory_device_for_elastic_search['shares'] = $memory_device_shares_device_ids;
                $memory_device_for_elastic_search['shares_count'] = count($memory_device_shares_device_ids);

                if(env('ELASTICSEARCH')) {
                    \Elasticsearch::update([
                        'id' => $memory['id'],
                        'index' => 'memories',
                        'body' => [
                            'doc' => $memory_device_for_elastic_search,
                            'doc_as_upsert' => true
                        ]
                    ]);
                }

                $data['data'] = $memory;
            } else {
                $data['status'] = false;    
            }
        } catch (\Exception $e) {
            $data['status'] = false;
        }

        return $data;
    }

    public function storeClicks(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'memory_id' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 401);
        }

        $data = ['status' => true];
        
        try {
            Memory::where('id', $request->input('memory_id'))->update([
                'clicks'=> \DB::raw('clicks+1')
            ]);

            if(env('ELASTICSEARCH')) {
                \Elasticsearch::update([
                    'id' => $request->input('memory_id'),
                    'index' => 'memories',
                    'body' => [
                        'script' => [
                            'source' => 'ctx._source.clicks += params.count',
                            'params' => [
                                'count' => 1
                            ],
                        ],
                        'upsert' => [
                            'clicks' => 1
                        ]
                    ]
                ]); 
            }
        } catch (\Exception $e) {
            $data['status'] = false;
        }

        return $data;
    }
}
