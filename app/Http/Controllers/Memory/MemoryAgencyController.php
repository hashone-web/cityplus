<?php

namespace App\Http\Controllers\Memory;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

use App\MemoryAgency;

class MemoryAgencyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $agencies = MemoryAgency::sortable(['id' => 'desc'])->paginate(\Config::get('constants.pagination_size'));

        return view('pages.memories.agencies.index', compact('agencies'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.memories.agencies.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => ['required', 'string', 'max:255', 'unique:memory_agencies'],
        ]);

        if ($validator->fails()) {
            return redirect()
                ->route('agencies.create')
                ->withErrors($validator)
                ->withInput();
        }

        try {
            DB::beginTransaction();

            $agency = new MemoryAgency();
            $agency->name = $request->input('name');
            $agency->business_name = $request->input('business_name');
            $agency->contact_number = $request->input('contact_number');
            $agency->save();

            DB::commit();

            \Toastr::success('Agency Created successfully', 'Success', []);
        } catch (\Exception $e) {
            DB::rollback();
            \Toastr::error('Error occured during creating agency', 'Error', []);
        }

        return redirect()->route('agencies.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $agency = MemoryAgency::find($id);

        if($agency) {
            return view('pages.memories.agencies.create', compact('agency'));
        }

        abort(404);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $agency = MemoryAgency::find($id);

        if($agency) {
            $validator = Validator::make($request->all(), [
                'name' => ['required', 'string', 'max:255', 'unique:memory_agencies,name,' . $id],
            ]);

            if ($validator->fails()) {
                return redirect()
                    ->route('agencies.edit', ['id' => $id])
                    ->withErrors($validator)
                    ->withInput();
            }

            try {
                DB::beginTransaction();

                $agency->name = $request->input('name');
                $agency->business_name = $request->input('business_name');
                $agency->contact_number = $request->input('contact_number');
                $agency->update();

                DB::commit();

                \Toastr::success('Agency Updated successfully', 'Success', []);
            } catch (\Exception $e) {
                DB::rollback();
                \Toastr::error('Error occured during updating agency', 'Error', []);
            }

            return redirect()->route('agencies.index');
        }

        abort(404);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $agency = MemoryAgency::find($id);

        if($agency) {
            $agency->delete();

            \Toastr::success('Agency Deleted successfully', 'Success', []);

            return redirect()->route('agencies.index');
        }

        abort(404);
    }
}
