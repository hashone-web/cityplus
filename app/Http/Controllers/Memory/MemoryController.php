<?php

namespace App\Http\Controllers\Memory;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

use App\Memory;
use App\MemoryAgency;
use App\MemoryFromName;
use App\MemoryProof;
use App\MemoryFrameType;
use App\City;
use App\District;
use App\CityGroup;

class MemoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $memories = Memory::sortable(['id' => 'desc'])->paginate(\Config::get('constants.pagination_size'));

        return view('pages.memories.index', compact('memories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $agencies = MemoryAgency::all();

        $name_prefixes = ['સ્વ.'];
        $statuses = ['Published', 'Unpublished'];

        $memory_frame_types_raw = MemoryFrameType::get();
        $memory_frame_types = [];

        foreach ($memory_frame_types_raw as $memory_frame_type_raw) {
            if(!isset($memory_frame_types[$memory_frame_type_raw->type])) $memory_frame_types[$memory_frame_type_raw->type] = [];

            array_push($memory_frame_types[$memory_frame_type_raw->type], $memory_frame_type_raw);
        }

        $cities = City::all();
        
        $publish_to_cities = District::with(['cities' => function($q) {
            $q->where('status', 1);
        }])->where('status', 1)->get();

        $districts = District::with('cities')->where('status', 1)->get();
        $city_groups = CityGroup::with('cities')->where('status', 1)->get();

        return view('pages.memories.create', compact('agencies', 'name_prefixes', 'statuses', 'publish_to_cities', 'districts', 'city_groups', 'memory_frame_types'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'type' => ['required'],
            'heading' => ['required', 'string', 'max:255'],
            'name_prefix' => ['required', 'string', 'max:255'],
            'name' => ['required', 'string', 'max:255'],
            'photo' => ['required'],
            'death_date' => ['required'],
            'details' => ['required', 'string', 'max:400'],
            'publish_datetime' => ['required'],
            'end_datetime' => ['required'],
            'cities' => ['required'],
        ]);

        if ($validator->fails()) {
            return redirect()
                ->route('memories.create')
                ->withErrors($validator)
                ->withInput();
        }

        try {
            DB::beginTransaction();

            $request_all = $request->all();
            $request_all_keys = array_keys($request_all);

            $memory_from_names_keys = array_filter($request_all_keys, function($key) {
                return strpos($key, 'memory_from_names_') === 0;
            });

            $memory_from_names = [];

            foreach ($memory_from_names_keys as $memory_from_names_key) {
                $memory_from_names_key_str = str_replace('memory_from_names_', '', $memory_from_names_key);
                $memory_from_names_key_str_parts = explode('__', $memory_from_names_key_str);
                if(!isset($memory_from_names[$memory_from_names_key_str_parts[1]])) $memory_from_names[$memory_from_names_key_str_parts[1]] = [];
                $memory_from_names[$memory_from_names_key_str_parts[1]][$memory_from_names_key_str_parts[0]] = $request->input($memory_from_names_key);
            }

            $memory = new Memory();
            $memory->type = $request->input('type');
            $memory->heading = $request->input('heading');
            $memory->name_prefix = $request->input('name_prefix');
            $memory->name = $request->input('name');
            $memory->death_date = $request->input('death_date');
            $memory->death_date_tithi_text = $request->input('death_date_tithi_text');
            $memory->birth_date = $request->input('birth_date');
            $memory->details = $request->input('details');
            $memory->venue = $request->input('venue');
            $memory->address = $request->input('address');
            $memory->datetime = $request->input('datetime');
            $memory->memory_agency_id = $request->input('agency');
            $memory->special_notes = $request->input('special_notes');
            $memory->highlighting_notes = $request->input('highlighting_notes');
            $memory->is_featured = $request->input('is_featured') !== null;
            $memory->publish_datetime = $request->input('publish_datetime');
            $memory->end_datetime = $request->input('end_datetime');
            $memory->status = $request->input('status');
            $memory->created_by = \Auth::id();
            $memory->updated_by = \Auth::id();
            $memory->memory_frame_type_id = $request->input('memory_frame_type_id');
            $memory->save();

            foreach ($memory_from_names as $memory_from_name) {
                $new_memory_from_name = new MemoryFromName();
                $new_memory_from_name->name = $memory_from_name['name'];
                if(isset($memory_from_name['contact_number'])) {
                    $new_memory_from_name->contact_number = $memory_from_name['contact_number'];
                }
                $new_memory_from_name->memory_id = $memory->id;
                $new_memory_from_name->save();
            }

            if($request->has('photo')) {
                $photo = $request->file('photo');
                $memory->photo = $this->memoryPhotoUpload($memory, $photo);
                $memory->update();
            }

            if($request->has('proofs')) {
                $proofs = $request->file('proofs');
                foreach ($proofs as $proof_key => $proof) {
                    $proof_details = $this->memoryProofsUpload($memory, $proof);
                    $new_memory_proof = new MemoryProof();
                    $new_memory_proof->name = $proof_details['name'];
                    $new_memory_proof->original_name = $proof_details['original_name'];
                    $new_memory_proof->dynamic_id = $proof_details['dynamic_id'];
                    $new_memory_proof->mimetype = $proof_details['mimetype'];
                    $new_memory_proof->size = $proof_details['size'];
                    $new_memory_proof->sort = $proof_key;
                    $new_memory_proof->memory_id = $memory->id;
                    $new_memory_proof->save();
                }
            }

            $memory->cities()->sync($request->input('cities'));

            DB::commit();

            $this->indexMemoryForElasticSearch($memory->id);

            \Toastr::success('Memory Created successfully', 'Success', []);
        } catch (\Exception $e) {
            DB::rollback();

            \Toastr::error('Error occured during creating memory', 'Error', []);
        }

        return redirect()->route('memories.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $memory = Memory::with('from_names', 'proofs')->find($id);

        if($memory) {
            $agencies = MemoryAgency::all();

            $name_prefixes = ['સ્વ.'];
            $statuses = ['Published', 'Unpublished'];

            $memory_frame_types_raw = MemoryFrameType::get();
            $memory_frame_types = [];

            foreach ($memory_frame_types_raw as $memory_frame_type_raw) {
                if(!isset($memory_frame_types[$memory_frame_type_raw->type])) $memory_frame_types[$memory_frame_type_raw->type] = [];

                array_push($memory_frame_types[$memory_frame_type_raw->type], $memory_frame_type_raw);
            }

            $cities = City::all();
            
            $publish_to_cities = District::with(['cities' => function($q) {
                $q->where('status', 1);
            }])->where('status', 1)->get();

            $districts = District::with('cities')->where('status', 1)->get();
            $city_groups = CityGroup::with('cities')->where('status', 1)->get();

            return view('pages.memories.create', compact('agencies', 'name_prefixes', 'statuses', 'publish_to_cities', 'districts', 'city_groups', 'memory', 'memory_frame_types'));
        }

        abort(404);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $memory = Memory::with('from_names')->find($id);

        if($memory) {
            try {
                \DB::beginTransaction();

                $request_all = $request->all();
                $request_all_keys = array_keys($request_all);

                $memory->type = $request->input('type');
                $memory->heading = $request->input('heading');
                $memory->name_prefix = $request->input('name_prefix');
                $memory->name = $request->input('name');
                $memory->death_date = $request->input('death_date');
                $memory->death_date_tithi_text = $request->input('death_date_tithi_text');
                $memory->birth_date = $request->input('birth_date');
                $memory->details = $request->input('details');
                $memory->venue = $request->input('venue');
                $memory->address = $request->input('address');
                $memory->datetime = $request->input('datetime');
                $memory->memory_agency_id = $request->input('agency');
                $memory->special_notes = $request->input('special_notes');
                $memory->highlighting_notes = $request->input('highlighting_notes');
                $memory->is_featured = $request->input('is_featured') !== null;
                $memory->publish_datetime = $request->input('publish_datetime');
                $memory->end_datetime = $request->input('end_datetime');
                $memory->status = $request->input('status');
                $memory->updated_by = \Auth::id();
                $memory->memory_frame_type_id = $request->input('memory_frame_type_id');
                $memory->update();


                // Memory From Names
                $memory_from_names_keys = array_filter($request_all_keys, function($key) {
                    return strpos($key, 'memory_from_names_') === 0 && strpos($key, '__ex__') === false;
                });

                $memory_from_names = [];

                foreach ($memory_from_names_keys as $memory_from_names_key) {
                    $memory_from_names_key_str = str_replace('memory_from_names_', '', $memory_from_names_key);
                    $memory_from_names_key_str_parts = explode('__', $memory_from_names_key_str);
                    if(!isset($memory_from_names[$memory_from_names_key_str_parts[1]])) $memory_from_names[$memory_from_names_key_str_parts[1]] = [];
                    $memory_from_names[$memory_from_names_key_str_parts[1]][$memory_from_names_key_str_parts[0]] = $request->input($memory_from_names_key);
                }

                foreach ($memory_from_names as $memory_from_name) {
                    $new_memory_from_name = new MemoryFromName();
                    $new_memory_from_name->name = $memory_from_name['name'];
                    if(isset($memory_from_name['contact_number'])) {
                        $new_memory_from_name->contact_number = $memory_from_name['contact_number'];
                    }
                    $new_memory_from_name->memory_id = $memory->id;
                    $new_memory_from_name->save();
                }


                // Existing Memory From Names
                $existing_memory_from_names_keys = array_filter($request_all_keys, function($key) {
                    return strpos($key, 'memory_from_names_') === 0 && strpos($key, '__ex__') !== false;
                });

                $existing_memory_from_names_ids = $memory->from_names->pluck('id')->toArray();

                $new_memory_from_names_ids = [];
                $existing_memory_from_names = [];

                foreach ($existing_memory_from_names_keys as $existing_memory_from_names_key) {
                    $existing_memory_from_names_key_str = str_replace('ex__', '', $existing_memory_from_names_key);
                    $existing_memory_from_names_key_str = str_replace('memory_from_names_', '', $existing_memory_from_names_key_str);
                    $existing_memory_from_names_key_str_parts = explode('__', $existing_memory_from_names_key_str);
                    if(!isset($existing_memory_from_names[$existing_memory_from_names_key_str_parts[1]])) $existing_memory_from_names[$existing_memory_from_names_key_str_parts[1]] = [];
                    $existing_memory_from_names[$existing_memory_from_names_key_str_parts[1]][$existing_memory_from_names_key_str_parts[0]] = $request->input($existing_memory_from_names_key);
                }

                foreach ($existing_memory_from_names as $existing_memory_from_name_id => $existing_memory_from_name) {
                    $existing_memory_from_name_record = MemoryFromName::find($existing_memory_from_name_id);
                    if($existing_memory_from_name_record) {
                        $existing_memory_from_name_record->name = $existing_memory_from_name['name'];
                        $existing_memory_from_name_record->contact_number = $existing_memory_from_name['contact_number'];
                        $existing_memory_from_name_record->update();
                    }
                    array_push($new_memory_from_names_ids, $existing_memory_from_name_id);
                }

                $deleted_memory_from_names_ids = array_diff($existing_memory_from_names_ids, $new_memory_from_names_ids);

                MemoryFromName::whereIn('id', $deleted_memory_from_names_ids)->delete();

                if($request->has('photo')) {
                    $photo = $request->file('photo');
                    $memory->photo = $this->memoryPhotoUpload($memory, $photo);
                    $memory->update();
                }

                if($request->has('proofs')) {
                    $proofs = $request->file('proofs');
                    foreach ($proofs as $proof_key => $proof) {
                        $proof_details = $this->memoryProofsUpload($memory, $proof);
                        $new_memory_proof = new MemoryProof();
                        $new_memory_proof->name = $proof_details['name'];
                        $new_memory_proof->original_name = $proof_details['original_name'];
                        $new_memory_proof->dynamic_id = $proof_details['dynamic_id'];
                        $new_memory_proof->mimetype = $proof_details['mimetype'];
                        $new_memory_proof->size = $proof_details['size'];
                        $new_memory_proof->sort = $proof_key;
                        $new_memory_proof->memory_id = $memory->id;
                        $new_memory_proof->save();
                    }
                }

                $memory->cities()->sync($request->input('cities'));

                \DB::commit();

                $this->indexMemoryForElasticSearch($memory->id);

                \Toastr::success('Memory Updated successfully', 'Success', []);
            } catch (\Exception $e) {
                \DB::rollback();

                \Toastr::error('Error occured during updating memory', 'Error', []);
            }

            return redirect()->route('memories.index');
        }
        
        abort(404);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $memory = Memory::find($id);

        if($memory) {
            \Elasticsearch::delete([
                'id' => $memory->id,
                'index' => 'memories'
            ]);

            $memory->delete();

            \Toastr::success('Memory Deleted successfully', 'Success', []);

            return redirect()->route('memories.index');
        }

        abort(404);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroyProof($id)
    {
        $memory_proof = MemoryProof::find($id);

        if($memory_proof) {
            $memory_proof->delete();

            $public_storage_path = 'app/public/';
            $path = 'memories/' . $memory_proof->memory_id . '/proofs/' . $memory_proof->name;
            $memory_proof_file_path = storage_path($public_storage_path . $path);

            unlink($memory_proof_file_path);

            return [
                'status' => true
            ];
        }

        abort(404);
    }

    public function memoryPhotoUpload($memory, $photo)
    {
        $photo_extension = $photo->getClientOriginalExtension();
        $photo_filename = Str::random(10) . '.' . $photo_extension;

        $public_storage_path = 'app/public/';
        $path = 'memories/' . $memory->id . '/photo/';
        $app_path = storage_path($public_storage_path . $path);

        if (!file_exists($app_path)) {
            \File::makeDirectory($app_path, 0777, true);
        } else {
            \File::deleteDirectory($app_path, true);
        }

        $photo->move($app_path, $photo_filename);

        $sizes = ['128px', '256px', '515px', '1080px'];

        foreach ($sizes as $size) {
            $height = str_replace('px', '', $size);

            if (!file_exists($app_path . '/' . $size)) {
                \File::makeDirectory($app_path . '/' . $size, 0777, true);
            }

            $resized_media = \Image::make($app_path . '/' . $photo_filename)
                ->resize(null, $height, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($app_path . '/' . $size . '/' . $photo_filename, 90);
        }

        return $photo_filename;
    }

    public function memoryProofsUpload($memory, $proof)
    {
        $proof_details = [];

        $proof_original_name = $proof->getClientOriginalName();
        $proof_extension = $proof->getClientOriginalExtension();
        $proof_mime_type = $proof->getClientMimeType();
        $proof_size = $proof->getSize();
        $proof_filename = Str::random(10) . '.' . $proof_extension;

        $public_storage_path = 'app/public/';
        $path = 'memories/' . $memory->id . '/proofs/';
        $app_path = storage_path($public_storage_path . $path);

        $proof->move($app_path, $proof_filename);

        $proof_details['name'] = $proof_filename;
        $proof_details['original_name'] = $proof_original_name;
        $proof_details['dynamic_id'] = Str::random(10);
        $proof_details['mimetype'] = $proof_mime_type;
        $proof_details['size'] = $proof_size;

        return $proof_details;
    }
}
