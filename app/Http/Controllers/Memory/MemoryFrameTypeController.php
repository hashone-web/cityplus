<?php

namespace App\Http\Controllers\Memory;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

use App\MemoryFrameType;

class MemoryFrameTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $frame_types = MemoryFrameType::sortable(['id' => 'desc'])->paginate(\Config::get('constants.pagination_size'));

        return view('pages.memories.frame_types.index', compact('frame_types'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.memories.frame_types.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'type' => ['required'],
            'name' => ['required', 'string', 'max:255'],
            'thumbnail' => ['required'],
        ]);

        if ($validator->fails()) {
            return redirect()
                ->route('frame_types.create')
                ->withErrors($validator)
                ->withInput();
        }

        try {
            DB::beginTransaction();

            $memory_frame_type = new MemoryFrameType();
            $memory_frame_type->type = $request->input('type');
            $memory_frame_type->name = $request->input('name');
            $memory_frame_type->thumbnail = '';
            $memory_frame_type->save();

            if($request->has('thumbnail')) {
                $thumbnail = $request->file('thumbnail');
                $memory_frame_type->thumbnail = $this->thumbnailUpload($memory_frame_type, $thumbnail);
                $memory_frame_type->update();
            }

            DB::commit();

            \Toastr::success('Frame Created successfully', 'Success', []);
        } catch (\Exception $e) {
            DB::rollback();

            \Toastr::error('Error occured during creating frame', 'Error', []);
        }

        return redirect()->route('frame_types.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $frame_type = MemoryFrameType::find($id);

        if($frame_type) {
            return view('pages.memories.frame_types.create', compact('frame_type'));
        }

        abort(404);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $memory_frame_type = MemoryFrameType::find($id);

        if($memory_frame_type) {
            $validator = Validator::make($request->all(), [
                'type' => ['required'],
                'name' => ['required', 'string', 'max:255']
            ]);

            if ($validator->fails()) {
                return redirect()
                    ->route('frame_types.edit', ['id' => $id])
                    ->withErrors($validator)
                    ->withInput();
            }

            try {
                DB::beginTransaction();

                $memory_frame_type->type = $request->input('type');
                $memory_frame_type->name = $request->input('name');

                if($request->has('thumbnail')) {
                    $thumbnail = $request->file('thumbnail');
                    $memory_frame_type->thumbnail = $this->thumbnailUpload($memory_frame_type, $thumbnail);
                }

                $memory_frame_type->update();

                DB::commit();

                \Toastr::success('Frame Updated successfully', 'Success', []);
            } catch (\Exception $e) {
                DB::rollback();
                \Toastr::error('Error occured during updating frame', 'Error', []);
            }

            return redirect()->route('frame_types.index');
        }

        abort(404);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $frame_type = MemoryFrameType::find($id);

        if($frame_type) {
            $frame_type->delete();

            \Toastr::success('Frame Deleted successfully', 'Success', []);

            return redirect()->route('frame_types.index');
        }

        abort(404);
    }

    private function thumbnailUpload($memory_frame_type, $thumbnail)
    {
        $thumbnail_extension = $thumbnail->getClientOriginalExtension();
        $thumbnail_filename = Str::random(10) . '.' . $thumbnail_extension;

        $public_storage_path = 'app/public/';
        $path = 'memories/frames/' . $memory_frame_type->id . '/thumbnail/';
        $app_path = storage_path($public_storage_path . $path);

        if (!file_exists($app_path)) {
            \File::makeDirectory($app_path, 0777, true);
        } else {
            \File::deleteDirectory($app_path, true);
        }

        $thumbnail->move($app_path, $thumbnail_filename);

        return $thumbnail_filename;
    }
}
