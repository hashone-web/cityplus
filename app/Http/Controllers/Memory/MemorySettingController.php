<?php

namespace App\Http\Controllers\Memory;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

use App\MemorySetting;

class MemorySettingController extends Controller
{
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {
        $setting = MemorySetting::first();

        return view('pages.memories.settings.edit', compact('setting'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        try {
            DB::beginTransaction();

            $setting = MemorySetting::firstOrNew(['id' => 1]);
            $setting->confirmation_message_text = $request->input('confirmation_message_text');
            $setting->confirmation_message_text_gujarati = $request->input('confirmation_message_text_gujarati');
            $setting->share_description = $request->input('share_description');
            $setting->share_description_gujarati = $request->input('share_description_gujarati');
            $setting->save();

            $public_storage_path = 'app/public/';
            $path = '/memories/settings/';
            $app_path = storage_path($public_storage_path . $path);

            if (!file_exists($app_path)) {
                \File::makeDirectory($app_path, 0777, true);
            }

            if($request->file('share_bottom_image') !== null) {
                $old_share_bottom_image = $setting->share_bottom_image;

                $share_bottom_image = $request->file('share_bottom_image');
                $share_bottom_image_extension = $share_bottom_image->getClientOriginalExtension();

                $share_bottom_image_filename = \Str::random(16) . '.' . $share_bottom_image_extension;

                $share_bottom_image->move($app_path, $share_bottom_image_filename);
            
                $setting->share_bottom_image = $share_bottom_image_filename;
                $setting->update();

                \File::delete($app_path . '/' . $old_share_bottom_image);
            }

            if($request->file('share_bottom_image_gujarati') !== null) {
                $old_share_bottom_image_gujarati = $setting->share_bottom_image_gujarati;

                $share_bottom_image_gujarati = $request->file('share_bottom_image_gujarati');
                $share_bottom_image_gujarati_extension = $share_bottom_image_gujarati->getClientOriginalExtension();

                $share_bottom_image_gujarati_filename = \Str::random(16) . '.' . $share_bottom_image_gujarati_extension;

                $share_bottom_image_gujarati->move($app_path, $share_bottom_image_gujarati_filename);
            
                $setting->share_bottom_image_gujarati = $share_bottom_image_gujarati_filename;
                $setting->update();

                \File::delete($app_path . '/' . $old_share_bottom_image_gujarati);
            }

            DB::commit();

            \Toastr::success('Memories Settings Updated successfully', 'Success', []);
        } catch (\Exception $e) {
            DB::rollback();

            dd($e);

            \Toastr::error('Error occured during updating memories settings', 'Error', []);
        }

        return redirect()->route('memories.settings.edit');
    }
}
