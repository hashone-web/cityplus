<?php

namespace App\Http\Controllers\News;

use App\Category;
use App\City;
use App\CityGroup;
use App\District;
use App\Http\Controllers\Controller;
use App\Language;
use App\News;
use App\NewsStatistic;
use App\NewsMedia;
use App\NewsView;
use Illuminate\Http\Request;

use App\Events\NewsPublished;
use App\Events\NewsFeatured;

use App\UserFcmToken;
use App\User;
use App\NewsRejectionReason;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

use Illuminate\Pagination\LengthAwarePaginator as Paginator;
use ProtoneMedia\LaravelFFMpeg\Filesystem\Media;

class NewsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $pagination_size = $request->query('limit') !== null? (int) $request->query('limit'): \Config::get('constants.pagination_size');
        $page = $request->input('page') !== null? (int) $request->input('page'): 1;
        $from = ($page - 1) * $pagination_size;

        $is_news_useful_page = \Request::route()->getName() == 'news.useful';

        $statuses = ['Draft', 'Pending', 'Published', 'Unpublished', 'Rejected'];
        $categories = Category::all();
        $reporters = User::role('Reporter')->where('status', 'Active')->get();
        $districts = District::with('cities')->get();

        $query_parameters = $request->query();

        $elastic_search_params = '{
                    "index":"news",
                    "body":{
                        "sort":{
                            "datetime":{
                                "order":"desc"
                            }
                        }
                    },
                    "from":'. $from .',
                    "size":'. $pagination_size .'
                }';

        $elastic_search_params = json_decode($elastic_search_params, true);

        if($is_news_useful_page) {
            if(!isset($elastic_search_params['body']['query'])) $elastic_search_params['body']['query'] = [];
            if(!isset($elastic_search_params['body']['query']['bool'])) $elastic_search_params['body']['query']['bool'] = [];
            if(!isset($elastic_search_params['body']['query']['bool']['must'])) $elastic_search_params['body']['query']['bool']['must'] = [];
            array_push($elastic_search_params['body']['query']['bool']['must'], ['match' => ['is_useful' => 1]]);
        }

        if($request->input('is_draft') !== null) {
            if(!isset($elastic_search_params['body']['query'])) $elastic_search_params['body']['query'] = [];
            if(!isset($elastic_search_params['body']['query']['bool'])) $elastic_search_params['body']['query']['bool'] = [];
            if(!isset($elastic_search_params['body']['query']['bool']['must'])) $elastic_search_params['body']['query']['bool']['must'] = [];
            array_push($elastic_search_params['body']['query']['bool']['must'], ['match' => ['status' => 'Draft']]);
        }

        if($request->input('is_pending') !== null) {
            if(!isset($elastic_search_params['body']['query'])) $elastic_search_params['body']['query'] = [];
            if(!isset($elastic_search_params['body']['query']['bool'])) $elastic_search_params['body']['query']['bool'] = [];
            if(!isset($elastic_search_params['body']['query']['bool']['must'])) $elastic_search_params['body']['query']['bool']['must'] = [];
            array_push($elastic_search_params['body']['query']['bool']['must'], ['match' => ['status' => 'Pending']]);
        }

        if($request->input('is_unpublished') !== null) {
            if(!isset($elastic_search_params['body']['query'])) $elastic_search_params['body']['query'] = [];
            if(!isset($elastic_search_params['body']['query']['bool'])) $elastic_search_params['body']['query']['bool'] = [];
            if(!isset($elastic_search_params['body']['query']['bool']['must'])) $elastic_search_params['body']['query']['bool']['must'] = [];
            array_push($elastic_search_params['body']['query']['bool']['must'], ['match' => ['status' => 'Unpublished']]);
        }

        if($request->input('is_rejected') !== null) {
            if(!isset($elastic_search_params['body']['query'])) $elastic_search_params['body']['query'] = [];
            if(!isset($elastic_search_params['body']['query']['bool'])) $elastic_search_params['body']['query']['bool'] = [];
            if(!isset($elastic_search_params['body']['query']['bool']['must'])) $elastic_search_params['body']['query']['bool']['must'] = [];
            array_push($elastic_search_params['body']['query']['bool']['must'], ['match' => ['status' => 'Rejected']]);
        }

        if($request->input('is_draft') == null && $request->input('is_pending') == null && $request->input('is_unpublished') == null && $request->input('is_rejected') == null) {
            if(!isset($elastic_search_params['body']['query'])) $elastic_search_params['body']['query'] = [];
            if(!isset($elastic_search_params['body']['query']['bool'])) $elastic_search_params['body']['query']['bool'] = [];
            if(!isset($elastic_search_params['body']['query']['bool']['must'])) $elastic_search_params['body']['query']['bool']['must'] = [];
            array_push($elastic_search_params['body']['query']['bool']['must'], ['match' => ['status' => 'Published']]);
        }

        if($request->input('is_notification_scheduled') !== null) {
            if(!isset($elastic_search_params['body']['query'])) $elastic_search_params['body']['query'] = [];
            if(!isset($elastic_search_params['body']['query']['bool'])) $elastic_search_params['body']['query']['bool'] = [];
            if(!isset($elastic_search_params['body']['query']['bool']['must'])) $elastic_search_params['body']['query']['bool']['must'] = [];
            array_push($elastic_search_params['body']['query']['bool']['must'], ['match' => ['is_notification_scheduled' => 1]]);
            
            array_push($elastic_search_params['body']['query']['bool']['must'], [
                'range' => [
                    'notification_datetime' => [
                        'lt' => \Carbon::now()
                    ]
                ]
            ]);
        }

        if($request->input('is_breaking') !== null) {
            if(!isset($elastic_search_params['body']['query'])) $elastic_search_params['body']['query'] = [];
            if(!isset($elastic_search_params['body']['query']['bool'])) $elastic_search_params['body']['query']['bool'] = [];
            if(!isset($elastic_search_params['body']['query']['bool']['must'])) $elastic_search_params['body']['query']['bool']['must'] = [];
            array_push($elastic_search_params['body']['query']['bool']['must'], ['match' => ['is_breaking' => 1]]);
        }

        if($request->input('is_featured') !== null) {
            if(!isset($elastic_search_params['body']['query'])) $elastic_search_params['body']['query'] = [];
            if(!isset($elastic_search_params['body']['query']['bool'])) $elastic_search_params['body']['query']['bool'] = [];
            if(!isset($elastic_search_params['body']['query']['bool']['must'])) $elastic_search_params['body']['query']['bool']['must'] = [];
            array_push($elastic_search_params['body']['query']['bool']['must'], ['match' => ['is_featured' => 1]]);
        }

        if($request->input('status') !== null) {
            if(!isset($elastic_search_params['body']['query'])) $elastic_search_params['body']['query'] = [];
            if(!isset($elastic_search_params['body']['query']['bool'])) $elastic_search_params['body']['query']['bool'] = [];
            if(!isset($elastic_search_params['body']['query']['bool']['must'])) $elastic_search_params['body']['query']['bool']['must'] = [];
            array_push($elastic_search_params['body']['query']['bool']['must'], ['match' => ['status' => $request->input('status')]]);
        }

        if($request->input('category') !== null) {
            if(!isset($elastic_search_params['body']['query'])) $elastic_search_params['body']['query'] = [];
            if(!isset($elastic_search_params['body']['query']['bool'])) $elastic_search_params['body']['query']['bool'] = [];
            if(!isset($elastic_search_params['body']['query']['bool']['filter'])) $elastic_search_params['body']['query']['bool']['filter'] = [];
            
            $elastic_search_params['body']['query']['bool']['filter'] = [
                'terms' => [
                    'categories_ids' => [ $request->input('category') ]
                ]
            ];
        }

        if($request->input('city') !== null) {
            if(!isset($elastic_search_params['body']['query'])) $elastic_search_params['body']['query'] = [];
            if(!isset($elastic_search_params['body']['query']['bool'])) $elastic_search_params['body']['query']['bool'] = [];
            if(!isset($elastic_search_params['body']['query']['bool']['must'])) $elastic_search_params['body']['query']['bool']['must'] = [];
            array_push($elastic_search_params['body']['query']['bool']['must'], ['match' => ['city_id' => $request->input('city')]]);
        }

        if($request->input('publish-city') !== null) {
            if(!isset($elastic_search_params['body']['query'])) $elastic_search_params['body']['query'] = [];
            if(!isset($elastic_search_params['body']['query']['bool'])) $elastic_search_params['body']['query']['bool'] = [];
            if(!isset($elastic_search_params['body']['query']['bool']['filter'])) $elastic_search_params['body']['query']['bool']['filter'] = [];
            
            $elastic_search_params['body']['query']['bool']['filter'] = [
                'terms' => [
                    'cities' => [ $request->input('publish-city') ]
                ]
            ];
        }

        if($request->input('reporter') !== null) {
            if(!isset($elastic_search_params['body']['query'])) $elastic_search_params['body']['query'] = [];
            if(!isset($elastic_search_params['body']['query']['bool'])) $elastic_search_params['body']['query']['bool'] = [];
            if(!isset($elastic_search_params['body']['query']['bool']['must'])) $elastic_search_params['body']['query']['bool']['must'] = [];
            array_push($elastic_search_params['body']['query']['bool']['must'], ['term' => ['reporter.id' => $request->input('reporter')]]);
        }

        if(!auth()->user()->can('view all news')) {
            if(!isset($elastic_search_params['body']['query'])) $elastic_search_params['body']['query'] = [];
            if(!isset($elastic_search_params['body']['query']['bool'])) $elastic_search_params['body']['query']['bool'] = [];
            if(!isset($elastic_search_params['body']['query']['bool']['must'])) $elastic_search_params['body']['query']['bool']['must'] = [];
            array_push($elastic_search_params['body']['query']['bool']['must'], ['match' => ['created_by' => auth()->id()]]);
        }        

        $raw_news = \Elasticsearch::search($elastic_search_params);

        $total = $raw_news['hits']['total']['value'];
        $last_page = ceil($total / $pagination_size);

        $news = [];
        $news_ids = [];
        
        foreach ($raw_news['hits']['hits'] as $key => $raw_news_single) {
            unset($raw_news_single['_source']['likes']);
            unset($raw_news_single['_source']['dislikes']);
            unset($raw_news_single['_source']['sensitivities']);

            array_push($news_ids, $raw_news_single['_source']['id']);

            $news[] = $raw_news_single['_source'];
        }

        $news_statistics = NewsStatistic::whereIn('news_id', $news_ids)->get()->keyBy('news_id')->toArray();
        $news_views = NewsView::whereIn('news_id', $news_ids)
            ->select('news_id', DB::raw('count(*) as views'))
            ->groupBy('news_id')
            ->get()->keyBy('news_id')->toArray();

        $pagination = [
            'show_pagination' => ($total / $pagination_size) > 1,
            'total' => $total,
            'per_page' => $pagination_size,
            'current_page' => $page,
            'last_page' => (int)$last_page,
            'from' => $from + 1,
            'to' => $from + $pagination_size
        ];

        $paginator = new Paginator($news, $pagination['total'], $pagination_size, $page, [
            'path'  => $request->url(),
            'query' => $request->query(),
        ]);

        // $news = News::with(['categories', 'thumbs.parent', 'created_by_user', 'updated_by_user', 'city', 'notifications'])
        //     ->leftJoin(\DB::raw('(SELECT news_id, IFNULL(count(id), 0) AS reactions_claps_count FROM news_reactions where type ="Claps" GROUP BY news_id) as nrc'), 'nrc.news_id', '=', 'news.id')
        //     ->leftJoin(\DB::raw('(SELECT news_id, IFNULL(count(id), 0) AS views FROM news_views GROUP BY news_id) as nv'), 'nv.news_id', '=', 'news.id')
        //     ->leftJoin(\DB::raw('(SELECT news_id, IFNULL(count(id), 0) AS shares_whatsapp_count FROM news_shares where type ="Whatsapp" GROUP BY news_id) as ns'), 'ns.news_id', '=', 'news.id')
        //     ->addSelect('news.*')
        //     ->addSelect('nrc.reactions_claps_count as reactions_claps_count')
        //     ->addSelect('nv.views as views')
        //     ->addSelect('ns.shares_whatsapp_count as shares_whatsapp_count');

        // if($is_news_useful_page) {
        //     $news = $news->where('is_useful', 1);
        // }

        // if($request->input('is_draft') !== null) {
        //     $news = $news->where('status', 'Draft');
        // }

        // if($request->input('is_pending') !== null) {
        //     $news = $news->where('status', 'Pending');
        // }

        // if($request->input('is_unpublished') !== null) {
        //     $news = $news->where('status', 'Unpublished');
        // }

        // if($request->input('is_rejected') !== null) {
        //     $news = $news->where('status', 'Rejected');
        // }

        // if($request->input('is_draft') == null && $request->input('is_pending') == null && $request->input('is_unpublished') == null && $request->input('is_rejected') == null) {
        //     $news = $news->where('status', 'Published');
        // }

        // if($request->input('is_notification_scheduled') !== null) {
        //     $news = $news->where('is_notification_scheduled', 1);
        //     $news = $news->where('notification_datetime', '>', \Carbon::now());
        // }

        // if($request->input('is_breaking') !== null) {
        //     $news = $news->where('is_breaking', 1);
        // }

        // if($request->input('is_featured') !== null) {
        //     $news = $news->where('is_featured', 1);
        // }

        // if($request->input('status') !== null) {
        //     $news = $news->where('status', $request->input('status'));
        // }

        // if($request->input('category') !== null) {
        //     $news = $news->whereHas('categories', function($q) use($request) {
        //         $q->where('categories.id', $request->input('category'));
        //     });
        // }

        // if($request->input('city') !== null) {
        //     $news = $news->where('city_id', $request->input('city'));
        // }

        // if($request->input('publish-city') !== null) {
        //     $news = $news->whereHas('cities', function ($query) use ($request) {
        //         $query->where('news_cities.city_id', '=', $request->input('publish-city'));
        //     });
        // }

        // if($request->input('reporter') !== null) {
        //     $news = $news->whereHas('reporters', function ($query) use ($request) {
        //         $query->where('news_users.user_id', '=', $request->input('reporter'));
        //     });
        // }

        // if(!auth()->user()->can('view all news')) {
        //     $news = $news->where('created_by', auth()->id());
        // }

        // $news = $news->sortable(['datetime' => 'desc'])
        //     ->paginate(\Config::get('constants.pagination_size'))
        //     ->appends(request()->query());


        $counts = [];
        $counts['draft'] = News::where('status', 'Draft')->count();
        $counts['pending'] = News::where('status', 'Pending')->count();
        $counts['pending_breaking'] = News::where('status', 'Pending')->where('is_breaking', 1)->count();
        $counts['notification_scheduled'] = News::where('is_notification_scheduled', 1)->where('notification_datetime', '>', \Carbon::now())->where('status', 'Published')->count();
        $counts['breaking'] = News::where('is_breaking', 1)->where('status', 'Published')->count();
        $counts['featured'] = News::where('is_featured', 1)->where('status', 'Published')->count(); 

        return view('pages.news.index', compact('news', 'is_news_useful_page', 'query_parameters', 'statuses', 'categories', 'reporters', 'districts', 'counts', 'paginator', 'news_statistics', 'news_views'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $reporters = User::role('Reporter')->where('status', 'Active')->get();
        
        $cities = City::all();
        
        $publish_to_cities = District::with(['cities' => function($q) {
            $q->where('status', 1);
        }])->where('status', 1)->get();

        $districts = District::with('cities')->where('status', 1)->get();
        $city_groups = CityGroup::with('cities')->where('status', 1)->get();

        $categories_not_for_selection = \Config::get('constants.categories');
        $categories_not_for_selection = $categories_not_for_selection[env('APP_ENV')];
        $categories_not_for_selection = array_values($categories_not_for_selection);
        
        $categories = Category::where('status', 1)->whereNotIn('id', $categories_not_for_selection)->orderBy('status', 'desc')->orderBy('sort', 'asc')->get();
        $languages = Language::all();

        $statuses = ['Draft', 'Pending', 'Published', 'Unpublished', 'Rejected'];
        $notification_types = \Config::get('constants.notification_types');

        return view('pages.news.create', compact('reporters', 'statuses', 'cities', 'publish_to_cities', 'districts', 'city_groups', 'categories', 'languages', 'notification_types'));
    }

    public function reportingCities(Request $request, $id) {
        // $user = User::with('reporting_cities')->find($id);

        // return view('partials.news.reporting_cities')->with(['cities' => $user->reporting_cities]);
        $cities = City::all();

        return view('partials.news.reporting_cities')->with(['cities' => $cities]);
    }

    public function convertHTMLtoText(Request $request)
    {
        $text = $this->convertHTMLtoTextHelper($request->input('html'));

        return [
            'status' => true,
            'data' => $text
        ];
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        ini_set('memory_limit', '-1');
        ini_set('max_execution_time', 300); // 5 minutes

        $validator = Validator::make($request->all(), [
            'headline' => ['required', 'string', 'max:100'],
            'article' => ['nullable', 'string'],
        ]);

        if ($validator->fails()) {
            return redirect()
                ->route('news.create')
                ->withErrors($validator)
                ->withInput();
        }

        try {
            DB::beginTransaction();

            $news = new News();
            $news->headline = $request->input('headline');
            $news->article = $request->input('article');
            if($request->input('article_text') !== null) {
                $news->article_text = $request->input('article_text');
            } else {
                $news->article_text = $this->convertHTMLtoTextHelper($request->input('article'));
            }
            $news->city_id = $request->input('city');
            $news->language_id = 2;
            $news->status = $request->input('status');
            if($request->input('notification_type') !== null) {
                $news->notification_type = $request->input('notification_type');
            }
            $news->is_breaking = $request->input('is_breaking') !== null? true: false;
            $news->is_important = $request->input('is_important') !== null? true: false;
            $news->is_featured = $request->input('is_featured') !== null? true: false;
            $news->v2_cities = $request->input('v2_cities') !== null? true: false;
            $news->is_useful = $request->input('is_useful') !== null? true: false;
            $news->is_home = $request->input('is_home') !== null? true: false;
            if($news->is_useful && $request->input('is_useful_expiry_datetime') !== null) {
                $news->is_useful_expiry_datetime = \Carbon::createFromFormat('Y-m-d', $request->input('is_useful_expiry_datetime'))->startOfDay()->format('Y-m-d H:i:s');
            }
            if($request->input('datetime_current') !== null) {
                $news->datetime = \Carbon::now();
            } else {
                $news->datetime = \Carbon::createFromFormat('Y-m-d H:i', $request->input('datetime'))->format('Y-m-d H:i:s');
            }
            $news->created_by = \Auth::id();
            $news->updated_by = \Auth::id();

            $send_notification = $request->input('send_notification') !== null? true: false;
//            if($news->is_breaking) $send_notification = true;

            if($request->input('source_name') !== null) {
                $source = ['name' => $request->input('source_name')];
                if($request->input('source_link') !== null) {
                    $source['link'] = $request->input('source_link');
                }
                $news->source = $source;
            }

            if($news->is_breaking) {
                $news->is_breaking_datetime = \Carbon::now();
            }
            if($news->is_featured) {
                $news->is_featured_datetime = \Carbon::now();
            }

            if($news->status == 'Published') {
                $news->published_datetime = \Carbon::now();
            }

            $news->save();

            $news->categories()->sync($request->input('categories'));
            
            $news->reporters()->sync([ 137 ]);
            if($request->has('cities')) {
                $news->cities()->sync($request->input('cities'));
            } else {
                $news->cities()->sync([$news->city_id]);
            }

            $videos_for_concat = [];
            $media_data_for_concat = null;

            if($request->file('media') !== null && count($request->file('media'))) {
                $all_media = $request->file('media');

                $media_data = $this->generateMediaData($request);

                foreach ($all_media as $media_index => $media) {
                    $news_media = $this->storeNewsOriginalMedia($news, $media, $media_index, $media_data);
                    if($news_media !== null) {
                        $this->mediaUpload($news, $news_media, $media, $media_data);
                    } else {
                        if($media_data_for_concat === null) {
                            $media_original_name = $media->getClientOriginalName();
                            $media_extension = $media->getClientOriginalExtension();

                            $media_unique_id = preg_replace('/[\W_]+/u', '', $media_original_name);
                            $media_unique_id = substr($media_unique_id, 0, 10);
                            $media_unique_id = $media->getSize() . '' . $media_unique_id;

                            $media_data_for_concat = [];
                            $media_data_for_concat['sort'] = $media_data[$media_unique_id]['sort'];
                            $media_data_for_concat['is_sensitive'] = $media_data[$media_unique_id]['is_sensitive'];
                            $media_data_for_concat['is_file_photo'] = $media_data[$media_unique_id]['is_file_photo'];
                            $media_data_for_concat['courtesy'] = $media_data[$media_unique_id]['courtesy'];
                            if(isset($media_data[$media_unique_id]['watermark_position']) && $media_data[$media_unique_id]['watermark_position'] !== null&& $media_data[$media_unique_id]['watermark_position'] != 'none') {
                                $media_data_for_concat['watermark_position'] = $media_data[$media_unique_id]['watermark_position'];
                            }
                        }

                        array_push($videos_for_concat, $media);
                    }
                }

                if(count($videos_for_concat)) {
                    $public_storage_path = 'app/public/';
                    $path = 'news/' . $news->id . '/media/concat_original/';
                    $app_path = storage_path($public_storage_path . $path);

                    if (!file_exists($app_path)) {
                        \File::makeDirectory($app_path, 0777, true);
                    }

                    $raw_videos_for_concat_paths = [];

                    foreach ($videos_for_concat as $video_for_concat) {
                        $media_unique_id = preg_replace('/[\W_]+/u', '', $video_for_concat->getClientOriginalName());
                        $media_unique_id = substr($media_unique_id, 0, 10);
                        $media_unique_id = $video_for_concat->getSize() . '' . $media_unique_id;

                        $video_for_concat_filename = $video_for_concat->getClientOriginalName();
                        $video_for_concat->move($app_path, $video_for_concat_filename);

                        $raw_videos_for_concat_paths[$media_data[$media_unique_id]['sort']] = $video_for_concat_filename;
                    }

                    $concat_videos_data = [
                        'news' => $news,
                        'videos_for_concat' => $raw_videos_for_concat_paths,
                        'media_data' => $media_data_for_concat
                    ];

                    $this->concatVideos($concat_videos_data);

                    News::where('id', $news->id)->update([ 'status' => 'Pending' ]);
                }
            }
            DB::commit();

            $toast_message = 'News Created successfully';

            if($news->status == 'Published' && $send_notification) {
                $data = [
                    'id' => $news->id,
                    'title' => $news->headline,
                    'content' => $news->article_text !== null? $news->article_text: $news->article,
                    'priority' => $request->input('high_priority_notification') !== null? 10: 1,
                    'additional_data' => [
                        'is_breaking' => $news->is_breaking,
                        'allowed_cities' => [],
                        'type' => 'news',
                        'id' => $news->id,
                        'notification_type' => 0
                    ]
                ];

                if($request->input('notification_type') !== null) {
                    $notification_types = \Config::get('constants.notification_types');
                    if(isset($notification_types[$request->input('notification_type')])) {
                        $data['additional_data']['notification_type'] = $notification_types[$request->input('notification_type')];
                    }
                }

                $news->load('thumbs');

                $thumb = $news->thumbs->first();
                if($thumb) {
                    $data['big_picture'] = $thumb->full_path;
                }

                $notification_datetime = \Carbon::now();

                if($request->input('send_after') !== null) {
                    $data['send_after'] = \Carbon::createFromFormat('Y-m-d H:i', $request->input('send_after'))->format('Y-m-d H:i:s');
                    $notification_datetime = $data['send_after'];
                    $news->is_notification_scheduled = 1;
                }

                if(config('app.env') === 'production') {
                    $included_segments = env('INCLUDED_SEGMENTS', 'Android Test');
                    $included_segments = [$included_segments];
                    $this->onesignalNotification($data, $included_segments);
                }

                $news->notification_status = 'Sent';
                $news->notification_datetime = $notification_datetime;
                $news->update();

                $toast_message = 'News Created successfully with Notification';
            }

            if($news->status == 'Published') {
                event(new NewsPublished([ 'id' => $news->id ]));
            }
            if($news->is_featured) {
                event(new NewsFeatured([ 'id' => $news->id ]));
            }

            $news_for_elastic_search = $this->findNewsForElasticSearch($news->id);

            if(env('ELASTICSEARCH')) {
                \Elasticsearch::index([
                    'id' => $news->id,
                    'index' => 'news',
                    'body' => $news_for_elastic_search,
                ]);
            }

            $fcm_ids = UserFcmToken::where('id', '!=', \Auth::id())->get()->pluck('token')->toArray();

            $data = [
                'title' => $news->headline,
                'content' => 'News status changed to ' . $news->status . ' by '. \Auth::user()->name,
                'url' => route('news.edit', [ 'id' => $news->id ]),
                'ids' => $fcm_ids
            ];

            $this->webPushlNotification($data);

            \Toastr::success($toast_message, 'Success', []);
        } catch (\Exception $e) {
            DB::rollback();

            \Toastr::error('Error occured during creating news', 'Error', []);
        }

        return redirect()->route('home');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $news = News::with('category', 'city', 'reporters', 'thumbs.parent', 'downloads', 'created_by_user', 'updated_by_user')
            ->leftJoin(\DB::raw('(SELECT news_id, IFNULL(count(id), 0) AS reactions_claps_count FROM news_reactions where type ="Claps" GROUP BY news_id) as nrc'), 'nrc.news_id', '=', 'news.id')
            ->leftJoin(\DB::raw('(SELECT news_id, IFNULL(count(id), 0) AS reactions_happy_count FROM news_reactions where type ="Happy" GROUP BY news_id) as nrh'), 'nrh.news_id', '=', 'news.id')
            ->leftJoin(\DB::raw('(SELECT news_id, IFNULL(count(id), 0) AS reactions_sad_count FROM news_reactions where type ="Sad" GROUP BY news_id) as nrs'), 'nrs.news_id', '=', 'news.id')
            ->leftJoin(\DB::raw('(SELECT news_id, IFNULL(count(id), 0) AS views FROM news_views GROUP BY news_id) as nv'), 'nv.news_id', '=', 'news.id')
            ->leftJoin(\DB::raw('(SELECT news_id, IFNULL(count(id), 0) AS shares_whatsapp_count FROM news_shares where type ="Whatsapp" GROUP BY news_id) as ns'), 'ns.news_id', '=', 'news.id')
            ->addSelect('news.*')
            ->addSelect('nrc.reactions_claps_count as reactions_claps_count')
            ->addSelect('nrh.reactions_happy_count as reactions_happy_count')
            ->addSelect('nrs.reactions_sad_count as reactions_sad_count')
            ->addSelect('nv.views as views')
            ->addSelect('ns.shares_whatsapp_count as shares_whatsapp_count')->find($id);

        if($news) {
            return view('pages.news.show', compact('news'));
        }

        abort(404);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $news = News::with('categories', 'language', 'city', 'reporters', 'attachments')->find($id);

        if($news) {
            if(!auth()->user()->can('edit all news') && (auth()->user()->can('edit news') && auth()->id() != $news->created_by)) {
                abort(404);
            }

            $reporters = User::role('Reporter')->where('status', 'Active')->get();
            
            $cities = City::all();
            $publish_to_cities = District::with(['cities' => function($q) {
                $q->where('status', 1);
            }])->where('status', 1)->get();
            $districts = District::with('cities')->where('status', 1)->get();
            $city_groups = CityGroup::with('cities')->where('status', 1)->get();

            $categories_not_for_selection = \Config::get('constants.categories');
            $categories_not_for_selection = $categories_not_for_selection[env('APP_ENV')];
            $categories_not_for_selection = array_values($categories_not_for_selection);
            
            $categories = Category::where('status', 1)->whereNotIn('id', $categories_not_for_selection)->orderBy('status', 'desc')->orderBy('sort', 'asc')->get();
            $languages = Language::all();
            $rejection_reasons = NewsRejectionReason::all();
            
            $statuses = ['Draft', 'Pending', 'Published', 'Unpublished', 'Rejected'];
            $notification_types = \Config::get('constants.notification_types');

            return view('pages.news.create', compact('reporters', 'statuses', 'cities', 'publish_to_cities', 'districts', 'city_groups', 'categories', 'news', 'languages', 'notification_types', 'rejection_reasons'));
        }

        abort(404);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        ini_set('memory_limit', '-1');

        $media_data = $this->generateMediaData($request);

        $news = News::find($id);

        if($news) {
            $validator = Validator::make($request->all(), [
                'headline' => ['required', 'string', 'max:100'],
                'article' => ['nullable', 'string'],
            ]);

            if ($validator->fails()) {
                return redirect()
                    ->route('news.create')
                    ->withErrors($validator)
                    ->withInput();
            }

            try {
                DB::beginTransaction();

                $old_is_breaking = $news->is_breaking;
                $old_is_featured = $news->is_featured;

                $news->headline = $request->input('headline');
                $news->article = $request->input('article');
                if($request->input('article_text') !== null) {
                    $news->article_text = $request->input('article_text');
                } else {
                    $news->article_text = $this->convertHTMLtoTextHelper($request->input('article'));
                }
                $news->city_id = $request->input('city');
                $news->language_id = 2;

                if($news->status !== $request->input('status') && $request->input('status') == 'Published') {
                    $news->published_datetime = \Carbon::now();
                }

                if($request->input('notification_type') !== null && $news->notification_type !== $request->input('notification_type')) {
                    $news->notification_type = $request->input('notification_type');
                }

                $news->status = $request->input('status');
                $news->is_breaking = $request->input('is_breaking') !== null? true: false;
                $news->is_important = $request->input('is_important') !== null? true: false;
                $news->is_featured = $request->input('is_featured') !== null? true: false;
                $news->v2_cities = $request->input('v2_cities') !== null? true: false;
                $news->is_useful = $request->input('is_useful') !== null? true: false;
                $news->is_home = $request->input('is_home') !== null? true: false;
                if($news->is_useful && $request->input('is_useful_expiry_datetime') !== null) {
                    $news->is_useful_expiry_datetime = \Carbon::createFromFormat('Y-m-d', $request->input('is_useful_expiry_datetime'))->startOfDay()->format('Y-m-d H:i:s');
                } else {
                    $news->is_useful_expiry_datetime = NULL;
                }
                if($request->input('datetime_current') !== null) {
                    $news->datetime = \Carbon::now();
                } else {
                    $news->datetime = \Carbon::createFromFormat('Y-m-d H:i', $request->input('datetime'))->format('Y-m-d H:i:s');
                }
                $news->updated_by = \Auth::id();

                $send_notification = $request->input('send_notification') !== null? true: false;

                if($old_is_breaking == 0 && $news->is_breaking) {
                    $news->is_breaking_datetime = \Carbon::now();
                }
                if($old_is_featured == 0 && $news->is_featured) {
                    $news->is_featured_datetime = \Carbon::now();
                }

                $source = [];

                if($request->input('source_name') !== null || $news->source !== null) {
                    $source['name'] = $request->input('source_name');
                    if($request->input('source_link') !== null || isset($news->source->link)) {
                        $source['link'] = $request->input('source_link');
                    }
                    if($source['name'] == null) $source = '';
                    $news->source = $source;
                }

                if($news->status == 'Rejected' && $request->input('rejection_reasons') !== null && count($request->input('rejection_reasons'))) {
                    $news->rejection_reasons = implode(', ', $request->input('rejection_reasons'));
                } else {
                    $news->rejection_reasons = null;
                }

                $news->update();
                if($source === '' || $news->source === '') {
                    \DB::statement('UPDATE news SET source = null where id = ' . $news->id);
                }

                $news->categories()->sync($request->input('categories'));

                $news->reporters()->sync([ 137 ]);
                if($request->has('cities')) {
                    $news->cities()->sync($request->input('cities'));
                } else {
                    $news->cities()->sync([$news->city_id]);
                }

                if($request->file('media') !== null && count($request->file('media'))) {
                    $all_media = $request->file('media');

                    foreach ($all_media as $media_index => $media) {
                        $news_media = $this->storeNewsOriginalMedia($news, $media, $media_index, $media_data);
                        $this->mediaUpload($news, $news_media, $media, $media_data);
                    }
                }

                foreach ($media_data as $media_data_key => $media_data_value) {
                    if(strpos($media_data_key, 'existing__') === 0) {
                        $media_id = (int) str_replace('existing__', '', $media_data_key);
                        $this->updateExistingMedia($news->id, $media_id, $media_data_value);
                    }
                }

                DB::commit();

                $toast_message = 'News Updated successfully';

                if($news->notification_status == 'Pending' && $news->status == 'Published' && $send_notification) {
                    $data = [
                        'id' => $news->id,
                        'title' => $news->headline,
                        'content' => $news->article_text !== null? $news->article_text: $news->article,
                        'priority' => $request->input('high_priority_notification') !== null? 10: 1,
                        'additional_data' => [
                            'is_breaking' => $news->is_breaking,
                            'allowed_cities' => [],
                            'type' => 'news',
                            'id' => $news->id,
                            'notification_type' => 0
                        ]
                    ];

                    if($request->input('notification_type') !== null) {
                        $notification_types = \Config::get('constants.notification_types');
                        if(isset($notification_types[$request->input('notification_type')])) {
                            $data['additional_data']['notification_type'] = $notification_types[$request->input('notification_type')];
                        }
                    }

                    $news->load('thumbs');

                    $thumb = $news->thumbs->first();
                    if($thumb) {
                        $data['big_picture'] = $thumb->full_path;
                    }

                    $notification_datetime = \Carbon::now();

                    if($request->input('send_after') !== null) {
                        $data['send_after'] = \Carbon::createFromFormat('Y-m-d H:i', $request->input('send_after'))->format('Y-m-d H:i:s');
                        $notification_datetime = $data['send_after'];
                        $news->is_notification_scheduled = 1;
                    }

                    if(config('app.env') === 'production') {
                        $included_segments = env('INCLUDED_SEGMENTS', 'Android Test');
                        $included_segments = [$included_segments];
                        $this->onesignalNotification($data, $included_segments);
                    }

                    $news->notification_status = 'Sent';
                    $news->notification_datetime = $notification_datetime;
                    $news->update();

                    $toast_message = 'News Updated successfully with Notification';
                }

                if($news->is_featured !== $request->input('is_featured') && $news->is_featured) {
                    event(new NewsFeatured([ 'id' => $news->id ]));
                }

                $news->load('poll');

                $news_for_elastic_search = $this->findNewsForElasticSearch($news->id);

                if(env('ELASTICSEARCH')) {
                    \Elasticsearch::update([
                        'id' => $news->id,
                        'index' => 'news',
                        'body' => [
                            'doc' => $news_for_elastic_search,
                            'doc_as_upsert' => true
                        ]
                    ]);
                }

                $fcm_ids = UserFcmToken::where('id', '!=', \Auth::id())->get()->pluck('token')->toArray();

                $data = [
                    'title' => $news->headline,
                    'content' => 'News status changed to ' . $news->status . ' by '. \Auth::user()->name,
                    'url' => route('news.edit', [ 'id' => $news->id ]),
                    'ids' => $fcm_ids
                ];

                $this->webPushlNotification($data);

                \Toastr::success($toast_message, 'Success', []);
            } catch (\Exception $e) {
                DB::rollback();

                \Toastr::error('Error occured during updating news', 'Error', []);
            }

            return redirect()->route('home');
        }

        abort(404);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $news = News::find($id);

        if($news) {
            $news->delete();

            \Elasticsearch::delete([
                'id' => $news->id,
                'index' => 'news'
            ]);


            \Toastr::success('News Deleted successfully', 'Success', []);

            return redirect()->back();
        }

        abort(404);
    }
}
