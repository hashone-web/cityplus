<?php

namespace App\Http\Controllers\News;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use App\NewsRejectionReason;

class NewsRejectionReasonController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $rejection_reasons = NewsRejectionReason::sortable()->paginate(\Config::get('constants.pagination_size'));

        return view('pages.rejection_reasons.index', compact('rejection_reasons'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.rejection_reasons.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'title' => ['required', 'string', 'max:255', 'unique:news_rejection_reasons'],
        ]);

        if ($validator->fails()) {
            return redirect()
                ->route('rejection_reasons.create')
                ->withErrors($validator)
                ->withInput();
        }

        try {
            DB::beginTransaction();

            $rejection_reason = new NewsRejectionReason();
            $rejection_reason->title = $request->input('title');
            $rejection_reason->save();

            DB::commit();

            \Toastr::success('News Rejection Reason Created successfully', 'Success', []);
        } catch (\Exception $e) {
            DB::rollback();
            \Toastr::error('Error occured during creating News Rejection Reason', 'Error', []);
        }

        return redirect()->route('rejection_reasons.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $rejection_reason = NewsRejectionReason::find($id);

        if($rejection_reason) {
            return view('pages.rejection_reasons.create', compact('rejection_reason'));
        }

        abort(404);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rejection_reason = NewsRejectionReason::find($id);

        if($rejection_reason) {
            $validator = Validator::make($request->all(), [
                'title' => ['required', 'string', 'max:255', 'unique:news_rejection_reasons,title,' . $id],
            ]);

            if ($validator->fails()) {
                return redirect()
                    ->route('rejection_reasons.edit', ['id' => $id])
                    ->withErrors($validator)
                    ->withInput();
            }

            try {
                DB::beginTransaction();

                $rejection_reason->title = $request->input('title');
                $rejection_reason->update();

                DB::commit();

                \Toastr::success('News Rejection Reason Updated successfully', 'Success', []);
            } catch (\Exception $e) {
                DB::rollback();
                \Toastr::error('Error occured during updating News Rejection Reason', 'Error', []);
            }

            return redirect()->route('rejection_reasons.index');
        }

        abort(404);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $rejection_reason = NewsRejectionReason::find($id);

        if($rejection_reason) {
            $rejection_reason->delete();

            \Toastr::success('News Rejection Reason Deleted successfully', 'Success', []);

            return redirect()->route('rejection_reasons.index');
        }

        abort(404);
    }
}
