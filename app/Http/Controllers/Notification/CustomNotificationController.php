<?php

namespace App\Http\Controllers\Notification;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CustomNotificationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('pages.custom_notifications.create');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $all_data = $request->all();
        $json_array = [];

        foreach ($all_data as $key => $value) {
            if(strpos($key, 'data_') !== false) {
                $pure_key = str_replace('data_', '', $key);
                $pure_keys = explode('___', $pure_key);
                $json_array[$pure_keys[1]][$pure_keys[0]] = $value;
            }
        }

        $additional_data = [];

        foreach ($json_array as $key => $value) {
            $additional_data[$value['key']] = $value['value'];
        }

        $data = [
            'title' => $request->input('title'),
            'content' => $request->input('content'),
            'priority' => $request->input('priority'),
            'additional_data' => $additional_data
        ];

        $notification_datetime = \Carbon::now();

        if($request->input('send_after') !== null) {
            $data['send_after'] = \Carbon::createFromFormat('Y-m-d H:i', $request->input('send_after'))->format('Y-m-d H:i:s');
            $notification_datetime = $data['send_after'];
            $news->is_notification_scheduled = 1;
        }

        if(config('app.env') === 'production') {
            $included_segments = ['Android Test'];
            $eata = $this->onesignalNotification($data, $included_segments);

            dd($eata);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
