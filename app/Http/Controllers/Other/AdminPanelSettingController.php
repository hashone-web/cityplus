<?php

namespace App\Http\Controllers\Other;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use App\AdminPanelSetting;

class AdminPanelSettingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {
        $admin_panel_settings = AdminPanelSetting::first();

        return view('pages.admin_panel_settings.edit', compact('admin_panel_settings'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        try {
            DB::beginTransaction();

            $admin_panel_settings = AdminPanelSetting::firstOrNew(['id' => 1]);
            // IsFeatured by Shares
            $admin_panel_settings->is_featured_shares_scan_period_1 = $request->input('is_featured_shares_scan_period_1');
            $admin_panel_settings->is_featured_shares_count_1 = $request->input('is_featured_shares_count_1');
            $admin_panel_settings->is_featured_shares_scan_period_2 = $request->input('is_featured_shares_scan_period_2');
            $admin_panel_settings->is_featured_shares_count_2 = $request->input('is_featured_shares_count_2');
            $admin_panel_settings->is_featured_shares_scan_period_3 = $request->input('is_featured_shares_scan_period_3');
            $admin_panel_settings->is_featured_shares_count_3 = $request->input('is_featured_shares_count_3');
            
            // IsFeatured by Reactions
            $admin_panel_settings->is_featured_reactions_scan_period_1 = $request->input('is_featured_reactions_scan_period_1');
            $admin_panel_settings->is_featured_reactions_count_1 = $request->input('is_featured_reactions_count_1');
            $admin_panel_settings->is_featured_reactions_scan_period_2 = $request->input('is_featured_reactions_scan_period_2');
            $admin_panel_settings->is_featured_reactions_count_2 = $request->input('is_featured_reactions_count_2');
            $admin_panel_settings->is_featured_reactions_scan_period_3 = $request->input('is_featured_reactions_scan_period_3');
            $admin_panel_settings->is_featured_reactions_count_3 = $request->input('is_featured_reactions_count_3');

            $admin_panel_settings->save();

            DB::commit();

            \Toastr::success('Settings Updated successfully', 'Success', []);
        } catch (\Exception $e) {
            DB::rollback();

            dd($e);

            \Toastr::error('Error occured during updating settings', 'Error', []);
        }

        return redirect()->route('admin.panel.settings.edit');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
