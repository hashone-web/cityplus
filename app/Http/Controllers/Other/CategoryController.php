<?php

namespace App\Http\Controllers\Other;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

use App\Category;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Category::sortable()->orderBy('status', 'desc')->orderBy('sort', 'asc')->paginate(\Config::get('constants.pagination_size'));

        return view('pages.categories.index', compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
         return view('pages.categories.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => ['required', 'string', 'max:255', 'unique:categories'],
        ]);

        if ($validator->fails()) {
            return redirect()
                ->route('categories.create')
                ->withErrors($validator)
                ->withInput();
        }

        try {
            DB::beginTransaction();

            $category = new Category();
            $category->name = $request->input('name');
            $category->gujarati_name = $request->input('gujarati_name');
            $category->save();

            if($request->has('image')) {
                $image = $request->file('image');
                $category->image = $this->categoryImageUpload($category, $image);
                $category->update();
            }

            DB::commit();

            \Toastr::success('Category Created successfully', 'Success', []);
        } catch (\Exception $e) {
            DB::rollback();

            \Toastr::error('Error occured during creating category', 'Error', []);
        }

        return redirect()->route('categories.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $category = Category::find($id);

        if($category) {
            return view('pages.categories.create', compact('category'));
        }

        abort(404);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $category = Category::find($id);

        if($category) {
            $validator = Validator::make($request->all(), [
                'name' => ['required', 'string', 'max:255', 'unique:categories,name,' . $id],
            ]);

            if ($validator->fails()) {
                return redirect()
                    ->route('categories.edit', ['id' => $id])
                    ->withErrors($validator)
                    ->withInput();
            }

            try {
                DB::beginTransaction();

                $category->name = $request->input('name');
                $category->gujarati_name = $request->input('gujarati_name');
                if($request->has('image')) {
                    $image = $request->file('image');
                    $category->image = $this->categoryImageUpload($category, $image);
                }
                $category->update();

                DB::commit();

                \Toastr::success('Category Updated successfully', 'Success', []);
            } catch (\Exception $e) {
                DB::rollback();

                \Toastr::error('Error occured during updating category', 'Error', []);
            }

            return redirect()->route('categories.index');
        }

        abort(404);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $category = Category::find($id);

        if($category) {
            $category->delete();

            \Toastr::success('Category Deleted successfully', 'Success', []);

            return redirect()->route('categories.index');
        }

        abort(404);
    }

    public function toggleStatus(Request $request, $id)
    {
        $category = Category::find($id);

        if($category) {
            $category->status = $request->input('status') == "true"? 1: 0;
            $category->update();

            return [
                'status' => true,
                'data' => $category
            ];
        }

        abort(404);
    }

    /**
     * Sort a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function sorting()
    {
        $categories = Category::orderBy('status', 'desc')->orderBy('sort', 'asc')->get();

        return view('pages.categories.sorting', compact('categories'));
    }

    public function sortingUpdate(Request $request)
    {
        $categories_id = json_decode($request->input('sort'), true);

        foreach ($categories_id as $key => $category_id) {
            Category::where('id', $category_id)->update(['sort' => $key]);
        }

        return [
            'status' => true
        ];
    }

    public function categoryImageUpload($category, $image)
    {
        $image_extension = $image->getClientOriginalExtension();
        $image_filename = Str::random(10) . '.' . $image_extension;

        $public_storage_path = 'app/public/';
        $path = 'categories/' . $category->id . '/image/';
        $app_path = storage_path($public_storage_path . $path);

        if (!file_exists($app_path)) {
            \File::makeDirectory($app_path, 0777, true);
        } else {
            \File::deleteDirectory($app_path, true);
        }

        $image->move($app_path, $image_filename);

        $sizes = ['128px', '256px', '515px', '1080px'];

        foreach ($sizes as $size) {
            $height = str_replace('px', '', $size);

            if (!file_exists($app_path . '/' . $size)) {
                \File::makeDirectory($app_path . '/' . $size, 0777, true);
            }

            $resized_media = \Image::make($app_path . '/' . $image_filename)
                ->resize(null, $height, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($app_path . '/' . $size . '/' . $image_filename, 90);
        }

        return $image_filename;
    }
}
