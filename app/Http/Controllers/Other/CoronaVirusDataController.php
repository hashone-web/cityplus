<?php

namespace App\Http\Controllers\Other;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\CoronaVirusData;

class CoronaVirusDataController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $corona_virus_data = CoronaVirusData::orderByDesc('total')->get();
        $gujarat_corona_virus_data = CoronaVirusData::where('name', 'gujarat')->first();

        return view('pages.corona_virus_data.edit', compact('corona_virus_data', 'gujarat_corona_virus_data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $corona_virus_data = CoronaVirusData::find($id);
        $original_corona_virus_data = json_decode(json_encode($corona_virus_data));

        $total = $request->input('total');
        $active = $request->input('active');
        $recovered = $request->input('recovered');
        $delta_recovered = $request->input('delta_recovered');
        $deaths = $request->input('deaths');
        $delta_deaths = $request->input('delta_deaths');
        $delta_total = $request->input('delta_total');

        $now = date('Y-m-d');
        
        if($corona_virus_data) {
            if($request->input('total') != $original_corona_virus_data->total) {
                $total_diff = $total - $corona_virus_data->total;
                $corona_virus_data->total = $total;
                if($request->input('delta_total') == $original_corona_virus_data->delta_total) {
                    $corona_virus_data->delta_total = $delta_total + $total_diff;
                }
            }
            if($request->input('active') != $original_corona_virus_data->active) {
                $corona_virus_data->active = $active;
            }
            if($request->input('recovered') != $original_corona_virus_data->recovered) {
                $total_diff = $recovered - $corona_virus_data->recovered;
                $corona_virus_data->recovered = $recovered;
                if($original_corona_virus_data->delta_recovered_updated_at == $now) {
                    $corona_virus_data->delta_recovered = $corona_virus_data->delta_recovered + $total_diff;
                } else {
                    $corona_virus_data->delta_recovered = $total_diff;
                }
                $corona_virus_data->delta_recovered_updated_at = $now;
            }
            if($request->input('deaths') != $original_corona_virus_data->deaths) {
                $total_diff = $deaths - $corona_virus_data->deaths;
                $corona_virus_data->deaths = $deaths;
                if($original_corona_virus_data->delta_deaths_updated_at == $now) {
                    $corona_virus_data->delta_deaths = $corona_virus_data->delta_deaths + $total_diff;
                } else {
                    $corona_virus_data->delta_deaths = $total_diff;
                }
                $corona_virus_data->delta_deaths_updated_at = $now;
            }
            if($request->input('delta_total') != $original_corona_virus_data->delta_total) {
                $total_diff = $delta_total - $corona_virus_data->delta_total;
                $corona_virus_data->delta_total = $delta_total;
                if($request->input('total') == $original_corona_virus_data->total) {
                    $corona_virus_data->total = $total + $total_diff;
                }
            }
            if($request->input('delta_recovered') != $original_corona_virus_data->delta_recovered) {
                $total_diff = $delta_recovered - $corona_virus_data->delta_recovered;
                $corona_virus_data->delta_recovered = $delta_recovered;
                if($request->input('recovered') == $original_corona_virus_data->recovered) {
                    $corona_virus_data->recovered = $recovered + $total_diff;
                }
            }
            if($request->input('delta_deaths') != $original_corona_virus_data->delta_deaths) {
                $total_diff = $delta_deaths - $corona_virus_data->delta_deaths;
                $corona_virus_data->delta_deaths = $delta_deaths;
                if($request->input('deaths') == $original_corona_virus_data->deaths) {
                    $corona_virus_data->deaths = $deaths + $total_diff;
                }
            }
            $corona_virus_data->active = ($corona_virus_data->total - ($corona_virus_data->recovered + $corona_virus_data->deaths));
            $corona_virus_data->manual = $request->input('manual') !== null? 1: 0;
            $corona_virus_data->update();

            if($corona_virus_data->parent_id != null) {
                $parent_corona_virus_data = CoronaVirusData::find($corona_virus_data->parent_id);
                
                $diff_total = $corona_virus_data->total - $original_corona_virus_data->total;
                $diff_active = $corona_virus_data->active - $original_corona_virus_data->active;
                $diff_recovered = $corona_virus_data->recovered - $original_corona_virus_data->recovered;
                $diff_deaths = $corona_virus_data->deaths - $original_corona_virus_data->deaths;
                $diff_delta_total = $corona_virus_data->delta_total - $original_corona_virus_data->delta_total;
                $diff_delta_recovered = $corona_virus_data->delta_recovered - $original_corona_virus_data->delta_recovered;
                $diff_delta_deaths = $corona_virus_data->delta_deaths - $original_corona_virus_data->delta_deaths;

                $parent_corona_virus_data->total = $parent_corona_virus_data->total + $diff_total;
                $parent_corona_virus_data->active = $parent_corona_virus_data->active + $diff_active;
                $parent_corona_virus_data->recovered = $parent_corona_virus_data->recovered + $diff_recovered;
                $parent_corona_virus_data->deaths = $parent_corona_virus_data->deaths + $diff_deaths;
                $parent_corona_virus_data->delta_total = $parent_corona_virus_data->delta_total + $diff_delta_total;
                $parent_corona_virus_data->delta_recovered = $parent_corona_virus_data->delta_recovered + $diff_delta_recovered;
                $parent_corona_virus_data->delta_deaths = $parent_corona_virus_data->delta_deaths + $diff_delta_deaths;

                
                if($request->input('recovered') != $original_corona_virus_data->recovered) {
                    $diff_delta_recovered = $corona_virus_data->delta_recovered - $original_corona_virus_data->delta_recovered;
                    
                    if($parent_corona_virus_data->delta_recovered_updated_at == $now) {
                        $parent_corona_virus_data->delta_recovered = $parent_corona_virus_data->delta_recovered + $diff_delta_recovered;
                    } else {
                        $parent_corona_virus_data->delta_recovered = $corona_virus_data->delta_recovered;
                    }
                    
                    $parent_corona_virus_data->delta_recovered_updated_at = $now;
                }

                
                if($request->input('deaths') != $original_corona_virus_data->deaths) {
                    $diff_delta_deaths = $corona_virus_data->delta_deaths - $original_corona_virus_data->delta_deaths;

                    if($parent_corona_virus_data->delta_deaths_updated_at == $now) {
                        $parent_corona_virus_data->delta_deaths = $parent_corona_virus_data->delta_deaths + $diff_delta_deaths;
                    } else {
                        $parent_corona_virus_data->delta_deaths = $corona_virus_data->delta_deaths;
                    }
                    
                    $parent_corona_virus_data->delta_deaths_updated_at = $now;
                }
                
                $parent_corona_virus_data->update();
            }

            \Toastr::success(ucfirst($corona_virus_data->name) . ' Corona Virus Data Updated successfully', 'Success', []);
        
            return redirect()->route('corona.virus.data.index');
        }

        abort(404);
    }

    public function lastUpdatedAt()
    {
        $now = \Carbon::now();

        CoronaVirusData::where('name', 'gujarat')->update(['last_updated_at' => $now]);

        \Toastr::success('Corona Virus Data Card Date changed successfully', 'Success', []);
        
        return redirect()->route('corona.virus.data.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
