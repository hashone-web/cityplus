<?php

namespace App\Http\Controllers\Other;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Device;
use App\DeviceLocation;

class DeviceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $query_parameters = $request->query();

        $devices = Device::with('cities', 'user.user');

        if($request->query('is_login') !== null) {
            $devices = $devices->has('user');
        }

        if($request->query('is_location') !== null) {
            $devices = $devices->has('locations');
        }

        if($request->query('platform') !== null) {
            $devices = $devices->where('platform', $request->query('platform'));
        }

        if($request->query('notification_level') !== null) {
            $devices = $devices->where('notification_level', $request->query('notification_level'));
        }

        if($request->query('search_type') !== null && ($request->query('search_type') == 'mobile_number' || $request->query('search_type') == 'name') && $request->query('search_value') !== null) {
            $devices = $devices->has('user');
            $devices = $devices->whereHas('user.user', function ($query) use($request) {
                $query->where($request->query('search_type'), 'LIKE', '%' . $request->query('search_value') . '%');
            });
        }

        $devices = $devices->withCount(['shares', 'bookmarks', 'reactions', 'reports', 'locations'])->sortable(['created_at' => 'desc'])->paginate(\Config::get('constants.pagination_size'));

        return view('pages.devices.index', compact('devices', 'query_parameters'));
    }

    public function locations(Request $request, $id)
    {
        $device = Device::find($id);

        if($device) {
            $device_locations = DeviceLocation::where('device_id', $device->id)->sortable(['created_at' => 'desc'])->paginate(\Config::get('constants.pagination_size'));

            return view('pages.devices.locations', compact('device', 'device_locations'));
        }

        abort(404);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
