<?php

namespace App\Http\Controllers\Other;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\City;
use App\MarketValue;
use App\News;

class PublicController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $query_parameters = $request->query();

    	$cities = City::with('district')->withCount('devices as devices_count')->has('district')->sortable()
            ->leftJoin(\DB::raw('(SELECT city_id, name FROM city_names where language_id = 2 GROUP BY city_id, name) as cn'), 'cn.city_id', '=', 'cities.id')
            ->addSelect('cities.*')
            ->addSelect('cn.name as gujarati_name')
            ->where('status', 1)
            ->get();

        $date = \Carbon::now()->format('Y-m-d');

        $market_values_raw = MarketValue::where('current_date', $date)->get();

        $market_values = [
            'gold' => [
                'value' => null
            ],
            'silver' => [
                'value' => null
            ],
            'sensex' => [
                'value' => null
            ],
        ];

        foreach ($market_values_raw as $market_value) {
            $value = [
                'value' => (int) $market_value->current
            ];

            if($market_value->type == 'gold' || $market_value->type == 'silver' || $market_value->type == 'sensex') {
                $value['is_increased'] = $market_value->is_increased;
            }

            $market_values[$market_value->type] = $value;
        }

        $news = News::with(['categories', 'language', 'media', 'thumbs', 'downloads', 'city' => function($q) {
                $q->leftJoin(\DB::raw('(SELECT city_id, name FROM city_names where language_id = 2 GROUP BY city_id, name) as cn'), 'cn.city_id', '=', 'cities.id');
            }])->has('city')
            ->addSelect('news.*');

        $news = $news->orderByDesc('news.datetime');

        if($request->query('city') !== null) {
            $user_cities = [ $request->query('city') ];
        } else {
            $user_cities = [1];
        }

        $news = $news->join('news_cities', function ($j) use($user_cities)
            {
                $j->on('news.id', '=', 'news_cities.news_id');
                $j->whereIn('news_cities.city_id', $user_cities);
            })->distinct()->addSelect('news.*');

        $news = $news->where('news.datetime', '<', \Carbon::now());
        $news = $news->where('news.status', 'Published');

        $news = $news->paginate(\Config::get('constants.pagination_size'));    

        return view('public.index', compact('cities', 'market_values', 'news', 'query_parameters'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function single(Request $request, $id)
    {
        $query_parameters = $request->query();

        $cities = City::with('district')->withCount('devices as devices_count')->has('district')->sortable()
            ->leftJoin(\DB::raw('(SELECT city_id, name FROM city_names where language_id = 2 GROUP BY city_id, name) as cn'), 'cn.city_id', '=', 'cities.id')
            ->addSelect('cities.*')
            ->addSelect('cn.name as gujarati_name')
            ->where('status', 1)
            ->get();

        $date = \Carbon::now()->format('Y-m-d');

        $market_values_raw = MarketValue::where('current_date', $date)->get();

        $market_values = [
            'gold' => [
                'value' => null
            ],
            'silver' => [
                'value' => null
            ],
            'sensex' => [
                'value' => null
            ],
        ];

        foreach ($market_values_raw as $market_value) {
            $value = [
                'value' => (int) $market_value->current
            ];

            if($market_value->type == 'gold' || $market_value->type == 'silver' || $market_value->type == 'sensex') {
                $value['is_increased'] = $market_value->is_increased;
            }

            $market_values[$market_value->type] = $value;
        }

        $news = News::with(['categories', 'language', 'media', 'thumbs', 'downloads', 'city' => function($q) {
                $q->leftJoin(\DB::raw('(SELECT city_id, name FROM city_names where language_id = 2 GROUP BY city_id, name) as cn'), 'cn.city_id', '=', 'cities.id');
            }])
            ->addSelect('news.*');

        $news = $news->orderByDesc('news.datetime');

        $news = $news->find($id);    

        return view('public.single', compact('cities', 'market_values', 'news', 'query_parameters'));
    }
}
