<?php

namespace App\Http\Controllers\Other;

use App\Http\Controllers\Controller;
use App\Setting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class SettingController extends Controller
{
    public function edit() {
        $setting = Setting::first();

        return view('pages.settings.edit', compact('setting'));
    }

    public function update(Request $request) {
        $validator = Validator::make($request->all(), [
            'contact_whatsapp_number' => ['required', 'numeric'],
        ]);

        if ($validator->fails()) {
            return redirect()
                ->route('settings.edit')
                ->withErrors($validator)
                ->withInput();
        }

        try {
            DB::beginTransaction();

            $setting = Setting::firstOrNew(['id' => 1]);
            $setting->contact_whatsapp_number = $request->input('contact_whatsapp_number');
            $setting->contact_email = $request->input('contact_email');
            $setting->android_share_app_link = $request->input('android_share_app_link');
            $setting->android_share_description = $request->input('android_share_description');
            $setting->android_share_description_gujarati = $request->input('android_share_description_gujarati');
            $setting->ios_share_app_link = $request->input('ios_share_app_link');
            $setting->ios_share_description = $request->input('ios_share_description');
            $setting->ios_share_description_gujarati = $request->input('ios_share_description_gujarati');
            $setting->android_qr_code_link = $request->input('android_qr_code_link');
            $setting->ios_qr_code_link = $request->input('ios_qr_code_link');
            $setting->file_photo_description = $request->input('file_photo_description');
            $setting->file_photo_description_gujarati = $request->input('file_photo_description_gujarati');
            $setting->privacy_policy_page_link = $request->input('privacy_policy_page_link');
            $setting->terms_of_use_page_link = $request->input('terms_of_use_page_link');
            $setting->instagram_page_link = $request->input('instagram_page_link');
            $setting->reporter_join_link = $request->input('reporter_join_link');
            $setting->market_values = $request->input('market_values') == 'yes'? 1: 0;
            $setting->corona_virus_data = $request->input('corona_virus_data') == 'yes'? 1: 0;
            $setting->ads_type = $request->input('ads_type');
            $setting->bank_fd_rates_link = $request->input('bank_fd_rates_link');
            $setting->poll_policy = $request->input('poll_policy');
            $setting->corona_virus_data_message = $request->input('corona_virus_data_message');
            $setting->save();

            $public_storage_path = 'app/public/';
            $path = 'settings/';
            $app_path = storage_path($public_storage_path . $path);

            if (!file_exists($app_path)) {
                \File::makeDirectory($app_path, 0777, true);
            }

            if($request->file('news_share_bottom_image') !== null) {
                $old_news_share_bottom_image = $setting->news_share_bottom_image;

                $news_share_bottom_image = $request->file('news_share_bottom_image');
                $news_share_bottom_image_extension = $news_share_bottom_image->getClientOriginalExtension();

                $news_share_bottom_image_filename = \Str::random(16) . '.' . $news_share_bottom_image_extension;

                $news_share_bottom_image->move($app_path, $news_share_bottom_image_filename);
            
                $setting->news_share_bottom_image = $news_share_bottom_image_filename;
                $setting->update();

                \File::delete($app_path . '/' . $old_news_share_bottom_image);
            }

            if($request->file('news_share_bottom_image_gujarati') !== null) {
                $old_news_share_bottom_image_gujarati = $setting->news_share_bottom_image_gujarati;

                $news_share_bottom_image_gujarati = $request->file('news_share_bottom_image_gujarati');
                $news_share_bottom_image_gujarati_extension = $news_share_bottom_image_gujarati->getClientOriginalExtension();

                $news_share_bottom_image_gujarati_filename = \Str::random(16) . '.' . $news_share_bottom_image_gujarati_extension;

                $news_share_bottom_image_gujarati->move($app_path, $news_share_bottom_image_gujarati_filename);
            
                $setting->news_share_bottom_image_gujarati = $news_share_bottom_image_gujarati_filename;
                $setting->update();

                \File::delete($app_path . '/' . $old_news_share_bottom_image_gujarati);
            }

            if($request->file('watermark_image') !== null) {
                $old_watermark_image = $setting->watermark_image;

                $watermark_image = $request->file('watermark_image');
                $watermark_image_extension = $watermark_image->getClientOriginalExtension();

                $watermark_image_filename = \Str::random(16) . '.' . $watermark_image_extension;

                $watermark_image->move($app_path, $watermark_image_filename);
            
                $setting->watermark_image = $watermark_image_filename;
                $setting->update();

                \File::delete($app_path . '/' . $old_watermark_image);
            }

            DB::commit();

            \Toastr::success('Settings Updated successfully', 'Success', []);
        } catch (\Exception $e) {
            DB::rollback();

            \Toastr::error('Error occured during updating settings', 'Error', []);
        }

        return redirect()->route('settings.edit');
    }
}
