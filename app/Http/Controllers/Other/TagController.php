<?php

namespace App\Http\Controllers\Other;

use App\Http\Controllers\Controller;
use App\Tag;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class TagController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tags = Tag::sortable()->paginate(\Config::get('constants.pagination_size'));

        return view('pages.tags.index', compact('tags'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
         return view('pages.tags.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => ['required', 'string', 'max:255', 'unique:tags'],
        ]);

        if ($validator->fails()) {
            return redirect()
                ->route('tags.create')
                ->withErrors($validator)
                ->withInput();
        }

        try {
            DB::beginTransaction();

            $tag = new Tag();
            $tag->name = $request->input('name');
            $tag->save();

            DB::commit();

            \Toastr::success('Tag Created successfully', 'Success', []);
        } catch (\Exception $e) {
            DB::rollback();

            \Toastr::error('Error occured during creating tag', 'Error', []);
        }

        return redirect()->route('tags.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $tag = Tag::find($id);

        if($tag) {
            return view('pages.tags.create', compact('tag'));
        }

        abort(404);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $tag = Tag::find($id);

        if($tag) {
            $validator = Validator::make($request->all(), [
                'name' => ['required', 'string', 'max:255', 'unique:tags,name,' . $id],
            ]);

            if ($validator->fails()) {
                return redirect()
                    ->route('tags.edit', ['id' => $id])
                    ->withErrors($validator)
                    ->withInput();
            }

            try {
                DB::beginTransaction();

                $tag->name = $request->input('name');
                $tag->update();

                DB::commit();

                \Toastr::success('Tag Updated successfully', 'Success', []);
            } catch (\Exception $e) {
                DB::rollback();

                \Toastr::error('Error occured during updating tag', 'Error', []);
            }

            return redirect()->route('tags.index');
        }

        abort(404);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $tag = Tag::find($id);

        if($tag) {
            $tag->delete();

            \Toastr::success('Tag Deleted successfully', 'Success', []);

            return redirect()->route('tags.index');
        }

        abort(404);
    }
}
