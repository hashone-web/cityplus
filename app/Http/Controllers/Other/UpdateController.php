<?php

namespace App\Http\Controllers\Other;

use App\Http\Controllers\Controller;
use App\Update;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class UpdateController extends Controller
{
    public function edit() {
        $update = Update::first();

        return view('pages.updates.edit', compact('update'));
    }

    public function update(Request $request) {
        $validator = Validator::make($request->all(), [
            'name' => ['required', 'string'],
        ]);

        if ($validator->fails()) {
            return redirect()
                ->route('updates.edit')
                ->withErrors($validator)
                ->withInput();
        }

        try {
            DB::beginTransaction();

            $update = Update::firstOrNew(['id' => 1]);
            $update->name = $request->input('name');
            $update->app_ver = $request->input('app_ver');
            $update->link = $request->input('link');
            $update->link_text = $request->input('link_text');
            $update->force = $request->input('force') !== null? true: false;
            $update->save();

            DB::commit();

            \Toastr::success('Module Updated successfully', 'Success', []);
        } catch (\Exception $e) {
            dd($e);
            DB::rollback();

            \Toastr::error('Error occured during updating module', 'Error', []);
        }

        return redirect()->route('updates.edit');
    }
}
