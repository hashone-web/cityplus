<?php

namespace App\Http\Controllers\Plus;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

use App\PlusSetting;

class PlusSettingController extends Controller
{
    public function edit()
    {
    	$setting = PlusSetting::first();

        return view('pages.plus.settings.edit', compact('setting'));
    }

    public function update(Request $request) {
        try {
            DB::beginTransaction();

            $setting = PlusSetting::firstOrNew(['id' => 1]);
            $setting->news = $request->input('news') == 'yes'? 1: 0;
            $setting->city_selection = $request->input('city_selection') == 'yes'? 1: 0;
            $setting->horoscope = $request->input('horoscope') == 'yes'? 1: 0;
            $setting->death_notes = $request->input('death_notes') == 'yes'? 1: 0;
            $setting->bank_fd_rates = $request->input('bank_fd_rates') == 'yes'? 1: 0;
            $setting->useful_information = $request->input('useful_information') == 'yes'? 1: 0;
            $setting->saved_news = $request->input('saved_news') == 'yes'? 1: 0;
            $setting->save();

            DB::commit();

            \Toastr::success('Settings Updated successfully', 'Success', []);
        } catch (\Exception $e) {
            DB::rollback();

            \Toastr::error('Error occured during updating settings', 'Error', []);
        }

        return redirect()->route('plus.settings.edit');
    }
}
