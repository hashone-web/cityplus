<?php

namespace App\Http\Controllers\Poll;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

use App\Device;
use App\PollResultDeviceView;

class PollAPIController extends Controller
{
    public function storeResultViews(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'poll_id' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 401);
        }

        $data = ['status' => true];

        $device = Device::firstOrNew(['unique_id' => $request->input('device_id')]);
        $device->save();
        
        try {
            $poll_result_device_view = PollResultDeviceView::firstOrNew(['device_id' => $device->id, 'poll_id' => $request->input('poll_id')]);
            $poll_result_device_view->save();
        } catch (\Exception $e) {
            $data['status'] = false;
        }

        return $data;
    }
}
