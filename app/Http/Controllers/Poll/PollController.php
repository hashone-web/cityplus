<?php

namespace App\Http\Controllers\Poll;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

use Validator;

use App\Poll;
use App\Device;
use App\PollOption;
use App\DevicePollOption;
use App\News;

class PollController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $polls = Poll::withCount('result_views')->sortable(['id' => 'desc'])
                    ->paginate(\Config::get('constants.pagination_size'))
                    ->appends(request()->query());

        return view('pages.polls.index', compact('polls'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $show_result_options = ['Never', 'After Poll End', 'After User Vote', 'Always'];
        $statuses = ['Pending', 'Active', 'Inactive'];

        $news = News::orderByDesc('news.datetime')->limit(100)->get();

        return view('pages.polls.create', compact('show_result_options', 'statuses', 'news'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request_all = $request->all();
        $request_all_keys = array_keys($request_all);

        $poll_option_keys = array_filter($request_all_keys, function($key) {
            return strpos($key, 'poll_option_title__') === 0;
        });

        $validator = \Validator::make($request->all(), [
            'title' => ['required', 'string', 'max:150'],
            'description' => ['nullable', 'string', 'max:400'],
            'start_datetime' => ['required', 'date_format:Y-m-d H:i'],
            'end_datetime' => ['required', 'date_format:Y-m-d H:i'],
            'result_end_datetime' => ['nullable', 'date_format:Y-m-d H:i'],
            'policy' => ['nullable', 'string', 'max:400'],
            'show_result' => ['required'],
            'type' => ['required'],
            'status' => ['required'],
        ]);

        if ($validator->fails()) {
            return redirect()
                ->route('polls.create')
                ->withErrors($validator)
                ->withInput();
        }

        try {
            \DB::beginTransaction();

            $poll = new Poll();
            $poll->title = $request->input('title');
            $poll->description = $request->input('description');
            $poll->start_datetime = $request->input('start_datetime');
            $poll->end_datetime = $request->input('end_datetime');
            $poll->policy = $request->input('policy');
            $poll->allow_multiple_options = $request->input('allow_multiple_options') !== null? true: false;
            $poll->show_result = $request->input('show_result');
            $poll->result_end_datetime = $request->input('result_end_datetime');
            $poll->only_for_registered_user = $request->input('only_for_registered_user') !== null? true: false;
            $poll->type = $request->input('type');
            $poll->policy = $request->input('policy');
            if($poll->type == 'Standalone') {
                $poll->position = $request->input('position');
            } else {
                $poll->news_id = $request->input('news_id');
            }
            $poll->status = $request->input('status');
            $poll->save();

            if($poll->news_id !== null) {
                $news_for_elastic_search = $this->findNewsForElasticSearch($poll->news_id);

                if(env('ELASTICSEARCH')) {
                    \Elasticsearch::update([
                        'id' => $poll->news_id,
                        'index' => 'news',
                        'body' => [
                            'doc' => $news_for_elastic_search,
                            'doc_as_upsert' => true
                        ]
                    ]);
                }
            }

            if($request->has('image')) {
                $image = $request->file('image');
                $poll->image = $this->pollImageUpload($poll, $image);
                $poll->update();
            }

            foreach ($poll_option_keys as $poll_option_key) {
                $poll_option = new PollOption();
                $poll_option->title = $request->input($poll_option_key);
                $poll_option->poll_id = $poll->id;
                $poll_option->save();
            }

            \DB::commit();

            \Toastr::success('Poll Created successfully', 'Success', []);
        } catch (\Exception $e) {
            \DB::rollback();

            \Toastr::error('Error occured during creating poll', 'Error', []);
        }

        return redirect()->route('polls.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $poll = Poll::with('all_options')->find($id);

        if($poll) {
            return view('pages.polls.show', compact('poll'));
        }

        abort(404);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $poll = Poll::with('all_options')->find($id);

        if($poll) {
            $show_result_options = ['Never', 'After Poll End', 'After User Vote', 'Always'];
            $statuses = ['Pending', 'Active', 'Inactive'];

            $news = News::orderByDesc('news.datetime')->limit(100)->get();

            return view('pages.polls.create', compact('show_result_options', 'statuses', 'news', 'poll'));
        }

        abort(404);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $poll = Poll::with('options')->find($id);

        if($poll) {
            $request_all = $request->all();
            $request_all_keys = array_keys($request_all);

            $poll_option_keys = array_filter($request_all_keys, function($key) {
                return strpos($key, 'poll_option_title__') === 0;
            });

            $validator = \Validator::make($request->all(), [
                'title' => ['required', 'string', 'max:150'],
                'description' => ['nullable', 'string', 'max:400'],
                'start_datetime' => ['required', 'date_format:Y-m-d H:i'],
                'end_datetime' => ['required', 'date_format:Y-m-d H:i'],
                'result_end_datetime' => ['nullable', 'date_format:Y-m-d H:i'],
                'policy' => ['nullable', 'string', 'max:400'],
                'show_result' => ['required'],
                'type' => ['required'],
                'status' => ['required'],
            ]);

            if ($validator->fails()) {
                return redirect()
                    ->route('polls.edit', ['id' => $id])
                    ->withErrors($validator)
                    ->withInput();
            }

            try {
                \DB::beginTransaction();

                $poll_news_id = $poll->news_id;

                $poll->title = $request->input('title');
                $poll->description = $request->input('description');
                $poll->start_datetime = $request->input('start_datetime');
                $poll->end_datetime = $request->input('end_datetime');
                $poll->policy = $request->input('policy');
                $poll->allow_multiple_options = $request->input('allow_multiple_options') !== null? true: false;
                $poll->show_result = $request->input('show_result');
                if($poll->show_result == 'Never') {
                    $poll->result_end_datetime = null;
                } else {
                    $poll->result_end_datetime = $request->input('result_end_datetime');
                }
                $poll->only_for_registered_user = $request->input('only_for_registered_user') !== null? true: false;
                $poll->type = $request->input('type');
                $poll->policy = $request->input('policy');
                if($poll->type == 'Standalone') {
                    $poll->news_id = null;
                    $poll->position = $request->input('position');
                } else {
                    $poll->position = null;
                    $poll->news_id = $request->input('news_id');
                }
                $poll->status = $request->input('status');
                $poll->update();

                if($poll_news_id != $poll->news_id) {
                    if($poll_news_id !== null) {
                        $news_for_elastic_search = $this->findNewsForElasticSearch($poll_news_id);

                        if(env('ELASTICSEARCH')) {
                            \Elasticsearch::update([
                                'id' => $poll_news_id,
                                'index' => 'news',
                                'body' => [
                                    'doc' => $news_for_elastic_search,
                                    'doc_as_upsert' => true
                                ]
                            ]);
                        }
                    }

                    if($poll->news_id !== null) {
                        $news_for_elastic_search = $this->findNewsForElasticSearch($poll->news_id);

                        if(env('ELASTICSEARCH')) {
                            \Elasticsearch::update([
                                'id' => $poll->news_id,
                                'index' => 'news',
                                'body' => [
                                    'doc' => $news_for_elastic_search,
                                    'doc_as_upsert' => true
                                ]
                            ]);
                        }
                    }
                }

                if($request->has('image')) {
                    $image = $request->file('image');
                    $poll->image = $this->pollImageUpload($poll, $image);
                    $poll->update();
                }

                $existing_poll_options_ids = $poll->options->pluck('id')->toArray();
                $new_poll_options_ids = [];

                foreach($poll_option_keys as $poll_option_key) {
                    if(strpos($poll_option_key, 'poll_option_title__ex__') === 0) {
                        $poll_option_id = str_replace('poll_option_title__ex__', '', $poll_option_key);
                        $deleted = false;
                        if(strpos($poll_option_id, 'del__') === 0) {
                            $poll_option_id = str_replace('del__', '', $poll_option_id);
                            $deleted = true;
                        }
                        $poll_option = PollOption::withTrashed()->find($poll_option_id);
                        $poll_option->title = $request->input($poll_option_key);
                        if($deleted) $poll_option->deleted_at = null;
                        $poll_option->save();
                    } else {
                        $poll_option = PollOption::withTrashed()->firstOrNew(['title' => $request->input($poll_option_key), 'poll_id' => $poll->id]);
                        $poll_option->deleted_at = null;
                        $poll_option->save();
                    }
                    array_push($new_poll_options_ids, $poll_option->id);
                }

                $deleted_poll_options_ids = array_diff($existing_poll_options_ids, $new_poll_options_ids);

                PollOption::whereIn('id', $deleted_poll_options_ids)->delete();

                \DB::commit();

                \Toastr::success('Poll Updated successfully', 'Success', []);
            } catch (\Exception $e) {
                \DB::rollback();

                \Toastr::error('Error occured during updating poll', 'Error', []);
            }

            return redirect()->route('polls.edit', ['id' => $id]);
        }

        abort(404);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function destroyPollImage($id)
    {
        $poll = Poll::find($id);

        if($poll) {
            $poll->image = null;

            $public_storage_path = 'app/public/';
            $path = 'polls/' . $poll->id . '/image/';
            $app_path = storage_path($public_storage_path . $path);

            \File::deleteDirectory($app_path, true);

            $poll->update();

            return [
                'status' => true
            ];
        }

        abort(404);
    }

    public function votes(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'poll_option_id' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 401);
        }

        $now = \Carbon::now();

        $poll = Poll::where('status', 'Active')->where('start_datetime', '<', $now)->where('end_datetime', '>', $now)->first();

        if($poll) {
            $device = Device::firstOrNew(['unique_id' => $request->input('device_id')]);
            $device->save();

            $poll_option = PollOption::where('poll_id', $poll->id)->find($request->input('poll_option_id'));

            if($poll_option) {
                $poll_option_ids = PollOption::where('poll_id', $poll->id)->get()->pluck('id')->toArray();
                $device_poll_option_ids = DevicePollOption::whereIn('poll_option_id', $poll_option_ids)->where('device_id', $device->id)->count();

                if($device_poll_option_ids == 0 || $poll->allow_multiple_options == 1) {
                    $device_poll_option = DevicePollOption::firstOrNew([
                        'device_id' => $device->id,
                        'poll_option_id' => $poll_option->id,
                    ]);

                    if(!$device_poll_option->exists) {
                        $poll->votes = $poll->votes + 1;
                        $poll->update();

                        $poll_option->total_votes = $poll_option->total_votes + 1;
                        $poll_option->percentage_votes = round(($poll_option->total_votes / $poll->votes) * 100, 2);
                        $poll_option->update();

                        $device_poll_option->save();

                        $poll->load('options');

                        foreach ($poll->options as $option) {
                            $option->percentage_votes = round(($option->total_votes / $poll->votes) * 100, 2);
                            $option->update();
                        }
                    }

                    $poll = $this->pollResponce($device, $poll);

                    return response()->json([
                        'status' => true,
                        'data' => $poll
                    ], 200);
                }

                return response()->json([
                    'status' => false,
                    'message' => 'Already Voted'
                ], 422);
            }

            return response()->json([
                'status' => false,
                'message' => 'Poll Option not found'
            ], 410);
        }

        return response()->json([
            'status' => false,
            'message' => 'Poll Expired or Poll not found'
        ], 410);
    }

    public function pollImageUpload($poll, $image)
    {
        $image_extension = $image->getClientOriginalExtension();
        $image_filename = Str::random(10) . '.' . $image_extension;

        $public_storage_path = 'app/public/';
        $path = 'polls/' . $poll->id . '/image/';
        $app_path = storage_path($public_storage_path . $path);

        if (!file_exists($app_path)) {
            \File::makeDirectory($app_path, 0777, true);
        } else {
            \File::deleteDirectory($app_path, true);
        }

        $image->move($app_path, $image_filename);

        $sizes = ['128px', '256px', '515px', '1080px'];

        foreach ($sizes as $size) {
            $height = str_replace('px', '', $size);

            if (!file_exists($app_path . '/' . $size)) {
                \File::makeDirectory($app_path . '/' . $size, 0777, true);
            }

            $resized_media = \Image::make($app_path . '/' . $image_filename)
                ->resize(null, $height, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($app_path . '/' . $size . '/' . $image_filename, 90);
        }

        return $image_filename;
    }
}
