<?php

namespace App\Http\Controllers\Quote;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

use App\Device;
use App\Quote;
use App\QuoteDeviceReaction;
use App\QuoteDeviceShare;

class QuoteAPIController extends Controller
{
    public function storeReactions(Request $request) {
        $validator = Validator::make($request->all(), [
            'quote_id' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 401);
        }

        $data = ['status' => true];

        $device = Device::firstOrNew(['unique_id' => $request->input('device_id')]);
        $device->save();

        try {
            $quote_device_reaction = QuoteDeviceReaction::firstOrNew(['device_id' => $device->id, 'quote_id' => $request->input('quote_id')]);
            $quote_device_reaction->save();

            $quote = Quote::find($request->input('quote_id'));
            $quote_device_reactions_device_ids = [];

            if($quote) {
                $quote_device_reactions_device_ids = $quote->reactions()->pluck('device_id')->toArray();
            
                $quote = $quote->toArray();
                $quote_device_for_elastic_search = [];
                $quote_device_for_elastic_search['reactions'] = $quote_device_reactions_device_ids;
                $quote_device_for_elastic_search['reactions_count'] = count($quote_device_reactions_device_ids);

                if(env('ELASTICSEARCH')) {
                    \Elasticsearch::update([
                        'id' => $quote['id'],
                        'index' => 'quotes',
                        'body' => [
                            'doc' => $quote_device_for_elastic_search,
                            'doc_as_upsert' => true
                        ]
                    ]);
                }

                $data['data'] = $quote;
            } else {
                $data['status'] = false;
            }
        } catch (\Exception $e) {
            $data['status'] = false;
        }

        return $data;
    }

    public function storeShares(Request $request) {
        $validator = Validator::make($request->all(), [
            'quote_id' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 401);
        }

        $data = ['status' => true];

        $device = Device::firstOrNew(['unique_id' => $request->input('device_id')]);
        $device->save();

        try {
            $quote_device_share = QuoteDeviceShare::firstOrNew(['device_id' => $device->id, 'quote_id' => $request->input('quote_id')]);
            $quote_device_share->save();
            
            $quote = Quote::find($request->input('quote_id'));

            if($quote) {
                $quote_device_shares_device_ids = $quote->shares()->pluck('device_id')->toArray();

                $quote = $quote->toArray();
                $quote_device_for_elastic_search = [];

                $quote_device_for_elastic_search['shares'] = $quote_device_shares_device_ids;
                $quote_device_for_elastic_search['shares_count'] = count($quote_device_shares_device_ids);

                if(env('ELASTICSEARCH')) {
                    \Elasticsearch::update([
                        'id' => $quote['id'],
                        'index' => 'quotes',
                        'body' => [
                            'doc' => $quote_device_for_elastic_search,
                            'doc_as_upsert' => true
                        ]
                    ]);
                }

                $data['data'] = $quote;
            } else {
                $data['status'] = false;    
            }
        } catch (\Exception $e) {
            $data['status'] = false;
        }

        return $data;
    }
}
