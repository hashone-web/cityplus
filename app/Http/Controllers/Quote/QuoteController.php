<?php

namespace App\Http\Controllers\Quote;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

use App\Quote;

class QuoteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $quotes = Quote::sortable(['date' => 'desc'])->paginate(\Config::get('constants.pagination_size'));

        return view('pages.quotes.index', compact('quotes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.quotes.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'text' => ['required', 'string', 'max:400'],
            'date' => ['required'],
        ]);

        if ($validator->fails()) {
            return redirect()
                ->route('quotes.create')
                ->withErrors($validator)
                ->withInput();
        }

        try {
            DB::beginTransaction();

            $quote = new Quote();
            $quote->text = $request->input('text');
            $quote->date = $request->input('date');
            $quote->save();

            DB::commit();

            $this->indexQuoteForElasticSearch($quote->id);

            \Toastr::success('Quote Created successfully', 'Success', []);
        } catch (\Exception $e) {
            DB::rollback();
            \Toastr::error('Error occured during creating quote', 'Error', []);
        }

        return redirect()->route('quotes.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $quote = Quote::find($id);

        if($quote) {
            return view('pages.quotes.create', compact('quote'));
        }

        abort(404);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $quote = Quote::find($id);

        if($quote) {
            $validator = Validator::make($request->all(), [
                'text' => ['required', 'string', 'max:255'],
                'date' => ['required'],
            ]);

            if ($validator->fails()) {
                return redirect()
                    ->route('quotes.edit', ['id' => $id])
                    ->withErrors($validator)
                    ->withInput();
            }

            try {
                DB::beginTransaction();

                $quote->text = $request->input('text');
                $quote->date = $request->input('date');
                $quote->update();

                DB::commit();

                $this->indexQuoteForElasticSearch($quote->id);

                \Toastr::success('Quote Updated successfully', 'Success', []);
            } catch (\Exception $e) {
                DB::rollback();
                \Toastr::error('Error occured during updating quote', 'Error', []);
            }

            return redirect()->route('quotes.index');
        }

        abort(404);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $quote = Quote::find($id);

        if($quote) {
            $quote->delete();

            \Toastr::success('Quote Deleted successfully', 'Success', []);

            return redirect()->route('quotes.index');
        }

        abort(404);
    }
}
