<?php

namespace App\Http\Controllers\Reporter;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redis;
use Validator;
use Carbon\Carbon;
use DB;

use App\ReporterAppSetting;
use App\ReporterAppUpdate;
use App\Device;
use App\News;
use App\UserFcmToken;

class ApiController extends Controller
{
    public function news(Request $request)
    {
        $pagination_size = $request->query('limit') !== null? (int) $request->query('limit'): \Config::get('constants.pagination_size');
        $page = $request->input('page') !== null? (int) $request->input('page'): 1;
        $from = ($page - 1) * $pagination_size;

        $device_data = Redis::get('device_data:'. $request->input('device_id'));

        if($device_data !== null) {
            $device_data = json_decode($device_data);
            $device = $device_data->device;
        } else {
            $device = Device::where('unique_id', $request->input('device_id'))->first();
        }

        $status = 'Published';

        if($request->input('status') !== null) {
            $status = $request->input('status');
        }

        if(env('ELASTICSEARCH')) {
            $elastic_search_params = '{
                "index":"news",
                "body":{
                    "query":{
                        "bool":{
                            "must": [ 
                                {
                                    "match": {
                                        "status":"'. $status .'"
                                    }
                                },
                                {
                                    "match": {
                                        "created_by":"'. $request->user()->id .'"
                                    }
                                }
                            ]
                        }
                    },
                    "sort":{
                        "datetime":{
                            "order":"desc"
                        }
                    }
                },
                "from":'. $from .',
                "size":'. $pagination_size .'
            }';

            $elastic_search_params = json_decode($elastic_search_params, true);

            $raw_news = \Elasticsearch::search($elastic_search_params);
            $news = [];

            foreach ($raw_news['hits']['hits'] as $key => $raw_news_single) {
                $reaction = null;
                // if(in_array($device->id, $raw_news_single['_source']['likes'])) $reaction = 'Claps';
                // if(in_array($device->id, $raw_news_single['_source']['dislikes'])) $reaction = 'Sad';
                $raw_news_single['_source']['reaction'] = $reaction;

                $is_sensitive_viewed = $request->input('platform') !== null && $request->input('platform') == 'iOS' ? 0: null;
                // if(in_array($device->id, $raw_news_single['_source']['sensitivities'])) $is_sensitive_viewed = 1;
                $raw_news_single['_source']['is_sensitive_viewed'] = $is_sensitive_viewed;

                unset($raw_news_single['_source']['cities']);
                unset($raw_news_single['_source']['likes']);
                unset($raw_news_single['_source']['dislikes']);
                unset($raw_news_single['_source']['sensitivities']);

                $raw_news_single['_source']['poll'] = null;

                $news[] = $raw_news_single['_source'];
            }

            $total = $raw_news['hits']['total']['value'];
            $last_page = ceil($total / $pagination_size);

            $data = [
                'server_time' => time() * 1000,
                'status' => true,
                'data' => $news,
                'elastic_search' => true,
            ];

            $data['pagination'] = [
                'show_pagination' => ($total / $pagination_size) > 1,
                'total' => $total,
                'per_page' => $pagination_size,
                'current_page' => $page,
                'last_page' => $last_page,
                'from' => $from + 1,
                'to' => $from + $pagination_size
            ];

            return $data;
        }

        return [
            'status' => false,
            'message' => 'Cache service is not active'
        ];
    }

    public function createNews(Request $request)
    {
        ini_set('memory_limit', '-1');

        $validator = Validator::make($request->all(), [
            'headline' => ['nullable', 'string'],
            'article' => ['required', 'string'],
            'city' => ['required'],
            'is_breaking' => ['required'],
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 401);
        }

        try {
            DB::beginTransaction();

            $news = new News();
            $news->headline = $request->input('headline');
            $news->article = $request->input('article');
            $news->city_id = $request->input('city');
            $news->category_id = 1;
            $news->language_id = 2;
            $news->status = 'Pending';
            $news->is_breaking = $request->input('is_breaking') == 1? 1: 0;
            $news->datetime = \Carbon::now();
            $news->created_by = $request->user()->id;
            $news->updated_by = $request->user()->id;

            if($news->is_breaking) {
                $news->is_breaking_datetime = \Carbon::now();
            }

            $news->save();

            $news->reporters()->sync([ $request->user()->id ]);
            $news->cities()->sync([ $news->city_id ]);

            if($request->file('media') !== null && count($request->file('media'))) {
                $all_media = $request->file('media');

                $media_data = $this->generateMediaData($request);

                foreach ($all_media as $media_index => $media) {
                    $news_media = $this->storeNewsOriginalMedia($news, $media, $media_index, $media_data);
                    $this->mediaUpload($news, $news_media, $media, $media_data);
                }
            }
            if($request->file('attachments') !== null && count($request->file('attachments'))) {
                $all_attachments = $request->file('attachments');

                foreach ($all_attachments as $attachment_index => $attachment) {
                    $news_attachment = $this->storeNewsAttachment($news, $attachment, $attachment_index);
                    $this->attachmentUpload($news, $news_attachment, $attachment);
                }
            }
            DB::commit();

            $news_for_elastic_search = $this->findNewsForElasticSearch($news->id);

            if(env('ELASTICSEARCH')) {
                \Elasticsearch::index([
                    'id' => $news->id,
                    'index' => 'news',
                    'body' => $news_for_elastic_search,
                ]);
            }

            // $fcm_ids = UserFcmToken::where('id', '!=', \Auth::id())->get()->pluck('token')->toArray();

            // $data = [
            //     'title' => $news->headline,
            //     'content' => 'News status changed to ' . $news->status . ' by '. \Auth::user()->name,
            //     'url' => route('news.edit', [ 'id' => $news->id ]),
            //     'ids' => $fcm_ids
            // ];

            // $this->webPushlNotification($data);

            return [
                'status' => true,
                'data' => $news
            ];
        } catch (\Exception $e) {
            DB::rollback();

            return [
                'status' => false,
                'error' => $e
            ];
        }
    }

    public function settings(Request $request) {
        $setting = Redis::get('reporters-app.setting');

        if($setting !== null) {
            return [
                'status' => true,
                'data' => json_decode($setting, true),
                'redis' => true
            ];
        }

        $setting = ReporterAppSetting::first();

        Redis::set('reporters-app.setting', json_encode($setting), 'EX', 60);

        return [
            'status' => true,
            'data' => $setting
        ];
    }

    public function updates(Request $request) {
        $update = Redis::get('reporters-app.update');

        if($update !== null) {
            return [
                'status' => true,
                'data' => json_decode($update, true),
                'redis' => true
            ];
        }

        $update = ReporterAppUpdate::first();

        Redis::set('reporters-app.update', json_encode($update), 'EX', 60);

        return [
            'status' => true,
            'data' => $update
        ];
    }

    public function extras(Request $request) {
        $extras = Redis::get('reporters-app.extras');

        if($extras !== null) {
            return [
                'status' => true,
                'data' => json_decode($extras, true),
                'redis' => true
            ];
        }

        $update = ReporterAppUpdate::first();
        $setting = ReporterAppSetting::first();

        $data = [];
        $data['update'] = $update;
        $data['setting'] = $setting;

        Redis::set('reporters-app.extras', json_encode($data), 'EX', 60);

        return [
            'status' => true,
            'data' => $data
        ];
    }
}
