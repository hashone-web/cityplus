<?php

namespace App\Http\Controllers\Reporter;

use App\Http\Controllers\Controller;
use App\ReporterAppSetting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class SettingController extends Controller
{
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {
        $setting = ReporterAppSetting::first();

        return view('pages.reporters-app.settings.edit', compact('setting'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        try {
            DB::beginTransaction();

            $setting = ReporterAppSetting::firstOrNew(['id' => 1]);
            $setting->login_not_allowed_message = $request->input('login_not_allowed_message');
            $setting->privacy_policy_page_link = $request->input('privacy_policy_page_link');
            $setting->terms_of_use_page_link = $request->input('terms_of_use_page_link');
            $setting->save();

            DB::commit();

            \Toastr::success('Settings Updated successfully', 'Success', []);
        } catch (\Exception $e) {
            DB::rollback();

            \Toastr::error('Error occured during updating settings', 'Error', []);
        }

        return redirect()->route('reporters-app.settings.edit');
    }
}
