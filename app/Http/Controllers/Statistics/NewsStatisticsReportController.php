<?php

namespace App\Http\Controllers\Statistics;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\NewsStatisticsReport;

class NewsStatisticsReportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $latest_date = NewsStatisticsReport::selectRaw('max(`date`) as latest_date')->first()->toArray();
        $first_record_date = NewsStatisticsReport::selectRaw('min(`date`) as latest_date')->first()->toArray();

        $today = date('Y-m-d');

        $news_statistics_reports = NewsStatisticsReport::orderBy('date', 'desc')->where('date', '!=', $today)->where('date', '!=', $first_record_date)->get()->groupBy(['date', 'type'])->toArray();
        $total_news_statistics_reports = NewsStatisticsReport::orderBy('date', 'desc')->where('date', '!=', $today)->where('date', $latest_date)->get()->groupBy(['date', 'type'])->toArray();

        return view('pages.news_statistics_reports.index', compact('news_statistics_reports', 'total_news_statistics_reports'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
