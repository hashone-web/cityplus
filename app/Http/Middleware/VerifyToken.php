<?php

namespace App\Http\Middleware;

use Closure;
use Validator;

class VerifyToken
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $validator = Validator::make($request->all(), [
            'device_id' => 'required',
            'token' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 401);
        }

        try {
            if($request->token !== null) {
                $token = $request->token;

                $token = base64_decode($token);
                $token = substr($token, 5);
                $token = substr($token, 0, -6);
                $token_first_five = substr($token, 0, 5);
                $token_after_nine = substr($token, 9);
                $final_token = $token_first_five.$token_after_nine;
                $final_token = base64_decode($final_token);
                $final_token = str_replace(':remote', '', $final_token);

                if($final_token && ($final_token == 'com.hashone.applicationtemplate' || $final_token == 'com.cityplus' || $final_token == 'com.cityplus.notification.test' || $final_token == 'com.cityplus.local.news.info' || $final_token == 'com.cityplus.local.news.info.test' || $final_token == 'com.cityplus.local.news.info.test.one' || $final_token == 'com.cityplus.local.news.info.gopal')) {
                    return $next($request);
                }
            }
        } catch(\Exception $e) {

        }

        return response([
            'status' => false,
            'message' => 'authentication failed'
        ]);
    }
}
