<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Kyslik\ColumnSortable\Sortable;

class Locality extends Model
{
    use SoftDeletes, Sortable;

    public $sortable = [
        'name',
        'created_at'
    ];

    public function city()
    {
        return $this->belongsTo('App\City', 'city_id');
    }
}
