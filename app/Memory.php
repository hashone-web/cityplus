<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Kyslik\ColumnSortable\Sortable;

class Memory extends Model
{
    use SoftDeletes, Sortable;

    public function getPhotoAttribute($value)
    {
        return asset('/storage/memories/' . $this->id . '/photo/' . $value);
    }

    public function setDatetimeAttribute($value)
    {
        if($value !== null) {
            $this->attributes['datetime'] = \Carbon::createFromFormat('Y-m-d H:i', $value)->format('Y-m-d H:i:s');
        }
    }

    public function setPublishDatetimeAttribute($value)
    {
        if($value !== null) {
            $this->attributes['publish_datetime'] = \Carbon::createFromFormat('Y-m-d H:i', $value)->format('Y-m-d H:i:s');
        }
    }

    public function setEndDatetimeAttribute($value)
    {
        if($value !== null) {
            $this->attributes['end_datetime'] = \Carbon::createFromFormat('Y-m-d H:i', $value)->format('Y-m-d H:i:s');
        }
    }

    public function created_by_user()
    {
        return $this->belongsTo('App\User', 'created_by');
    }

    public function updated_by_user()
    {
        return $this->belongsTo('App\User', 'updated_by');
    }

    public function agency()
    {
        return $this->belongsTo('App\MemoryAgency', 'agency_id');
    }

    public function from_names()
    {
        return $this->hasMany('App\MemoryFromName', 'memory_id');
    }

    public function proofs()
    {
        return $this->hasMany('App\MemoryProof', 'memory_id');
    }

    public function cities()
    {
        return $this->belongsToMany('App\City', 'memory_cities', 'memory_id', 'city_id');
    }

    public function reactions()
    {
        return $this->hasMany('App\MemoryDeviceReaction', 'memory_id');
    }

    public function shares()
    {
        return $this->hasMany('App\MemoryDeviceShare', 'memory_id');
    }
}
