<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Kyslik\ColumnSortable\Sortable;

class MemoryAgency extends Model
{
    use SoftDeletes, Sortable;

    public $sortable = [
    	'id',
        'name',
        'business_name',
        'created_at'
    ];
}
