<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MemoryDeviceReaction extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'device_id',
        'memory_id',
    ];
}
