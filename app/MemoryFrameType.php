<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Kyslik\ColumnSortable\Sortable;

class MemoryFrameType extends Model
{
    use SoftDeletes, Sortable;

    public function getThumbnailAttribute($value)
    {
        return asset('/storage/memories/frames/' . $this->id . '/thumbnail/' . $value);
    }
}
