<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MemorySetting extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id'
    ];

    protected $appends = ['share_bottom_image_full_path', 'share_bottom_image_gujarati_full_path'];

    public function getShareBottomImageFullPathAttribute($value)
    {
        return $this->share_bottom_image !== null? asset('/storage/memories/settings/' . $this->share_bottom_image): null;
    }

    public function getShareBottomImageGujaratiFullPathAttribute($value)
    {        
        return $this->share_bottom_image_gujarati !== null? asset('/storage/memories/settings/' . $this->share_bottom_image_gujarati): null;
    }
}
