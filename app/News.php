<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Kyslik\ColumnSortable\Sortable;

class News extends Model
{
    use SoftDeletes, Sortable;

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'source' => 'object',
    ];

    protected $appends = ['reporter'];

    public function getReporterAttribute()
    {
        $reporter = $this->reporters()->first();
        if($reporter && $reporter->anonymous_reporting) {
            $reporter->name = 'સીટીપ્લસ';
        }
        return $reporter;
    }

    public function getMediaViewsAttribute()
    {
        $data = $this->media_statistics()->first();
        return $data !== null? $data->media_views :0;
    }

    public function getMediaDownloadsAttribute()
    {
        $data = $this->media_statistics()->first();
        return $data !== null? $data->media_downloads :0;
    }

    public function setSourceAttribute($value)
    {
        if($value !== null) {
            $this->attributes['source'] = json_encode($value);
        }
    }

    public function categories()
    {
        return $this->belongsToMany('App\Category', 'news_categories', 'news_id', 'category_id');
    }

    public function language()
    {
        return $this->belongsTo('App\Language', 'language_id');
    }

    public function created_by_user()
    {
        return $this->belongsTo('App\User', 'created_by');
    }

    public function updated_by_user()
    {
        return $this->belongsTo('App\User', 'updated_by');
    }

    public function city()
    {
        return $this->belongsTo('App\City', 'city_id');
    }

    public function cities()
    {
        return $this->belongsToMany('App\City', 'news_cities', 'news_id', 'city_id');
    }

    public function reactions()
    {
        return $this->hasMany('App\NewsReaction', 'news_id');
    }

    public function likes()
    {
        return $this->hasMany('App\NewsReaction', 'news_id')->where('type', 'Claps');
    }

    public function dislikes()
    {
        return $this->hasMany('App\NewsReaction', 'news_id')->where('type', 'Sad');
    }

    public function sensitivities()
    {
        return $this->hasMany('App\NewsDeviceSensitivity', 'news_id');
    }

    public function shares()
    {
        return $this->hasMany('App\NewsShare', 'news_id')->where('type', 'Whatsapp');
    }

    public function media()
    {
        return $this->hasMany('App\NewsMedia', 'news_id')->where('type', 'original')->orderBy('sort');
    }

    public function attachments()
    {
        return $this->hasMany('App\NewsAttachment', 'news_id');
    }

    public function media_statistics()
    {
        return $this->hasOne('App\NewsStatistic', 'news_id');
    }

    public function thumbs()
    {
        return $this->hasMany('App\NewsMedia', 'news_id')->where('type', \Config::get('constants.thumb_size'))->orderBy('sort');
    }

    public function downloads()
    {
        return $this->hasMany('App\NewsMedia', 'news_id')->where('type', 'original')->orderBy('sort');
    }

    public function reporters()
    {
        return $this->belongsToMany('App\User', 'news_users', 'news_id', 'user_id');
    }

    public function notifications()
    {
        return $this->hasMany('App\NewsNotification', 'news_id');
    }

    public function poll()
    {
        return $this->hasOne('App\Poll', 'news_id');
    }
}
