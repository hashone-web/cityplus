<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NewsAttachment extends Model
{
    protected $appends = ['full_path'];

    public function getFullPathAttribute($value)
    {
        return asset('/storage/news/' . $this->news_id . '/attachments/' . $this->dynamic_id . '/' . $this->name);
    }
}
