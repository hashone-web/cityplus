<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NewsMedia extends Model
{
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'details' => 'array',
    ];

    // protected $appends = ['full_path', 'thumb'];

    public function getFullPathAttribute($value)
    {
        if($this->watermark_position !== null && strtolower($this->extension) == 'mp4') {
            $value = 'watermarked/' . $value;
        }
        return asset('/storage/news/' . $this->news_id . '/media/' . $this->dynamic_id . '/' . $value);
    }

    public function getThumbAttribute($value)
    {
        if($value == null) return $value;
        
        return asset('/storage/news/' . $this->news_id . '/media/' . $this->dynamic_id . '/' . $value);
    }

    public function getFullPathDynamicAttribute()
    {
        $size = $this->type !== 'original'? $this->type: '';

        return ($this->parent()->first() && ($this->type == 'thumb' || $this->parent()->first()->type == 'thumb')? 'thumb/': '') . ($size !== ''? $size . '/': '') . $this->name;
    }

    public function getThumbDynamicAttribute()
    {
        if(strtolower($this->extension) == 'mp4') {
            return $this->thumbs()->where('type', 'thumb')->first()? $this->thumbs()->where('type', 'thumb')->first()->thumb_dynamic: null;
        }
        return $this->thumbs()->where('type', \Config::get('constants.thumb_size'))->first()? $this->thumbs()->where('type', \Config::get('constants.thumb_size'))->first()->full_path_dynamic: null;
    }

    public function getParentFullPathAttribute()
    {
        return $this->parent()->first()? $this->parent()->first()->full_path: $this->full_path;
    }

    public function getParentIsVideoAttribute()
    {
        $parent_type = $this->parent()->first()? $this->parent()->first()->type: $this->type;
        return $parent_type == 'thumb';
    }

    public function news()
    {
        return $this->belongsTo('App\News', 'news_id');
    }

    public function parent()
    {
        return $this->belongsTo('App\NewsMedia', 'parent_id');
    }

    public function thumbs()
    {
        return $this->hasMany('App\NewsMedia', 'parent_id');
    }
}
