<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class NewsRejectionReason extends Model
{
    use Sortable;

    public $sortable = [
        'title',
    ];
}
