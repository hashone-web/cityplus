<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NewsStatisticsReport extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'type',
        'date',
    ];
}
