<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Kyslik\ColumnSortable\Sortable;

class Poll extends Model
{
    use SoftDeletes, Sortable;

    public $sortable = [
        'id',
        'status',
        'created_at'
    ];

    protected $appends = ['votable', 'time_label'];

    public function getVotableAttribute()
    {
        return $this->end_datetime > \Carbon::now() ? 1:0;
    }

    public function getTimeLabelAttribute()
    {
        $start_time = \Carbon::now();
        $end_time = $this->end_datetime;
        
        $start_time_obj = \Carbon::createFromTimeStamp(strtotime($start_time));
        $end_time_obj = \Carbon::createFromTimeStamp(strtotime($end_time));
        
        $minutes = (strtotime($end_time) - strtotime($start_time)) / 60;

        $diff_time = $end_time_obj->diff($start_time_obj);

        $end_datetime = 'Poll ended';
        if($minutes) {
            $z = 'left to vote';

            if($minutes >= 1440) {
                $d = $diff_time->format('%d');
                $h = $diff_time->format('%h');
                $di =  $d > 1? 'days': 'day';
                
                $end_datetime = "{$d} {$di}";
                
                if($h) {
                    $hi =  $h > 1? 'hours': 'hour';
                    $end_datetime = $end_datetime . " {$h} {$hi}";
                } else {
                    $i = $diff_time->format('%i');
                    if($i) {
                        $yi =  $i > 1? 'minutes': 'minute';
                        $end_datetime = $end_datetime . " {$i} {$yi}";
                    }
                }

                $end_datetime = $end_datetime . " {$z}";
            } else if($minutes < 1440 && $minutes >= 60) {
                $h = $diff_time->format('%h');
                $i = $diff_time->format('%i');
                $hi =  $h > 1? 'hours': 'hour';
                
                $end_datetime = "{$h} {$hi}";
                
                if($i) {
                    $yi =  $i > 1? 'minutes': 'minute';
                    $end_datetime = $end_datetime . " {$i} {$yi}";
                }
                
                $end_datetime = $end_datetime . " {$z}";
            } else if($minutes < 60 && $minutes > 1) {
                $i = $diff_time->format('%i');
                $yi =  $i > 1? 'minutes': 'minute';
                $end_datetime = "{$i} {$yi} {$z}";
            } else {
                $end_datetime = 'ending soon';
            }
        }
        
        return $end_datetime;
    }

    public function setStartDatetimeAttribute($value)
    {
        if($value !== null) {
            $this->attributes['start_datetime'] = \Carbon::createFromFormat('Y-m-d H:i', $value)->format('Y-m-d H:i:s');
        }
    }

    public function setEndDatetimeAttribute($value)
    {
        if($value !== null) {
            $this->attributes['end_datetime'] = \Carbon::createFromFormat('Y-m-d H:i', $value)->format('Y-m-d H:i:s');
        }
    }

    public function setResultEndDatetimeAttribute($value)
    {
        if($value !== null) {
            $this->attributes['result_end_datetime'] = \Carbon::createFromFormat('Y-m-d H:i', $value)->format('Y-m-d H:i:s');
        } else {
            $this->attributes['result_end_datetime'] = $value;
        }
    }

    public function options()
    {
        return $this->hasMany('App\PollOption', 'poll_id');
    }

    public function all_options()
    {
        return $this->hasMany('App\PollOption', 'poll_id')->withTrashed();
    }

    public function result_views()
    {
        return $this->hasMany('App\PollResultDeviceView', 'poll_id');
    }
}
