<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Kyslik\ColumnSortable\Sortable;

class Quote extends Model
{
    use SoftDeletes, Sortable;

    public $sortable = [
        'text',
        'date',
        'created_at'
    ];

    public function reactions()
    {
        return $this->hasMany('App\QuoteDeviceReaction', 'quote_id');
    }

    public function shares()
    {
        return $this->hasMany('App\QuoteDeviceShare', 'quote_id');
    }
}
