<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class QuoteDeviceReaction extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'device_id',
        'quote_id',
    ];
}
