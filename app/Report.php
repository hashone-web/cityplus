<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Report extends Model
{
    public function news()
    {
        return $this->belongsTo('App\News', 'news_id');
    }

    public function type()
    {
        return $this->belongsTo('App\ReportType', 'report_type_id');
    }
}
