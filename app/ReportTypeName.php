<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReportTypeName extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'report_type_id', 'language_id',
    ];
}
