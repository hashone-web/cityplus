<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReporterAppUpdate extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id'
    ];

    public function getAppVerAttribute($value)
    {
        return $value !== null? (int) $value: null;
    }
}
