<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id'
    ];

    protected $appends = ['news_share_bottom_image_full_path', 'news_share_bottom_image_gujarati_full_path', 'watermark_image_full_path'];

    public function getNewsShareBottomImageFullPathAttribute($value)
    {
        return $this->news_share_bottom_image !== null? asset('/storage/settings/' . $this->news_share_bottom_image): null;
    }

    public function getNewsShareBottomImageGujaratiFullPathAttribute($value)
    {        
        return $this->news_share_bottom_image_gujarati !== null? asset('/storage/settings/' . $this->news_share_bottom_image_gujarati): null;
    }

    public function getWatermarkImageFullPathAttribute($value)
    {        
        return $this->watermark_image !== null? asset('/storage/settings/' . $this->watermark_image): null;
    }
}
