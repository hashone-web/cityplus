<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Kyslik\ColumnSortable\Sortable;

class State extends Model
{
    use SoftDeletes, Sortable;

    public $sortable = [
        'name',
        'created_at'
    ];

    public function country()
    {
        return $this->belongsTo('App\Country', 'country_id');
    }
}
