<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Kyslik\ColumnSortable\Sortable;

class Tag extends Model
{
    use SoftDeletes, Sortable;

    public $sortable = [
        'name',
        'created_at'
    ];
}
