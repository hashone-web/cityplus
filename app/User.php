<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Kyslik\ColumnSortable\Sortable;
use Laravel\Passport\HasApiTokens;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable
{
    use HasRoles, HasApiTokens, Notifiable, SoftDeletes, Sortable;

    public $sortable = [
        'id',
        'name',
        'alternate_name',
        'mobile_number',
        'status',
        'created_at'
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'mobile_number', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    protected $appends = ['profile_picture_link', 'original_profile_picture_link', 'reporting_cities_ids'];

    public function getProfilePictureLinkAttribute()
    {
        return $this->profile_picture !== null ? asset('/storage/users/' . $this->id . '/profile_picture/128/' . $this->profile_picture) : asset('/assets/img/user/avatar-2.jpg');
    }

    public function getOriginalProfilePictureLinkAttribute()
    {
        return $this->profile_picture !== null ? asset('/storage/users/' . $this->id . '/profile_picture/' . $this->profile_picture) : asset('/assets/images/user/avatar-2.jpg');
    }

    public function setPasswordAttribute($password)
    {
        if (!empty($password))
        {
            $this->attributes['password'] = bcrypt($password);
        }
    }

    public function reporting_cities()
    {
        return $this->belongsToMany('App\City', 'user_cities', 'user_id', 'city_id');
    }

    public function news()
    {
        return $this->belongsToMany('App\News', 'news_users', 'user_id', 'news_id')->where('news.status', 'Published');
    }

    public function getReportingCitiesIdsAttribute()
    {
        return $this->reporting_cities()->pluck('city_id');
    }
}
