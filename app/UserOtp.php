<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserOtp extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'otp', 'status',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'otp' => 'integer',
    ];
}
