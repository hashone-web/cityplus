<?php

return [
    "project" => [
        "title" => "CityPlus",
        "description" => "CityPlus - Local News",
        "author" => "CityPlus"
    ],
    "pagination_size" => 20,
    "thumb_size" => "515px",
    "notification_types" => [
        "Silent" => 3,
        "Wake" => 2,
        "Vibrate & Wake" => 1,
        "Sound & Wake" => 0,
    ],
    "categories" => [
        "production" => [
            'feed' => 10,
            'trending' => 11
        ],
        "test" => [
            'feed' => 3,
            'trending' => 9
        ],
        "local" => [
            'feed' => 18,
            'trending' => 19
        ]
    ]
];
