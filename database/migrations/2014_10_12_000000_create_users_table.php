<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name')->nullable();
            $table->string('mobile_number')->unique();
            $table->timestamp('mobile_number_verified_at')->nullable();
            $table->string('password')->nullable();
            $table->string('profile_picture')->nullable();
            $table->enum('status', ['Pending', 'Active', 'Inactive', 'Rejected', 'Blocked'])->default('Pending');
            $table->enum('type', ['Admin', 'Application'])->default('Application');
            $table->rememberToken();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
