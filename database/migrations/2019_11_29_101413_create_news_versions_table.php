<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNewsVersionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('news_versions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('headline');
            $table->longText('article');
            $table->enum('status', ['Draft', 'Pending', 'Published', 'Unpublished'])->default('Pending');
            $table->float('version', 8, 1)->default(1);
            $table->json('source')->nullable();
            $table->boolean('is_breaking')->default(false);
            $table->unsignedBigInteger('category_id');
            $table->unsignedBigInteger('city_id');
            $table->unsignedBigInteger('locality_id');
            $table->unsignedBigInteger('news_id');
            $table->timestamps();

            $table->foreign('category_id')
                ->references('id')->on('categories')
                ->onDelete('cascade');

            $table->foreign('city_id')
                ->references('id')->on('cities')
                ->onDelete('cascade');

            $table->foreign('locality_id')
                ->references('id')->on('localities')
                ->onDelete('cascade');

            $table->foreign('news_id')
                ->references('id')->on('news')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('news_versions');
    }
}
