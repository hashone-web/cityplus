<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class MakeNameColumnsUniqueInCruds extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('countries', function (Blueprint $table) {
            $table->unique('name');
        });

        Schema::table('states', function (Blueprint $table) {
            $table->unique('name');
        });

        Schema::table('districts', function (Blueprint $table) {
            $table->unique('name');
        });

        Schema::table('cities', function (Blueprint $table) {
            $table->unique('name');
        });

        Schema::table('localities', function (Blueprint $table) {
            $table->unique('name');
        });

        Schema::table('categories', function (Blueprint $table) {
            $table->unique('name');
        });

        Schema::table('tags', function (Blueprint $table) {
            $table->unique('name');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('countries', function (Blueprint $table) {
            $table->dropUnique('name');
        });

        Schema::table('states', function (Blueprint $table) {
            $table->dropUnique('name');
        });

        Schema::table('districts', function (Blueprint $table) {
            $table->dropUnique('name');
        });

        Schema::table('cities', function (Blueprint $table) {
            $table->dropUnique('name');
        });

        Schema::table('localities', function (Blueprint $table) {
            $table->dropUnique('name');
        });

        Schema::table('categories', function (Blueprint $table) {
            $table->dropUnique('name');
        });

        Schema::table('tags', function (Blueprint $table) {
            $table->dropUnique('name');
        });
    }
}
