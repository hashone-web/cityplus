<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddTypeAndDetailsAndExtensionAndParentIdToNewsMedia extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('news_media', function (Blueprint $table) {
            $table->string('type')->after('mimetype');
            $table->json('details')->after('type')->nullable();
            $table->string('extension')->after('type');
            $table->unsignedBigInteger('parent_id')->nullable()->after('sort');

            $table->foreign('parent_id')
                ->references('id')->on('news_media')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('news_media', function (Blueprint $table) {
            $table->dropColumn('type');
            $table->dropColumn('extension');
            $table->dropColumn('parent_id');
        });
    }
}
