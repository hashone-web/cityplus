<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddAndroidShareAppLinkAndContactEmailToSettings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('settings', function (Blueprint $table) {
            $table->string('contact_email')->nullable();
            $table->string('android_share_app_link')->nullable();
            $table->string('ios_share_app_link')->nullable();
            $table->string('privacy_policy_page_link')->nullable();
            $table->string('terms_of_use_page_link')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('settings', function (Blueprint $table) {
            $table->dropColumn('contact_email');
            $table->dropColumn('android_share_app_link');
            $table->dropColumn('ios_share_app_link');
            $table->dropColumn('privacy_policy_page_link');
            $table->dropColumn('terms_of_use_page_link');
        });
    }
}
