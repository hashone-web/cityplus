<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddQrCodeAndShareDescriptionToSettings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('settings', function (Blueprint $table) {
            $table->string('android_qr_code_link')->nullable();
            $table->string('ios_qr_code_link')->nullable();
            $table->text('android_share_description')->nullable();
            $table->text('ios_share_description')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('settings', function (Blueprint $table) {
            $table->dropColumn('android_qr_code_link');
            $table->dropColumn('ios_qr_code_link');
            $table->dropColumn('android_share_description');
            $table->dropColumn('ios_share_description');
        });
    }
}
