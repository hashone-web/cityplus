<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddAdditionalTimestampsToNews extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('news', function (Blueprint $table) {
            $table->timestamp('notification_datetime')->nullable();
            $table->timestamp('is_breaking_datetime')->nullable();
            $table->timestamp('is_featured_datetime')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('news', function (Blueprint $table) {
            $table->dropColumn('notification_datetime');
            $table->dropColumn('is_breaking_datetime');
            $table->dropColumn('is_featured_datetime');
        });
    }
}
