<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCoronaVirusDataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('corona_virus_data', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('gujarati_name')->nullable();
            $table->bigInteger('total')->default(0);
            $table->bigInteger('active')->default(0);
            $table->bigInteger('recovered')->default(0);
            $table->bigInteger('deaths')->default(0);
            $table->bigInteger('delta_total')->default(0);
            $table->bigInteger('delta_active')->default(0);
            $table->bigInteger('delta_recovered')->default(0);
            $table->bigInteger('delta_deaths')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('corona_virus_data');
    }
}
