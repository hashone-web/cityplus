<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddManualToCoronaVirusData extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('corona_virus_data', function (Blueprint $table) {
            $table->boolean('manual')->default(false)->after('delta_deaths');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('corona_virus_data', function (Blueprint $table) {
            $table->dropColumn('manual');
        });
    }
}
