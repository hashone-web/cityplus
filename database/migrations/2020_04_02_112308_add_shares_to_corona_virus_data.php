<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddSharesToCoronaVirusData extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('corona_virus_data', function (Blueprint $table) {
            $table->bigInteger('shares')->default(0)->after('manual');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('corona_virus_data', function (Blueprint $table) {
            $table->dropColumn('shares');
        });
    }
}
