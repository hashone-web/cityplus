<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddParentIdToCoronaVirusData extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('corona_virus_data', function (Blueprint $table) {
            $table->unsignedBigInteger('parent_id')->nullable()->after('gujarati_name');

            $table->foreign('parent_id')
                ->references('id')->on('corona_virus_data')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('corona_virus_data', function (Blueprint $table) {
            \DB::statement('SET FOREIGN_KEY_CHECKS = 0');
            $table->dropColumn('parent_id');
            \DB::statement('SET FOREIGN_KEY_CHECKS = 1');
        });
    }
}
