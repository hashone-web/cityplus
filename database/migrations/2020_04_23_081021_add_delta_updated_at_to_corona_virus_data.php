<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddDeltaUpdatedAtToCoronaVirusData extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('corona_virus_data', function (Blueprint $table) {
            $table->date('delta_total_updated_at')->nullable()->after('delta_total');
            $table->date('delta_recovered_updated_at')->nullable()->after('delta_recovered');
            $table->date('delta_deaths_updated_at')->nullable()->after('delta_deaths');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('corona_virus_data', function (Blueprint $table) {
            $table->dropColumn('delta_total_updated_at');
            $table->dropColumn('delta_recovered_updated_at');
            $table->dropColumn('delta_deaths_updated_at');
        });
    }
}
