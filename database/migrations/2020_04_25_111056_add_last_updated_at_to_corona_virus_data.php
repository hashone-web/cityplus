<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddLastUpdatedAtToCoronaVirusData extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('corona_virus_data', function (Blueprint $table) {
            $table->datetime('last_updated_at')->nullable()->after('delta_deaths');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('corona_virus_data', function (Blueprint $table) {
            $table->dropColumn('last_updated_at');
        });
    }
}
