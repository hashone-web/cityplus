<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNewsStatisticsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('news_statistics', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('media_views')->default(0);
            $table->bigInteger('media_downloads')->default(0);
            $table->unsignedBigInteger('news_id');
            $table->timestamps();

            $table->foreign('news_id')
                ->references('id')->on('news')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('news_statistics');
    }
}
