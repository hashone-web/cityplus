<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddIsIncreasedToMarketValues extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('market_values', function (Blueprint $table) {
            $table->boolean('is_increased')->default(true)->after('current_date');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('market_values', function (Blueprint $table) {
            $table->dropColumn('is_increased');
        });
    }
}
