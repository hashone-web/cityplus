<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddIndexsToNewsShares extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('news_shares', function (Blueprint $table) {
            $table->index('news_id');
            $table->index('device_id');
            $table->index('type');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('news_shares', function (Blueprint $table) {
            $table->dropIndex(['news_id']);
            $table->dropIndex(['device_id']);
            $table->dropIndex(['type']);
        });
    }
}
