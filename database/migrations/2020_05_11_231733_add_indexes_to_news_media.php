<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddIndexesToNewsMedia extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('news_media', function (Blueprint $table) {
            $table->index('news_id');
            $table->index('parent_id');
            $table->index('type');
            $table->index('extension');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('news_media', function (Blueprint $table) {
            $table->dropIndex(['news_id']);
            $table->dropIndex(['parent_id']);
            $table->dropIndex(['type']);
            $table->dropIndex(['extension']);
        });
    }
}
