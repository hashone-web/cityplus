<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdminPanelSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admin_panel_settings', function (Blueprint $table) {
            $table->bigIncrements('id');
            // IsFeatured by Shares
            $table->integer('is_featured_shares_scan_period_1')->nullable();
            $table->integer('is_featured_shares_count_1')->nullable();
            $table->integer('is_featured_shares_scan_period_2')->nullable();
            $table->integer('is_featured_shares_count_2')->nullable();
            $table->integer('is_featured_shares_scan_period_3')->nullable();
            $table->integer('is_featured_shares_count_3')->nullable();

            // IsFeatured by Reactions
            $table->integer('is_featured_reactions_scan_period_1')->nullable();
            $table->integer('is_featured_reactions_count_1')->nullable();
            $table->integer('is_featured_reactions_scan_period_2')->nullable();
            $table->integer('is_featured_reactions_count_2')->nullable();
            $table->integer('is_featured_reactions_scan_period_3')->nullable();
            $table->integer('is_featured_reactions_count_3')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admin_panel_settings');
    }
}
