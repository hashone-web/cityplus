<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePollsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('polls', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title');
            $table->string('image')->nullable();
            $table->text('description')->nullable();
            $table->datetime('start_datetime');
            $table->datetime('end_datetime');
            $table->text('policy')->nullable();
            $table->boolean('allow_multiple_options')->default(false);
            $table->enum('show_result', ['Never', 'After Poll End', 'After User Vote', 'Always'])->default('Never');
            $table->datetime('result_end_datetime')->nullable();
            $table->boolean('only_for_registered_user')->default(false);
            $table->bigInteger('votes')->default(0);
            $table->bigInteger('shares')->default(0);
            $table->enum('status', ['Pending', 'Active', 'Inactive'])->default('Pending');
            $table->enum('type', ['Standalone', 'News']);
            $table->integer('position')->nullable();
            $table->unsignedBigInteger('news_id')->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('news_id')
                ->references('id')->on('news')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('polls');
    }
}
