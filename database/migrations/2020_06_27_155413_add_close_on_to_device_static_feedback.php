<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddCloseOnToDeviceStaticFeedback extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('device_static_feedback', function (Blueprint $table) {
            $table->enum('close_on', ['Feedback', 'Yes', 'No'])->nullable()->after('device_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('device_static_feedback', function (Blueprint $table) {
            $table->dropColumn('close_on');
        });
    }
}
