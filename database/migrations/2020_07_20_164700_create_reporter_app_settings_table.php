<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReporterAppSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reporter_app_settings', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('login_not_allowed_message')->nullable();
            $table->text('privacy_policy_page_link')->nullable();
            $table->text('terms_of_use_page_link')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reporter_app_settings');
    }
}
