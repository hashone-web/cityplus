<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReporterAppUpdatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reporter_app_updates', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('app_ver')->nullable();
            $table->text('link')->nullable();
            $table->text('link_text')->nullable();
            $table->boolean('force')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reporter_app_updates');
    }
}
