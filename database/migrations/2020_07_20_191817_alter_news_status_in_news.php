<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterNewsStatusInNews extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('news', function (Blueprint $table) {
            \DB::statement("ALTER TABLE news CHANGE COLUMN status status ENUM('Draft', 'Pending', 'Published', 'Unpublished', 'Rejected') NOT NULL DEFAULT 'Pending'");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('news', function (Blueprint $table) {
            \DB::statement("ALTER TABLE news CHANGE COLUMN status status ENUM('Draft', 'Pending', 'Published', 'Unpublished') NOT NULL DEFAULT 'Pending'");
        });
    }
}
