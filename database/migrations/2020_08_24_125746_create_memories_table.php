<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMemoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('memories', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->enum('type', ['remembrance', 'obituary', 'tribute']);
            $table->text('heading');
            $table->string('name_prefix');
            $table->string('name');
            $table->string('photo');
            $table->date('death_date');
            $table->date('birth_date')->nullable();
            $table->text('details');
            $table->text('venue')->nullable();
            $table->datetime('datetime')->nullable();
            $table->text('address')->nullable();
            $table->text('special_notes')->nullable();
            $table->text('highlighting_notes')->nullable();
            $table->boolean('is_featured')->default(0);
            $table->datetime('publish_datetime');
            $table->datetime('end_datetime');
            $table->enum('status', ['Published', 'Unpublished'])->default('Published');
            $table->integer('frame_type');
            $table->unsignedBigInteger('memory_agency_id')->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('memory_agency_id')
                ->references('id')->on('memory_agencies')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('memories');
    }
}
