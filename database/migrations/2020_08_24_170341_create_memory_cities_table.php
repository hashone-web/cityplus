<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMemoryCitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('memory_cities', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('memory_id');
            $table->unsignedBigInteger('city_id');
            $table->timestamps();

            $table->foreign('memory_id')
                ->references('id')->on('memories')
                ->onDelete('cascade');

            $table->foreign('city_id')
                ->references('id')->on('cities')
                ->onDelete('cascade');    
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('memory_cities');
    }
}
