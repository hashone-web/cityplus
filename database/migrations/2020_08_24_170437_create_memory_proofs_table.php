<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMemoryProofsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('memory_proofs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('original_name');
            $table->string('dynamic_id');
            $table->string('mimetype');
            $table->bigInteger('size');
            $table->integer('sort');
            $table->unsignedBigInteger('memory_id');
            $table->timestamps();

            $table->foreign('memory_id')
                ->references('id')->on('memories')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('memory_proofs');
    }
}
