<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMemorySettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('memory_settings', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('confirmation_message_text')->nullable();
            $table->string('confirmation_message_text_gujarati')->nullable();
            $table->string('share_bottom_image')->nullable();
            $table->string('share_bottom_image_gujarati')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('memory_settings');
    }
}
