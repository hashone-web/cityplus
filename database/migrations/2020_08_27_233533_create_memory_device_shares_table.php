<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMemoryDeviceSharesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('memory_device_shares', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('memory_id');
            $table->unsignedBigInteger('device_id');
            $table->enum('type', ['Whatsapp', 'Other'])->default('Other');
            $table->timestamps();

            $table->foreign('memory_id')
                ->references('id')->on('memories')
                ->onDelete('cascade');

            $table->foreign('device_id')
                ->references('id')->on('devices')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('memory_device_shares');
    }
}
