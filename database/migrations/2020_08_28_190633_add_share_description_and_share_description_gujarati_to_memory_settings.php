<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddShareDescriptionAndShareDescriptionGujaratiToMemorySettings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('memory_settings', function (Blueprint $table) {
            $table->text('share_description')->nullable();
            $table->text('share_description_gujarati')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('memory_settings', function (Blueprint $table) {
            $table->dropColumn('share_description');
            $table->dropColumn('share_description_gujarati');
        });
    }
}
