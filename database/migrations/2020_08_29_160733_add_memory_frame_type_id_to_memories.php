<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddMemoryFrameTypeIdToMemories extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('memories', function (Blueprint $table) {
            $table->unsignedBigInteger('memory_frame_type_id')->nullable();

            $table->foreign('memory_frame_type_id')
                ->references('id')->on('memory_frame_types')
                ->onDelete('cascade');

            $table->dropColumn('frame_type');    
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('memories', function (Blueprint $table) {
            $table->dropForeign('memories_memory_frame_type_id_foreign');
            $table->dropColumn('memory_frame_type_id');
            $table->integer('frame_type');
        });
    }
}
