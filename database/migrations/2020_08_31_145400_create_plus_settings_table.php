<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePlusSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('plus_settings', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->boolean('horoscope')->default(true);
            $table->boolean('death_notes')->default(true);
            $table->boolean('bank_fd_rates')->default(true);
            $table->boolean('useful_information')->default(true);
            $table->boolean('saved_news')->default(true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('plus_settings');
    }
}
