<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddCountsToCampaigns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('campaigns', function (Blueprint $table) {
            $table->bigInteger('impressions_count')->default(0);
            $table->bigInteger('clicks_count')->default(0);
            $table->bigInteger('reactions_count')->default(0);
            $table->bigInteger('shares_count')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('campaigns', function (Blueprint $table) {
            $table->dropColumn('impressions_count');
            $table->dropColumn('clicks_count');
            $table->dropColumn('reactions_count');
            $table->dropColumn('shares_count');
        });
    }
}
