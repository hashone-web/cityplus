<?php

use App\CoronaVirusData;
use Illuminate\Database\Seeder;

class CoronaVirusDataTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        CoronaVirusData::insert([
            // [
            //     'name' => 'world',
            //     'gujarati_name' => 'દુનિયા',
            //     'parent_id' => null,
            //     'created_at' => Carbon::now(),
            //     'updated_at' => Carbon::now()
            // ],
            // [
            //     'name' => 'india',
            //     'gujarati_name' => 'ભારત',
            //     'parent_id' => null,
            //     'created_at' => Carbon::now(),
            //     'updated_at' => Carbon::now()
            // ],
            // [
            //     'name' => 'gujarat',
            //     'gujarati_name' => 'ગુજરાત',
            //     'parent_id' => null,
            //     'created_at' => Carbon::now(),
            //     'updated_at' => Carbon::now()
            // ],
            [
                'name' => 'Rajkot',
                'gujarati_name' => 'રાજકોટ',
                'parent_id' => 3,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'name' => 'Surat',
                'gujarati_name' => 'સુરત',
                'parent_id' => 3,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'name' => 'Ahmedabad',
                'gujarati_name' => 'અમદાવાદ',
                'parent_id' => 3,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'name' => 'Vadodara',
                'gujarati_name' => 'વડોદરા',
                'parent_id' => 3,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'name' => 'Gandhinagar',
                'gujarati_name' => 'ગાંધીનગર',
                'parent_id' => 3,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'name' => 'Kutch',
                'gujarati_name' => 'કચ્છ',
                'parent_id' => 3,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'name' => 'Mehsana',
                'gujarati_name' => 'મહેસાણા',
                'parent_id' => 3,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'name' => 'Bhavnagar',
                'gujarati_name' => 'ભાવનગર',
                'parent_id' => 3,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'name' => 'Porbandar',
                'gujarati_name' => 'પોરબંદર',
                'parent_id' => 3,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'name' => 'Gir Somnath',
                'gujarati_name' => 'ગીર સોમનાથ',
                'parent_id' => 3,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'name' => 'Panchmahal',
                'gujarati_name' => 'પંચમહાલ',
                'parent_id' => 3,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'name' => 'Patan',
                'gujarati_name' => 'પાટણ',
                'parent_id' => 3,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'name' => 'Chhota Udaipur',
                'gujarati_name' => 'છોટા ઉદેપુર',
                'parent_id' => 3,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'name' => 'Jamnagar',
                'gujarati_name' => 'જામનગર',
                'parent_id' => 3,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'name' => 'Morbi',
                'gujarati_name' => 'મોરબી',
                'parent_id' => 3,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'name' => 'Anand',
                'gujarati_name' => 'આણંદ',
                'parent_id' => 3,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'name' => 'Sabarkantha',
                'gujarati_name' => 'સાબરકાંઠા',
                'parent_id' => 3,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'name' => 'Dahod',
                'gujarati_name' => 'દાહોદ',
                'parent_id' => 3,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'name' => 'Bharuch',
                'gujarati_name' => 'ભરૂચ',
                'parent_id' => 3,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'name' => 'Banaskantha',
                'gujarati_name' => 'બનાસકાંઠા',
                'parent_id' => 3,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'name' => 'Botad',
                'gujarati_name' => 'બોટાદ',
                'parent_id' => 3,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'name' => 'Kheda',
                'gujarati_name' => 'ખેડા',
                'parent_id' => 3,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'name' => 'Narmada',
                'gujarati_name' => 'નર્મદા',
                'parent_id' => 3,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'name' => 'Amreli',
                'gujarati_name' => 'અમરેલી',
                'parent_id' => 3,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'name' => 'Aravalli',
                'gujarati_name' => 'અરવલ્લી',
                'parent_id' => 3,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'name' => 'Dang',
                'gujarati_name' => 'ડાંગ',
                'parent_id' => 3,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'name' => 'Devbhoomi Dwarka',
                'gujarati_name' => 'દેવભૂમિ દ્વારકા',
                'parent_id' => 3,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'name' => 'Junagadh',
                'gujarati_name' => 'જૂનાગઢ',
                'parent_id' => 3,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'name' => 'Mahisagar',
                'gujarati_name' => 'મહીસાગર',
                'parent_id' => 3,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'name' => 'Navsari',
                'gujarati_name' => 'નવસારી',
                'parent_id' => 3,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'name' => 'Surendranagar',
                'gujarati_name' => 'સુરેન્દ્રનગર',
                'parent_id' => 3,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'name' => 'Tapi',
                'gujarati_name' => 'તાપી',
                'parent_id' => 3,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'name' => 'Valsad',
                'gujarati_name' => 'વલસાડ',
                'parent_id' => 3,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
        ]);
    }
}
