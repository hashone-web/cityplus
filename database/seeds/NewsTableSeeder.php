<?php

use App\News;
use App\NewsUser;
use Illuminate\Database\Seeder;

class NewsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        News::insert([
            [
                'headline' => 'રાજકોટ ક્રાઈમ બ્રાન્ચે 31 ડિસેમ્બર પહેલા લાખો ની કિંમત નો દારૂ નો જથ્થો ઝડપી પાડ્યો છે.',
                'article' => 'રાજકોટ ક્રાઈમ બ્રાન્ચે 31 ડિસેમ્બર પહેલા લાખો ની કિંમત નો દારૂ નો જથ્થો ઝડપી પાડ્યો છે. પોલોસે ચોક્કસ બાતમીને આધારે ગવરીદળ ગામ પાસેથી 29 લાખ થી વધુ નો દારૂ ઝડપી બે શખ્સોની ધરપકડ કરી છે. 31 ડિસેમ્બર નજીક આવી રહી છે ત્યારે લોકો ને નશામાં દુબળવાનો પ્લાન જાણેકે પોલિસે નિષફળ કરી નાખ્યો છે. પોલોસે ચોકસ બાતમીને આધારે ગવરીદળ ગામ પાસેથી લાખોની કિંમત નો વિદેશી દારૂ ઝળપી પાડ્યો છે.',
                'status' => 'Published',
                'is_breaking' => true,
                'category_id' => 1,
                'city_id' => 1,
                'created_by' => 1,
                'updated_by' => 1,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
        ]);

        NewsUser::insert([
            [
                'news_id' => 1,
                'user_id' => 1,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ]
        ]);

        News::insert([
            [
                'headline' => 'ભારતની અંતરીક્ષમાં વધુ એક સિદ્ધિ. ઈસરોએ લોન્ચ કર્યા 5 દેશોના 9 સેટેલાઈટ અને રીસેટ-2બીઆર1 સેટેલાઈટ પણ લોન્ચ કરાયો.',
                'article' => 'ISRO :- All 9 customer satellites(from Israel, Italy, Japan and USA) & #RISAT2BR1 successfully placed in their designated orbit by #PSLVC48. ભારતની અંતરીક્ષમાં વધુ એક સિદ્ધિ. ઈસરોએ લોન્ચ કર્યા 5 દેશોના 9 સેટેલાઈટ અને રીસેટ-2બીઆર1 સેટેલાઈટ પણ લોન્ચ કરાયો.',
                'status' => 'Draft',
                'is_breaking' => true,
                'category_id' => 2,
                'city_id' => 3,
                'created_by' => 2,
                'updated_by' => 2,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
        ]);

        NewsUser::insert([
            [
                'news_id' => 2,
                'user_id' => 4,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ]
        ]);

        News::insert([
            [
                'headline' => 'મોટામૌવા, મુંજકા, ઘંટેશ્વર, માધાપર અને મનહરપુર-1 ગામનો રાજકોટ મહાનગર પાલિકામાં કરાશે સમાવેશ.',
                'article' => 'મોટામૌવા, મુંજકા, ઘંટેશ્વર, માધાપર અને મનહરપુર-1 ગામનો રાજકોટ મહાનગર પાલિકામાં કરાશે સમાવેશ.18 ડિસેમ્બરે જનરલ બોર્ડમાં લેવાશે આખરી નિર્ણય.:-મેયર બીનાબેન આચાર્ય.',
                'status' => 'Pending',
                'is_breaking' => true,
                'category_id' => 3,
                'city_id' => 4,
                'created_by' => 3,
                'updated_by' => 3,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
        ]);

        NewsUser::insert([
            [
                'news_id' => 3,
                'user_id' => 3,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ]
        ]);

        News::insert([
            [
                'headline' => 'મોટામૌવા, મુંજકા, ઘંટેશ્વર, માધાપર અને મનહરપુર-1 ગામનો રાજકોટ મહાનગર પાલિકામાં કરાશે સમાવેશ.',
                'article' => 'મોટામૌવા, મુંજકા, ઘંટેશ્વર, માધાપર અને મનહરપુર-1 ગામનો રાજકોટ મહાનગર પાલિકામાં કરાશે સમાવેશ.18 ડિસેમ્બરે જનરલ બોર્ડમાં લેવાશે આખરી નિર્ણય.:-મેયર બીનાબેન આચાર્ય.',
                'status' => 'Unpublished',
                'is_breaking' => true,
                'category_id' => 3,
                'city_id' => 4,
                'created_by' => 3,
                'updated_by' => 3,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
        ]);

        NewsUser::insert([
            [
                'news_id' => 4,
                'user_id' => 3,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ]
        ]);
    }
}
