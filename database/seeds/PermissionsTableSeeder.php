<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Roles and Permissions
        Permission::create(['name' => 'edit roles']);
        Permission::create(['name' => 'edit permissions']);

        // Users
        Permission::create(['name' => 'create users']);
        Permission::create(['name' => 'view users']);
        Permission::create(['name' => 'edit users']);
        Permission::create(['name' => 'delete users']);

        // News
        Permission::create(['name' => 'create news']);
        Permission::create(['name' => 'view news']);
        Permission::create(['name' => 'view all news']);
        Permission::create(['name' => 'edit news']);
        Permission::create(['name' => 'edit all news']);
        Permission::create(['name' => 'delete news']);

        // Locations
        Permission::create(['name' => 'create locations']);
        Permission::create(['name' => 'view locations']);
        Permission::create(['name' => 'edit locations']);
        Permission::create(['name' => 'delete locations']);

        // Other CRUDs
        Permission::create(['name' => 'create other cruds']);
        Permission::create(['name' => 'view other cruds']);
        Permission::create(['name' => 'edit other cruds']);
        Permission::create(['name' => 'delete other cruds']);

        // Other
        Permission::create(['name' => 'view states']);

        Permission::create(['name' => 'publish news']);

        Permission::create(['name' => 'edit settings']);

        Permission::create(['name' => 'edit corona virus data']);

        // Polls CRUDs
        Permission::create(['name' => 'create polls']);
        Permission::create(['name' => 'view polls']);
        Permission::create(['name' => 'edit polls']);
        Permission::create(['name' => 'delete polls']);

        // Ads CRUDs
        Permission::create(['name' => 'advertisements crud']);

        // Memories CRUDs
        Permission::create(['name' => 'memories crud']);

        // Quotes CRUDs
        Permission::create(['name' => 'quotes crud']);

        // Plus CRUDs
        Permission::create(['name' => 'plus crud']);

        // User Reports
        Permission::create(['name' => 'view user reports']);

        Permission::create(['name' => 'view useful info']);
    }
}
