<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Admin
        $super_admin_role = Role::create(['name' => 'Super Admin']);

        // Admin
        $admin_role = Role::create(['name' => 'Admin']);
        $admin_role->syncPermissions([
            // Roles and Permissions
            'edit roles',
            'edit permissions',

            // Users
            'create users',
            'view users',
            'edit users',
            'delete users',

            // News
            'create news',
            'view news',
            'edit news',
            'delete news',

            // Locations
            'create locations',
            'view locations',
            'edit locations',
            'delete locations',

            // Other CRUDs
            'create other cruds',
            'view other cruds',
            'edit other cruds',
            'delete other cruds',

            // States
            'view states',
        ]);


        // Moderator
        $moderator_role = Role::create(['name' => 'Moderator']);
        $moderator_role->syncPermissions([
            // News
            'create news',
            'view news',
            'edit news',
            'delete news',

            // Locations
            'create locations',
            'view locations',
            'edit locations',
            'delete locations',

            // Other CRUDs
            'create other cruds',
            'view other cruds',
            'edit other cruds',
            'delete other cruds',
        ]);


        // Content Creator
        $content_creator = Role::create(['name' => 'Content Creator']);
        $content_creator->syncPermissions([
            // News
            'create news',
            'view news',
            'edit news',
            'delete news',

            // Locations
            'create locations',
            'view locations',
            'edit locations',
            'delete locations',

            // Other CRUDs
            'create other cruds',
            'view other cruds',
            'edit other cruds',
            'delete other cruds',
        ]);


        // Reporter
        $reporter_role = Role::create(['name' => 'Reporter']);
        $reporter_role->syncPermissions([
            // News
            'create news',
            'view news',
            'edit news',
            'delete news',
        ]);

        // User
        $user_role = Role::create(['name' => 'User']);
    }
}
