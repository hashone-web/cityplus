<?php

use App\Setting;
use Illuminate\Database\Seeder;

class SettingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $setting = Setting::firstOrNew(['id' => 1]);
        $setting->contact_whatsapp_number = '+918866553779';
        $setting->contact_email = 'jhalakjaviya@gmail.com';
        $setting->android_share_app_link = 'https://play.google.com/store/apps/details?id=com.cityplus.local.news.info';
        $setting->ios_share_app_link = '';
        $setting->privacy_policy_page_link = 'https://cityplus.co.in/pages/privacy-policy';
        $setting->terms_of_use_page_link = 'https://cityplus.co.in/pages/terms-of-use';
        $setting->save();
    }
}
