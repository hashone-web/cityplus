<?php

use App\UserCity;
use Illuminate\Database\Seeder;
use App\User;
use Carbon\Carbon;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::insert([
            [
                'name' => 'Jhalak Javiya',
                'mobile_number' => '8866553779',
                'password'	=> bcrypt('zncjK9Qax3CMFVtt'),
                'status' => 'Active',
                'type' => 'Admin',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'name' => 'Kapeel Patel',
                'mobile_number' => '8000880004',
                'password' => bcrypt('KzCL3cgU8p78Kvxz'),
                'status' => 'Active',
                'type' => 'Admin',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'name' => 'Ketan Moridhara',
                'mobile_number' => '9099454950',
                'password' => bcrypt('SEsfZsZURK6Z3qJY'),
                'status' => 'Active',
                'type' => 'Admin',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'name' => 'Harsh Bhut',
                'mobile_number' => '8238342405',
                'password' => bcrypt('zjcAJxcR5Rgaftcj'),
                'status' => 'Active',
                'type' => 'Admin',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'name' => 'Amisha Nena',
                'mobile_number' => '8128917008',
                'password' => bcrypt('sqATfj95UpvKZDXZ'),
                'status' => 'Active',
                'type' => 'Admin',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'name' => 'Binita Gajera',
                'mobile_number' => '9898575531',
                'password' => bcrypt('fZQRP4r8DjSyWErC'),
                'status' => 'Active',
                'type' => 'Admin',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
        ]);

        $user = User::where('mobile_number', '8866553779')->first();
        $user->assignRole('Super Admin');

        $user = User::where('mobile_number', '8000880004')->first();
        $user->assignRole('Super Admin');

        $user = User::where('mobile_number', '9099454950')->first();
        $user->assignRole('Admin');

        $user = User::where('mobile_number', '8238342405')->first();
        $user->assignRole('Moderator');

        $user = User::where('mobile_number', '8128917008')->first();
        $user->assignRole('Content Creator');

        $user = User::where('mobile_number', '9898575531')->first();
        $user->assignRole('Reporter');

        UserCity::insert([
            [
                'user_id' => 6,
                'city_id' => 1,
                'as_reporter' => true,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ]
        ]);

        UserCity::insert([
            [
                'user_id' => 6,
                'city_id' => 3,
                'as_reporter' => true,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ]
        ]);

        UserCity::insert([
            [
                'user_id' => 6,
                'city_id' => 5,
                'as_reporter' => true,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ]
        ]);
    }
}
