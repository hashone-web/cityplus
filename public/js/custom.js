/*!
 * StickyTableHeaders 0.1.24 (2018-01-14 23:29)
 * MIT licensed
 * Copyright (C) Jonas Mosbech - https://github.com/jmosbech/StickyTableHeaders
 */
!function(e,i,t){"use strict";var o="stickyTableHeaders",n=0,d={fixedOffset:0,leftOffset:0,marginTop:0,objDocument:document,objHead:"head",objWindow:i,scrollableArea:i,cacheHeaderHeight:!1,zIndex:3};e.fn[o]=function(t){return this.each(function(){var l=e.data(this,"plugin_"+o);l?"string"==typeof t?l[t].apply(l):l.updateOptions(t):"destroy"!==t&&e.data(this,"plugin_"+o,new function(t,l){var a=this;a.$el=e(t),a.el=t,a.id=n++,a.$el.bind("destroyed",e.proxy(a.teardown,a)),a.$clonedHeader=null,a.$originalHeader=null,a.cachedHeaderHeight=null,a.isSticky=!1,a.hasBeenSticky=!1,a.leftOffset=null,a.topOffset=null,a.init=function(){a.setOptions(l),a.$el.each(function(){var i=e(this);i.css("padding",0),a.$originalHeader=e("thead:first",this),a.$clonedHeader=a.$originalHeader.clone(),i.trigger("clonedHeader."+o,[a.$clonedHeader]),a.$clonedHeader.addClass("tableFloatingHeader"),a.$clonedHeader.css({display:"none",opacity:0}),a.$originalHeader.addClass("tableFloatingHeaderOriginal"),a.$originalHeader.after(a.$clonedHeader),a.$printStyle=e('<style type="text/css" media="print">.tableFloatingHeader{display:none !important;}.tableFloatingHeaderOriginal{position:static !important;}</style>'),a.$head.append(a.$printStyle)}),a.$clonedHeader.find("input, select").attr("disabled",!0),a.updateWidth(),a.toggleHeaders(),a.bind()},a.destroy=function(){a.$el.unbind("destroyed",a.teardown),a.teardown()},a.teardown=function(){a.isSticky&&a.$originalHeader.css("position","static"),e.removeData(a.el,"plugin_"+o),a.unbind(),a.$clonedHeader.remove(),a.$originalHeader.removeClass("tableFloatingHeaderOriginal"),a.$originalHeader.css("visibility","visible"),a.$printStyle.remove(),a.el=null,a.$el=null},a.bind=function(){a.$scrollableArea.on("scroll."+o,a.toggleHeaders),a.isWindowScrolling||(a.$window.on("scroll."+o+a.id,a.setPositionValues),a.$window.on("resize."+o+a.id,a.toggleHeaders)),a.$scrollableArea.on("resize."+o,a.toggleHeaders),a.$scrollableArea.on("resize."+o,a.updateWidth)},a.unbind=function(){a.$scrollableArea.off("."+o,a.toggleHeaders),a.isWindowScrolling||(a.$window.off("."+o+a.id,a.setPositionValues),a.$window.off("."+o+a.id,a.toggleHeaders)),a.$scrollableArea.off("."+o,a.updateWidth)},a.debounce=function(e,i){var t=null;return function(){var o=this,n=arguments;clearTimeout(t),t=setTimeout(function(){e.apply(o,n)},i)}},a.toggleHeaders=a.debounce(function(){a.$el&&a.$el.each(function(){var i,t,n,d=e(this),l=a.isWindowScrolling?isNaN(a.options.fixedOffset)?a.options.fixedOffset.outerHeight():a.options.fixedOffset:a.$scrollableArea.offset().top+(isNaN(a.options.fixedOffset)?0:a.options.fixedOffset),s=d.offset(),r=a.$scrollableArea.scrollTop()+l,c=a.$scrollableArea.scrollLeft(),f=a.isWindowScrolling?r>s.top:l>s.top;f&&(t=a.options.cacheHeaderHeight?a.cachedHeaderHeight:a.$clonedHeader.height(),n=(a.isWindowScrolling?r:0)<s.top+d.height()-t-(a.isWindowScrolling?0:l)),f&&n?(i=s.left-c+a.options.leftOffset,a.$originalHeader.css({position:"fixed","margin-top":a.options.marginTop,top:0,left:i,"z-index":a.options.zIndex}),a.leftOffset=i,a.topOffset=l,a.$clonedHeader.css("display",""),a.isSticky||(a.isSticky=!0,a.updateWidth(),d.trigger("enabledStickiness."+o)),a.setPositionValues()):a.isSticky&&(a.$originalHeader.css("position","static"),a.$clonedHeader.css("display","none"),a.isSticky=!1,a.resetWidth(e("td,th",a.$clonedHeader),e("td,th",a.$originalHeader)),d.trigger("disabledStickiness."+o))})},0),a.setPositionValues=a.debounce(function(){var e=a.$window.scrollTop(),i=a.$window.scrollLeft();!a.isSticky||e<0||e+a.$window.height()>a.$document.height()||i<0||i+a.$window.width()>a.$document.width()||a.$originalHeader.css({top:a.topOffset-(a.isWindowScrolling?0:e),left:a.leftOffset-(a.isWindowScrolling?0:i)})},0),a.updateWidth=a.debounce(function(){if(a.isSticky){a.$originalHeaderCells||(a.$originalHeaderCells=e("th,td",a.$originalHeader)),a.$clonedHeaderCells||(a.$clonedHeaderCells=e("th,td",a.$clonedHeader));var i=a.getWidth(a.$clonedHeaderCells);a.setWidth(i,a.$clonedHeaderCells,a.$originalHeaderCells),a.$originalHeader.css("width",a.$clonedHeader.width()),a.options.cacheHeaderHeight&&(a.cachedHeaderHeight=a.$clonedHeader.height())}},0),a.getWidth=function(t){var o=[];return t.each(function(t){var n,d=e(this);if("border-box"===d.css("box-sizing")){var l=d[0].getBoundingClientRect();n=l.width?l.width:l.right-l.left}else if("collapse"===e("th",a.$originalHeader).css("border-collapse"))if(i.getComputedStyle)n=parseFloat(i.getComputedStyle(this,null).width);else{var s=parseFloat(d.css("padding-left")),r=parseFloat(d.css("padding-right")),c=parseFloat(d.css("border-width"));n=d.outerWidth()-s-r-c}else n=d.width();o[t]=n}),o},a.setWidth=function(e,i,t){i.each(function(i){var o=e[i];t.eq(i).css({"min-width":o,"max-width":o})})},a.resetWidth=function(i,t){i.each(function(i){var o=e(this);t.eq(i).css({"min-width":o.css("min-width"),"max-width":o.css("max-width")})})},a.setOptions=function(i){a.options=e.extend({},d,i),a.$window=e(a.options.objWindow),a.$head=e(a.options.objHead),a.$document=e(a.options.objDocument),a.$scrollableArea=e(a.options.scrollableArea),a.isWindowScrolling=a.$scrollableArea[0]===a.$window[0]},a.updateOptions=function(e){a.setOptions(e),a.unbind(),a.bind(),a.updateWidth(),a.toggleHeaders()},a.init()}(this,t))})}}(jQuery,window);

window.poll_options = 0;
window.memory_from_names = 0;

$(document).ready(function () {
    $('ol.sortable').sortable();

    $(document).on("click", ".update-sorting", function (e) {
        e.preventDefault();

        var parent = $("ol.sortable.parent > li");
        var sort = [];
        $.each(parent, function (i, e) {
            var self = $(this);
            var ol = self.find('ol');
            var parent_id = $(this).find('span').text().split('|||')[0];
            parent_id = parent_id.replace(/ /g, '').replace(/\n|\r/g, "");

            sort.push(parent_id)
        });

        sort = JSON.stringify(sort);
        var id = $(this).data('id');

        $.ajax({
            url: '/categories/sorting',
            type: 'POST',
            cache: false,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: {
                sort: sort
            },
            success: function () {
                toastr.success('Category Sorting successfully')
            }
        });
    });

    $('.z-navbar-menu .nav-item.with-sub').hover(function() {
        $(this).find('.navbar-menu-sub').stop(true, true).delay(100).fadeIn(100);
    },
    function() {
        $(this).find('.navbar-menu-sub').stop(true, true).delay(100).fadeOut(100);
    });

    $('.navbar .dropdown.dropdown-profile').hover(function() {
        $(this).find('.dropdown-menu').stop(true, true).delay(100).fadeIn(100);
    },
    function() {
        $(this).find('.dropdown-menu').stop(true, true).delay(100).fadeOut(100);
    });

    $('.table-header-fixed').stickyTableHeaders({fixedOffset: $('.navbar-header')});

    $('#summernote').summernote({
        height: 350
    });

    $('#poll-form #start_datetime').datetimepicker({
        format: 'yyyy-mm-dd hh:ii',
        autoclose: true,
        todayHighlight: true,
        minuteStep: 5,
        startDate: "-30d"
    });

    $('#poll-form #end_datetime').datetimepicker({
        format: 'yyyy-mm-dd hh:ii',
        autoclose: true,
        todayHighlight: true,
        minuteStep: 5,
        startDate: "-30d"
    });

    $('#poll-form #result_end_datetime').datetimepicker({
        format: 'yyyy-mm-dd hh:ii',
        autoclose: true,
        todayHighlight: true,
        minuteStep: 5,
        startDate: "-30d"
    });

    $('#datetime.create_news_datetime').datetimepicker({
        format: 'yyyy-mm-dd hh:ii',
        autoclose: true,
        todayHighlight: true,
        minuteStep: 5,
        startDate: "-30d"
    });

    $('#send_after').datetimepicker({
        format: 'yyyy-mm-dd hh:ii',
        autoclose: true,
        todayHighlight: true,
        minuteStep: 5,
        startDate: new Date()
    });

    $('#is_useful_expiry_datetime').datetimepicker({
        format: 'yyyy-mm-dd',
        autoclose: true,
        todayHighlight: true,
        minView: 2,
        startDate: new Date(),
    });

    $('#quote-form #date').datetimepicker({
        format: 'yyyy-mm-dd',
        autoclose: true,
        minView: 2,
        todayHighlight: true,
        startDate: new Date(),
    });

    $('#campaign-form #start_date, #campaign-form #end_date').datetimepicker({
        format: 'yyyy-mm-dd',
        autoclose: true,
        minView: 2,
        todayHighlight: true
    });

    $('#memory-form #death_date, #memory-form #birth_date').datetimepicker({
        format: 'yyyy-mm-dd',
        autoclose: true,
        minView: 2,
        todayHighlight: true
    });

    $('#memory-clear-birth-date').on('click', function(e) {
        e.preventDefault();

        $('#memory-form #birth_date').val('');
    })

    $('#memory-form #datetime, #memory-form #publish_datetime, #memory-form #end_datetime').datetimepicker({
        format: 'yyyy-mm-dd hh:ii',
        autoclose: true,
        todayHighlight: true,
        minuteStep: 5,
    });

    // $('#campaign-form #end_date').datetimepicker({
    //     format: 'yyyy-mm-dd',
    //     autoclose: true,
    //     minView: 2,
    //     todayHighlight: true,
    //     startDate: new Date()
    // }).on('changeDate', function(e) {
    //     alert($(document).find('#campaign-form #start_date').datepicker('getDate'));
    //     var start_date_ts = +new Date($(document).find('#campaign-form #start_date').datepicker('getDate'));
    //     var end_date_ts = new Date(e.date);
    //     end_date_ts.setHours(0, 0, 0, 0);
    //     end_date_ts = +new Date(end_date_ts);

    //     alert('start_date_ts', start_date_ts);

    //     var day_diff = (end_date_ts - start_date_ts) / (60 * 60 * 24 * 1000);

    //     alert('day_diff', day_diff);
    // });

    $('#memory-form .memory-type-radio').on('change', function(e) {
        $('.memory-frame-types-wrap').hide();
        var memory_frame_type = $(this).val();
        $('.' + memory_frame_type + '-wrap').show();
        $('.' + memory_frame_type + '-wrap').find('input[type="radio"]').first(). prop("checked", true);
    })

    $('#datetime.create_news_datetime').val(date2str(new Date(), 'yyyy-MM-dd hh:mm'));

    $('#datetime.edit_news_datetime').datetimepicker({
        format: 'yyyy-mm-dd hh:ii',
        autoclose: true,
        todayHighlight: true,
        minuteStep: 5,
        startDate: "-30d"
    });

    $('.owl-carousel').owlCarousel({
        items: 1
    });

    $('#is_breaking').on('change', function () {
        if($(this).is(":checked")) {
            $('#send_notification').prop('checked', true);
            $('.high_priority_notification').show();
        }
    });

    $('#send_notification').on('change', function () {
        if($(this).is(":checked")) {
            $('.high_priority_notification').show();
        } else {
            $('.high_priority_notification').hide();
        }
    });

    $('#is_useful').on('change', function () {
        if($(this).is(":checked")) {
            $('.is-useful-expiry-datetime-wrap').show();
        } else {
            $('.is-useful-expiry-datetime-wrap').hide();
        }
    });

    $('.news-reporter-select').select2({
        placeholder: 'Select Reporter',
        searchInputPlaceholder: 'Search Reporter'
    }).on('select2:select', function (e) {
        // var reporter_id = $('.news-reporter-select').val();

        // $.get('/news/reporting-cities/' + reporter_id, function(data) {
        //     $('.news-city-select').html(data);
        //     $('.news-city-select').trigger('change.select2');
        // });
    });

    $('.poll-show-result-select').select2({
        placeholder: 'Result Show Type',
        minimumResultsForSearch: -1
    });

    $('.campaign-advertiser-select').select2({
        placeholder: 'Select Advertiser'
    });

    $('.campaign-status-select').select2({
        placeholder: 'Select Status',
        minimumResultsForSearch: -1
    });

    $('.poll-news-select').select2({
        placeholder: 'News',
    });

    $('.memory-name-prefix-select').select2({
        placeholder: 'Select Name Prefix',
        minimumResultsForSearch: -1
    });

    $('.memory-agency-select').select2({
        placeholder: 'Select Agency'
    });

    $('.memory-status-select').select2({
        placeholder: 'Select Status',
        minimumResultsForSearch: -1
    });

    $('.poll-status-select').select2({
        placeholder: 'Status',
        minimumResultsForSearch: -1
    });

    $('.filter-sort-select').select2({
        placeholder: 'Type',
        minimumResultsForSearch: -1
    });

    $('.filter-by-category-select').select2({
        placeholder: 'Category',
        minimumResultsForSearch: -1
    });

    $('.filter-by-reporter-select').select2({
        placeholder: 'Reporter',
    });

    $('.filter-by-publish-city-select').select2({
        placeholder: 'Publish City',
    });

    $('.filter-by-city-select').select2({
        placeholder: 'City',
    });

    $('.filter-by-status-select').select2({
        placeholder: 'Status',
        minimumResultsForSearch: -1
    });

    $('.filter-by-notification-select').select2({
        placeholder: 'Notification Level',
        minimumResultsForSearch: -1
    });

    $('.filter-by-platform-select').select2({
        placeholder: 'Platform',
        minimumResultsForSearch: -1
    });

    $('.filter-by-campaign-select').select2({
        placeholder: 'Campaign'
    });

    $('.filter-by-role-select').select2({
        placeholder: 'Role',
        minimumResultsForSearch: -1
    });

    $('.sort-by-select').select2({
        placeholder: 'Sort By',
        minimumResultsForSearch: -1
    });

    $('.sort-by-type-select').select2({
        placeholder: 'Sort Type',
        minimumResultsForSearch: -1
    });

    $('.news-status-select').select2({
        placeholder: 'Select Status',
        minimumResultsForSearch: -1
    });

    $('.news-status-select').on('change', function(e) {
        e.preventDefault();

        if($(this).val() == 'Rejected') {
            $('.rejection-reasons-wrap').show();
        } else {
            $('.rejection-reasons-wrap').hide();
        }
    });

    $('.news-notification-type-select').select2({
        placeholder: 'Select Notification Type',
        minimumResultsForSearch: -1
    });

    $('.news-category-select').select2({
        placeholder: 'Select Category',
        searchInputPlaceholder: 'Search Category',
        closeOnSelect: false
    });

    $('.news-language-select').select2({
        placeholder: 'Select Language',
        searchInputPlaceholder: 'Search Language'
    });

    $('.news-city-select').select2({
        placeholder: 'Select City',
        searchInputPlaceholder: 'Search City'
    });

    $('.news-cities-select').select2({
        placeholder: 'Select Cities',
        searchInputPlaceholder: 'Search Cities',
        closeOnSelect: false
    });

    $('.city-group-cities-select').select2({
        placeholder: 'Select Cities',
        searchInputPlaceholder: 'Search Cities',
        closeOnSelect: false
    });

    $('.role-permissions-select').select2({
        placeholder: 'Select Permissions',
        searchInputPlaceholder: 'Search Permissions',
        closeOnSelect: false
    });

    $('#news-media-sorting-form #media').change(function(e) {
        var files = e.target.files;

        var fileNameHtml = '<div class="mt-2">';

        var i;
        for (i = 0; i < files.length; i++) {
            fileNameHtml += '<div class="mt-1">' + (i+1) + '. <span class="text-dark">' + files[i].name + '</span></div>';
        }

        fileNameHtml += '</div>';

        $('#media-files-name').html(fileNameHtml);
    });

    $('#campaign-form #banner').change(function(e) {
        var files = e.target.files;

        var i;
        for (i = 0; i < files.length; i++) {
            var fileNameHtml = '';

            var unique_key = files[i].size + '' + files[i].name.replace(/[\W_]+/g, '').substring(0, 10);
            fileNameHtml = fileNameHtml + '<li class="news-new-media" data-media-id="'+ unique_key +'">';
                fileNameHtml = fileNameHtml + '<div class="clearfix">';
                    fileNameHtml = fileNameHtml + '<div class="float-left" style="width: 10%;"><image style="max-height: 132px;" id="' + unique_key + '"></div>';
                    fileNameHtml = fileNameHtml + '<div class="float-left" style="width: 90%; padding: 20px 20px 20px 0;">';
                        fileNameHtml = fileNameHtml + '<div class="clearfix mb-3">';
                            fileNameHtml = fileNameHtml + '<div class="float-right">';
                                fileNameHtml = fileNameHtml + '<label class="mr-3">Link/Number</label>';
                                fileNameHtml = fileNameHtml + '<input class="form-control" type="input" name="action_data__' + unique_key + '" style="display: inline-block; width: 400px;" placeholder="+918866553779">';
                            fileNameHtml = fileNameHtml + '</div>';
                            fileNameHtml = fileNameHtml + '<div class="float-right mr-5 form-inline">';
                                fileNameHtml = fileNameHtml + '<label class="mr-2">Action</label>';
                                fileNameHtml = fileNameHtml + '<select class="form-control" name="action__' + unique_key + '">';
                                fileNameHtml = fileNameHtml + '<option>call</option>';
                                fileNameHtml = fileNameHtml + '<option>whatsapp</option>';
                                fileNameHtml = fileNameHtml + '<option>link</option>';
                                fileNameHtml = fileNameHtml + '</select>';
                            fileNameHtml = fileNameHtml + '</div>';
                            fileNameHtml = fileNameHtml + '<div class="float-right mr-5 form-inline">';
                                fileNameHtml = fileNameHtml + '<label class="mr-2">Type</label>';
                                fileNameHtml = fileNameHtml + '<select class="form-control" name="type__' + unique_key + '">';
                                fileNameHtml = fileNameHtml + '<option>feed</option>';
                                fileNameHtml = fileNameHtml + '<option>detail</option>';
                                fileNameHtml = fileNameHtml + '</select>';
                            fileNameHtml = fileNameHtml + '</div>';
                        fileNameHtml = fileNameHtml + '</div>';
                        
                        fileNameHtml = fileNameHtml + '<div class="clearfix mb-3">';
                            fileNameHtml = fileNameHtml + '<div class="float-right">';
                                fileNameHtml = fileNameHtml + '<label class="mr-3" style="min-width: 82px; text-align: right;">Action Text</label>';
                                fileNameHtml = fileNameHtml + '<input class="form-control" type="input" name="action_text__' + unique_key + '" style="display: inline-block; width: 400px;" placeholder="Click here">';
                            fileNameHtml = fileNameHtml + '</div>';
                            fileNameHtml = fileNameHtml + '<div class="float-right mr-5">';
                                fileNameHtml = fileNameHtml + '<label class="mr-3">Action Image:</label>';
                                fileNameHtml = fileNameHtml + '<div class="custom-file" style="width: 213px; vertical-align: top;">';
                                    fileNameHtml = fileNameHtml + '<input type="file" class="custom-file-input action_image_upload" id="action_image__' + unique_key + '" name="action_image__' + unique_key + '" accept=".png,.jpg,.jpeg" data-accept="[\'.png\',\'.jpg\',\'.jpeg\',\'.PNG\',\'.JPG\',\'.JPEG\']">';
                                    fileNameHtml = fileNameHtml + '<label style="padding: 10px;" class="custom-file-label" for="action_image__' + unique_key + '">Choose file</label>';
                                fileNameHtml = fileNameHtml + '</div>';
                            fileNameHtml = fileNameHtml + '</div>';
                            fileNameHtml = fileNameHtml + '<div class="float-right mr-2"><image style="max-height: 32px;" id="action_image__action_image__' + unique_key + '"></div>';
                        fileNameHtml = fileNameHtml + '</div>';

                        fileNameHtml = fileNameHtml + '<div class="clearfix mb-3">';
                            fileNameHtml = fileNameHtml + '<div class="float-right">';
                                fileNameHtml = fileNameHtml + '<label class="mr-3" style="text-align: right;">Action Text Colour</label>';
                                fileNameHtml = fileNameHtml + '<input class="form-control" type="input" name="action_text_colour__' + unique_key + '" style="display: inline-block; width: 90px;" placeholder="Colour Hash">';
                            fileNameHtml = fileNameHtml + '</div>';
                            fileNameHtml = fileNameHtml + '<div class="float-right" style="margin-right: 35px;">';
                                fileNameHtml = fileNameHtml + '<label class="mr-3" style="text-align: right;">Action Button Colour</label>';
                                fileNameHtml = fileNameHtml + '<input class="form-control" type="input" name="action_button_colour__' + unique_key + '" style="display: inline-block; width: 90px;" placeholder="Colour Hash">';
                            fileNameHtml = fileNameHtml + '</div>';
                        fileNameHtml = fileNameHtml + '</div>';

                    fileNameHtml = fileNameHtml + '</div>';
                fileNameHtml = fileNameHtml + '</div>';
            fileNameHtml = fileNameHtml + '</li>';

            $('#campaign-banner-sorting').append(fileNameHtml);

            generateThumb(files[i], $('#' + unique_key));
        }
    });

    $(document).on('change', '.action_image_upload', function(e) {
        var files = e.target.files;

        generateThumb(files[0], $('#action_image__' + $(this).attr('id')));
    });

    $('#news_form #media, #video-editor-form #media').change(function(e) {
        if($('#news_form').hasClass('news_update_form')) {
            $('.news-new-media').remove();
        } else {
            $('#news-media-sorting').html('');
        }
        
        var files = e.target.files;

        var i;
        for (i = 0; i < files.length; i++) {
            var fileNameHtml = '';

            var is_video = files[i].name.split('.').pop().toLowerCase() == 'mp4';

            var unique_key = files[i].size + '' + files[i].name.replace(/[\W_]+/g, '').substring(0, 10);
            fileNameHtml = fileNameHtml + '<li class="news-new-media" data-media-id="'+ unique_key +'">';
                fileNameHtml = fileNameHtml + '<div class="clearfix">';
                    fileNameHtml = fileNameHtml + '<div class="float-left" style="width: 10%;"><image style="max-height: 78px;" id="' + unique_key + '"></div>';
                    fileNameHtml = fileNameHtml + '<div class="float-left" style="width: 90%; padding: 20px 10px 20px 0;">';
                        fileNameHtml = fileNameHtml + '<div class="float-left ml-5">' + files[i].name + '</div>';
                        fileNameHtml = fileNameHtml + '<div class="float-right">';
                            fileNameHtml = fileNameHtml + '<label class="mr-3">Courtesy</label>';
                            fileNameHtml = fileNameHtml + '<input class="form-control" type="input" name="courtesy__' + unique_key + '" style="display: inline-block; width: 200px;" placeholder="Media Courtesy">';
                        fileNameHtml = fileNameHtml + '</div>';
                        fileNameHtml = fileNameHtml + '<div class="float-right mr-5">';
                            fileNameHtml = fileNameHtml + '<label class="mr-2">Sensitive</label>';
                            fileNameHtml = fileNameHtml + '<input type="checkbox" name="is_sensitive__' + unique_key + '">';
                        fileNameHtml = fileNameHtml + '</div>';
                        if(!is_video) {
                            fileNameHtml = fileNameHtml + '<div class="float-right mr-5">';
                                fileNameHtml = fileNameHtml + '<label class="mr-2">Blur Background</label>';
                                fileNameHtml = fileNameHtml + '<input type="checkbox" name="is_blur__' + unique_key + '">';
                            fileNameHtml = fileNameHtml + '</div>';
                        }
                        fileNameHtml = fileNameHtml + '<div class="float-right mr-3">';
                            fileNameHtml = fileNameHtml + '<label class="mr-2">File Photo</label>';
                            fileNameHtml = fileNameHtml + '<input type="checkbox" name="is_file_photo__' + unique_key + '">';
                        fileNameHtml = fileNameHtml + '</div>';
                        fileNameHtml = fileNameHtml + '<div class="float-right mr-5 form-inline">';
                            fileNameHtml = fileNameHtml + '<label class="mr-2">Watermark</label>';
                            fileNameHtml = fileNameHtml + '<select class="form-control" name="watermark_position__' + unique_key + '">';
                            fileNameHtml = fileNameHtml + '<option>none</option>';
                            fileNameHtml = fileNameHtml + '<option>center</option>';
                            fileNameHtml = fileNameHtml + '<option>left</option>';
                            fileNameHtml = fileNameHtml + '<option>right</option>';
                            fileNameHtml = fileNameHtml + '<option>top-left</option>';
                            fileNameHtml = fileNameHtml + '<option>top</option>';
                            fileNameHtml = fileNameHtml + '<option>top-right</option>';
                            fileNameHtml = fileNameHtml + '<option>bottom-left</option>';
                            fileNameHtml = fileNameHtml + '<option>bottom</option>';
                            fileNameHtml = fileNameHtml + '<option>bottom-right</option>';
                            fileNameHtml = fileNameHtml + '</select>';
                        fileNameHtml = fileNameHtml + '</div>';
                        if(is_video) {
                            fileNameHtml = fileNameHtml + '<div class="float-right mr-5">';
                                fileNameHtml = fileNameHtml + '<label class="mr-2">Concat Video</label>';
                                fileNameHtml = fileNameHtml + '<input type="checkbox" name="concat_video__' + unique_key + '">';
                            fileNameHtml = fileNameHtml + '</div>';
                        }
                    fileNameHtml = fileNameHtml + '</div>';
                fileNameHtml = fileNameHtml + '</div>';
            fileNameHtml = fileNameHtml + '</li>';

            $('#news-media-sorting').append(fileNameHtml);

            generateThumb(files[i], $('#' + unique_key));
        }
    });

    $('.frame-type-thumbnail-input').change(function(e) {
        var files = e.target.files;

        var fileNameHtml = '<div class="mt-2">';

        var i;
        for (i = 0; i < files.length; i++) {
            fileNameHtml += '<div class="mt-1"><span class="text-dark">' + files[i].name + '</span><a class="ml-1 remove-frame-type-thumbnail-selected-image" href="javascript:;"><i class="fa fa-times-circle"></i></a></div>';
        }

        fileNameHtml += '</div>';

        $('#thumbnail-files-name').html(fileNameHtml);
    });

    $('#memory-form #photo').change(function(e) {
        var files = e.target.files;

        var fileNameHtml = '<div class="mt-2">';

        var i;
        for (i = 0; i < files.length; i++) {
            fileNameHtml += '<div class="mt-1"><span class="text-dark">' + files[i].name + '</span><a class="ml-1 remove-memory-photo-selected-image" href="javascript:;"><i class="fa fa-times-circle"></i></a></div>';
        }

        fileNameHtml += '</div>';

        $('#photo-files-name').html(fileNameHtml);
    });

    $('#memory-form #proofs').change(function(e) {
        var files = e.target.files;

        var fileNameHtml = '<div class="mt-2">';

        var i;
        for (i = 0; i < files.length; i++) {
            fileNameHtml += '<div class="mt-1">' + (i+1) + '. <span class="text-dark">' + files[i].name + '</span></div>';
        }

        fileNameHtml += '</div>';

        $('#proofs-files-name').html(fileNameHtml);
    });

    $('#image').change(function(e) {
        var files = e.target.files;

        var fileNameHtml = '<div class="mt-2">';

        var i;
        for (i = 0; i < files.length; i++) {
            fileNameHtml += '<div class="mt-1"><span class="text-dark">' + files[i].name + '</span><a class="ml-1 remove-poll-selected-image" href="javascript:;"><i class="fa fa-times-circle"></i></a></div>';
        }

        fileNameHtml += '</div>';

        $('#image-files-name').html(fileNameHtml);
    });

    $('#news_share_bottom_image').change(function(e) {
        var files = e.target.files;

        var fileNameHtml = '<div>';

        var i;
        for (i = 0; i < files.length; i++) {
            fileNameHtml += '<div>' + '<span class="text-dark">' + files[i].name + '</span></div>';
        }

        fileNameHtml += '</div>';

        $('.news_share_bottom_image').html(fileNameHtml);
    });

    $('#news_share_bottom_image_gujarati').change(function(e) {
        var files = e.target.files;

        var fileNameHtml = '<div>';

        var i;
        for (i = 0; i < files.length; i++) {
            fileNameHtml += '<div>' + '<span class="text-dark">' + files[i].name + '</span></div>';
        }

        fileNameHtml += '</div>';

        $('.news_share_bottom_image_gujarati').html(fileNameHtml);
    });

    $('#memories-settings-form #share_bottom_image').change(function(e) {
        var files = e.target.files;

        var fileNameHtml = '<div>';

        var i;
        for (i = 0; i < files.length; i++) {
            fileNameHtml += '<div>' + '<span class="text-dark">' + files[i].name + '</span></div>';
        }

        fileNameHtml += '</div>';

        $('#memories-settings-form .share_bottom_image').html(fileNameHtml);
    });

    $('#watermark_image').change(function(e) {
        var files = e.target.files;

        var fileNameHtml = '<div>';

        var i;
        for (i = 0; i < files.length; i++) {
            fileNameHtml += '<div>' + '<span class="text-dark">' + files[i].name + '</span></div>';
        }

        fileNameHtml += '</div>';

        $('.watermark_image').html(fileNameHtml);
    });

    $('#memories-settings-form #share_bottom_image_gujarati').change(function(e) {
        var files = e.target.files;

        var fileNameHtml = '<div>';

        var i;
        for (i = 0; i < files.length; i++) {
            fileNameHtml += '<div>' + '<span class="text-dark">' + files[i].name + '</span></div>';
        }

        fileNameHtml += '</div>';

        $('#memories-settings-form .share_bottom_image_gujarati').html(fileNameHtml);
    });

    $('.user-role-select').select2({
        placeholder: 'Select Role',
        minimumResultsForSearch: -1
    }).on('select2:select', function (e) {
        var user_role = $('.user-role-select').val();
        if(user_role == 'Reporter') {
            $('.reporter-cities-select').attr('required', true);
            $('#reporter-city-select-wrap').show();

            $('#alternate_name').attr('required', true);
            $('#reporter-alternate-name-input-wrap').show();

            $('#reporter-anonymous-reporting-input-wrap').show();
        } else {
            $('.reporter-cities-select').attr('required', false);
            $('#reporter-city-select-wrap').hide();

            $('#alternate_name').attr('required', false);
            $('#reporter-alternate-name-input-wrap').hide();

            $('#reporter-anonymous-reporting-input-wrap').hide();
        }
    });

    $('.reporter-cities-select').select2({
        placeholder: 'Select Cities',
        searchInputPlaceholder: 'Search Cities',
        closeOnSelect: false
    });

    $('.user-status-select').select2({
        placeholder: 'Select Status',
        minimumResultsForSearch: -1
    });

    $('.state-country-select').select2({
        placeholder: 'Select Country',
        searchInputPlaceholder: 'Search Country'
    });

    $('.district-state-select').select2({
        placeholder: 'Select State',
        searchInputPlaceholder: 'Search State'
    });

    $('.city-district-select').select2({
        placeholder: 'Select District',
        searchInputPlaceholder: 'Search District'
    });

    $('.locality-city-select').select2({
        placeholder: 'Select City',
        searchInputPlaceholder: 'Search City'
    });

    $('[id^=mobile_number]').keypress(validateNumber);
    $(document).on('keypress', '.only-numeric', validateNumber);
    $('.corona-virus-data-form input[type=text]').keypress(validateNumber);

    $uploadCrop = $('#upload-demo').croppie({
        url: $('#upload-demo-image').attr('src'),
        viewport: {
            width: 150,
            height: 150,
            type: 'circle'
        },
        boundary: {
            width: 150,
            height: 150
        }
    });

    $('#profile_picture').on('change', function () {
        var reader = new FileReader();
        reader.onload = function (e) {
            $uploadCrop.croppie('bind', {
                url: e.target.result
            }).then(function() {

            });
        }
        reader.readAsDataURL(this.files[0]);
    });

    $('#user_form').on('submit', function (e) {
        e.preventDefault();
        $uploadCrop.croppie('result', {
            type: 'canvas',
            size: 'viewport'
        }).then(function (base64) {
            $('#profile_picture_data64').val(base64);
            $('#user_form').unbind().submit()
        });
    });

    $('#news_form').on('submit', function (e) {
        var headline_value_length = $('#news_form').find('#headline').val().length;
        var headline_maxlength = $('#news_form').find('#headline').data("maxlength");
        //var article_value_length = $('#news_form').find('#article').val().length;
        var article_value_length = $(document).find('#article_ifr').contents().find("#tinymce").text().length;
        var article_maxlength = $('#news_form').find('#article').data("maxlength");

        if((headline_value_length > headline_maxlength) || (article_value_length > article_maxlength)) {
            alert('headline or article content length exceed the limit');
            return false;
        }

        if($('select[name="categories[]"] :selected').length == 0 && $('input[name="cities[]"]:checked').length == 0) {
            alert('please select atleast one Publish to City or Category');
            return false;
        }

        return true;
    });

    $('#city-group-form').on('submit', function (e) {
        if($('input[name="cities[]"]:checked').length == 0) {
            alert('please select atleast one City');
            return false;
        }

        return true;
    });

    $('.characters-count').on('propertychange input paste keypress', function(event) {
        var maxlength = $(this).data("maxlength") ? $(this).data("maxlength") : null;
        var currentLength = this.value.length;

        if(maxlength) {
            $(this).closest('.form-group').find('.remaining-length').text(maxlength - currentLength);
        }
        $(this).closest('.form-group').find('.current-length').text(currentLength);

        if(currentLength > maxlength) {
            $(this).addClass('is-invalid');
        } else {
            $(this).removeClass('is-invalid');
        }
    });

    $('.more-btn').on('click', function (e) {
        e.preventDefault();
        $('.more').removeClass('show-more-menu');
        $(this).parent().addClass('show-more-menu');
    });

    $('body').click(function(evt) {
        if(evt.target.class == "more-menu" || evt.target.class == "more-btn")
            return;
        //For descendants of menu_content being clicked, remove this check if you do not want to put constraint on descendants.
        if($(evt.target).closest('.more-menu').length || $(evt.target).closest('.more-btn').length)
            return;

        //Do processing of click event here for every element except with id menu_content
        $('.more').removeClass('show-more-menu');
    });

    var news_media_serial = [];

    $('#news-media-sorting').sortable({
        itemSelector: 'li',
        nested: false,
        vertical: false,
        serialize: function ($parent, $children, parentIsContainer) {
            if(parentIsContainer === false) {
                var media_id = $($parent[0]).data('media-id');
                if(media_id !== undefined) news_media_serial.push(media_id);
            }
        }
    });

    $('#news-media-sorting-form').on('submit', function (e) {
        news_media_serial = [];
        $('#news-media-sorting').sortable('serialize');
        $('#media_sorting').val(JSON.stringify(news_media_serial));

        return true;
    })

    $('#news_form, #video-editor-form').on('submit', function (e) {
        news_media_serial = [];
        $('#news-media-sorting').sortable('serialize');
        $('#media_sorting').val(JSON.stringify(news_media_serial));

        return true;
    });

    $('.status-change').on('click', function(e) {
        var checked = $(this).is(':checked');

        toggleStatus(checked, $(this).data('table'), $(this).data('id'));
    });

    $('.cb-input').on('change', function(e) {
        var checked = $(this).is(':checked');

        var type = $(this).data('type');
        var cities = $(this).data('cities');

        for (var i = cities.length - 1; i >= 0; i--) {
            $('#city-cb-' + cities[i]).prop('checked', checked);
        }
    });

    $('.cb-input-all').on('change', function(e) {
        var checked = $(this).is(':checked');

        var type = $(this).data('type');

        $('.' + type + '-cb-group').prop('checked', checked);
        $('.' + type + '-cb-group').trigger(e);

        if(type == 'city-group' && checked === false) {
            $('.district-cb-group').prop('checked', false);
            $('.city-cb-group').prop('checked', false);
            $('#district-cb-all').prop('checked', false);
            $('#city-cb-all').prop('checked', false);
        }

        if(type == 'district' && checked === false) {
            $('.city-cb-group').prop('checked', false);
            $('#city-cb-all').prop('checked', false);
        }
    });

    $('.poll-news-radio').on('change', function () {
        if($(this).val() == 'News') {
            $('.poll-position-wrap').hide();
            $('.poll-position-input').prop('required', false);
            $('.poll-news-wrap').show();
            $('.poll-news-select').prop('required', true);
        } else {
            $('.poll-news-wrap').hide();
            $('.poll-news-select').prop('required', false);
            $('.poll-position-wrap').show();
            $('.poll-position-input').prop('required', true);
        }
    });

    if($('#memory-form').length) {
        var poll_form_type = $('#memory-form').data('type');
        if(poll_form_type == 'create') {
            // addMemoryFromName();
        } else {

        }
    }

    if($('#poll-form').length) {
        var poll_form_type = $('#poll-form').data('type');
        if(poll_form_type == 'create') {
            addPollOption();
            addPollOption();
        } else {

        }
    }

    $(document).on('click', '.add-new-memory-from-name', function(e) {
        e.preventDefault();

        addMemoryFromName();
    });

    $(document).on('click', '.add-new-poll-option', function(e) {
        e.preventDefault();

        addPollOption();
    });

    $(document).on('click', '.remove-memory-from-name', function(e) {
        e.preventDefault();

        window.memory_from_names = window.memory_from_names - 1;

        $(this).closest('.memory-from-name').remove();

        if(window.memory_from_names <= 1) {
            // $('.remove-memory-from-name').remove();
        }
    });

    $(document).on('click', '.remove-poll-option', function(e) {
        e.preventDefault();

        window.poll_options = window.poll_options - 1;

        $(this).closest('.poll-option').remove();

        if(window.poll_options <= 2) {
            $('.remove-poll-option').remove();
        }
    });

    $('.poll-show-result-select').on('change', function(e) {
        e.preventDefault();

        if($(this).val() == 'Never') {
            $('#result_end_datetime').prop('required', false);
            $('.result-end-datetime-wrap').hide();
        } else {
            $('#result_end_datetime').prop('required', true);
            $('.result-end-datetime-wrap').show();
        }
    });

    $(document).on('click', '.remove-poll-selected-image', function(e) {
        $('.poll-image-input').replaceWith($('.poll-image-input').val('').clone(true));
        $('#image-files-name').html('');
    });

    $(document).on('click', '.remove-memory-photo-selected-image', function(e) {
        $('.memory-photo-input').replaceWith($('.memory-photo-input').val('').clone(true));
        $('#photo-files-name').html('');
    });

    $(document).on('click', '.remove-frame-type-thumbnail-selected-image', function(e) {
        $('.frame-type-thumbnail-input').replaceWith($('.frame-type-thumbnail-input').val('').clone(true));
        $('#thumbnail-files-name').html('');
    });

    $(document).on('click', '.remove-memory-proof', function(e) {
        var id = $(this).data('memory-proof-id');

        var self = $(this).closest('.memory-proof-wrap');

        event.preventDefault();

        // $(window).scrollTop(1.1);

        $.confirm({
            animation: 'scale',
            closeAnimation: 'scale',
            title: 'Memory Proof Delete!',
            content: 'Are you sure?',
            buttons: {
                cancel: {
                    btnClass: 'btn-warning',
                    action: function () {

                    }
                },
                confirm: {
                    btnClass: 'btn-primary',
                    action: function () {
                        $.ajax({
                            url: '/memories/proofs/' + id,
                            type: 'DELETE',
                            success: function(result) {
                                if(result.status) {
                                    self.remove();
                                }
                            }
                        });
                    }
                }
            }
        });

        return true;
    });

    $(document).on('click', '.remove-poll-image', function(e) {
        var id = $(this).data('poll-id');

        event.preventDefault();

        $(window).scrollTop(1.1);

        $.confirm({
            animation: 'scale',
            closeAnimation: 'scale',
            title: 'Poll Image Delete!',
            content: 'Are you sure?',
            buttons: {
                cancel: {
                    btnClass: 'btn-warning',
                    action: function () {

                    }
                },
                confirm: {
                    btnClass: 'btn-primary',
                    action: function () {
                        $.ajax({
                            url: '/polls/' + id + '/image/delete',
                            type: 'POST',
                            success: function(result) {
                                if(result.status) {
                                    $('#image-files-name').html('');
                                }
                            }
                        });
                    }
                }
            }
        });

        return true;
    });

    $('.recover-poll-option').on('click', function(e) {
        $(this).closest('.poll-option').removeClass('poll-option-deleted');
        var name = $(this).closest('.poll-option').find('.poll-option-title-input').prop('name');
        name = name.replace('poll_option_title__ex__', 'poll_option_title__ex__del__');
        $(this).closest('.poll-option').find('.poll-option-title-input').prop('name', name);
        $(this).remove();
    });

    $('.toggle-deleted-poll-options').on('click', function(e) {
        $('.poll-option-deleted').toggleClass('d-none');
        $(this).text($(this).text() == '(show deleted)'? '(hide deleted)': '(show deleted)')
    })
});

window.addMemoryFromName = function() {
    window.memory_from_names = window.memory_from_names + 1;

    var html = generateMemoryFromNameHTML(window.memory_from_names);
        
    $('.memory-from-names-wrap').append(html);
}

window.addPollOption = function() {
    window.poll_options = window.poll_options + 1;

    var html = generatePollOptionHTML(window.poll_options);
        
    $('.poll-options-wrap').append(html);
}

window.validateNumber = function(event) {
    var key = window.event ? event.keyCode : event.which;
    if (event.keyCode === 8 || event.keyCode === 46) {
        return true;
    } else if (key < 48 || key > 57) {
        return false;
    } else {
        return true;
    }
};

window.deleteRecords = function(event, type, id) {
    event.preventDefault();

    if(type == 'News Media') {
        var delete_confirm = confirm("News Media Delete!");
        if (delete_confirm == true) {
            var target_id = 'existing__' + id;
            var target = $('li[data-media-id="'+ target_id +'"]');
            deleteNewsMediaByAjax(id, target);
        }
    } else if(type == 'Campaign Banner') {
        var delete_confirm = confirm("Campaign Banner Delete!");
        if (delete_confirm == true) {
            var target_id = 'existing__' + id;
            var target = $('li[data-banner-id="'+ target_id +'"]');
            deleteCampaignBannerByAjax(id, target);
        }
    } else {
        $(window).scrollTop(1.1);

        $.confirm({
            animation: 'scale',
            closeAnimation: 'scale',
            title: type + ' Delete!',
            content: 'Are you sure?',
            buttons: {
                cancel: {
                    btnClass: 'btn-warning',
                    action: function () {

                    }
                },
                confirm: {
                    btnClass: 'btn-primary',
                    action: function () {
                        document.getElementById(id).submit();
                    }
                }
            }
        });
    }

    return true;
}

window.deleteNewsMediaByAjax = function(id, target) {
    $.ajax({
        url: '/news/media/' + id + '/delete',
        type: 'POST',
        success: function(result) {
            target.remove();
            // window.location.reload();
        }
    });
}

window.deleteCampaignBannerByAjax = function(id, target) {
    $.ajax({
        url: '/campaigns/banners/' + id + '/delete',
        type: 'POST',
        success: function(result) {
            target.remove();
            // window.location.reload();
        }
    });
}

window.filterNews = function (parameters, type, state) {
    var query_parameters_exists = parameters.split('?');
    var qObj = {};
    // if(query_parameters_exists.length > 1) {
    //     var qObj = query_parameters_exists[1].split("&").reduce(function(prev, curr, i, arr) {
    //         var p = curr.split("=");
    //         prev[decodeURIComponent(p[0])] = decodeURIComponent(p[1]);
    //         return prev;
    //     }, {});
    //     if(state) {
    //         qObj[type] = 1;
    //     } else {
    //         delete qObj[type];
    //     }
    // }
    if(state) {
        qObj[type] = 1;
    } else {
        delete qObj[type];
    }
    var finalURL = query_parameters_exists[0];
    if(Object.keys(qObj).length) {
        finalURL = finalURL + '?' + $.param(qObj);
    }
    window.location.href = finalURL;
}

window.filterDevices = function (parameters, type) {
    var state = $('input[name="' + type + '"]').is(':checked');
    
    var query_parameters_exists = parameters.split('?');
    var qObj = {};
    if(query_parameters_exists.length > 1) {
        var qObj = query_parameters_exists[1].split("&").reduce(function(prev, curr, i, arr) {
            var p = curr.split("=");
            prev[decodeURIComponent(p[0])] = decodeURIComponent(p[1]);
            return prev;
        }, {});
        if(state) {
            qObj[type] = 1;
        } else {
            delete qObj[type];
        }
    }
    if(state) {
        qObj[type] = 1;
    } else {
        delete qObj[type];
    }
    var finalURL = query_parameters_exists[0];
    if(Object.keys(qObj).length) {
        finalURL = finalURL + '?' + $.param(qObj);
    }
    window.location.href = finalURL;
}

window.toggleStatus = function(status, table, id) {
    $.ajax({
        url: "/" + table + "/" + id + "/toggle-status",
        type: 'POST',
        data: {
            "status": status
        },
        success: function(data) {
            location.reload();
        }
    });
}

window.date2str = function(x, y) {
    var z = {
        M: x.getMonth() + 1,
        d: x.getDate(),
        h: x.getHours(),
        m: x.getMinutes(),
        s: x.getSeconds()
    };
    y = y.replace(/(M+|d+|h+|m+|s+)/g, function(v) {
        return ((v.length > 1 ? "0" : "") + eval('z.' + v.slice(-1))).slice(-2)
    });

    return y.replace(/(y+)/g, function(v) {
        return x.getFullYear().toString().slice(-v.length)
    });
}

window.generateMemoryFromNameHTML = function(number) {
    var html = '<div class="row mt-3 memory-from-name">';
        html = html + '<div class="col-md-2">';
        html = html + '<input class="form-control" type="text" name="memory_from_names_name__' + number + '" placeholder="Name" required>';
        html = html + '</div>';
        html = html + '<div class="col-md-2">';
        html = html + '<input class="form-control only-numeric" type="text" name="memory_from_names_contact_number__' + number + '" placeholder="Contact Number" minlength="10" maxlength="10">';
        html = html + '</div>';
        html = html + '<div class="col-md-2">';
        html = html + '<a class="btn btn-primary remove-memory-from-name" href="javascript:;"><i class="fa fa-minus"></i></a>';
        html = html + '</div>';
        html = html + '</div>';
        return html;
}

window.generatePollOptionHTML = function(number) {
    var html = '<div class="row mt-3 poll-option">';
        html = html + '<div class="col-md-2">';
        html = html + '<input class="form-control" type="text" name="poll_option_title__' + number + '" placeholder="Option Title" required>';
        html = html + '</div>';
        html = html + '<div class="col-md-2">';
        html = html + '<a class="btn btn-primary add-new-poll-option mr-2" href="javascript:;"><i class="fa fa-plus"></i></a>';
        if(number > 2) {
            html = html + '<a class="btn btn-primary remove-poll-option" href="javascript:;"><i class="fa fa-minus"></i></a>';
        }
        html = html + '</div>';
        html = html + '</div>';
        return html;
}

window.generateThumb = function(file, target) {
    var fileReader = new FileReader();
    if (file.type.match('image')) {
        fileReader.onload = function () {
            target.prop('src', fileReader.result);
        };
        fileReader.readAsDataURL(file);
    } else {
        fileReader.onload = function () {
            var blob = new Blob([fileReader.result], {
                type: file.type
            });
            var url = URL.createObjectURL(blob);
            var video = document.createElement('video');
            var timeupdate = function () {
                if (snapImage()) {
                    video.removeEventListener('timeupdate', timeupdate);
                    video.pause();
                }
            };
            video.addEventListener('loadeddata', function () {
                if (snapImage()) {
                    video.removeEventListener('timeupdate', timeupdate);
                }
            });
            var snapImage = function () {
                var canvas = document.createElement('canvas');
                canvas.width = video.videoWidth;
                canvas.height = video.videoHeight;
                canvas.getContext('2d').drawImage(video, 0, 0, canvas.width, canvas.height);
                var image = canvas.toDataURL();
                var success = image.length > 100000;
                if (success) {
                    target.prop('src', image);
                    URL.revokeObjectURL(url);
                }
                return success;
            };
            video.addEventListener('timeupdate', timeupdate);
            video.preload = 'metadata';
            video.src = url;
            // Load video in Safari / IE11
            video.muted = true;
            video.playsInline = true;
            video.play();
        };
        fileReader.readAsArrayBuffer(file);
    }
}