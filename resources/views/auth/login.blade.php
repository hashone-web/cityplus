@extends('layouts.admin')

@push('content-class', 'content-fixed content-auth')
@push('container-class', 'container')

@section('content')
    <div class="media align-items-stretch justify-content-center ht-100p pos-relative">
        <div class="media-body align-items-center d-none d-lg-flex">
            <div class="mx-wd-600">
                <img src="http://themepixels.me/dashforge/assets/img/img15.png" class="img-fluid" alt="">
            </div>
        </div><!-- media-body -->
        <div class="sign-wrapper mg-lg-l-50 mg-xl-l-60">
            <form method="POST" action="{{ route('login') }}">
                @csrf
                <div class="wd-100p">
                    <h3 class="tx-color-01 mg-b-5">{{ __('Sign In') }}</h3>
                    <p class="tx-color-03 tx-16 mg-b-40">Welcome back! Please signin to continue.</p>

                    <div class="form-group">
                        <label for="mobile_number">{{ __('Mobile Number') }}</label>
                        <input id="mobile_number" type="text" class="form-control @error('mobile_number') is-invalid @enderror"
                               name="mobile_number" placeholder="9876543210" value="{{ old('mobile_number') }}" required
                               autocomplete="mobile_number" autofocus>
                        @error('mobile_number')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <input id="password" type="password"
                               class="form-control @error('password') is-invalid @enderror" name="password"
                               placeholder="Enter your password" required autocomplete="current-password">
                        @error('password')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    <button type="submit" class="btn btn-brand-02 btn-block">
                        {{ __('Sign In') }}
                    </button>
                    @if (Route::has('register'))
                    <div class="tx-13 mg-t-20 tx-center">Don't have an account? <a href="page-signup.html">Create an
                            Account</a></div>
                    @endif
                </div>
            </form>
        </div><!-- sign-wrapper -->
    </div><!-- media -->
@endsection
