<?php
    $admin_panel_activities = [];
    $admin_panel_activities_all = \App\AdminPanelActivity::all();

    foreach ($admin_panel_activities_all as $admin_panel_activity) {
        $admin_panel_activities[$admin_panel_activity->type] = \DB::table($admin_panel_activity->type)->where('created_at', '>', $admin_panel_activity->last_checked)->count();
    }
?>
<!DOCTYPE html>
<html lang="en">
<head>

    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    @if (config('app.env') === 'production')
    <meta http-equiv="Content-Security-Policy" content="upgrade-insecure-requests">
    @endif
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Meta -->
    <meta name="description" content="{{ \Config::get('constants.project.description') }}">
    <meta name="author" content="{{ \Config::get('constants.project.author') }}">

    <!-- Favicon -->
    <link rel="apple-touch-icon" sizes="57x57" href="/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
    <link rel="manifest" href="/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">

    <title>@stack('title')</title>

    <!-- vendor css -->
    <link href="https://fonts.googleapis.com/css2?family=Hind+Vadodara:wght@400;600&display=swap" rel="stylesheet">
    <link href="{{ asset('lib/@fortawesome/fontawesome-free/css/all.min.css') }}" rel="stylesheet">
    <link href="{{ asset('lib/ionicons/css/ionicons.min.css') }}" rel="stylesheet">
    <link href="{{ asset('lib/jqvmap/jqvmap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('lib/select2/css/select2.min.css') }}" rel="stylesheet">

    <!-- DashForge CSS -->
    <link rel="stylesheet" href="{{ asset('assets/css/dashforge.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/dashforge.auth.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/dashforge.dashboard.css') }}">
    <link rel="stylesheet" href="{{ asset('css/owl.carousel.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/owl.theme.default.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/plugins/summernote-master/summernote.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/plugins/bootstrap-datetimepicker/css/bootstrap-datepicker3.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css')}}">

    <link rel="stylesheet" href="{{ asset('assets/plugins/jquery-confirm/jquery-confirm.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/toastr.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/jquery.dataTables.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/croppie.min.css') }}">

    @yield('styles')

    <link rel="stylesheet" href="{{ asset(mix('css/custom.min.css')) }}">
</head>
<body class="page-profile">

<header class="navbar navbar-header navbar-header-fixed">
    <a href="" id="mainMenuOpen" class="burger-menu"><i data-feather="menu"></i></a>
    <div class="navbar-brand">
        <a href="{{ \Auth::check() ? route('home'): url('/') }}" class="df-logo">City<span>Plus</span></a>
    </div><!-- navbar-brand -->
    @auth
        <div id="navbarMenu" class="navbar-menu-wrapper">
            <div class="navbar-menu-header">
                <a href="{{ \Auth::check() ? route('home'): url('/') }}" class="df-logo">dash<span>forge</span></a>
                <a id="mainMenuClose" href=""><i data-feather="x"></i></a>
            </div><!-- navbar-menu-header -->
            <ul class="nav navbar-menu z-navbar-menu" style="max-width: 100%;">
                <li class="nav-label pd-l-20 pd-lg-l-25 d-lg-none">Main Navigation</li>
                <li class="nav-item {{ (\Request::is('home') || \Request::is('news*') ? 'active' : '') }}">
                    <a href="{{ route('home') }}" class="nav-link"><i data-feather="pie-chart"></i> News</a>
                </li>
                
                @can('view locations')
                <li class="nav-item with-sub {{ (\Request::is('countries*') || \Request::is('states*') || \Request::is('districts*') || \Request::is('cities*') || \Request::is('city-groups*') ? 'active' : '') }}">
                    <a href="" class="nav-link"><i data-feather="package"></i>Locations</a>
                    <ul class="navbar-menu-sub">
                        <li class="nav-sub-item"><a href="{{ route('countries.index') }}" class="nav-sub-link"><i
                                    data-feather="map-pin"></i>Countries</a></li>
                        <li class="nav-sub-item"><a href="{{ route('states.index') }}" class="nav-sub-link"><i
                                    data-feather="map-pin"></i>States</a></li>
                        <li class="nav-sub-item"><a href="{{ route('districts.index') }}" class="nav-sub-link"><i
                                    data-feather="map-pin"></i>Districts</a></li>
                        <li class="nav-sub-item"><a href="{{ route('cities.index') }}" class="nav-sub-link"><i
                                    data-feather="map-pin"></i>Cities</a></li>
                        <li class="nav-sub-item"><a href="{{ route('city.groups.index') }}" class="nav-sub-link"><i
                                    data-feather="map-pin"></i>City Groups</a>
                        </li>
                    </ul>
                </li>
                @endcan

                @can('view users')
                <li class="nav-item with-sub {{ (\Request::is('users*') ? 'active' : '') }}">
                    <a href="" class="nav-link"><i data-feather="package"></i>Users</a>
                    <ul class="navbar-menu-sub">
                        <li class="nav-sub-item"><a href="{{ route('users.index') }}" class="nav-sub-link"><i
                                    data-feather="users"></i>Users</a></li>
                        <li class="nav-sub-item"><a href="{{ route('users.reporters') }}" class="nav-sub-link"><i
                                    data-feather="users"></i>Reporters</a></li>
                    </ul>
                </li>
                @endcan

                @can('view polls')
                <li class="nav-item {{ \Request::is('polls*')? 'active': '' }}">
                    <a href="{{ route('polls.index') }}" class="nav-link"><i data-feather="pie-chart"></i> Polls</a>
                </li>
                @endcan

                @can('view other cruds')
                <li class="nav-item with-sub {{ (\Request::is('categories*') || \Request::is('tags*') ? 'active' : '') }}">
                    <a href="" class="nav-link"><i data-feather="package"></i>Other</a>
                    <ul class="navbar-menu-sub">
                        <li class="nav-sub-item"><a href="{{ route('categories.index') }}" class="nav-sub-link"><i
                                    data-feather="inbox"></i>Categories</a></li>
                        <li class="nav-sub-item"><a href="{{ route('public.pages.index') }}" class="nav-sub-link"><i
                                    data-feather="link-2"></i>Public Pages</a></li>
                        <li class="nav-sub-item"><a href="{{ route('report.types.index') }}" class="nav-sub-link"><i
                                    data-feather="printer"></i>Report Types</a></li>
                        @can('edit settings')
                        <li class="nav-sub-item"><a href="{{ route('settings.edit') }}" class="nav-sub-link"><i
                                    data-feather="settings"></i>Settings</a></li>
                        <li class="nav-sub-item"><a href="{{ route('admin.panel.settings.edit') }}" class="nav-sub-link"><i
                                    data-feather="settings"></i>Admin Panel Settings</a></li>            
                        <li class="nav-sub-item"><a href="{{ route('updates.edit') }}" class="nav-sub-link"><i
                                    data-feather="sliders"></i>Updates</a></li>
                        <li class="nav-sub-item"><a href="{{ route('application.installs.index') }}" class="nav-sub-link"><i
                                    data-feather="activity"></i>Application Installs</a></li>
                        <li class="nav-sub-item"><a href="{{ route('devices.index') }}" class="nav-sub-link"><i
                                    data-feather="smartphone"></i>Devices Details</a></li>
                        <li class="nav-sub-item"><a href="{{ route('news.statistics.reports.index') }}" class="nav-sub-link"><i
                                    data-feather="smartphone"></i>News Statistics Reports</a></li>
                        <li class="nav-sub-item"><a href="{{ route('daily.active.user.by.city.reports.index') }}" class="nav-sub-link"><i
                                    data-feather="smartphone"></i>Cities Statistics Reports</a></li>
                        <li class="nav-sub-item"><a href="{{ route('custom.notifications.index') }}" class="nav-sub-link"><i
                                    data-feather="smartphone"></i>Custom Notifications</a></li>
                        <li class="nav-sub-item"><a href="{{ route('devices.feedbacks.static.index') }}" class="nav-sub-link"><i
                                    data-feather="smartphone"></i>Static Feedbacks</a></li>                                
                        @endcan
                        @can('edit roles')
                        <li class="nav-sub-item">
                            <a href="{{ route('roles.index') }}" class="nav-sub-link"><i data-feather="users"></i>Roles</a>
                        </li>
                        @endcan

                        @can('edit permissions')
                        <li class="nav-sub-item">
                            <a href="{{ route('permissions.index') }}" class="nav-sub-link"><i data-feather="users"></i>Permissions</a>
                        </li>
                        @endcan
                    </ul>
                </li>
                @endcan

                @can('edit settings')
                <li class="nav-item with-sub {{ (\Request::is('reporters-app*') ? 'active' : '') }}">
                    <a href="" class="nav-link"><i data-feather="package"></i>Reporters App</a>
                    <ul class="navbar-menu-sub">
                        <li class="nav-sub-item"><a href="{{ route('reporters-app.settings.edit') }}" class="nav-sub-link"><i
                                    data-feather="settings"></i>Settings</a></li>
                        <li class="nav-sub-item"><a href="{{ route('reporters-app.updates.edit') }}" class="nav-sub-link"><i
                                    data-feather="sliders"></i>Updates</a></li>
                        <li class="nav-sub-item"><a href="{{ route('rejection_reasons.index') }}" class="nav-sub-link"><i
                                    data-feather="inbox"></i>News Rejection Reasons</a></li>                                             
                    </ul>
                </li>
                @endcan

                @can('advertisements crud')
                <li class="nav-item with-sub {{ (\Request::is('advertisers*') || \Request::is('campaigns*') || \Request::is('ads-statistics-reports*') ? 'active' : '') }}">
                    <a href="" class="nav-link"><i data-feather="package"></i>Ads</a>
                    <ul class="navbar-menu-sub">
                        <li class="nav-sub-item"><a href="{{ route('advertisers.index') }}" class="nav-sub-link"><i
                                    data-feather="settings"></i>Advertisers</a></li>
                        <li class="nav-sub-item"><a href="{{ route('campaigns.index') }}" class="nav-sub-link"><i
                                    data-feather="sliders"></i>Campaigns</a></li>
                        <li class="nav-sub-item"><a href="{{ route('ads.statistics.reports.index') }}" class="nav-sub-link"><i
                                    data-feather="smartphone"></i>Statistics Reports</a></li>                                
                    </ul>
                </li>
                @endcan

                @can('memories crud')
                <li class="nav-item with-sub {{ (\Request::is('memories*') || \Request::is('agencies*') ? 'active' : '') }}">
                    <a href="" class="nav-link"><i data-feather="package"></i>Memories</a>
                    <ul class="navbar-menu-sub">
                        <li class="nav-sub-item"><a href="{{ route('agencies.index') }}" class="nav-sub-link"><i
                                    data-feather="airplay"></i>Agencies</a></li>
                        <li class="nav-sub-item"><a href="{{ route('frame_types.index') }}" class="nav-sub-link"><i
                                    data-feather="smartphone"></i>Memory Frames</a></li>
                        <li class="nav-sub-item"><a href="{{ route('memories.index') }}" class="nav-sub-link"><i
                                    data-feather="clipboard"></i>Memories</a></li>
                        <li class="nav-sub-item"><a href="{{ route('memories.settings.edit') }}" class="nav-sub-link"><i
                                    data-feather="settings"></i>Settings</a></li>                         
                    </ul>
                </li>
                @endcan

                @if(auth()->user()->can('plus crud') || auth()->user()->can('view useful info'))
                <li class="nav-item with-sub {{ (\Request::is('plus/settings*') || \Request::is('corona-virus-data*') ? 'active' : '') }}">
                    <a href="" class="nav-link"><i data-feather="package"></i>Plus</a>
                    <ul class="navbar-menu-sub">
                        @can('edit corona virus data')
                        <li class="nav-sub-item">
                            <a href="{{ route('corona.virus.data.index') }}" class="nav-sub-link"><i data-feather="file-text"></i>Corona Virus Data</a></li>            
                        @endcan
                        @can('quotes crud')
                            <li class="nav-sub-item"><a href="{{ route('quotes.index') }}" class="nav-sub-link"><i
                                    data-feather="smartphone"></i>Quotes</a></li>
                        @endcan
                        @can('view useful info')
                        <li class="nav-sub-item"><a href="{{ route('news.useful') }}" class="nav-sub-link"><i
                                    data-feather="smartphone"></i>Useful Info</a></li>
                        @endcan
                        @can('edit settings')
                        <li class="nav-sub-item"><a href="{{ route('plus.settings.edit') }}" class="nav-sub-link"><i
                                    data-feather="settings"></i>Settings</a></li>
                        @endcan                                  
                    </ul>
                </li>
                @endif
                

                @can('view user reports')
                <li class="nav-item {{ (\Request::is('reports*') ? 'active' : '') }}">
                    <a href="{{ route('reports.index') }}" class="nav-link"><i data-feather="file-text"></i>Reports @if(isset($admin_panel_activities['reports'])) <span class="link-count">{{ $admin_panel_activities['reports'] }}</span> @endif</a></li>
                @endcan
            </ul>
        </div><!-- navbar-menu-wrapper -->
        <div class="navbar-right">
            <div class="dropdown dropdown-profile">
                <a href="" class="dropdown-link" data-toggle="dropdown" data-display="static">
                    <div class="avatar avatar-sm"><img src="{{ \Auth::user()->profile_picture_link }}" class="rounded-circle" alt=""></div>
                </a><!-- dropdown-link -->
                <div class="dropdown-menu dropdown-menu-right tx-13">
                    <div class="avatar avatar-lg mg-b-15"><img src="{{ \Auth::user()->profile_picture_link }}" class="rounded-circle" alt=""></div>
                    <h6 class="tx-semibold mg-b-5">{{ Auth::user()->name }}</h6>
                    <p class="mg-b-25 tx-12 tx-color-03">{{ \Auth::user()->getRoleNames()->first() }}</p>

                    <a href="{{ route('profile.edit') }}" class="dropdown-item"><i data-feather="edit-3"></i> Edit Profile</a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                        <i data-feather="log-out"></i>{{ __('Sign Out') }}
                    </a>

                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                </div><!-- dropdown-menu -->
            </div><!-- dropdown -->
        </div><!-- navbar-right -->
    @else
        <div class="navbar-right w-auto">
            <ul class="nav navbar-menu ml-auto">
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                </li>
                @if (Route::has('register'))
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                    </li>
                @endif
            </ul>
        </div>
    @endauth
</header><!-- navbar -->

<div class="content @stack('content-class') pd-20">
    <div class="@stack('container-class') pd-x-0 pd-lg-x-10 pd-xl-x-0">
        @yield('breadcrumb')
        @yield('content')
    </div><!-- container -->
</div><!-- content -->

<script src="{{ asset('lib/jquery/jquery.min.js') }}"></script>
<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>

<script src="{{ asset('lib/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
<script src="{{ asset('lib/feather-icons/feather.min.js') }}"></script>
<script src="{{ asset('lib/perfect-scrollbar/perfect-scrollbar.min.js') }}"></script>
<script src="{{ asset('lib/jquery.flot/jquery.flot.js') }}"></script>
<script src="{{ asset('lib/jquery.flot/jquery.flot.stack.js') }}"></script>
<script src="{{ asset('lib/jquery.flot/jquery.flot.resize.js') }}"></script>
<script src="{{ asset('lib/select2/js/select2.min.js') }}"></script>
<script src="{{ asset('assets/plugins/summernote-master/summernote.min.js') }}"></script>
<script src="{{ asset('assets/plugins/bootstrap-datetimepicker/js/bootstrap-datepicker.min.js') }}"></script>
<script src="{{ asset('assets/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js')}}"></script>

<script src="{{ asset('assets/js/dashforge.js') }}"></script>

<!-- append theme customizer -->
<script src="{{ asset('lib/js-cookie/js.cookie.js') }}"></script>

<script src="{{ asset('assets/plugins/jquery-confirm/jquery-confirm.min.js') }}"></script>
<script src="{{ asset('js/toastr.min.js') }}"></script>
<script src="{{ asset('js/croppie.min.js') }}"></script>

<script src="{{ asset('js/transliteration-input.bundle.js') }}"></script>
<script src="{{ asset('js/owl.carousel.min.js') }}"></script>
<script src="{{ asset('js/jquery-sortable.js') }}"></script>
<script src="{{ asset('js/jquery.dataTables.min.js') }}"></script>

<script src="{{ asset('lib/tinymce/tinymce.min.js') }}"></script>

<script src="{{ asset('js/jquery-sortable-min.js') }}"></script>

@yield('scripts')

<script src="{{ asset(mix('js/custom.min.js')) }}"></script>

@yield('last-scripts')

<script type="text/javascript" src="{{ asset('js/transliteration.I.js') }}"></script>
<script type="text/javascript">
    google.load("elements", "1", {
        packages: "transliteration"
    });
</script>

<script type="text/javascript">
function onLoad() {
    var options = {
        sourceLanguage: 'en',
        destinationLanguage: ['gu'],
        shortcutKey: 'ctrl+g',
        transliterationEnabled: true
    };
    var control = new google.elements.transliteration.TransliterationControl(options);

    var ids = [];

    if(document.getElementById('quote-form')) {
        ids.push('text');
    }

    if(document.getElementById('memory-form')) {
        ids.push('heading');
        ids.push('name');
        ids.push('name');
        ids.push('details');
        ids.push('venue');
        ids.push('address');
        ids.push('special_notes');
        ids.push('highlighting_notes');
    }

    if(document.getElementById('headline')) {
        ids.push('headline');
    }
    if(document.getElementById('article_text')) {
        ids.push('article_text');
    }
    if(document.getElementById('gujarati_name')) {
        ids.push('gujarati_name');
    }
    if(ids.length >= 1) {
        control.makeTransliteratable(ids);
    }

    if(document.getElementById('article')) {
        tinymce.init({
            selector: '#article',
            height : '300',
            branding: false,
            menubar:false,
            plugins: "lists link wordcount",
            toolbar: "undo redo | bold italic underline | link | numlist bullist",
            statusbar: "wordcount",
            setup: function(editor) {
                editor.on('init', function(e) {
                    control.makeTransliteratable(['article_ifr']);
                });

                editor.on('keyup', function(e) {
                    var self = $('#news_form #article');

                    var maxlength = self.data("maxlength") ? self.data("maxlength") : null;
                    var currentLength = $(document).find('#article_ifr').contents().find("#tinymce").text().length;

                    if(maxlength) {
                        self.closest('.form-group').find('.remaining-length').text(maxlength - currentLength);
                    }
                    self.closest('.form-group').find('.current-length').text(currentLength);

                    if(currentLength > maxlength) {
                        $(document).find('.tox-tinymce').addClass('is-invalid');
                    } else {
                        $(document).find('.tox-tinymce').removeClass('is-invalid');
                    }

                    convertHTMLtoText()

                    setTimeout(function() {
                        convertHTMLtoText()
                    }, 500)
                });
            }
        });
    }
}

function convertHTMLtoText() {
    $.ajax({
        /* the route pointing to the post function */
        url: '{{ route("news.convert-html-to-text") }}',
        type: 'POST',
        /* send the csrf-token and the input to the controller */
        data: { _token: $('meta[name="csrf-token"]').attr('content'), html: $(document).find('#article_ifr').contents().find("#tinymce").html() },
        dataType: 'JSON',
        /* remind that 'data' is the response of the AjaxController */
        success: function (data) { 
            $('#article_text').text(data.data)
        }
    });
}

google.setOnLoadCallback(onLoad);
</script>

<!-- The core Firebase JS SDK is always required and must be listed first -->
<script src="https://www.gstatic.com/firebasejs/7.15.5/firebase-app.js"></script>

<!-- TODO: Add SDKs for Firebase products that you want to use
     https://firebase.google.com/docs/web/setup#available-libraries -->
<script src="https://www.gstatic.com/firebasejs/7.15.5/firebase-messaging.js"></script>

<script>
    // Your web app's Firebase configuration
    var firebaseConfig = {
        apiKey: "AIzaSyBdMfk3XvJtvaXsTTGlbiePw9-3HGTYGAg",
        projectId: "cityplus-web",
        messagingSenderId: "56161085787",
        appId: "1:56161085787:web:32115ea78a1a0e3bc8127e",
    };
    // Initialize Firebase
    firebase.initializeApp(firebaseConfig);

    // Retrieve an instance of Firebase Messaging so that it can handle background
    // messages.
    const messaging = firebase.messaging();

    messaging
        .requestPermission()
        .then(function () {
            console.log("Notification permission granted.");

            // get the token in the form of promise
            return messaging.getToken()
        })
        .then(function(token) {
            console.log('token', token);
            @auth
            $.ajax({
                url : "{{ route('users.fcm.token.store', ['id' => \Auth::id() ]) }}",
                type: "POST",
                data : { token: token },
                success: function(data, textStatus, jqXHR)
                {
                    console.log(data);
                },
                error: function (jqXHR, textStatus, errorThrown)
                {

                }
            });
            @endauth
        })
        .catch(function (err) {
            console.log("Unable to get permission to notify.", err);
        });

        messaging.onMessage(function(payload) {
            navigator.serviceWorker.getRegistration('/firebase-cloud-messaging-push-scope').then(registration => {
                var notification_data = JSON.parse(payload.data.notification);

                registration.showNotification(
                    notification_data.title,
                    notification_data
                )
            });
        });
</script>

{!! Toastr::message() !!}

</body>
</html>
