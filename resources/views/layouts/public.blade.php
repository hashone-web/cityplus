<!DOCTYPE html>
<html lang="en">
	<head>
		<!-- Required meta tags -->
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<!-- bootstrap-link -->
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
		<!-- Meta -->
		<meta name="description" content="Responsive Bootstrap 4 Dashboard Template">
		<meta name="author" content="ThemePixels">
		<title>CityPlus - Local News & More</title>

		<link rel="apple-touch-icon" sizes="57x57" href="/apple-icon-57x57.png">
		<link rel="apple-touch-icon" sizes="60x60" href="/apple-icon-60x60.png">
		<link rel="apple-touch-icon" sizes="72x72" href="/apple-icon-72x72.png">
		<link rel="apple-touch-icon" sizes="76x76" href="/apple-icon-76x76.png">
		<link rel="apple-touch-icon" sizes="114x114" href="/apple-icon-114x114.png">
		<link rel="apple-touch-icon" sizes="120x120" href="/apple-icon-120x120.png">
		<link rel="apple-touch-icon" sizes="144x144" href="/apple-icon-144x144.png">
		<link rel="apple-touch-icon" sizes="152x152" href="/apple-icon-152x152.png">
		<link rel="apple-touch-icon" sizes="180x180" href="/apple-icon-180x180.png">
		<link rel="icon" type="image/png" sizes="192x192"  href="/android-icon-192x192.png">
		<link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
		<link rel="icon" type="image/png" sizes="96x96" href="/favicon-96x96.png">
		<link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
		<link rel="manifest" href="/manifest.json">
		<meta name="msapplication-TileColor" content="#ffffff">
		<meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
		<meta name="theme-color" content="#ffffff">

		@yield('head')
		
		<!-- Select2 CSS -->
		<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
		<!-- vendor css -->
		<link href="{{ asset('lib/select2/css/select2.min.css') }}" rel="stylesheet">
		<link href="{{ asset('lib/ionicons/css/ionicons.min.css') }}" rel="stylesheet">
		<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css" rel="stylesheet">
		<link href="https://pagecdn.io/lib/easyfonts/hind-vadodara.css" rel="stylesheet" />
		<!-- DashForge CSS -->
		<link rel="stylesheet" href="{{ asset(mix('/css/public.min.css')) }}">
		<!-- DashForge CSS -->
		<link rel="stylesheet" href="{{ asset('assets/css/dashforge.css') }}">
		<!-- slider-CSS -->
	    <link href="https://googledrive.com/host/0B1RR6fhjI2QROGt0MTFoVkhMdUk/fonts.css">
	    <link href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.2.3/animate.min.css">
	</head>
	<body>
		<header class="navbar navbar-header navbar-header-fixed">
			<div class="container">
				<a href="" id="sidebarMenuOpen" class="burger-menu"><i data-feather="arrow-left"></i></a>
				<div class="navbar-brand">
					<a href="{{ route('public.home.index') }}" class="df-logo">CITY PLUS</a>
				</div><!-- navbar-brand -->

				<div class="navbar-right">
					<div class="navbar-image__icon">
						<img src='{{ asset("images/aim.png") }}' class="img-dropdown__icon">
						<div class="form-group ">
							<select class="select2-select" name="city" onchange="location = this.value;">
								@foreach($cities as $city)
								<option class="select2" value="{{ route('public.home.index', array_merge($query_parameters, ['city' => $city->id])) }}"  @if(isset($query_parameters['city']) && $query_parameters['city'] == $city->id) selected @endif>{{ $city->gujarati_name }}</option>
								@endforeach
							</select>
						</div>
					</div>
				</div>
			</div><!-- nav-wrapper -->
		</header>
		<div class=" @stack('content-class') ">
			<div class="@stack('container-class') ">
				@yield('content')
			</div>
		</div>
		<script src="{{ asset('lib/jquery/jquery.min.js') }}"></script>
		<script src="{{ asset('lib/feather-icons/feather.min.js') }}"></script>
		<script src="{{ asset('lib/select2/js/select2.min.js') }}"></script>

		
		<script src="https://kenwheeler.github.io/slick/slick/slick.js"></script>
		<script src="{{ asset('js/public.js') }}"></script>

		<script type="text/javascript">
			jQuery(document).ready(function($) {
				$('.select2-select').select2();
			})
		</script>
	</body>
</html>