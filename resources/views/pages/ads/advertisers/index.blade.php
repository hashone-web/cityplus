@extends('layouts.admin')

@php($title = 'Advertisers')
@push('title', yieldTitle($title))

@section('breadcrumb-title', $title)

@section('breadcrumb-link')
    <li class="breadcrumb-item active" aria-current="page">{{ $title }}</li>
@endsection

@section('breadcrumb-btn')
    <a class="btn btn-sm pd-x-15 btn-primary btn-uppercase mg-l-5" href="{{ route('advertisers.create') }}"><i data-feather="plus" class="wd-10 mg-r-5"></i>Create Advertiser</a>
@endsection

@section('breadcrumb')
    @include('components.breadcrumb')
@endsection

@push('content-class', 'content-fixed')
@push('container-class', 'container-fluid')

@section('content')
    <div class="card">
        <div class="card-body">
            <div data-label="Example" class="df-example demo-table">
                <div class="table-responsive">
                    <table class="table table-striped mg-b-0">
                        <thead>
                        <tr>
                            <th scope="col">ID</th>
                            <th scope="col">@sortablelink('name')</th>
                            <th scope="col">@sortablelink('business_name', 'Business Name')</th>
                            <th scope="col">Contact Number</th>
                            <th scope="col">@sortablelink('created_at', 'Added On')</th>
                            @if(auth()->user()->can('advertisements crud'))
                                <th scope="col">Actions</th>
                            @endif
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($advertisers as $advertiser)
                        <tr>
                            <th scope="row">{{ $advertiser->id }}</th>
                            <td>{{ $advertiser->name }}</td>
                            <td>{{ $advertiser->business_name }}</td>
                            <td>{{ $advertiser->contact_number }}</td>
                            <td>{{ getDateFormat($advertiser->created_at) }}</td>
                            @if(auth()->user()->can('advertisements crud'))
                                <td>
                                    @can('advertisements crud')<a href="{{ route('advertisers.edit', ['id' => $advertiser->id ]) }}"><i data-feather="edit-2" width="18" height="18"></i></a>@endcan
                                </td>
                            @endif
                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div><!-- table-responsive -->
            </div><!-- df-example -->
        </div>
    </div>
    <div class="mt-4">
        {!! $advertisers->appends(\Request::except('page'))->render() !!}
    </div>
@endsection