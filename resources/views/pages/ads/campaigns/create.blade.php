@extends('layouts.admin')

@php($title = isset($campaign) ? 'Edit Campaign - ' . $campaign->name: 'Create Campaign')
@push('title', yieldTitle($title))

@section('breadcrumb-title', $title)

@section('breadcrumb-link')
    <li class="breadcrumb-item active" aria-current="page">
        <a href="{{ route('campaigns.index') }}">Campaigns</a>
    </li>
    <li class="breadcrumb-item active" aria-current="page">{{ $title }}</li>
@endsection

@section('breadcrumb')
    @include('components.breadcrumb')
@endsection

@push('content-class', 'content-fixed')
@push('container-class', 'container-fluid')

@section('content')
    <div class="card">
        <div class="card-body">
            <form id="campaign-form" action="{{ isset($campaign) ? route('campaigns.update', ['id' => $campaign->id]) : route('campaigns.store') }}" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="form-row">
                    <div class="form-group col-md-3">
                        <label for="name">Name: <span class="tx-danger">*</span></label>
                        <input type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" id="name" placeholder="CoolBios App Ad Campaign" name="name" pattern="^[A-Za-z0-9 ]+$" maxlength="30" value="{{isset($campaign) ? $campaign->name : old('name') }}" required>
                        @if($errors->has('name'))
                            <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('name') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="form-group col-md-3">
                        <label for="district">Advertisers: <span class="tx-danger">*</span></label>
                        <select class="form-control campaign-advertiser-select" id="advertiser" name="advertiser" required>
                            <option disabled selected value>Select Advertiser</option>
                            @foreach($advertisers as $advertiser)
                                <option value="{{ $advertiser->id }}" @if(isset($campaign) && $campaign->advertiser_id == $advertiser->id) selected @endif>{{ $advertiser->name }} @if(isset($advertiser->business_name)) {{ ' (' . $advertiser->business_name . ')' }} @endif</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-row mt-2">
                    <div class="form-group col-md-2">
                        <label for="budget">Budget: <span class="tx-danger">*</span></label>
                        <input type="text" class="form-control{{ $errors->has('budget') ? ' is-invalid' : '' }}" id="budget" placeholder="5000" name="budget" pattern="^[0-9 ]+$" maxlength="10" value="{{isset($campaign) ? $campaign->budget : old('budget') }}" required>
                        @if($errors->has('budget'))
                            <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('budget') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="form-group col-md-2">
                        <label for="start_date">Start Date: <span class="tx-danger">*</span></label>
                        <div class="input-group">
                            <input id="start_date" type="text" class="form-control readonly-datepicker" name="start_date" value="{{isset($campaign) ? $campaign->start_date : old('start_date') }}" required onkeypress="return false;">
                            <span class="input-group-addon">
                                <span class="glyphicon glyphicon-calendar"></span>
                            </span>
                        </div>
                        @if ($errors->has('start_date'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('start_date') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group col-md-2">
                        <label for="end_date">End Date: <span class="tx-danger">*</span></label>
                        <div class="input-group">
                            <input id="end_date" type="text" class="form-control readonly-datepicker" name="end_date" value="{{isset($campaign) ? $campaign->end_date : old('end_date') }}" required onkeypress="return false;">
                            <span class="input-group-addon">
                                <span class="glyphicon glyphicon-calendar"></span>
                            </span>
                        </div>
                        @if ($errors->has('end_date'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('end_date') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-12 file-area">
                        <label for="banner">Banner:</label>
                        <input type="file" class="custom-file-input{{ $errors->has('banner') ? ' is-invalid' : '' }}" id="banner" name="banner[]" accept=".png,.jpg,.jpeg" data-accept="['.png','.jpg','.jpeg','.PNG','.JPG','.JPEG']" multiple>
                        <div class="file-dummy">
                            <div class="success">Upload Ad Banners.</div>
                            <div class="default">Please select some files</div>
                        </div>
                    </div>
                    <div class="form-group col-md-12 file-area">
                        <div class="mt-3">
                            <ol id="campaign-banner-sorting">
                                @if(isset($campaign))
                                    @foreach($campaign->banners as $banner_index => $banner)
                                        <?php 
                                            $banner_unique_id = 'existing__' . $banner->id;
                                        ?>
                                        <li data-banner-id="{{ $banner_unique_id }}">
                                            <div class="clearfix">
                                                <div class="float-left" style="width: 10%;">
                                                    <img style="max-height: 132px;" src="{{ $banner->media }}">
                                                </div>
                                                <div class="float-left" style="width: 90%; padding: 20px 20px 20px 0;">
                                                    <div class="clearfix">
                                                        <div class="clearfix mb-3">
                                                            <div class="float-right mr-2 ml-3">
                                                                <a href="javascript:;" role="menuitem" onclick="deleteRecords(event, 'Campaign Banner', {{ $banner->id }})"><i class="fa fa-trash text-danger"></i></a>
                                                            </div>
                                                            <div class="float-right">
                                                                <label class="mr-3">Link/Number</label>
                                                                <input class="form-control" type="input" name="{{ 'action_data__' . $banner_unique_id }}" style="display: inline-block; width: 400px;" value="{{ $banner->action_data }}">
                                                            </div>
                                                            <div class="float-right mr-5 form-inline">
                                                                <label class="mr-2">Action</label>
                                                                <select class="form-control" name="{{ 'action__' . $banner_unique_id }}">
                                                                    <option @if($banner->action == 'call') selected @endif>call</option>
                                                                    <option @if($banner->action == 'whatsapp') selected @endif>whatsapp</option>
                                                                    <option @if($banner->action == 'link') selected @endif>link</option>
                                                                </select>
                                                            </div>
                                                            <div class="float-right mr-5 form-inline">
                                                                <label class="mr-2">Type</label>
                                                                <select class="form-control" name="{{ 'type__' . $banner_unique_id }}">
                                                                    <option @if($banner->type == 'feed') selected @endif>feed</option>
                                                                    <option @if($banner->type == 'detail') selected @endif>detail</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="clearfix mb-3">
                                                        <div class="float-right" style="margin-right: 35px;">
                                                            <label class="mr-3" style="min-width: 82px; text-align: right;">Action Text</label>
                                                            <input class="form-control" type="input" name="{{ 'action_text__' . $banner_unique_id }}" style="display: inline-block; width: 400px;" value="{{ $banner->action_text }}">
                                                        </div>
                                                        <div class="float-right mr-5">
                                                            <label class="mr-3">Action Image:</label>
                                                            <div class="custom-file" style="width: 213px; vertical-align: top;">
                                                                <input type="file" class="custom-file-input action_image_upload" id="{{ 'action_image__' . $banner_unique_id }}" name="{{ 'action_image__' . $banner_unique_id }}" accept=".png,.jpg,.jpeg" data-accept="['.png','.jpg','.jpeg','.PNG','.JPG','.JPEG']">
                                                                <label style="padding: 10px;" class="custom-file-label" for="{{ 'action_image__' . $banner_unique_id }}">Choose file</label>
                                                            </div>
                                                        </div>
                                                        @if($banner->action_image)
                                                        <div class="float-right mr-2">
                                                            <img id="{{ 'action_image__action_image__' . $banner_unique_id }}" style="max-height: 32px;" src="{{ $banner->action_image }}">
                                                        </div>
                                                        @endif
                                                    </div>
                                                    <div class="clearfix">
                                                        <div class="float-right" style="margin-right: 35px;">
                                                            <label class="mr-3" style="text-align: right;">Action Text Colour</label>
                                                            <input class="form-control" type="input" name="{{ 'action_text_colour__' . $banner_unique_id }}" style="display: inline-block; width: 90px;" value="{{ $banner->action_text_colour }}">
                                                        </div>
                                                        <div class="float-right" style="margin-right: 35px;">
                                                            <label class="mr-3" style="text-align: right;">Action Button Colour</label>
                                                            <input class="form-control" type="input" name="{{ 'action_button_colour__' . $banner_unique_id }}" style="display: inline-block; width: 90px;" value="{{ $banner->action_button_colour }}">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                    @endforeach
                                @endif
                            </ol>
                        </div>
                        @if ($errors->has('banner'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('banner') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="notes">Notes:</label>
                        <textarea rows="3" class="form-control characters-count{{ $errors->has('notes') ? ' is-invalid' : '' }}" id="notes" data-maxlength="400" maxlength="400" placeholder="Notes" name="notes">{{isset($campaign) ? $campaign->notes : old('notes') }}</textarea>
                        <div class="clearfix mt-2">
                            <div class="float-left mr-3">
                                <span>Characters</span>
                                <span class="current-length">0</span>
                            </div>
                            <div class="float-left">
                                <span>Characters remaining</span>
                                <span class="remaining-length">400</span>
                            </div>
                        </div>
                        @if ($errors->has('notes'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('notes') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-2">
                        <label for="status">Status: <span class="tx-danger">*</span></label>
                        <select class="form-control campaign-status-select{{ $errors->has('status') ? ' is-invalid' : '' }}" id="status" name="status" required>
                            @foreach($statuses as $status)
                                <option value="{{ $status }}" @if(isset($campaign) && $campaign->status == $status) selected @elseif(!isset($campaign) && $status == 'Active') selected @else @endif>{{ $status }}</option>
                            @endforeach
                        </select>
                        @if ($errors->has('status'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('status') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <h4 class="mt-1">Publish to Cities:<span class="tx-danger">*</span></h4>
                <div class="form-row">
                    <div class="form-group col-md-4">
                        <div class="cb-wrap">
                            <div class="mb-3 cb-headline-wrap">
                                <div class="custom-control custom-checkbox group-cb-all-wrap">
                                    <input type="checkbox" class="custom-control-input cb-input-all" id="city-group-cb-all" data-type="city-group">
                                    <label class="custom-control-label" for="city-group-cb-all"><h5>Region/CityGroup</h5></label>
                                </div>
                            </div>
                            <div class="cb-group">
                                @foreach($city_groups as $city_group)
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input cb-input city-group-cb-group" id="city-group-cb-{{ $city_group->id }}" data-type="city_groups" data-cities="{{ json_encode($city_group->cities->pluck('id')->toArray()) }}">
                                    <label class="custom-control-label" for="city-group-cb-{{ $city_group->id }}">{{ $city_group->name }}</label>
                                </div>
                                @endforeach  
                            </div>
                        </div>
                    </div>
                    <div class="form-group col-md-4">
                        <div class="cb-wrap">
                            <div class="mb-3 cb-headline-wrap">
                                <div class="custom-control custom-checkbox group-cb-all-wrap">
                                    <input type="checkbox" class="custom-control-input cb-input-all" id="district-cb-all" data-type="district">
                                    <label class="custom-control-label" for="district-cb-all"><h5>Districts</h5></label>
                                </div>
                            </div>
                            <div class="cb-group">
                                @foreach($districts as $district)
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input cb-input district-cb-group" id="district-cb-{{ $district->id }}" data-type="districts" data-cities="{{ json_encode($district->cities->pluck('id')->toArray()) }}">
                                    <label class="custom-control-label" for="district-cb-{{ $district->id }}">{{ $district->name }}</label>
                                </div>
                                @endforeach  
                            </div>
                        </div>
                    </div>
                    <div class="form-group col-md-4">
                        <div class="cb-wrap">
                            <div class="mb-3 cb-headline-wrap">
                                <span class="custom-control custom-checkbox group-cb-all-wrap">
                                    <input type="checkbox" class="custom-control-input cb-input-all" id="city-cb-all" data-type="city">
                                    <label class="custom-control-label" for="city-cb-all"><h5>Cities</h5></label>
                                </span>
                            </div>
                            <div class="cb-group">
                                @foreach($publish_to_cities as $publish_to_district)
                                @foreach($publish_to_district->cities as $publish_to_city)
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input city-cb-group" id="city-cb-{{ $publish_to_city->id }}" name="cities[]" value="{{ $publish_to_city->id }}" @if(isset($campaign) && in_array($publish_to_city->id, $campaign->cities->pluck('id')->toArray())) checked @endif>
                                    <label class="custom-control-label" for="city-cb-{{ $publish_to_city->id }}">{{ $publish_to_city->name }}</label>
                                </div>
                                @endforeach
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
                <div class="text-right">
                    <a href="{{ route('campaigns.index') }}" class="btn btn-warning mg-r-1">Cancel</a>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </form>
        </div>
    </div>
@endsection