@extends('layouts.admin')

@php($title = 'Campaigns')
@push('title', yieldTitle($title))

@section('breadcrumb-title', $title)

@section('breadcrumb-link')
    <li class="breadcrumb-item active" aria-current="page">{{ $title }}</li>
@endsection

@section('breadcrumb-btn')
    <a class="btn btn-sm pd-x-15 btn-primary btn-uppercase mg-l-5" href="{{ route('campaigns.create') }}"><i data-feather="plus" class="wd-10 mg-r-5"></i>Create Campaign</a>
@endsection

@section('breadcrumb')
    @include('components.breadcrumb')
@endsection

@push('content-class', 'content-fixed')
@push('container-class', 'container-fluid')

@section('content')
    <div class="card">
        <div class="card-body">
            <div data-label="Example" class="df-example demo-table">
                <div class="table-responsive">
                    <table class="table table-striped mg-b-0">
                        <thead>
                        <tr>
                            <th scope="col">ID</th>
                            <th scope="col">@sortablelink('name')</th>
                            <th scope="col">Advertiser</th>
                            <th scope="col">Budget</th>
                            <th scope="col">@sortablelink('start_date', 'Start Date')</th>
                            <th scope="col">@sortablelink('end_date', 'End Date')</th>
                            <th scope="col">Status</th>
                            <th scope="col">Impressions</th>
                            <th scope="col">Clicks</th>
                            <th scope="col">Reactions</th>
                            <th scope="col">Shares</th>
                            <th scope="col">@sortablelink('created_at', 'Added On')</th>
                            @if(auth()->user()->can('advertisements crud'))
                                <th scope="col">Actions</th>
                            @endif
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($campaigns as $campaign)
                        <tr>
                            <th scope="row">{{ $campaign->id }}</th>
                            <td>{{ $campaign->name }}</td>
                            <td>{{ $campaign->advertiser->name }} @if(isset($campaign->advertiser->business_name)) {{ ' (' . $campaign->advertiser->business_name . ')' }} @endif</td>
                            <td>{{ $campaign->budget }}</td>
                            <td>{{ $campaign->start_date }}</td>
                            <td>{{ $campaign->end_date }}</td>
                            <td>{{ $campaign->status }}</td>
                            <td>{{ adjustAdsData($campaign->impressions_count) }}</td>
                            <td>{{ $campaign->clicks_count }}</td>
                            <td>{{ $campaign->reactions_count }}</td>
                            <td>{{ $campaign->shares_count }}</td>
                            <td>{{ getDateFormat($campaign->created_at) }}</td>
                            @if(auth()->user()->can('advertisements crud'))
                                <td>
                                    @can('advertisements crud')<a href="{{ route('campaigns.edit', ['id' => $campaign->id ]) }}"><i data-feather="edit-2" width="18" height="18"></i></a>@endcan
                                </td>
                            @endif
                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div><!-- table-responsive -->
            </div><!-- df-example -->
        </div>
    </div>
    <div class="mt-4">
        {!! $campaigns->appends(\Request::except('page'))->render() !!}
    </div>
@endsection