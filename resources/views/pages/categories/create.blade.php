@extends('layouts.admin')

@php($title = isset($category) ? 'Edit Category - ' . $category->name: 'Create Category')
@push('title', yieldTitle($title))

@section('breadcrumb-title', $title)

@section('breadcrumb-link')
    <li class="breadcrumb-item active" aria-current="page">
        <a href="{{ route('categories.index') }}">Categories</a>
    </li>
    <li class="breadcrumb-item active" aria-current="page">{{ $title }}</li>
@endsection

@section('breadcrumb')
    @include('components.breadcrumb')
@endsection

@push('content-class', 'content-fixed')
@push('container-class', 'container-fluid')

@section('content')
    <div class="card">
        <div class="card-body">
            <form action="{{ isset($category) ? route('categories.update', ['id' => $category->id]) : route('categories.store') }}" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="form-row">
                    <div class="form-group col-md-3">
                        <label for="name">Name: <span class="tx-danger">*</span></label>
                        <input type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" id="name" placeholder="Name" name="name" maxlength="100" value="{{isset($category) ? $category->name : old('name') }}" required>
                        @if ($errors->has('name'))
                            <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('name') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="form-group col-md-3">
                        <label for="gujarati_name">Gujarati Name: <span class="tx-danger">*</span></label>
                        <input type="text" class="form-control{{ $errors->has('gujarati_name') ? ' is-invalid' : '' }}" id="gujarati_name" placeholder="Gujarati Name" name="gujarati_name" maxlength="100" value="{{isset($category) ? $category->gujarati_name : old('gujarati_name') }}" required>
                        @if ($errors->has('gujarati_name'))
                            <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('gujarati_name') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="form-group col-md-3">
                        <label for="image">Image: <span class="tx-danger">*</span></label>
                        <div class="custom-file">
                            <input type="file" class="category-image-input custom-file-input{{ $errors->has('image') ? ' is-invalid' : '' }}" id="image" name="image" accept=".png,.jpg,.jpeg" data-accept="['.png','.jpg','.jpeg','.PNG','.JPG','.JPEG']">
                            <label class="custom-file-label" for="image">Choose file</label>
                        </div>
                        <div id="image-files-name">
                            @if(isset($category) && $category->image !== null)<div class="mt-2">{{ $category->image }}</div>@endif
                        </div>
                        @if ($errors->has('image'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('image') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="text-right">
                    <a href="{{ route('categories.index') }}" class="btn btn-warning mg-r-1">Cancel</a>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </form>
        </div>
    </div>
@endsection
