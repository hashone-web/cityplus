@extends('layouts.admin')

@php($title = 'Categories')
@push('title', yieldTitle($title))

@section('breadcrumb-title', $title)

@section('breadcrumb-link')
    <li class="breadcrumb-item active" aria-current="page">{{ $title }}</li>
@endsection

@section('breadcrumb-btn')
    <a class="btn btn-sm pd-x-15 btn-primary btn-uppercase mg-l-5" href="{{ route('categories.sorting.edit') }}"><i data-feather="align-justify" class="wd-10 mg-r-5"></i>Category Sorting</a>
    <a class="btn btn-sm pd-x-15 btn-primary btn-uppercase mg-l-5" href="{{ route('categories.create') }}"><i data-feather="plus" class="wd-10 mg-r-5"></i>Create Category</a>
@endsection

@section('breadcrumb')
    @include('components.breadcrumb')
@endsection

@push('content-class', 'content-fixed')
@push('container-class', 'container-fluid')

@section('content')
    <div class="card">
        <div class="card-body">
            <div data-label="Example" class="df-example demo-table">
                <div class="table-responsive">
                    <table class="table table-striped mg-b-0">
                        <thead>
                        <tr>
                            <th scope="col">ID</th>
                            <th scope="col">@sortablelink('name')</th>
                            <th scope="col">Gujarati Name</th>
                            <th scope="col">Image</th>
                            <th scope="col">@sortablelink('created_at', 'Added On')</th>
                            @if(auth()->user()->can('edit other cruds') || auth()->user()->can('delete other cruds'))
                                <th scope="col">Actions</th>
                                <th style="width: 5%;">Status</th>
                            @endif
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($categories as $category)
                            <tr>
                                <th scope="row">{{ $category->id }}</th>
                                <td>{{ $category->name }}</td>
                                <td>{{ $category->gujarati_name }}</td>
                                <td>@if($category->image !== null)<img style="max-height: 32px;" src="{{ $category->image_full_path }}">@endif</td>
                                <td>{{ getDateFormat($category->created_at) }}</td>
                                @if(auth()->user()->can('edit other cruds') || auth()->user()->can('delete other cruds'))
                                    <td>
                                        @can('edit other cruds')<a href="{{ route('categories.edit', ['id' => $category->id ]) }}"><i data-feather="edit-2" width="18" height="18"></i></a>@endcan
                                        @can('delete other cruds')
                                            <a href="javascript:;" onclick="deleteRecords(event, 'Category', 'category-{{ $category->id }}-delete')"><i data-feather="trash" width="18" height="18" stroke="red"></i></a>

                                            <form id="category-{{ $category->id }}-delete" action="{{ route('categories.delete', ['id' => $category->id]) }}" method="POST" style="display: none;">
                                                @method('delete')
                                                @csrf
                                            </form>
                                        @endcan
                                    </td>
                                    <td>
                                        <input id="category-{{ $category->id }}" class="toggle form-check-input status-change" type="checkbox" name="status" @if($category->status == 1) checked @endif data-id="{{ $category->id }}" data-table="categories">
                                        <label for="category-{{ $category->id }}"><span class="before"></span><span class="after"></span></label>
                                    </td>
                                @endif
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div><!-- table-responsive -->
            </div><!-- df-example -->
        </div>
    </div>
    <div class="mt-4">
        {!! $categories->appends(\Request::except('page'))->render() !!}
    </div>
@endsection
