@extends('layouts.admin')

@php($title = 'Categories Sorting')
@push('title', yieldTitle($title))

@section('breadcrumb-title', $title)

@section('breadcrumb-link')
    <li class="breadcrumb-item active" aria-current="page">
        <a href="{{ route('categories.index') }}">Categories</a>
    </li>
    <li class="breadcrumb-item active" aria-current="page">{{ $title }}</li>
@endsection

@section('breadcrumb-btn')
    <a class="btn btn-sm pd-x-15 btn-warning btn-uppercase mg-l-5" href="{{ route('categories.index') }}"><i data-feather="arrow-left" class="wd-10 mg-r-5"></i>Back</a>
@endsection

@section('breadcrumb')
    @include('components.breadcrumb')
@endsection

@push('content-class', 'content-fixed')
@push('container-class', 'container-fluid')

@section('content')
    <div class="card">
        <div class="card-body">
            <div data-label="Example" class="df-example demo-table">
                <div class="table-responsive">
                    <form class="schema-table-display" action="{{ route('categories.sorting.update') }}" method="POST">
                        {{ csrf_field() }}
                        <div class="m-b-lg mt-5">
                            <input type="hidden" name="display">
                            <ol class="dragable_label sortable parent">
                                @foreach($categories as $key => $category)
                                    <li>
                                        <div class="row">
                                            <div class="col-lg-6 sorting-text-wrap">
                                                <span class="hide" style="display:none">{{ $category->id }}|||</span>{{ $category->name }}
                                            </div>
                                        </div>
                                    </li>
                                @endforeach
                            </ol>
                        </div>
                        <div class="form-group mt-3">
                            <a href="javascript:void(0);" class="btn btn-primary btn-rounded shadow-1 float-right mr-0 update-sorting">Update Sorting</a>
                        </div>
                    </form>
                </div><!-- table-responsive -->
            </div><!-- df-example -->
        </div>
    </div>
@endsection