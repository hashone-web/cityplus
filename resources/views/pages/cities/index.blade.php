@extends('layouts.admin')

@php($title = 'Cities')
@push('title', yieldTitle($title))

@section('breadcrumb-title', $title)

@section('breadcrumb-link')
    <li class="breadcrumb-item active" aria-current="page">{{ $title }}</li>
@endsection

@section('breadcrumb-btn')
    <a class="btn btn-sm pd-x-15 btn-primary btn-uppercase mg-l-5" href="{{ route('cities.create') }}"><i data-feather="plus" class="wd-10 mg-r-5"></i>Create City</a>
@endsection

@section('content-header')
    <div class="float-left ml-3">
        <a class="btn btn-primary" href="{{ route('cities.index') }}"><i class="fa fa-redo"></i></a>
    </div>
    
    <div class="float-left ml-5 form-inline">
        <form method="GET" action="{{ route('cities.index') }}" autocomplete="off">
            <input class="form-control float-left card-header__filter-wrap__search-value mr-2" type="text" name="search_value" value="{{isset($query_parameters['search_value']) ? $query_parameters['search_value'] : ''}}" placeholder="Search City Name">
            <button type="submit" class="btn btn-primary btn-addon btn-rounded float-left card-header__filter-wrap__search-btn">Search</button>
        </form>
    </div>
    
    <div class="float-left ml-3">
        <select class="filter-by-status-select" onchange="location = this.value;" style="width: 120px;">
            <option disabled selected value>Status</option>
            @foreach($statuses as $status)
                <option value="{{ route('cities.index', array_merge($query_parameters, ['status' => $status])) }}" @if(isset($query_parameters['status']) && $query_parameters['status'] == $status) selected @endif>{{ $status }}</option>
            @endforeach
        </select>
    </div>
@endsection

@section('breadcrumb')
    @include('components.breadcrumb')
@endsection

@push('content-class', 'content-fixed')
@push('container-class', 'container-fluid')

@section('content')
    <div class="card">
        <div class="card-body">
            <div data-label="Example" class="df-example demo-table">
                <div class="table-responsive">
                    <table class="table table-striped mg-b-0">
                        <thead>
                        <tr>
                            <th scope="col">ID</th>
                            <th scope="col">@sortablelink('name')</th>
                            <th scope="col">Gujarati Name</th>
                            <th scope="col">Devices</th>
                            <th scope="col">District</th>
                            <th scope="col">@sortablelink('created_at', 'Added On')</th>
                            @if(auth()->user()->can('edit locations') || auth()->user()->can('delete locations'))
                                <th scope="col">Actions</th>
                                <th style="width: 5%;">Status</th>
                            @endif
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($cities as $city)
                            <tr class="{{ $city->status != 1 ? 'inactive-content': 'active-content' }}">
                                <th scope="row">{{ $city->id }}</th>
                                <td>{{ $city->name }}</td>
                                <td>{{ $city->gujarati_name }}</td>
                                <td>{{ $city->devices_count }}</td>
                                <td>{{ $city->district->name }}</td>
                                <td>{{ getDateFormat($city->created_at) }}</td>
                                @if(auth()->user()->can('edit locations') || auth()->user()->can('delete locations'))
                                    <td>
                                        @can('edit locations')<a href="{{ route('cities.edit', ['id' => $city->id ]) }}"><i data-feather="edit-2" width="18" height="18"></i></a>@endcan
                                        @can('delete locations')
                                            <a href="javascript:;" onclick="deleteRecords(event, 'City', 'city-{{ $city->id }}-delete')"><i data-feather="trash" width="18" height="18" stroke="red"></i></a>

                                            <form id="city-{{ $city->id }}-delete" action="{{ route('cities.delete', ['id' => $city->id]) }}" method="POST" style="display: none;">
                                                @method('delete')
                                                @csrf
                                            </form>
                                        @endcan
                                    </td>
                                    <td>
                                        <input id="city-{{ $city->id }}" class="toggle form-check-input status-change" type="checkbox" name="status" @if($city->status == 1) checked @endif data-id="{{ $city->id }}" data-table="cities">
                                        <label for="city-{{ $city->id }}"><span class="before"></span><span class="after"></span></label>
                                    </td>
                                @endif
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div><!-- table-responsive -->
            </div><!-- df-example -->
        </div>
    </div>
    <div class="mt-4">
        {!! $cities->appends(\Request::except('page'))->render() !!}
    </div>
@endsection
