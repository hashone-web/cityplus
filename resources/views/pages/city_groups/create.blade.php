@extends('layouts.admin')

@php($title = isset($city_group) ? 'Edit City Group - ' . $city_group->name: 'Create City Group')
@push('title', yieldTitle($title))

@section('breadcrumb-title', $title)

@section('breadcrumb-link')
    <li class="breadcrumb-item active" aria-current="page">
        <a href="{{ route('city.groups.index') }}">City Groups</a>
    </li>
    <li class="breadcrumb-item active" aria-current="page">{{ $title }}</li>
@endsection

@section('breadcrumb')
    @include('components.breadcrumb')
@endsection

@push('content-class', 'content-fixed')
@push('container-class', 'container-fluid')

@section('content')
    <div class="card">
        <div class="card-body">
            <form id="city-group-form" action="{{ isset($city_group) ? route('city.groups.update', ['id' => $city_group->id]) : route('city.groups.store') }}" method="POST">
                @csrf
                <div class="form-row">
                    <div class="form-group col-md-3">
                        <label for="name">Name: <span class="tx-danger">*</span></label>
                        <input type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" id="name" placeholder="Name" name="name" pattern="^[A-Za-z ]+$" maxlength="30" value="{{isset($city_group) ? $city_group->name : old('name') }}" required>
                        @if ($errors->has('name'))
                            <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('name') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>
                <h4 class="mt-2">Cities: <span class="tx-danger">*</span></h4>
                <div class="form-row">
                    <div class="form-group col-md-4">
                        <div class="cb-wrap">
                            <div class="mb-3 cb-headline-wrap">
                                <div class="custom-control custom-checkbox group-cb-all-wrap">
                                    <input type="checkbox" class="custom-control-input cb-input-all" id="district-cb-all" data-type="district">
                                    <label class="custom-control-label" for="district-cb-all"><h5>Districts</h5></label>
                                </div>
                            </div>
                            <div class="cb-group">
                                @foreach($districts as $district)
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input cb-input district-cb-group" id="district-cb-{{ $district->id }}" data-type="districts" data-cities="{{ json_encode($district->cities->pluck('id')->toArray()) }}">
                                    <label class="custom-control-label" for="district-cb-{{ $district->id }}">{{ $district->name }}</label>
                                </div>
                                @endforeach  
                            </div>
                        </div>
                    </div>
                    <div class="form-group col-md-4">
                        <div class="cb-wrap">
                            <div class="mb-3 cb-headline-wrap">
                                <span class="custom-control custom-checkbox group-cb-all-wrap">
                                    <input type="checkbox" class="custom-control-input cb-input-all" id="city-cb-all" data-type="city">
                                    <label class="custom-control-label" for="city-cb-all"><h5>Cities</h5></label>
                                </span>
                            </div>
                            <div class="cb-group">
                                @foreach($cities as $city)
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input city-cb-group" id="city-cb-{{ $city->id }}" name="cities[]" value="{{ $city->id }}" @if(isset($city_group) && in_array($city->id, $city_group->cities->pluck('id')->toArray())) checked @endif>
                                    <label class="custom-control-label" for="city-cb-{{ $city->id }}">{{ $city->name }}</label>
                                </div>
                                @endforeach  
                            </div>
                        </div>
                    </div>
                </div>
                <div class="text-right">
                    <a href="{{ route('city.groups.index') }}" class="btn btn-warning mg-r-1">Cancel</a>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </form>
        </div>
    </div>
@endsection