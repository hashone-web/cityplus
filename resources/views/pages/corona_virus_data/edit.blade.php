@extends('layouts.admin')

@php($title = 'Corona Virus Data')
@push('title', yieldTitle($title))

@section('breadcrumb-title', $title)

@section('breadcrumb-link')
    <li class="breadcrumb-item active" aria-current="page">{{ $title }}</li>
@endsection

@section('breadcrumb')
    @include('components.breadcrumb')
@endsection

@push('content-class', 'content-fixed')
@push('container-class', 'container-fluid')

@section('content')
    <div class="container">
        <?php
            $corona_virus_data_all = [];
            $corona_virus_data_main = array_slice($corona_virus_data->toArray(), 0, 3);
            $corona_virus_data_gujarat = array_slice($corona_virus_data->toArray(), 3);
            
            array_push($corona_virus_data_all, $corona_virus_data_main);
            array_push($corona_virus_data_all, $corona_virus_data_gujarat);
        ?>
        @foreach($corona_virus_data_all as $index => $corona_virus_data_all_s)
        @if($index == 1)
            <div class="clearfix mt-3">
                <form action="{{ route('corona.virus.data.update.last.updated.at') }}" method="POST">
                    @csrf
                    <div class="float-left">
                        <h4 style="line-height: 38px;">Last updated:</h4>
                    </div>
                    <div class="float-left ml-4">
                        <h4 style="line-height: 38px;">{{ getDateFormat($gujarat_corona_virus_data->last_updated_at) }}</h4>
                    </div>
                    <div class="float-left ml-5">
                        <button type="submit" class="btn btn-primary">Update Now</button>
                    </div>
                </form>
            </div>
        @endif
        <div class="card mt-2">
            <div class="card-body">
                <div data-label="Example" class="df-example demo-table">
                    <div class="table-responsive">
                        <table class="table table-striped mg-b-0 table-fixed @if($index == 1) table-header-fixed @endif">
                            <thead>
                            <tr style="box-shadow: 0 2px 2px -1px rgba(0, 0, 0, 0.4);">
                                <th scope="col" style="width: 175px;">Name</th>
                                <th scope="col" style="width: 12%;">Total <span class="tx-danger">*</span></th>
                                <th scope="col">Active <span class="tx-danger">*</span></th>
                                <th scope="col">Recovered <span class="tx-danger">*</span></th>
                                <th scope="col">Today Recovered <span class="tx-danger">*</span></th>
                                <th scope="col">Deaths <span class="tx-danger">*</span></th>
                                <th scope="col">Today Deaths <span class="tx-danger">*</span></th>
                                <th scope="col">Toady New <span class="tx-danger">*</span></th>
                                <th scope="col" style="width: 72px;">Manual</th>
                                <th scope="col" style="width: 82px;">Action</th>
                            </tr>
                            </thead>
                            <tbody>
                                @foreach(json_decode(json_encode($corona_virus_data_all_s)) as $corona_virus_data_s)
                                <tr>
                                    <form class="corona-virus-data-form" action="{{ route('corona.virus.data.update', ['id' => $corona_virus_data_s->id]) }}" method="POST" enctype="multipart/form-data">
                                        @csrf
                                        <?php
                                            $validate = ($corona_virus_data_s->total - ($corona_virus_data_s->recovered + $corona_virus_data_s->deaths)) == $corona_virus_data_s->active;
                                        ?>
                                        <td>
                                            @if($corona_virus_data_s->parent_id == null)
                                                <h6 class="mb-0">{{ ucfirst($corona_virus_data_s->name) }} @if($validate) <i class="fa fa-check-circle"></i> @else <i class="fa fa-times-circle"></i> @endif</h6>
                                                <div><img class="reaction-img reaction-img--share" src="{{ asset('assets/img/share.png') }}"> <span style="color: green;">{{ $corona_virus_data_s->shares }}</span></div>
                                            @else
                                                <h6 class="mb-0" style="padding: 10px 0;">{{ ucfirst($corona_virus_data_s->name) }} @if($validate) <i class="fa fa-check-circle"></i> @else <i class="fa fa-times-circle"></i> @endif</h6>
                                            @endif
                                        </td>
                                        <td>
                                            <input type="text" class="form-control{{ $errors->has('total') ? ' is-invalid' : '' }}" id="total_{{$corona_virus_data_s->id}}" placeholder="Total" name="total" value="{{isset($corona_virus_data_s) ? $corona_virus_data_s->total : old('total') }}" required="">
                                            @if ($errors->has('total'))
                                                <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('total') }}</strong>
                                            </span>
                                            @endif
                                        </td>
                                        <td>
                                            <input type="text" class="form-control{{ $errors->has('active') ? ' is-invalid' : '' }}" id="active_{{$corona_virus_data_s->id}}" placeholder="Name" name="active" value="{{isset($corona_virus_data_s) ? $corona_virus_data_s->active : old('active') }}" required="">
                                            @if ($errors->has('active'))
                                                <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('active') }}</strong>
                                            </span>
                                            @endif
                                        </td>
                                        <td>
                                            <input type="text" class="form-control{{ $errors->has('recovered') ? ' is-invalid' : '' }}" id="recovered_{{$corona_virus_data_s->id}}" placeholder="Name" name="recovered" value="{{isset($corona_virus_data_s) ? $corona_virus_data_s->recovered : old('recovered') }}" required="">
                                            @if ($errors->has('recovered'))
                                                <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('recovered') }}</strong>
                                            </span>
                                            @endif
                                        </td>
                                        <td>
                                            <input type="text" class="form-control{{ $errors->has('delta_recovered') ? ' is-invalid' : '' }}" id="recovered_{{$corona_virus_data_s->id}}" placeholder="Name" name="delta_recovered" value="{{isset($corona_virus_data_s) ? $corona_virus_data_s->delta_recovered : old('delta_recovered') }}" required="">
                                            @if ($errors->has('delta_recovered'))
                                                <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('delta_recovered') }}</strong>
                                            </span>
                                            @endif
                                        </td>
                                        <td>
                                            <input type="text" class="form-control{{ $errors->has('deaths') ? ' is-invalid' : '' }}" id="deaths_{{$corona_virus_data_s->id}}" placeholder="Name" name="deaths" value="{{isset($corona_virus_data_s) ? $corona_virus_data_s->deaths : old('deaths') }}" required="">
                                            @if ($errors->has('deaths'))
                                                <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('deaths') }}</strong>
                                            </span>
                                            @endif
                                        </td>
                                        <td>
                                            <input type="text" class="form-control{{ $errors->has('delta_deaths') ? ' is-invalid' : '' }}" id="deaths_{{$corona_virus_data_s->id}}" placeholder="Name" name="delta_deaths" value="{{isset($corona_virus_data_s) ? $corona_virus_data_s->delta_deaths : old('delta_deaths') }}" required="">
                                            @if ($errors->has('delta_deaths'))
                                                <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('delta_deaths') }}</strong>
                                            </span>
                                            @endif
                                        </td>
                                        <td>
                                            <input type="text" class="form-control{{ $errors->has('delta_total') ? ' is-invalid' : '' }}" id="delta_total_{{$corona_virus_data_s->id}}" placeholder="Name" name="delta_total" value="{{isset($corona_virus_data_s) ? $corona_virus_data_s->delta_total : old('delta_total') }}" required="">
                                            @if ($errors->has('delta_total'))
                                                <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('delta_total') }}</strong>
                                            </span>
                                            @endif
                                        </td>
                                        <td>
                                            <div class="custom-control custom-checkbox" style="padding-top: 9px; padding-bottom: 7px;">
                                                <input type="checkbox" class="custom-control-input" id="manual_{{$corona_virus_data_s->id}}" name="manual" value="yes" @if(isset($corona_virus_data_s) && $corona_virus_data_s->manual == 1) checked @endif>
                                                <label class="custom-control-label" for="manual_{{$corona_virus_data_s->id}}"></label>
                                            </div>
                                        </td>
                                        <td>
                                            <button type="submit" class="btn btn-primary">Save</button>
                                        </td>
                                    </form>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        @endforeach
    </div>
@endsection