@extends('layouts.admin')

@php($title = 'Custom Notifications')
@push('title', yieldTitle($title))

@section('breadcrumb-title', $title)
@section('breadcrumb-link')
<li class="breadcrumb-item active" aria-current="page">{{ $title }}</li>
@endsection

@section('breadcrumb')
	@include('components.breadcrumb')
@endsection

@push('content-class', 'content-fixed')
@push('container-class', 'container-fluid')

@section('styles')
<style type="text/css">
	.checkbox-inline, .radio-inline {
	    position: relative;
	    display: inline-block;
	    padding-left: 20px;
	    margin-bottom: 0;
	    font-weight: 400;
	    vertical-align: middle;
	    cursor: pointer;
	}
	.radio-inline input[type=checkbox], .radio-inline input[type=radio] {
	    margin: 4px 0 0;
	    line-height: normal;
	}
	.checkbox input[type=checkbox], .checkbox-inline input[type=checkbox], .radio input[type=radio], .radio-inline input[type=radio] {
	    position: absolute;
	    margin-left: -20px;
	}
	.filter-content > .form-group {
        float: left;
        margin-right: 32px;
        width: 250px;
    }

    .filter-grabber-content {
        line-height: 35px;
    }
</style>
@endsection

@section('content')
<div class="card">
	<div class="card-body">
		<form id="custom-notification-form" action="{{ route('custom.notifications.store') }}" method="POST">
			@csrf
			<div class="form-row">
				<div class="form-group col-md-6">
					<label for="title">Title<span style="color: red;">*</span></label>
					<div>
						<input type="text" class="form-control" id="title" placeholder="Enter Title" name="title" value="" required autofocus>
					</div>
					@if ($errors->has('title'))
					<span class="help-block">
						<strong>{{ $errors->first('title') }}</strong>
					</span>
					@endif
				</div>
			</div>
			<div class="form-row">
				<div class="form-group col-md-6">
					<label for="message">Message<span style="color: red;">*</span></label>
					<div>
						<textarea class="form-control" id="message" placeholder="Enter Message" name="content" required autofocus></textarea>
					</div>
					@if ($errors->has('message'))
					<span class="help-block">
						<strong>{{ $errors->first('message') }}</strong>
					</span>
					@endif
				</div>
			</div>
			<div class="form-row">
                    <div class="form-group col-md-3">
                        <label for="media">Big Picture:</label>
                        <div class="custom-file">
                            <input type="file" class="custom-file-input{{ $errors->has('media') ? ' is-invalid' : '' }}" id="media" name="bigpicture" accept=".png,.jpg,.jpeg" data-accept="['.png','.jpg','.jpeg','.PNG','.JPG','.JPEG']">
                            <label class="custom-file-label" for="media">Choose file</label>
                        </div>
                        <div id="media-files-name"></div>
                        @if ($errors->has('media'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('media') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
			<div class="form-row">
				<div class="form-group col-md-6">
					<label for="url">URL</label>
					<div>
						<input type="text" class="form-control" id="url" placeholder="Enter URL" name="url" value="">
					</div>
					@if ($errors->has('url'))
					<span class="help-block">
						<strong>{{ $errors->first('url') }}</strong>
					</span>
					@endif
				</div>
			</div>
			<div class="form-row">
				<div class="form-group col-md-6">
					<label for="priority">Priority</label>
					<div>
						<label class="radio-inline mr-3">
						<input type="radio" name="priority" value="10" checked="">High
						</label>
						<label class="radio-inline">
						<input type="radio" name="priority" value="1">Low
						</label>
					</div>
					@if ($errors->has('priority'))
					<span class="help-block">
					<strong>{{ $errors->first('priority') }}</strong>
					</span>
					@endif
				</div>
			</div>
			<div class="form-row">
				<div class="form-group col-md-3">
					<label class="control-label" for="send_after">Scheduled on:</label>
					<div class="input-group">
                        <input id="send_after" type="text" class="form-control readonly-datepicker" name="send_after" value="{{isset($news) ? \Carbon::createFromFormat('Y-m-d H:i:s', $news->datetime)->format('Y-m-d H:i') : old('datetime') }}" onkeypress="return false;">
                        <span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                    </div>
					<div class="m-t-sm">
						<a id="clear-scheduled" href="javascript:void(0);">Clear Scheduled</a>
					</div>
				</div>
			</div>
			<div class="form-group" style="margin-top: 32px;">
				<h2>Data:</h2>
				<div>
					<div class="add-new-field-btn-wrap" style="margin-bottom: 32px;">
						<a href="javascript:void(0);" class="btn btn-primary btn-addon btn-rounded add-new-data-field"><i class="fa fa-plus mr-3"></i>Add New Data Field</a>
					</div>
					<div class="data-wrap"></div>
				</div>
			</div>
			<div class="text-right">
				<a href="{{ route('categories.index') }}" class="btn btn-warning mg-r-1">Cancel</a>
				<button type="submit" class="btn btn-primary">Submit</button>
			</div>
		</form>
	</div>
</div>
@endsection
@section('scripts')
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/handlebars.js/4.0.10/handlebars.min.js"></script>
<script id="data_template" type="text/x-handlebars-template">
	<div class="filter-content clearfix">
		<div class="form-group"><input type="text" name="data_key___@{{timestamp}}" class="form-control" placeholder="Key" value="@{{field.data_key}}"></div>
			<div class="form-group"><input type="text" name="data_value___@{{timestamp}}" class="form-control" placeholder="Value" value="@{{field.data_value}}"></div>
			<div class="form-group">
			<div class="btn btn-warning btn-addon btn-rounded bs-example-remove" data-id="@{{timestamp}}"><i class="fa fa-close"></i>Remove</div>
		</div>
	</div>
</script>
<script>
var global_obj = {};
$(function () {
	var dtp = $('#datetimepicker').datetimepicker({
		autoclose: true,
		showMeridian: true
	});

	function parseQuery(queryString) {
		var query = {};
		var pairs = (queryString[0] === '?' ? queryString.substr(1) : queryString).split('&');
		for (var i = 0; i < pairs.length; i++) {
			var pair = pairs[i].split('=');
			query[decodeURIComponent(pair[0])] = decodeURIComponent(pair[1] || '');
		}
		return query;
	}

	var options = parseQuery(document.location.search);

	$('#clear-scheduled').on('click', function (e) {
		e.preventDefault();
		$('#send_after').val('');
	})

	$(document).on('click', '.add-new-data-field', function (e) {
		e.preventDefault();
		if (!global_obj.data_module) global_obj.data_module = {};

		var id = +new Date();
		global_obj.data_module['d__' + id] = {
			data_key: '',
			data_value: ''
		};

		renderHtml();
	});

	var data_template_source = $('#data_template').html();
	var data_template = Handlebars.compile(data_template_source);

	function renderHtml() {
		var data = {};
		$('#custom-notification-form').serializeArray().map(function (x) {
			if (x.name.indexOf('data_') !== -1) {
				data[x.name] = x.value;
			}
		})

		$.each(data, function (field_index, field) {
			var fields_arr = field_index.split('___');
			var field_id_stg = fields_arr.pop();
			var field_id = field_id_stg;
			var field_name = fields_arr.shift();
			var field_id_stg_arr = field_id_stg.split('__');
			var field_type = field_id_stg_arr.shift();
			if (field_type == 'd') {
				if (global_obj.data_module[field_id]) {
					global_obj.data_module[field_id][field_name] = field;
				}
			}
		})

		$.each(global_obj, function (i, e) {
			if (i == 'data_module') {
				var finalHtml = '';
				$.each(e, function (field_index, field) {
					var html = data_template({
						timestamp: field_index,
						field: field
					});
					finalHtml += html;
				})
				$('.data-wrap').html(finalHtml);
			}
		});
	}

	var ic = 0;

	function preData(options) {
		if (!global_obj.data_module) global_obj.data_module = {};

		$.each(options, function (i, e) {
			var id = +new Date();
			id = parseInt(id) + (ic++);
			global_obj.data_module['d__' + id] = {
				data_key: i,
				data_value: e
			};
		});

		renderHtml();
	}

	preData(options);

	$(document).on('click', '.bs-example-remove', function (e) {
		e.preventDefault();

		var self = $(this);
		var id = self.data('id');

		delete global_obj.data_module[id];

		renderHtml();
	})
});

function shownewDiv() {
	document.getElementById('create-new').style.display = "block";
	document.getElementById('exiting').style.display = "none";
	$(".panel-heading").show();
}

function showexitingDiv() {
	document.getElementById('exiting').style.display = "block";
	document.getElementById('create-new').style.display = "none";
	$(".panel-heading").show();
}
</script>
@endsection