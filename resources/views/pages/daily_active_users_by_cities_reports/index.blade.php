@extends('layouts.admin')

@php($title = 'News Statistics Reports')
@push('title', yieldTitle($title))

@section('breadcrumb-title', $title)

@section('breadcrumb-link')
    <li class="breadcrumb-item active" aria-current="page">{{ $title }}</li>
@endsection

@section('breadcrumb-btn')
    
@endsection

@section('breadcrumb')
    @include('components.breadcrumb')
@endsection

@push('content-class', 'content-fixed')
@push('container-class', 'container-fluid')

@section('content')
    <div class="card">
        <div id="graph"></div>
    </div>
    <div class="card mt-4">
        <div class="card-body">
            <div data-label="Example" class="df-example demo-table">
                <div class="table-responsive">
                    <table class="table table-striped mg-b-0" id="news-statistics-reports-table">
                        <thead>
                            <tr>
                                <th>Date</th>
                                <th>City</th>
                                <th>Today Active</th>
                                <th>Weekly Active</th>
                                <th>Monthly Active</th>
                                <th>Today Registered</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($reports as $report)
                                <tr>
                                    <td>{{ $report['devices_active_24'][0]['date'] }}</td>
                                    <td>{{ $report['devices_active_24'][0]['city']['name'] }}</td>
                                    <td>{{ $report['devices_active_24'][0]['data'] }}</td>
                                    <td>{{ $report['devices_active_168'][0]['data'] }}</td>
                                    <td>{{ $report['devices_active_720'][0]['data'] }}</td>
                                    <td>{{ $report['devices_registered_24'][0]['data'] }}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="mt-4">
        
    </div>
@endsection

@section('last-scripts')
    <script type="text/javascript">
        $(document).ready( function () {
            $('#news-statistics-reports-table').DataTable({
                "order": [[ 0, "desc" ]],
                "lengthMenu": [[20, 50, -1], [20, 50, "All"]],
                "iDisplayLength": 20
            });
        });
    </script>
@endsection