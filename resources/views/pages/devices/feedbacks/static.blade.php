@extends('layouts.admin')

@php($title = 'Static Feedbacks')
@push('title', yieldTitle($title))

@section('breadcrumb-title', $title . ' (' . $static_feedbacks_total . ')')

@section('breadcrumb-link')
    <li class="breadcrumb-item active" aria-current="page">{{ $title }}</li>
@endsection

@section('content-header')
    
@endsection

@section('breadcrumb-btn')
    <div class="mt-3"><h4>No Feedback <span class="tx-danger">({{ $no_feedback_percentage . '%' }})</span></h4></div>
@endsection

@section('breadcrumb')
    @include('components.breadcrumb')
@endsection

@push('content-class', 'content-fixed')
@push('container-class', 'container-fluid')

@section('content')
    <div class="row mb-5">
        <div class="col-md-6">
            <div class="card">
                <div class="card-header bd-b-0 d-flex align-items-center justify-content-between">
                    <h4 class="lh-5 mg-b-0">Yes <span class="tx-success">({{ $yes_percentage . '%' }})</span></h4>
                </div><!-- card-header -->
                <div class="card-body pd-0">
                    <ul class="list-unstyled mg-b-0">
                      @foreach($feedback_options['Yes'] as $feedback_option_id => $feedback_option)
                      <li class="list-item">
                        <div class="media">
                          <div class="crypto-icon crypto-icon-sm bg-success">
                            {{ $feedback_option_id }}
                          </div>
                          <div class="media-body mg-l-15">
                            <p class="tx-medium mg-b-0" style="line-height: 36px;">{{ $feedback_option['title'] }}</p>
                          </div>
                        </div><!-- media -->
                        <div class="text-right">
                          <p class="tx-normal tx-rubik mg-b-0">{{ $feedback_option['count'] }}</p>
                          <p class="mg-b-0 tx-12 tx-rubik tx-success">{{ $feedback_option['percentage'] }}%</p>
                        </div>
                      </li>
                      @endforeach
                    </ul>
                </div><!-- card-body -->
            </div><!-- card -->
        </div>
        <div class="col-md-6">
            <div class="card">
                <div class="card-header bd-b-0 d-flex align-items-center justify-content-between">
                    <h4 class="lh-5 mg-b-0">No <span class="tx-danger">({{ $no_percentage . '%' }})</span></h4>
                </div><!-- card-header -->
                <div class="card-body pd-0">
                    <ul class="list-unstyled mg-b-0">
                      @foreach($feedback_options['No'] as $feedback_option_id => $feedback_option)
                      <li class="list-item">
                        <div class="media">
                          <div class="crypto-icon crypto-icon-sm bg-danger">
                            {{ $feedback_option_id }}
                          </div>
                          <div class="media-body mg-l-15">
                            <p class="tx-medium mg-b-0" style="line-height: 36px;">{{ $feedback_option['title'] }}</p>
                          </div>
                        </div><!-- media -->
                        <div class="text-right">
                          <p class="tx-normal tx-rubik mg-b-0">{{ $feedback_option['count'] }}</p>
                          <p class="mg-b-0 tx-12 tx-rubik tx-danger">{{ $feedback_option['percentage'] }}%</p>
                        </div>
                      </li>
                      @endforeach
                    </ul>
                </div><!-- card-body -->
            </div><!-- card -->
        </div>
    </div>
    <div class="row mb-4">
        <div class="col-md-6">
            <div class="card">
                <div class="card-header bd-b-0 d-flex align-items-center justify-content-between">
                    <h4 class="lh-5 mg-b-0">Yes - Messages</h4>
                </div><!-- card-header -->
                <div class="card-body pd-0">
                    <ul class="list-unstyled mg-b-0">
                      @foreach($yes_messages as $yes_message_key => $yes_message)
                      <li class="list-item">
                        <div class="media">
                          <div class="crypto-icon crypto-icon-sm bg-success">
                            {{ $yes_message_key+1 }}
                          </div>
                          <div class="media-body mg-l-15">
                            <p class="tx-medium mg-b-0" style="line-height: 36px;">{{ $yes_message->message }}</p>
                          </div>
                        </div><!-- media -->
                        <div class="text-right">
                          <p class="tx-normal tx-rubik mg-b-0">{{ getDateFormat($yes_message->created_at) }}</p>
                        </div>
                      </li>
                      @endforeach
                    </ul>
                </div><!-- card-body -->
            </div><!-- card -->
        </div>
        <div class="col-md-6">
            <div class="card">
                <div class="card-header bd-b-0 d-flex align-items-center justify-content-between">
                    <h4 class="lh-5 mg-b-0">No - Messages</h4>
                </div><!-- card-header -->
                <div class="card-body pd-0">
                    <ul class="list-unstyled mg-b-0">
                      @foreach($no_messages as $no_message_key => $no_message)
                      <li class="list-item">
                        <div class="media">
                          <div class="crypto-icon crypto-icon-sm bg-danger">
                            {{ $no_message_key+1 }}
                          </div>
                          <div class="media-body mg-l-15">
                            <p class="tx-medium mg-b-0" style="line-height: 36px;">{{ $no_message->message }}</p>
                          </div>
                        </div><!-- media -->
                        <div class="text-right">
                          <p class="tx-normal tx-rubik mg-b-0">{{ getDateFormat($no_message->created_at) }}</p>
                        </div>
                      </li>
                      @endforeach
                    </ul>
                </div><!-- card-body -->
            </div><!-- card -->
        </div>
    </div>
@endsection