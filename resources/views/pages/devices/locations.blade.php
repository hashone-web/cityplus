@extends('layouts.admin')

@php($title = 'Devices Locations')
@push('title', yieldTitle($title))

@section('breadcrumb-title', $title)

@section('breadcrumb-link')
    <li class="breadcrumb-item active" aria-current="page">{{ $title }}</li>
@endsection

@section('breadcrumb')
    @include('components.breadcrumb')
@endsection

@push('content-class', 'content-fixed')
@push('container-class', 'container-fluid')

@section('content')
    <div class="card">
        <div class="card-body">
            <div data-label="Example" class="df-example demo-table">
                <div class="table-responsive">
                    <table class="table table-striped mg-b-0">
                        <thead>
                        <tr>
                            <th scope="col">@sortablelink('created_at', 'DateTime')</th>
                            <th scope="col">Full Address</th>
                            <th scope="col">Address</th>
                            <th scope="col">City Name</th>
                            <th scope="col">State</th>
                            <th scope="col">Country Name</th>
                            <th scope="col">Pin Code</th>
                            <th scope="col">Latitude</th>
                            <th scope="col">Longitute</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($device_locations as $device_location)
                        <?php
                            $data = [];
                            try {
                                $data = json_decode($device_location->location_details, true);
                            } catch (\Exception $e) {
                                dd($e);
                            }
                        ?>
                        <tr>
                            <td>{{ getDateFormat($device_location->created_at) }}</td>
                            <td>{{ $data['full_address'] }}</td>
                            <td>{{ $data['address'] }}</td>
                            <td>{{ $data['city_name'] }}</td>
                            <td>{{ $data['state_name'] }}</td>
                            <td>{{ $data['country_name'] }}</td>
                            <td>{{ $data['postal_code'] }}</td>
                            <td>{{ $data['latitude'] }}</td>
                            <td>{{ $data['longitute'] }}</td>
                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div><!-- table-responsive -->
            </div><!-- df-example -->
        </div>
    </div>
    <div class="mt-4">
        {!! $device_locations->appends(\Request::except('page'))->render() !!}
    </div>
@endsection