@extends('layouts.admin')

@php($title = isset($district) ? 'Edit District - ' . $district->name: 'Create District')
@push('title', yieldTitle($title))

@section('breadcrumb-title', $title)

@section('breadcrumb-link')
    <li class="breadcrumb-item active" aria-current="page">
        <a href="{{ route('districts.index') }}">Districts</a>
    </li>
    <li class="breadcrumb-item active" aria-current="page">{{ $title }}</li>
@endsection

@section('breadcrumb')
    @include('components.breadcrumb')
@endsection

@push('content-class', 'content-fixed')
@push('container-class', 'container-fluid')

@section('content')
    <div class="card">
        <div class="card-body">
            <form action="{{ isset($district) ? route('districts.update', ['id' => $district->id]) : route('districts.store') }}" method="POST">
                @csrf
                <div class="form-row">
                    <div class="form-group col-md-3">
                        <label for="name">Name: <span class="tx-danger">*</span></label>
                        <input type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" id="name" placeholder="Name" name="name" pattern="^[A-Za-z\- ()]+$" maxlength="30" value="{{isset($district) ? $district->name : old('name') }}" required>
                        @if ($errors->has('name'))
                            <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('name') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="form-group col-md-3">
                        <label for="gujarati_name">Gujarati Name: <span class="tx-danger">*</span></label>
                        <input type="text" class="form-control{{ $errors->has('gujarati_name') ? ' is-invalid' : '' }}" id="gujarati_name" placeholder="Gujarati Name" name="gujarati_name" maxlength="30" value="{{isset($district) ? $district->gujarati_name : old('gujarati_name') }}" required>
                        @if ($errors->has('gujarati_name'))
                            <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('gujarati_name') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="form-group col-md-3">
                        <label for="state">State: <span class="tx-danger">*</span></label>
                        <select class="form-control district-state-select" id="state" name="state" required>
                            <option disabled selected value>Select State</option>
                            @foreach($states as $state)
                                <option value="{{ $state->id }}" @if(isset($district) && $district->state_id == $state->id) selected @endif>{{ $state->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="text-right">
                    <a href="{{ route('districts.index') }}" class="btn btn-warning mg-r-1">Cancel</a>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </form>
        </div>
    </div>
@endsection
