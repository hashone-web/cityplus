@extends('layouts.admin')

@php($title = 'Districts')
@push('title', yieldTitle($title))

@section('breadcrumb-title', $title)

@section('breadcrumb-link')
    <li class="breadcrumb-item active" aria-current="page">{{ $title }}</li>
@endsection

@section('breadcrumb-btn')
    <a class="btn btn-sm pd-x-15 btn-primary btn-uppercase mg-l-5" href="{{ route('districts.create') }}"><i data-feather="plus" class="wd-10 mg-r-5"></i>Create District</a>
@endsection

@section('content-header')
    <div class="float-left ml-3">
        <a class="btn btn-primary" href="{{ route('districts.index') }}"><i class="fa fa-redo"></i></a>
    </div>
    
    <div class="float-left ml-5 form-inline">
        <form method="GET" action="{{ route('districts.index') }}" autocomplete="off">
            <input class="form-control float-left card-header__filter-wrap__search-value mr-2" type="text" name="search_value" value="{{isset($query_parameters['search_value']) ? $query_parameters['search_value'] : ''}}" placeholder="Search District Name">
            <button type="submit" class="btn btn-primary btn-addon btn-rounded float-left card-header__filter-wrap__search-btn">Search</button>
        </form>
    </div>
    
    <div class="float-left ml-3">
        <select class="filter-by-status-select" onchange="location = this.value;" style="width: 120px;">
            <option disabled selected value>Status</option>
            @foreach($statuses as $status)
                <option value="{{ route('districts.index', array_merge($query_parameters, ['status' => $status])) }}" @if(isset($query_parameters['status']) && $query_parameters['status'] == $status) selected @endif>{{ $status }}</option>
            @endforeach
        </select>
    </div>
@endsection

@section('breadcrumb')
    @include('components.breadcrumb')
@endsection

@push('content-class', 'content-fixed')
@push('container-class', 'container-fluid')

@section('content')
    <div class="card">
        <div class="card-body">
            <div data-label="Example" class="df-example demo-table">
                <div class="table-responsive">
                    <table class="table table-striped mg-b-0">
                        <thead>
                        <tr>
                            <th scope="col">ID</th>
                            <th scope="col">@sortablelink('name')</th>
                            <th scope="col">Gujarati Name</th>
                            <th scope="col">State</th>
                            <th scope="col">@sortablelink('created_at', 'Added On')</th>
                            @if(auth()->user()->can('edit locations') || auth()->user()->can('delete locations'))
                                <th scope="col">Actions</th>
                                <th style="width: 5%;">Status</th>
                            @endif
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($districts as $district)
                            <tr class="{{ $district->status != 1 ? 'inactive-content': 'active-content' }}">
                                <th scope="row">{{ $district->id }}</th>
                                <td>{{ $district->name }}</td>
                                <td>{{ $district->gujarati_name }}</td>
                                <td>{{ $district->state->name }}</td>
                                <td>{{ getDateFormat($district->created_at) }}</td>
                                @if(auth()->user()->can('edit locations') || auth()->user()->can('delete locations'))
                                    <td>
                                        @can('edit locations')<a href="{{ route('districts.edit', ['id' => $district->id ]) }}"><i data-feather="edit-2" width="18" height="18"></i></a>@endcan
                                        @can('delete locations')
                                            <a href="javascript:;" onclick="deleteRecords(event, 'District', 'district-{{ $district->id }}-delete')"><i data-feather="trash" width="18" height="18" stroke="red"></i></a>

                                            <form id="district-{{ $district->id }}-delete" action="{{ route('districts.delete', ['id' => $district->id]) }}" method="POST" style="display: none;">
                                                @method('delete')
                                                @csrf
                                            </form>
                                        @endcan
                                    </td>
                                    <td>
                                        <input id="district-{{ $district->id }}" class="toggle form-check-input status-change" type="checkbox" name="status" @if($district->status == 1) checked @endif data-id="{{ $district->id }}" data-table="districts">
                                        <label for="district-{{ $district->id }}"><span class="before"></span><span class="after"></span></label>
                                    </td>
                                @endif
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div><!-- table-responsive -->
            </div><!-- df-example -->
        </div>
    </div>
    <div class="mt-4">
        {!! $districts->appends(\Request::except('page'))->render() !!}
    </div>
@endsection
