@extends('layouts.admin')

@php($title = isset($locality) ? 'Edit Locality - ' . $locality->name: 'Create Locality')
@push('title', yieldTitle($title))

@section('breadcrumb-title', $title)

@section('breadcrumb-link')
    <li class="breadcrumb-item active" aria-current="page">
        <a href="{{ route('localities.index') }}">Localities</a>
    </li>
    <li class="breadcrumb-item active" aria-current="page">{{ $title }}</li>
@endsection

@section('breadcrumb')
    @include('components.breadcrumb')
@endsection

@push('content-class', 'content-fixed')
@push('container-class', 'container-fluid')

@section('content')
    <div class="card">
        <div class="card-body">
            <form action="{{ isset($locality) ? route('localities.update', ['id' => $locality->id]) : route('localities.store') }}" method="POST">
                @csrf
                <div class="form-row">
                    <div class="form-group col-md-3">
                        <label for="name">Name: <span class="tx-danger">*</span></label>
                        <input type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" id="name" placeholder="Name" name="name" pattern="^[A-Za-z ]+$" maxlength="30" value="{{isset($locality) ? $locality->name : old('name') }}" required>
                        @if ($errors->has('name'))
                            <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('name') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="form-group col-md-3">
                        <label for="city">City: <span class="tx-danger">*</span></label>
                        <select class="form-control locality-city-select" id="city" name="city" required>
                            <option disabled selected value>Select City</option>
                            @foreach($cities as $city)
                                <option value="{{ $city->id }}" @if(isset($locality) && $locality->city_id == $city->id) selected @endif>{{ $city->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="text-right">
                    <a href="{{ route('localities.index') }}" class="btn btn-warning mg-r-1">Cancel</a>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </form>
        </div>
    </div>
@endsection
