@extends('layouts.admin')

@php($title = 'Localities')
@push('title', yieldTitle($title))

@section('breadcrumb-title', $title)

@section('breadcrumb-link')
    <li class="breadcrumb-item active" aria-current="page">{{ $title }}</li>
@endsection

@section('breadcrumb-btn')
    <a class="btn btn-sm pd-x-15 btn-primary btn-uppercase mg-l-5" href="{{ route('localities.create') }}"><i data-feather="plus" class="wd-10 mg-r-5"></i>Create Locality</a>
@endsection

@section('breadcrumb')
    @include('components.breadcrumb')
@endsection

@push('content-class', 'content-fixed')
@push('container-class', 'container-fluid')

@section('content')
    <div class="card">
        <div class="card-body">
            <div data-label="Example" class="df-example demo-table">
                <div class="table-responsive">
                    <table class="table table-striped mg-b-0">
                        <thead>
                        <tr>
                            <th scope="col">ID</th>
                            <th scope="col">@sortablelink('name')</th>
                            <th scope="col">City</th>
                            <th scope="col">@sortablelink('created_at', 'Added On')</th>
                            @if(auth()->user()->can('edit locations') || auth()->user()->can('delete locations'))
                                <th scope="col">Actions</th>
                            @endif
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($localities as $locality)
                            <tr>
                                <th scope="row">{{ $locality->id }}</th>
                                <td>{{ $locality->name }}</td>
                                <td>{{ $locality->city->name }}</td>
                                <td>{{ getDateFormat($locality->created_at) }}</td>
                                @if(auth()->user()->can('edit locations') || auth()->user()->can('delete locations'))
                                    <td>
                                        @can('edit locations')<a href="{{ route('localities.edit', ['id' => $locality->id ]) }}"><i data-feather="edit-2" width="18" height="18"></i></a>@endcan
                                        @can('delete locations')
                                            <a href="javascript:;" onclick="deleteRecords(event, 'Locality', 'locality-{{ $locality->id }}-delete')"><i data-feather="trash" width="18" height="18" stroke="red"></i></a>

                                            <form id="locality-{{ $locality->id }}-delete" action="{{ route('localities.delete', ['id' => $locality->id]) }}" method="POST" style="display: none;">
                                                @method('delete')
                                                @csrf
                                            </form>
                                        @endcan
                                    </td>
                                @endif
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div><!-- table-responsive -->
            </div><!-- df-example -->
        </div>
    </div>
    <div class="mt-4">
        {!! $localities->appends(\Request::except('page'))->render() !!}
    </div>
@endsection
