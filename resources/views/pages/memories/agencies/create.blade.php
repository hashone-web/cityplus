@extends('layouts.admin')

@php($title = isset($agency) ? 'Edit Agency - ' . $agency->name: 'Create Agency')
@push('title', yieldTitle($title))

@section('breadcrumb-title', $title)

@section('breadcrumb-link')
    <li class="breadcrumb-item active" aria-current="page">
        <a href="{{ route('agencies.index') }}">Agencies</a>
    </li>
    <li class="breadcrumb-item active" aria-current="page">{{ $title }}</li>
@endsection

@section('breadcrumb')
    @include('components.breadcrumb')
@endsection

@push('content-class', 'content-fixed')
@push('container-class', 'container-fluid')

@section('content')
    <div class="card">
        <div class="card-body">
            <form action="{{ isset($agency) ? route('agencies.update', ['id' => $agency->id]) : route('agencies.store') }}" method="POST">
                @csrf
                <div class="form-row">
                    <div class="form-group col-md-3">
                        <label for="name">Name: <span class="tx-danger">*</span></label>
                        <input type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" id="name" placeholder="Kapeel Patel" name="name" pattern="^[A-Za-z ]+$" maxlength="30" value="{{isset($agency) ? $agency->name : old('name') }}" required>
                        @if ($errors->has('name'))
                            <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('name') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="form-group col-md-3">
                        <label for="business_name">Business Name:</label>
                        <input type="text" class="form-control{{ $errors->has('business_name') ? ' is-invalid' : '' }}" id="business_name" placeholder="Hashone Tech LLP" name="business_name" pattern="^[A-Za-z ]+$" maxlength="30" value="{{isset($agency) ? $agency->business_name : old('business_name') }}">
                        @if ($errors->has('business_name'))
                            <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('business_name') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="form-group col-md-3">
                        <label for="contact_number">Contact Number:</label>
                        <input type="text" class="form-control{{ $errors->has('contact_number') ? ' is-invalid' : '' }}" id="contact_number" placeholder="8000880005" name="contact_number" pattern="^[0-9]+$" maxlength="10" value="{{isset($agency) ? $agency->contact_number : old('contact_number') }}">
                        @if ($errors->has('contact_number'))
                            <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('contact_number') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>
                <div class="text-right">
                    <a href="{{ route('agencies.index') }}" class="btn btn-warning mg-r-1">Cancel</a>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </form>
        </div>
    </div>
@endsection