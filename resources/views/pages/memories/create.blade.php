@extends('layouts.admin')

@php($title = isset($memory) ? 'Edit Memory - ' . $memory->name: 'Create Memory')
@push('title', yieldTitle($title))

@section('breadcrumb-title', $title)

@section('breadcrumb-link')
    <li class="breadcrumb-item active" aria-current="page">
        <a href="{{ route('memories.index') }}">Memories</a>
    </li>
    <li class="breadcrumb-item active" aria-current="page">{{ $title }}</li>
@endsection

@section('breadcrumb')
    @include('components.breadcrumb')
@endsection

@push('content-class', 'content-fixed')
@push('container-class', 'container-fluid')

@section('content')
    <div class="card">
        <div class="card-body">
            <form id="memory-form" action="{{ isset($memory) ? route('memories.update', ['id' => $memory->id]) : route('memories.store') }}" method="POST" enctype="multipart/form-data" data-type="{{ isset($memory)? 'edit': 'create' }}" autocomplete="off">
                @csrf
                <div class="form-row">
                    <div class="form-group col-md-3">
                        <div class="mb-2">Type: <span class="tx-danger">*</span></div>
                        <div class="form-inline">
                            <div class="custom-control custom-radio mr-3">
                                <input type="radio" id="customRadio1" name="type" class="custom-control-input memory-type-radio" value="remembrance" @if(isset($memory) && $memory->type == 'remembrance') checked @elseif(!isset($memory)) checked @else @endif>
                                <label class="custom-control-label" for="customRadio1">Remembrance</label>
                            </div>
                            <div class="custom-control custom-radio mr-3">
                                <input type="radio" id="customRadio2" name="type" class="custom-control-input memory-type-radio" value="obituary" @if(isset($memory) && $memory->type == 'obituary') checked @endif>
                                <label class="custom-control-label" for="customRadio2">Obituary</label>
                            </div>
                            <div class="custom-control custom-radio">
                                <input type="radio" id="customRadio3" name="type" class="custom-control-input memory-type-radio" value="tribute" @if(isset($memory) && $memory->type == 'tribute') checked @endif>
                                <label class="custom-control-label" for="customRadio3">Tribute</label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-row memory-frames-wrap">
                    <div class="form-group col-md-12">
                        <div class="mb-2">Frame: <span class="tx-danger">*</span></div>
                        <div class="clearfix">
                            @foreach($memory_frame_types as $memory_frame_type_name => $memory_frame_type_frames)
                                <div class="clearfix memory-frame-types-wrap {{ $memory_frame_type_name . '-wrap' }}" @if(!isset($memory) && $memory_frame_type_name !== 'remembrance') style="display: none;" @elseif(isset($memory) && $memory->type !== $memory_frame_type_name) style="display: none;" @else @endif>
                                @foreach($memory_frame_type_frames as $memory_frame_type_frame_index => $memory_frame_type_frame)
                                    <div class="float-left mr-2">
                                        <label for="frameRadio{{ $memory_frame_type_frame->id }}">
                                            <input type="radio" id="frameRadio{{ $memory_frame_type_frame->id }}" name="memory_frame_type_id" value="{{ $memory_frame_type_frame->id }}" @if(!isset($memory) && $memory_frame_type_frame_index == 0 && $memory_frame_type_name == 'remembrance') checked @elseif(isset($memory) && $memory->memory_frame_type_id == $memory_frame_type_frame->id) checked @else @endif>
                                            <img style="max-height: 130px; padding: 2px;" src="{{ $memory_frame_type_frame->thumbnail }}">
                                        </label>
                                    </div>
                                @endforeach
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="heading">Heading: <span class="tx-danger">*</span></label>
                        <input class="form-control characters-count{{ $errors->has('heading') ? ' is-invalid' : '' }}" id="heading" data-maxlength="50" maxlength="50" placeholder="Heading" name="heading" value="{{isset($memory) ? $memory->heading : old('heading') }}" required="">
                        <div class="clearfix mt-2">
                            <div class="float-left mr-3">
                                <span>Characters</span>
                                <span class="current-length">0</span>
                            </div>
                            <div class="float-left">
                                <span>Characters remaining</span>
                                <span class="remaining-length">50</span>
                            </div>
                        </div>
                        @if ($errors->has('heading'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('heading') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-2">
                        <label for="name_prefix">Name Prefix: <span class="tx-danger">*</span></label>
                        <select class="form-control memory-name-prefix-select{{ $errors->has('name_prefix') ? ' is-invalid' : '' }}" id="name_prefix" name="name_prefix" required>
                            @foreach($name_prefixes as $name_prefix)
                                <option value="{{ $name_prefix }}" @if(isset($memory) && $memory->name_prefix == $name_prefix) selected @endif>{{ $name_prefix }}</option>
                            @endforeach
                        </select>
                        @if ($errors->has('name_prefix'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('name_prefix') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group col-md-4">
                        <label for="name">Name: <span class="tx-danger">*</span></label>
                        <input type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" id="name" placeholder="Name" name="name" maxlength="100" value="{{isset($memory) ? $memory->name : old('name') }}" required>
                        @if ($errors->has('name'))
                            <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('name') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>
                <div class="form-row mt-1">
                    <div class="form-group col-md-3">
                        <label for="photo">Photo: <span class="tx-danger">*</span></label>
                        <div class="custom-file">
                            <input type="file" class="memory-photo-input custom-file-input{{ $errors->has('photo') ? ' is-invalid' : '' }}" id="photo" name="photo" accept=".png,.jpg,.jpeg" data-accept="['.png','.jpg','.jpeg','.PNG','.JPG','.JPEG']" @if(!isset($memory)) required="" @endif>
                            <label class="custom-file-label" for="photo">Choose file</label>
                        </div>
                        <div id="photo-files-name">
                            @if(isset($memory) && $memory->photo !== null)<div class="mt-2">{{ $memory->photo }}</div>@endif
                        </div>
                        @if ($errors->has('photo'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('photo') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-2">
                        <label for="death_date">Death Date: <span class="tx-danger">*</span></label>
                        <div class="input-group">
                            <input id="death_date" type="text" class="form-control readonly-datepicker" name="death_date" value="{{isset($memory) ? $memory->death_date : old('death_date') }}" required onkeypress="return false;">
                            <span class="input-group-addon">
                                <span class="glyphicon glyphicon-calendar"></span>
                            </span>
                        </div>
                        @if ($errors->has('death_date'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('death_date') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group col-md-4">
                        <label for="death_date_tithi_text">Tithi Text:</label>
                        <input type="text" class="form-control{{ $errors->has('death_date_tithi_text') ? ' is-invalid' : '' }}" id="death_date_tithi_text" placeholder="Tithi Text" name="death_date_tithi_text" maxlength="100" value="{{isset($memory) ? $memory->death_date_tithi_text : old('death_date_tithi_text') }}">
                        @if ($errors->has('death_date_tithi_text'))
                            <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('death_date_tithi_text') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="form-group col-md-2">
                        <label for="birth_date">Birth Date:</label>
                        <div class="input-group">
                            <input id="birth_date" type="text" class="form-control readonly-datepicker" name="birth_date" value="{{isset($memory) ? $memory->birth_date : old('birth_date') }}" onkeypress="return false;">
                            <span class="input-group-addon">
                                <span class="glyphicon glyphicon-calendar"></span>
                            </span>
                        </div>
                        <div class="m-t-sm">
                            <a id="memory-clear-birth-date" href="javascript:void(0);">Clear Birth Date</a>
                        </div>
                        @if ($errors->has('birth_date'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('birth_date') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="details">Details: <span class="tx-danger">*</span></label>
                        <textarea rows="3" class="form-control characters-count{{ $errors->has('details') ? ' is-invalid' : '' }}" id="details" data-maxlength="400" maxlength="400" placeholder="Details" name="details" required="">{{isset($memory) ? $memory->details : old('details') }}</textarea>
                        <div class="clearfix mt-2">
                            <div class="float-left mr-3">
                                <span>Characters</span>
                                <span class="current-length">0</span>
                            </div>
                            <div class="float-left">
                                <span>Characters remaining</span>
                                <span class="remaining-length">400</span>
                            </div>
                        </div>
                        @if ($errors->has('details'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('details') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="mb-5"><hr /></div>
                <div class="clearfix mb-4">
                    <div class="clearfix">
                        <h4 class="mt-1">From Names:</h4>
                    </div>
                    <div class="memory-from-names-wrap mt-3">
                        @if(isset($memory))
                            @foreach($memory->from_names as $key => $from_name)
                                <div class="row mt-3 memory-from-name">
                                    <div class="col-md-2">
                                        <input class="form-control memory-from-name-title-input" type="text" name="memory_from_names_name__ex__{{ $from_name->id }}" placeholder="Name" value="{{ $from_name->name }}" required>
                                    </div>
                                    <div class="col-md-2">
                                        <input class="form-control memory-from-name-title-input only-numeric" type="text" name="memory_from_names_contact_number__ex__{{ $from_name->id }}" placeholder="Contact Number" value="{{ $from_name->contact_number }}" minlength="10" maxlength="10">
                                    </div>
                                    <div class="col-md-2">
                                        <a class="btn btn-primary remove-memory-from-name" href="javascript:;"><i class="fa fa-minus"></i></a>
                                    </div>
                                </div>
                            @endforeach
                        @endif
                    </div>
                    <div class="row mt-3 memory-from-name">
                        <div class="col-md-6 memory-from-name-btn-wrap clearfix">
                            <a class="btn btn-primary add-new-memory-from-name mr-2 float-left" href="javascript:;"><i class="fa fa-plus"></i></a>
                            <span class="float-left" style="line-height: 38px;">Add New From Name</span>
                        </div>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="venue">Venue:</label>
                        <textarea rows="3" class="form-control characters-count{{ $errors->has('venue') ? ' is-invalid' : '' }}" id="venue" data-maxlength="400" maxlength="400" placeholder="Venue" name="venue">{{isset($memory) ? $memory->venue : old('venue') }}</textarea>
                        <div class="clearfix mt-2">
                            <div class="float-left mr-3">
                                <span>Characters</span>
                                <span class="current-length">0</span>
                            </div>
                            <div class="float-left">
                                <span>Characters remaining</span>
                                <span class="remaining-length">400</span>
                            </div>
                        </div>
                        @if ($errors->has('venue'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('venue') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group col-md-6">
                        <label for="address">Address: </label>
                        <textarea rows="3" class="form-control characters-count{{ $errors->has('address') ? ' is-invalid' : '' }}" id="address" data-maxlength="400" maxlength="400" placeholder="Address" name="address">{{isset($memory) ? $memory->address : old('address') }}</textarea>
                        <div class="clearfix mt-2">
                            <div class="float-left mr-3">
                                <span>Characters</span>
                                <span class="current-length">0</span>
                            </div>
                            <div class="float-left">
                                <span>Characters remaining</span>
                                <span class="remaining-length">400</span>
                            </div>
                        </div>
                        @if ($errors->has('address'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('address') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="form-row mt-2">
                    <div class="form-group col-md-2">
                        <label for="datetime">DateTime:</label>
                        <div class="input-group">
                            <input id="datetime" type="text" class="form-control readonly-datepicker" name="datetime" value="{{isset($memory) ? \Carbon::createFromFormat('Y-m-d H:i:s', $memory->datetime)->format('Y-m-d H:i') : old('datetime') }}" required onkeypress="return false;">
                            <span class="input-group-addon">
                                <span class="glyphicon glyphicon-calendar"></span>
                            </span>
                        </div>
                        @if ($errors->has('datetime'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('datetime') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group col-md-3">
                        <label for="district">Agency:</label>
                        <select class="form-control memory-agency-select" id="agency" name="agency">
                            <option disabled selected value>Select Advertiser</option>
                            @foreach($agencies as $agency)
                                <option value="{{ $agency->id }}" @if(isset($memory) && $memory->memory_agency_id == $agency->id) selected @endif>{{ $agency->name }} @if(isset($agency->business_name)) {{ ' (' . $agency->business_name . ')' }} @endif</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="mb-5"><hr /></div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="special_notes">Special Notes:</label>
                        <textarea rows="3" class="form-control characters-count{{ $errors->has('special_notes') ? ' is-invalid' : '' }}" id="special_notes" data-maxlength="400" maxlength="400" placeholder="Special Notes" name="special_notes">{{isset($memory) ? $memory->special_notes : old('special_notes') }}</textarea>
                        <div class="clearfix mt-2">
                            <div class="float-left mr-3">
                                <span>Characters</span>
                                <span class="current-length">0</span>
                            </div>
                            <div class="float-left">
                                <span>Characters remaining</span>
                                <span class="remaining-length">400</span>
                            </div>
                        </div>
                        @if ($errors->has('special_notes'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('special_notes') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group col-md-6">
                        <label for="highlighting_notes">Highlighting Notes:</label>
                        <textarea rows="3" class="form-control characters-count{{ $errors->has('highlighting_notes') ? ' is-invalid' : '' }}" id="highlighting_notes" data-maxlength="400" maxlength="400" placeholder="Highlighting Notes" name="highlighting_notes">{{isset($memory) ? $memory->highlighting_notes : old('highlighting_notes') }}</textarea>
                        <div class="clearfix mt-2">
                            <div class="float-left mr-3">
                                <span>Characters</span>
                                <span class="current-length">0</span>
                            </div>
                            <div class="float-left">
                                <span>Characters remaining</span>
                                <span class="remaining-length">400</span>
                            </div>
                        </div>
                        @if ($errors->has('highlighting_notes'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('highlighting_notes') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="form-row mt-1">
                    <div class="form-group col-md-3">
                        <label for="proofs">Proofs:</label>
                        <div class="custom-file">
                            <input type="file" class="memory-proofs-input custom-file-input{{ $errors->has('proofs') ? ' is-invalid' : '' }}" id="proofs" name="proofs[]" accept=".pdf,.doc,.docx,.txt,.png,.jpg,.jpeg" data-accept="['.png','.jpg','.jpeg','.PNG','.JPG','.JPEG','.pdf','.doc','.docx','.txt']" multiple="">
                            <label class="custom-file-label" for="proofs">Choose file</label>
                        </div>
                        <div id="proofs-files-name">
                            @if(isset($memory))
                                @foreach($memory->proofs as $proof)
                                <div class="mt-2 memory-proof-wrap">{{ $proof->name }}<a class="ml-1 remove-memory-proof" href="javascript:;" data-memory-proof-id="{{ $proof->id }}"><i class="fa fa-times-circle"></i></a></div>
                                @endforeach
                            @endif
                        </div>
                        @if ($errors->has('proofs'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('proofs') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="clearfix">
                    <div class="form-group float-left" style="padding-top: 9px;">
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" id="is_featured" name="is_featured" value="yes" @if(isset($memory) && $memory->is_featured == 1) checked @endif>
                            <label class="custom-control-label" for="is_featured">Featured</label>
                        </div>
                    </div>
                </div>
                <div class="form-row mt-2">
                    <div class="form-group col-md-2">
                        <label for="publish_datetime">Publish DateTime: <span class="tx-danger">*</span></label>
                        <div class="input-group">
                            <input id="publish_datetime" type="text" class="form-control readonly-datepicker" name="publish_datetime" value="{{isset($memory) ? \Carbon::createFromFormat('Y-m-d H:i:s', $memory->publish_datetime)->format('Y-m-d H:i') : old('publish_datetime') }}" required onkeypress="return false;">
                            <span class="input-group-addon">
                                <span class="glyphicon glyphicon-calendar"></span>
                            </span>
                        </div>
                        @if ($errors->has('publish_datetime'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('publish_datetime') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group col-md-2">
                        <label for="end_datetime">End DateTime: <span class="tx-danger">*</span></label>
                        <div class="input-group">
                            <input id="end_datetime" type="text" class="form-control readonly-datepicker" name="end_datetime" value="{{isset($memory) ? \Carbon::createFromFormat('Y-m-d H:i:s', $memory->end_datetime)->format('Y-m-d H:i') : old('end_datetime') }}" required onkeypress="return false;">
                            <span class="input-group-addon">
                                <span class="glyphicon glyphicon-calendar"></span>
                            </span>
                        </div>
                        @if ($errors->has('end_datetime'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('end_datetime') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-2">
                        <label for="status">Status: <span class="tx-danger">*</span></label>
                        <select class="form-control memory-status-select{{ $errors->has('status') ? ' is-invalid' : '' }}" id="status" name="status" required>
                            <option disabled selected value>Select Status</option>
                            @foreach($statuses as $status)
                                <option value="{{ $status }}" @if(isset($memory) && $memory->status == $status) selected @elseif(!isset($memory) && $status == 'Published') selected @else @endif>{{ $status }}</option>
                            @endforeach
                        </select>
                        @if ($errors->has('status'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('status') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <h4 class="mt-1">Publish to Cities:<span class="tx-danger">*</span></h4>
                <div class="form-row">
                    <div class="form-group col-md-4">
                        <div class="cb-wrap">
                            <div class="mb-3 cb-headline-wrap">
                                <div class="custom-control custom-checkbox group-cb-all-wrap">
                                    <input type="checkbox" class="custom-control-input cb-input-all" id="city-group-cb-all" data-type="city-group">
                                    <label class="custom-control-label" for="city-group-cb-all"><h5>Region/CityGroup</h5></label>
                                </div>
                            </div>
                            <div class="cb-group">
                                @foreach($city_groups as $city_group)
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input cb-input city-group-cb-group" id="city-group-cb-{{ $city_group->id }}" data-type="city_groups" data-cities="{{ json_encode($city_group->cities->pluck('id')->toArray()) }}">
                                    <label class="custom-control-label" for="city-group-cb-{{ $city_group->id }}">{{ $city_group->name }}</label>
                                </div>
                                @endforeach  
                            </div>
                        </div>
                    </div>
                    <div class="form-group col-md-4">
                        <div class="cb-wrap">
                            <div class="mb-3 cb-headline-wrap">
                                <div class="custom-control custom-checkbox group-cb-all-wrap">
                                    <input type="checkbox" class="custom-control-input cb-input-all" id="district-cb-all" data-type="district">
                                    <label class="custom-control-label" for="district-cb-all"><h5>Districts</h5></label>
                                </div>
                            </div>
                            <div class="cb-group">
                                @foreach($districts as $district)
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input cb-input district-cb-group" id="district-cb-{{ $district->id }}" data-type="districts" data-cities="{{ json_encode($district->cities->pluck('id')->toArray()) }}">
                                    <label class="custom-control-label" for="district-cb-{{ $district->id }}">{{ $district->name }}</label>
                                </div>
                                @endforeach  
                            </div>
                        </div>
                    </div>
                    <div class="form-group col-md-4">
                        <div class="cb-wrap">
                            <div class="mb-3 cb-headline-wrap">
                                <span class="custom-control custom-checkbox group-cb-all-wrap">
                                    <input type="checkbox" class="custom-control-input cb-input-all" id="city-cb-all" data-type="city">
                                    <label class="custom-control-label" for="city-cb-all"><h5>Cities</h5></label>
                                </span>
                            </div>
                            <div class="cb-group">
                                @foreach($publish_to_cities as $publish_to_district)
                                @foreach($publish_to_district->cities as $publish_to_city)
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input city-cb-group" id="city-cb-{{ $publish_to_city->id }}" name="cities[]" value="{{ $publish_to_city->id }}" @if(isset($memory) && in_array($publish_to_city->id, $memory->cities->pluck('id')->toArray())) checked @endif>
                                    <label class="custom-control-label" for="city-cb-{{ $publish_to_city->id }}">{{ $publish_to_city->name }}</label>
                                </div>
                                @endforeach
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
                <div class="text-right">
                    <a href="{{ route('memories.index') }}" class="btn btn-warning mg-r-1">Cancel</a>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </form>
        </div>
    </div>
@endsection

@section('last-scripts')
    @if(isset($memory))
    <script type="text/javascript">
        window.memory_from_names = {{count($memory->from_names)}};
    </script>
    @endif
@endsection