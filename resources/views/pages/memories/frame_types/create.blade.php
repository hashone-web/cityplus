@extends('layouts.admin')

@php($title = isset($frame_type) ? 'Edit Frame Type - ' . $frame_type->name: 'Create Frame Type')
@push('title', yieldTitle($title))

@section('breadcrumb-title', $title)

@section('breadcrumb-link')
    <li class="breadcrumb-item active" aria-current="page">
        <a href="{{ route('frame_types.index') }}">Frame Types</a>
    </li>
    <li class="breadcrumb-item active" aria-current="page">{{ $title }}</li>
@endsection

@section('breadcrumb')
    @include('components.breadcrumb')
@endsection

@push('content-class', 'content-fixed')
@push('container-class', 'container-fluid')

@section('content')
    <div class="card">
        <div class="card-body">
            <form action="{{ isset($frame_type) ? route('frame_types.update', ['id' => $frame_type->id]) : route('frame_types.store') }}" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="form-row">
                    <div class="form-group col-md-3">
                        <div class="mb-2">Type: <span class="tx-danger">*</span></div>
                        <div class="form-inline">
                            <div class="custom-control custom-radio mr-3">
                                <input type="radio" id="customRadio1" name="type" class="custom-control-input" value="remembrance" @if(isset($frame_type) && $frame_type->type == 'remembrance') checked @elseif(!isset($frame_type)) checked @else @endif>
                                <label class="custom-control-label" for="customRadio1">Remembrance</label>
                            </div>
                            <div class="custom-control custom-radio mr-3">
                                <input type="radio" id="customRadio2" name="type" class="custom-control-input" value="obituary" @if(isset($frame_type) && $frame_type->type == 'obituary') checked @endif>
                                <label class="custom-control-label" for="customRadio2">Obituary</label>
                            </div>
                            <div class="custom-control custom-radio">
                                <input type="radio" id="customRadio3" name="type" class="custom-control-input" value="tribute" @if(isset($frame_type) && $frame_type->type == 'tribute') checked @endif>
                                <label class="custom-control-label" for="customRadio3">Tribute</label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-3">
                        <label for="name">Name: <span class="tx-danger">*</span></label>
                        <input type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" id="name" placeholder="Name" name="name" maxlength="255" value="{{isset($frame_type) ? $frame_type->name : old('name') }}" required>
                        @if ($errors->has('name'))
                            <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('name') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>
                <div class="form-row mt-1">
                    <div class="form-group col-md-3">
                        <label for="thumbnail">Thumbnail: <span class="tx-danger">*</span></label>
                        <div class="custom-file">
                            <input type="file" class="frame-type-thumbnail-input custom-file-input{{ $errors->has('thumbnail') ? ' is-invalid' : '' }}" id="thumbnail" name="thumbnail" accept=".png,.jpg,.jpeg" data-accept="['.png','.jpg','.jpeg','.PNG','.JPG','.JPEG']" @if(!isset($frame_type)) required="" @endif>
                            <label class="custom-file-label" for="thumbnail">Choose file</label>
                        </div>
                        <div id="thumbnail-files-name">
                            @if(isset($frame_type) && $frame_type->thumbnail !== null)<div class="mt-2"><img src="{{ $frame_type->thumbnail }}" style="max-height: 100px;"></div>@endif
                        </div>
                        @if ($errors->has('thumbnail'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('thumbnail') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="text-right">
                    <a href="{{ route('frame_types.index') }}" class="btn btn-warning mg-r-1">Cancel</a>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </form>
        </div>
    </div>
@endsection