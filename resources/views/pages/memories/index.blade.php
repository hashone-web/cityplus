@extends('layouts.admin')

@php($title = 'Memories')
@push('title', yieldTitle($title))

@section('breadcrumb-title', $title)

@section('breadcrumb-link')
    <li class="breadcrumb-item active" aria-current="page">{{ $title }}</li>
@endsection

@section('breadcrumb-btn')
    <a class="btn btn-sm pd-x-15 btn-primary btn-uppercase mg-l-5" href="{{ route('memories.create') }}"><i data-feather="plus" class="wd-10 mg-r-5"></i>Create Memory</a>
@endsection

@section('breadcrumb')
    @include('components.breadcrumb')
@endsection

@push('content-class', 'content-fixed')
@push('container-class', 'container-fluid')

@section('content')
    <div class="card">
        <div class="card-body">
            <div data-label="Example" class="df-example demo-table">
                <div class="table-responsive">
                    <table class="table table-striped mg-b-0">
                        <thead>
                        <tr>
                            <th scope="col">ID</th>
                            <th scope="col">Type</th>
                            <th scope="col" style="width: 300px;">Name</th>
                            <th scope="col">Death Date</th>
                            <th scope="col">Publish Date</th>
                            <th scope="col">End Date</th>
                            <th scope="col">Status</th>
                            <th scope="col">@sortablelink('created_at', 'Added On')</th>
                            @if(auth()->user()->can('memories crud') || auth()->user()->can('memories crud'))
                                <th scope="col">Actions</th>
                            @endif
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($memories as $memory)
                        <tr>
                            <th scope="row">{{ $memory->id }}</th>
                            <td>{{ ucwords($memory->type) }}</td>
                            <td>{{ $memory->name_prefix . ' ' . $memory->name }}</td>
                            <td>{{ getDateOnlyFormat($memory->death_date) }}</td>
                            <td>{{ getDateFormat($memory->publish_datetime) }}</td>
                            <td>{{ getDateFormat($memory->end_datetime) }}</td>
                            <td>{{ $memory->status }}</td>
                            <td>{{ getDateFormat($memory->created_at) }}</td>
                            @if(auth()->user()->can('memories crud') || auth()->user()->can('memories crud'))
                                <td>
                                    @can('memories crud')<a href="{{ route('memories.edit', ['id' => $memory->id ]) }}"><i data-feather="edit-2" width="18" height="18"></i></a>@endcan
                                    @can('memories crud')
                                        <a href="javascript:;" onclick="deleteRecords(event, 'Memory', 'country-{{ $memory->id }}-delete')"><i data-feather="trash" width="18" height="18" stroke="red"></i></a>

                                        <form id="country-{{ $memory->id }}-delete" action="{{ route('memories.delete', ['id' => $memory->id]) }}" method="POST" style="display: none;">
                                            @method('delete')
                                            @csrf
                                        </form>
                                    @endcan
                                </td>
                            @endif
                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div><!-- table-responsive -->
            </div><!-- df-example -->
        </div>
    </div>
    <div class="mt-4">
        {!! $memories->appends(\Request::except('page'))->render() !!}
    </div>
@endsection