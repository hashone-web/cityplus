@extends('layouts.admin')

@php($title = 'Memories Settings')
@push('title', yieldTitle($title))

@section('breadcrumb-title', $title)

@section('breadcrumb-link')
    <li class="breadcrumb-item active" aria-current="page">{{ $title }}</li>
@endsection

@section('breadcrumb')
    @include('components.breadcrumb')
@endsection

@push('content-class', 'content-fixed')
@push('container-class', 'container-fluid')

@section('content')
    <div class="card">
        <div class="card-body">
            <form id="memories-settings-form" action="{{ route('memories.settings.update') }}" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="confirmation_message_text">Confirmation Message Text:</label>
                        <input type="text" class="form-control{{ $errors->has('confirmation_message_text') ? ' is-invalid' : '' }}" id="confirmation_message_text" name="confirmation_message_text" value="{{ isset($setting) ? $setting->confirmation_message_text : old('confirmation_message_text') }}" placeholder="Confirmation Message Text">
                        @if ($errors->has('confirmation_message_text'))
                            <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('confirmation_message_text') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="form-group col-md-6">
                        <label for="confirmation_message_text_gujarati">Confirmation Message Text Gujarati:</label>
                        <input type="text" class="form-control{{ $errors->has('confirmation_message_text_gujarati') ? ' is-invalid' : '' }}" id="confirmation_message_text_gujarati" name="confirmation_message_text_gujarati" value="{{ isset($setting) ? $setting->confirmation_message_text_gujarati : old('confirmation_message_text_gujarati') }}" placeholder="Confirmation Message Text Gujarati">
                        @if ($errors->has('confirmation_message_text_gujarati'))
                            <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('confirmation_message_text_gujarati') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="share_description">Share Description:</label>
                        <textarea class="form-control{{ $errors->has('share_description') ? ' is-invalid' : '' }}" id="share_description" name="share_description" placeholder="Share Description">{{ isset($setting) ? $setting->share_description : old('share_description') }}</textarea>
                        @if ($errors->has('share_description'))
                            <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('share_description') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="form-group col-md-6">
                        <label for="share_description_gujarati">Share Description Gujarati:</label>
                        <textarea class="form-control{{ $errors->has('share_description_gujarati') ? ' is-invalid' : '' }}" id="share_description_gujarati" name="share_description_gujarati" placeholder="Share Description Gujarati">{{ isset($setting) ? $setting->share_description_gujarati : old('share_description_gujarati') }}</textarea>
                        @if ($errors->has('share_description_gujarati'))
                            <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('share_description_gujarati') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-3">
                        <label for="share_bottom_image">Share Bottom Image:</label>
                        <div class="custom-file">
                            <input type="file" class="custom-file-input{{ $errors->has('share_bottom_image') ? ' is-invalid' : '' }}" id="share_bottom_image" name="share_bottom_image" accept=".png,.jpg,.jpeg" data-accept="['.png','.jpg','.jpeg','.PNG','.JPG','.JPEG']" multiple>
                            <label class="custom-file-label" for="share_bottom_image">Choose file</label>
                        </div>
                        <div class="share_bottom_image" class="mt-2" style="color: #0168fa;">
                        @if(isset($setting) && $setting->share_bottom_image)
                            <a class="mt-2 d-block" href="{{ $setting->share_bottom_image_full_path }}" target="_blank">
                                <img src="{{ $setting->share_bottom_image_full_path }}" style="max-height: 25px;">
                            </a>
                        @endif
                        </div>
                        @if ($errors->has('share_bottom_image'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('share_bottom_image') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group col-md-3">
                        <label for="share_bottom_image_gujarati">Share Bottom Image Gujarati:</label>
                        <div class="custom-file">
                            <input type="file" class="custom-file-input{{ $errors->has('share_bottom_image_gujarati') ? ' is-invalid' : '' }}" id="share_bottom_image_gujarati" name="share_bottom_image_gujarati" accept=".png,.jpg,.jpeg" data-accept="['.png','.jpg','.jpeg','.PNG','.JPG','.JPEG']" multiple>
                            <label class="custom-file-label" for="share_bottom_image_gujarati">Choose file</label>
                        </div>
                        <div class="share_bottom_image_gujarati" class="mt-2" style="color: #0168fa;">
                        @if(isset($setting) && $setting->share_bottom_image_gujarati)
                            <a class="mt-2 d-block" href="{{ $setting->share_bottom_image_gujarati_full_path }}" target="_blank">
                                <img src="{{ $setting->share_bottom_image_gujarati_full_path }}" style="max-height: 25px;">
                            </a>
                        @endif
                        </div>
                        @if ($errors->has('share_bottom_image_gujarati'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('share_bottom_image_gujarati') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="text-right">
                    <a href="{{ route('home') }}" class="btn btn-warning mg-r-1">Cancel</a>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </form>
        </div>
    </div>
@endsection
