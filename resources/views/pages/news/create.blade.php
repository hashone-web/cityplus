@extends('layouts.admin')

@php($title = isset($news) ? 'Edit News - ' . $news->headline: 'Create News')
@push('title', yieldTitle($title))

@section('breadcrumb-title', $title)

@section('breadcrumb-link')
    <li class="breadcrumb-item active" aria-current="page">{{ $title }}</li>
@endsection

@section('breadcrumb')
    @include('components.breadcrumb')
@endsection

@push('content-class', 'content-fixed')
@push('container-class', 'container-fluid')

@section('content')
    <div class="card">
        <div class="card-body">
            <form id="news_form" class="{{ isset($news) ? 'news_update_form': 'news_form' }}" action="{{ isset($news) ? route('news.update', ['id' => $news->id]) : route('news.store') }}" method="POST" enctype="multipart/form-data">
                @csrf
                <input type="hidden" name="media_sorting" id="media_sorting">
                <div class="clearfix">
                    <div class="form-group" style="width: 800px;">
                        <label for="headline">Headline: <span class="tx-danger">*</span></label>
                        <input type="text" class="form-control characters-count{{ $errors->has('headline') ? ' is-invalid' : '' }}" id="headline" data-maxlength="100" placeholder="Headline" name="headline" value="{{isset($news) ? $news->headline : old('headline') }}" required autofocus>
                        <div class="clearfix mt-2">
                            <div class="float-left mr-3">
                                <span>Characters</span>
                                <span class="current-length">0</span>
                            </div>
                            <div class="float-left">
                                <span>Characters remaining</span>
                                <span class="remaining-length">100</span>
                            </div>
                        </div>
                        @if ($errors->has('headline'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('headline') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="article">Article:</label>
                        <textarea rows="8" class="form-control characters-count{{ $errors->has('article') ? ' is-invalid' : '' }}" id="article" data-maxlength="850" placeholder="Article" name="article">{{isset($news) ? $news->article : old('article') }}</textarea>
                        <div class="clearfix mt-2">
                            <div class="float-left mr-3">
                                <span>Characters</span>
                                <span class="current-length">0</span>
                            </div>
                            <div class="float-left">
                                <span>Characters remaining</span>
                                <span class="remaining-length">850</span>
                            </div>
                        </div>
                        @if ($errors->has('article'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('article') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group col-md-6">
                        <label for="article_text">Article Text Only:</label>
                        <textarea rows="8" class="form-control characters-count{{ $errors->has('article_text') ? ' is-invalid' : '' }}" id="article_text" data-maxlength="850" placeholder="Article" name="article_text" style="height: 300px;">{{isset($news) ? $news->article_text : old('article_text') }}</textarea>
                        <div class="clearfix mt-2">
                            <div class="float-left mr-3">
                                <span>Characters</span>
                                <span class="current-length">0</span>
                            </div>
                            <div class="float-left">
                                <span>Characters remaining</span>
                                <span class="remaining-length">850</span>
                            </div>
                        </div>
                        @if ($errors->has('article_text'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('article_text') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                @if(isset($news) && count($news->attachments))
                <div class="clearfix">
                    <div class="form-group float-left mr-3">
                        <h4>Attachments:</h4>
                    </div>
                    @foreach($news->attachments as $attachment_index => $attachment)
                        <div class="form-group float-left mr-4" style="padding-top: 5px;">
                            <a href="{{ $attachment->full_path }}" target="_blank"><span>{{ $attachment_index+1 . ') ' . $attachment->original_name }}</span></a>
                        </div>
                    @endforeach
                </div>
                @endif
                <div class="form-row" id="media-add-tag">
                    <div class="form-group col-md-2" @if(auth()->user()->hasRole('Reporter')) style="display: none" @endif>
                        <label for="datetime">News DateTime: <span class="tx-danger">*</span></label>
                        <div class="input-group">
                            <input id="datetime" type="text" class="form-control readonly-datepicker {{ isset($news) ? 'edit_news_datetime': 'create_news_datetime' }}" name="datetime" value="{{isset($news) ? \Carbon::createFromFormat('Y-m-d H:i:s', $news->datetime)->format('Y-m-d H:i') : old('datetime') }}" required onkeypress="return false;">
                            <span class="input-group-addon">
                                <span class="glyphicon glyphicon-calendar"></span>
                            </span>
                        </div>
                        @if ($errors->has('datetime'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('datetime') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group col-md-2" style="padding-left: 32px; @if(auth()->user()->hasRole('Reporter')) display: none; @endif">
                        <label for="datetime" style="display: block; min-height: 30px;"></label>
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" id="datetime_current" name="datetime_current" value="yes">
                            <label class="custom-control-label" for="datetime_current">Current Time</label>
                        </div>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-12">
                        <label for="category">Categories:</label>
                        <select class="form-control news-category-select{{ $errors->has('category') ? ' is-invalid' : '' }}" id="category" name="categories[]" multiple="">
                            @foreach($categories as $category)
                                <option value="{{ $category->id }}" @if(isset($news) && in_array($category->id, $news->categories->pluck('id')->toArray())) selected @endif>{{ $category->name }}</option>
                            @endforeach
                        </select>
                        @if ($errors->has('category'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('category') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-12 file-area">
                        <label for="media">Media:</label>
                        <input type="file" class="custom-file-input{{ $errors->has('media') ? ' is-invalid' : '' }}" id="media" name="media[]" accept=".png,.jpg,.jpeg,.mp4,.mpeg,.3gp" data-accept="['.png','.jpg','.jpeg','.mp4','.mpeg','.3gp','.PNG','.JPG','.JPEG','.MP4','.MPEG','.3GP']" multiple>
                        <div class="file-dummy">
                            <div class="success">Great, your files are selected. Keep on.</div>
                            <div class="default">Please select some files</div>
                        </div>
                    </div>
                    <div class="form-group col-md-12 file-area">
                        <div class="mt-3">
                            <ol id="news-media-sorting">
                                @if(isset($news))
                                    @foreach($news->media as $media_index => $media)
                                        <?php 
                                            $media_unique_id = 'existing__' . $media->id;
                                        ?>
                                        <li data-media-id="{{ $media_unique_id }}">
                                            <div class="clearfix">
                                                <div class="float-left" style="width: 10%;">
                                                    <img style="max-height: 78px;" src="{{ $media->thumb }}">
                                                </div>
                                                <div class="float-left" style="width: 90%; padding: 20px 10px 20px 0;">
                                                    <div class="float-left ml-5">{{ $media->original_name }}</div>
                                                    @can('delete news')
                                                    <div class="float-right mr-2 ml-3">
                                                        <a href="javascript:;" role="menuitem" onclick="deleteRecords(event, 'News Media', {{ $media->id }})"><i class="fa fa-trash text-danger"></i></a>
                                                    </div>
                                                    @endcan
                                                    <div class="float-right">
                                                        <label class="mr-3">Courtesy</label>
                                                        <input class="form-control" type="input" name="{{ 'courtesy__' . $media_unique_id }}" value="{{ $media->courtesy }}" style="display: inline-block; width: 200px;" placeholder="Media Courtesy">
                                                    </div>
                                                    <div class="float-right mr-5">
                                                        <label class="mr-2">Sensitive</label>
                                                        <input type="checkbox" name="{{ 'is_sensitive__' . $media_unique_id }}" @if($media->is_sensitive == true) checked @endif>
                                                    </div>
                                                    <div class="float-right mr-3">
                                                        <label class="mr-2">File Photo</label>
                                                        <input type="checkbox" name="{{ 'is_file_photo__' . $media_unique_id }}" @if($media->is_file_photo == true) checked @endif>
                                                    </div>
                                                    @if($media->watermark_position)
                                                    <div class="float-right mr-5">
                                                        <i class="far fa-images"></i> {{ ucwords($media->watermark_position) }}
                                                    </div>
                                                    @endif
                                                </div>
                                            </div>
                                        </li>
                                    @endforeach
                                @endif
                            </ol>
                        </div>
                        @if ($errors->has('media'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('media') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-3">
                        <label for="city">Origin City: <span class="tx-danger">*</span></label>
                        <select class="form-control news-city-select{{ $errors->has('city') ? ' is-invalid' : '' }}" id="city" name="city" required>
                            <option disabled selected value>Origin City</option>
                            @foreach($cities as $city)
                                <option value="{{ $city->id }}" @if(isset($news) && $news->city->id == $city->id) selected @endif>{{ $city->name }}</option>
                            @endforeach
                        </select>
                        @if ($errors->has('city'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('city') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="form-row" @if(auth()->user()->hasRole('Reporter')) style="display: none" @endif>
                    <div class="form-group col-md-3">
                        <label for="source_name">Source Name:</label>
                        <input type="text" class="form-control characters-count{{ $errors->has('source_name') ? ' is-invalid' : '' }}" id="source_name" placeholder="Source Name" name="source_name" value="{{ isset($news) && $news->source !== null && isset($news->source->name) ? $news->source->name : old('source_name') }}" maxlength="255">
                        @if ($errors->has('source_name'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('source_name') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group col-md-6">
                        <label for="source_link">Source Link:</label>
                        <input type="text" class="form-control characters-count{{ $errors->has('source_link') ? ' is-invalid' : '' }}" id="source_link" placeholder="Source Link" name="source_link" value="{{ isset($news) && $news->source !== null && isset($news->source->link) ? $news->source->link : old('source_link') }}">
                        @if ($errors->has('source_link'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('source_link') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="clearfix mt-1">
                    <div class="form-group float-left" style="padding-top: 9px;">
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" id="is_breaking" name="is_breaking" value="yes" @if(isset($news) && $news->is_breaking == 1) checked @endif>
                            <label class="custom-control-label" for="is_breaking">Breaking News</label>
                        </div>
                    </div>
                    <div class="form-group float-left ml-5" style="padding-top: 9px; @if(auth()->user()->hasRole('Reporter')) display: none; @endif">
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" id="is_featured" name="is_featured" value="yes" @if(isset($news) && $news->is_featured == 1) checked @endif>
                            <label class="custom-control-label" for="is_featured">Featured News</label>
                        </div>
                    </div>
                    @can('publish news')
                    @if(!isset($news) || (isset($news) && $news->notification_status == 'Pending'))
                    <div class="form-group float-left ml-5" style="padding-top: 9px;">
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" id="send_notification" name="send_notification" value="yes">
                            <label class="custom-control-label" for="send_notification">Send Notification</label>
                        </div>
                    </div>

                    <div class="form-group float-left ml-5 high_priority_notification form-inline" style="display: none;">
                        <label for="notification_type" class="mr-3">Notification Type: <span class="tx-danger">*</span></label>
                        <select class="form-control news-notification-type-select{{ $errors->has('notification_type') ? ' is-invalid' : '' }}" id="notification_type" name="notification_type" style="width: 200px;">
                            <option disabled selected value>Notification Type</option>
                            @foreach($notification_types as $notification_type_name => $notification_type_id)
                                <option @if($notification_type_name == 'Silent') selected @endif>{{ $notification_type_name }}</option>
                            @endforeach
                        </select>
                        @if ($errors->has('notification_type'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('notification_type') }}</strong>
                            </span>
                        @endif
                    </div>

                    <div class="form-group float-left ml-5 high_priority_notification form-inline" style="display: none;">
                        <label for="send_after">Scheduled Notification: </label>
                        <input id="send_after" type="text" class="form-control readonly-datepicker ml-3" name="send_after">
                    </div>
                    @endif
                    @endcan
                </div>
                @can('publish news')
                <div class="clearfix mt-1">
                    <div class="form-group float-left" style="padding-top: 9px;">
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" id="is_useful" name="is_useful" value="yes" @if(isset($news) && $news->is_useful == 1) checked @endif>
                            <label class="custom-control-label" for="is_useful">Useful News</label>
                        </div>
                    </div>
                    <div class="form-group float-left ml-5 is-useful-expiry-datetime-wrap form-inline" @if(isset($news) && $news->is_useful == 1) @else style="display: none;" @endif>
                        <label for="is_useful_expiry_datetime">Useful Expiry Datetime: </label>
                        <input id="is_useful_expiry_datetime" type="text" class="form-control readonly-datepicker ml-3" name="is_useful_expiry_datetime" value="{{ (isset($news) && $news->is_useful_expiry_datetime !== null) ? \Carbon::createFromFormat('Y-m-d H:i:s', $news->is_useful_expiry_datetime)->format('Y-m-d') : old('is_useful_expiry_datetime') }}">
                    </div>
                    <div class="form-group float-left ml-5" style="padding-top: 7px;">
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" id="is_home" name="is_home" value="yes" @if(isset($news) && $news->is_home == 1) checked @endif>
                            <label class="custom-control-label" for="is_home"><b style="font-size: 16px;">Add to Home(v3)</b></label>
                        </div>
                    </div>
                </div>
                @endcan
                @can('publish news')
                <div class="form-row">
                    <div class="form-group col-md-3">
                        <label for="status">Status: <span class="tx-danger">*</span></label>
                        <select class="form-control news-status-select{{ $errors->has('status') ? ' is-invalid' : '' }}" id="status" name="status" required>
                            <option disabled selected value>Select Status</option>
                            @foreach($statuses as $status)
                                <option value="{{ $status }}" @if(isset($news) && $news->status == $status) selected @elseif(!isset($news) && $status == 'Published') selected @else @endif>{{ $status }}</option>
                            @endforeach
                        </select>
                        @if ($errors->has('status'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('status') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                @else
                    @if(isset($news))
                        <div class="form-row"><input type="hidden" name="status" value="{{$news->status}}"></div>
                    @else
                        @if(auth()->user()->hasRole('Reporter'))
                            <div class="form-row"><input type="hidden" name="status" value="Pending"></div>
                        @else
                            <div class="form-row"><input type="hidden" name="status" value="Draft"></div>
                        @endif
                    @endif
                @endcan

                @if(isset($news))
                    <div class="form-row rejection-reasons-wrap" @if($news->status !== 'Rejected') style="display: none;" @endif>
                        <div class="form-group float-left mr-4 ml-1" style="padding-top: 3px;"><h4>Rejection Reasons:</h4></div>
                        @foreach($rejection_reasons as $rejection_reason)
                            <div class="form-group float-left mr-3" style="padding-top: 9px;">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" id="{{ 'rejection_reason__' . $rejection_reason['id'] }}" name="rejection_reasons[]" value="{{ $rejection_reason['title'] }}" @if(in_array($rejection_reason['title'], explode(', ', $news->rejection_reasons))) checked @endif>
                                    <label class="custom-control-label" for="{{ 'rejection_reason__' . $rejection_reason['id'] }}">{{ $rejection_reason['title'] }}</label>
                                </div>
                            </div>
                        @endforeach
                    </div>
                @endif
                <div class="text-right">
                    <a href="{{ route('home') }}" class="btn btn-warning mg-r-1">Cancel</a>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </form>
        </div>
    </div>
@endsection
