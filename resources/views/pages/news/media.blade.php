@extends('layouts.admin')

@php($title = 'News')
@push('title', yieldTitle($title))

@section('breadcrumb-title', $title)

@section('breadcrumb-link')
    <li class="breadcrumb-item active" aria-current="page">
        <a href="{{ route('home') }}">{{ $title }}</a>
    </li>
    <li class="breadcrumb-item active" aria-current="page">Add News</li>
@endsection

@section('breadcrumb')
    @include('components.breadcrumb')
@endsection

@push('content-class', 'content-fixed')
@push('container-class', 'container-fluid')

@section('content')
    <div class="card">
        <div class="card-body">
            <form id="news-media-sorting-form" action="{{ route('news.media.update', ['id' => $news->id]) }}" method="POST" enctype="multipart/form-data">
                @csrf
                <input type="hidden" name="media_sorting" id="media_sorting">
                <h2>Upload New Media</h2>
                <div class="form-row mt-3">
                    <div class="form-group col-md-3">
                        <div class="custom-file">
                            <input type="file" class="custom-file-input" id="media" name="media[]" accept=".png,.jpg,.jpeg,.mp4,.mpeg,.3gp" data-accept="['.png','.jpg','.jpeg','.mp4','.mpeg','.3gp','.PNG','.JPG','.JPEG','.MPEG','.3GP']" multiple>
                            <label class="custom-file-label" for="media">Choose file</label>
                        </div>
                        <div id="media-files-name"></div>
                    </div>
                </div>
                <h2 class="mt-5">Existing Media</h2>
                <div class="mt-3">
                    <ol id="news-media-sorting">
                        @foreach($news->media as $media_index => $media)
                            <li data-media-id="{{ $media->id }}">
                                <div class="clearfix">
                                    <div class="float-left" style="min-width: 150px; display: inline-block;"><img src="{{ $media->thumb }}" style="max-height: 48px;"></div>
                                    <div class="float-left hide" style="display:none">{{ $media->id }}|||</div>
                                    <div class="float-left">{{ $media->original_name }}</div>
                                    @can('delete news')
                                    <span class="float-right mr-5">
                                        <a href="javascript:;" role="menuitem" onclick="deleteRecords(event, 'News Media', {{ $media->id }})"><i class="fa fa-trash text-danger"></i></a>
                                    </span>
                                    @endcan
                                    <div class="float-right mr-5">
                                        <label class="mr-2">Courtesy</label>
                                        <input class="form-control" type="input" name="{{ 'courtesy__' . $media->id }}" value="{{ $media->courtesy }}" style="display: inline-block; width: 300px;" placeholder="Media Courtesy">
                                    </div>
                                    <div class="float-right mr-5">
                                        <label class="mr-2">Sensitive</label>
                                        <input type="checkbox" name="{{ 'is_sensitive__' . $media->id }}" @if($media->is_sensitive == true) checked @endif>
                                    </div>
                                    <div class="float-right mr-5">
                                        <label class="mr-2">File Photo</label>
                                        <input type="checkbox" name="{{ 'is_file_photo__' . $media->id }}" @if($media->is_file_photo == true) checked @endif>
                                    </div>
                                </div>
                            </li>
                        @endforeach
                    </ol>
                </div>
                <div class="text-right">
                    <a href="{{ route('home') }}" class="btn btn-warning mg-r-1">Cancel</a>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </form>
        </div>
    </div>
@endsection
