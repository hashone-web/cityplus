@extends('layouts.admin')

@php($title = 'News Statistics Reports')
@push('title', yieldTitle($title))

@section('breadcrumb-title', $title)

@section('breadcrumb-link')
    <li class="breadcrumb-item active" aria-current="page">{{ $title }}</li>
@endsection

@section('breadcrumb-btn')
    
@endsection

@section('breadcrumb')
    @include('components.breadcrumb')
@endsection

@push('content-class', 'content-fixed')
@push('container-class', 'container-fluid')

@section('content')
    <div class="card">
        <div id="graph"></div>
    </div>
    <div class="card mt-4">
        <div class="card-body">
            <div data-label="Example" class="df-example demo-table">
                <div class="table-responsive">
                    <table class="table table-striped mg-b-0" id="news-statistics-reports-table">
                        <thead>
                            <tr>
                                <th>Date</th>
                                <th>News</th>
                                <th>Views</th>
                                <th>Reactions</th>
                                <th>Shares</th>
                                <th>News Media Views</th>
                                <th>News Media Donwloads</th>
                                <th>Notifications</th>
                                <th>Notification Clicks</th>
                                <th>Active Devices</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($total_news_statistics_reports as $date => $total_news_statistics_report)
                                <tr>
                                    <td>Total:</td>
                                    <td>{{ isset($news_statistics_report['news'])? $news_statistics_report['mews'][0]['total']: 0 }}</td>
                                    <td>{{ isset($news_statistics_report['views'])? $total_news_statistics_report['views'][0]['total']: 0 }}</td>
                                    <td>{{ isset($news_statistics_report['reactions_claps'])? $total_news_statistics_report['reactions_claps'][0]['total']: 0 }}</td>
                                    <td>{{ isset($news_statistics_report['shares'])? $total_news_statistics_report['shares'][0]['total']: 0 }}</td>
                                    <td>{{ isset($news_statistics_report['media_views'])? $total_news_statistics_report['media_views'][0]['total']: 0 }}</td>
                                    <td>{{ isset($news_statistics_report['media_downloads'])? $total_news_statistics_report['media_downloads'][0]['total']: 0 }}</td>
                                    <td>{{ isset($news_statistics_report['notification_count'])? $news_statistics_report['notification_count'][0]['total']: 0 }}</td>
                                    <td>{{ isset($news_statistics_report['notification_clicks'])? $news_statistics_report['notification_clicks'][0]['total']: 0 }}</td>
                                    <td>{{ isset($news_statistics_report['active_devices_in_last_24_hours'])? $news_statistics_report['active_devices_in_last_24_hours'][0]['total']: 0 }}</td>
                                </tr>
                            @endforeach
                            @foreach($news_statistics_reports as $date => $news_statistics_report)
                                <tr>
                                    <td>{{ $date }}</td>
                                    <td>{{ isset($news_statistics_report['news'])? $news_statistics_report['news'][0]['data']: 0 }}</td>
                                    <td>{{ isset($news_statistics_report['views'])? $news_statistics_report['views'][0]['data']: 0 }}</td>
                                    <td>{{ isset($news_statistics_report['reactions_claps'])? $news_statistics_report['reactions_claps'][0]['data']: 0 }}</td>
                                    <td>{{ isset($news_statistics_report['shares'])? $news_statistics_report['shares'][0]['data']: 0 }}</td>
                                    <td>{{ isset($news_statistics_report['media_views'])? $news_statistics_report['media_views'][0]['data']: 0 }}</td>
                                    <td>{{ isset($news_statistics_report['media_downloads'])? $news_statistics_report['media_downloads'][0]['data']: 0 }}</td>
                                    <td>{{ isset($news_statistics_report['notification_count'])? $news_statistics_report['notification_count'][0]['data']: 0 }}</td>
                                    <td>{{ isset($news_statistics_report['notification_clicks'])? $news_statistics_report['notification_clicks'][0]['data']: 0 }}</td>
                                    <td>{{ isset($news_statistics_report['active_devices_in_last_24_hours'])? $news_statistics_report['active_devices_in_last_24_hours'][0]['data']: 0 }}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="mt-4">
        
    </div>
@endsection

@section('last-scripts')
    <script type="text/javascript">
        $(document).ready( function () {
            $('#news-statistics-reports-table').DataTable({
                "order": [[ 0, "desc" ]],
                "lengthMenu": [[20, 50, -1], [20, 50, "All"]],
                "iDisplayLength": 20
            });
        });
    </script>
@endsection