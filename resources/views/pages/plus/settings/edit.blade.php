@extends('layouts.admin')

@php($title = 'Plus Settings')
@push('title', yieldTitle($title))

@section('breadcrumb-title', $title)

@section('breadcrumb-link')
    <li class="breadcrumb-item active" aria-current="page">{{ $title }}</li>
@endsection

@section('breadcrumb')
    @include('components.breadcrumb')
@endsection

@push('content-class', 'content-fixed')
@push('container-class', 'container-fluid')

@section('content')
    <div class="card">
        <div class="card-body">
            <form id="plus-settings-form" action="{{ route('plus.settings.update') }}" method="POST" enctype="multipart/form-data">
                @csrf
                <h5>Plus Section Display:</h5>
                <div class="form-row mt-3">
                    <div class="form-group float-left" style="padding-top: 9px; padding-left: 6px;">
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" id="news" name="news" value="yes" @if(isset($setting) && $setting->news == 1) checked @endif>
                            <label class="custom-control-label" for="news">News</label>
                        </div>
                    </div>
                    <div class="form-group float-left ml-5" style="padding-top: 9px;">
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" id="city_selection" name="city_selection" value="yes" @if(isset($setting) && $setting->city_selection == 1) checked @endif>
                            <label class="custom-control-label" for="city_selection">City Selection</label>
                        </div>
                    </div>
                    <div class="form-group float-left ml-5" style="padding-top: 9px;">
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" id="horoscope" name="horoscope" value="yes" @if(isset($setting) && $setting->horoscope == 1) checked @endif>
                            <label class="custom-control-label" for="horoscope">Horoscope</label>
                        </div>
                    </div>
                    <div class="form-group float-left ml-5" style="padding-top: 9px;">
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" id="death_notes" name="death_notes" value="yes" @if(isset($setting) && $setting->death_notes == 1) checked @endif>
                            <label class="custom-control-label" for="death_notes">Death Notes</label>
                        </div>
                    </div>
                    <div class="form-group float-left ml-5" style="padding-top: 9px;">
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" id="bank_fd_rates" name="bank_fd_rates" value="yes" @if(isset($setting) && $setting->bank_fd_rates == 1) checked @endif>
                            <label class="custom-control-label" for="bank_fd_rates">Bank FD rates</label>
                        </div>
                    </div>
                    <div class="form-group float-left ml-5" style="padding-top: 9px;">
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" id="useful_information" name="useful_information" value="yes" @if(isset($setting) && $setting->useful_information == 1) checked @endif>
                            <label class="custom-control-label" for="useful_information">Useful Information</label>
                        </div>
                    </div>
                    <div class="form-group float-left ml-5" style="padding-top: 9px;">
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" id="saved_news" name="saved_news" value="yes" @if(isset($setting) && $setting->saved_news == 1) checked @endif>
                            <label class="custom-control-label" for="saved_news">Saved News</label>
                        </div>
                    </div>
                </div>
                <div class="text-right">
                    <a href="{{ route('home') }}" class="btn btn-warning mg-r-1">Cancel</a>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </form>
        </div>
    </div>
@endsection