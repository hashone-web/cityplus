@extends('layouts.admin')

@php($title = isset($poll) ? 'Edit Poll - ' . $poll->title: 'Create Poll')
@push('title', yieldTitle($title))

@section('breadcrumb-title', $title)

@section('breadcrumb-link')
    <li class="breadcrumb-item active" aria-current="page">
        <a href="{{ route('polls.index') }}">Polls</a>
    </li>
    <li class="breadcrumb-item active" aria-current="page">{{ $title }}</li>
@endsection

@section('breadcrumb')
    @include('components.breadcrumb')
@endsection

@push('content-class', 'content-fixed')
@push('container-class', 'container-fluid')

@section('content')
    <div class="card">
        <div class="card-body">
            <form id="poll-form" action="{{ isset($poll) ? route('polls.update', ['id' => $poll->id]) : route('polls.store') }}" method="POST" enctype="multipart/form-data" data-type="{{ isset($poll)? 'edit': 'create' }}">
                @csrf
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="title">Title: <span class="tx-danger">*</span></label>
                        <input type="text" class="form-control characters-count{{ $errors->has('title') ? ' is-invalid' : '' }}" id="title" placeholder="Title" name="title" data-maxlength="150" maxlength="150" value="{{isset($poll) ? $poll->title : old('title') }}" required>
                        <div class="clearfix mt-2">
                            <div class="float-left mr-3">
                                <span>Characters</span>
                                <span class="current-length">0</span>
                            </div>
                            <div class="float-left">
                                <span>Characters remaining</span>
                                <span class="remaining-length">150</span>
                            </div>
                        </div>
                        @if ($errors->has('title'))
                            <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('title') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="description">Description:</label>
                        <textarea rows="3" class="form-control characters-count{{ $errors->has('description') ? ' is-invalid' : '' }}" id="description" data-maxlength="400" maxlength="400" placeholder="Description" name="description">{{isset($poll) ? $poll->description : old('description') }}</textarea>
                        <div class="clearfix mt-2">
                            <div class="float-left mr-3">
                                <span>Characters</span>
                                <span class="current-length">0</span>
                            </div>
                            <div class="float-left">
                                <span>Characters remaining</span>
                                <span class="remaining-length">400</span>
                            </div>
                        </div>
                        @if ($errors->has('description'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('description') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="form-row mt-1">
                    <div class="form-group col-md-3">
                        <label for="image">Image:</label>
                        <div class="custom-file">
                            <input type="file" class="poll-image-input custom-file-input{{ $errors->has('image') ? ' is-invalid' : '' }}" id="image" name="image" accept=".png,.jpg,.jpeg" data-accept="['.png','.jpg','.jpeg','.PNG','.JPG','.JPEG']">
                            <label class="custom-file-label" for="image">Choose file</label>
                        </div>
                        <div id="image-files-name">
                            @if(isset($poll) && $poll->image !== null)<div class="mt-2">{{ $poll->image }} <a class="ml-1 remove-poll-image" href="javascript:;" data-poll-id="{{ $poll->id }}"><i class="fa fa-times-circle"></i></a></div>@endif
                        </div>
                        @if ($errors->has('image'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('image') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="form-row mt-2">
                    <div class="form-group col-md-2">
                        <label for="start_datetime">Start DateTime: <span class="tx-danger">*</span></label>
                        <div class="input-group">
                            <input id="start_datetime" type="text" class="form-control readonly-datepicker" name="start_datetime" value="{{isset($poll) ? \Carbon::createFromFormat('Y-m-d H:i:s', $poll->start_datetime)->format('Y-m-d H:i') : old('start_datetime') }}" required onkeypress="return false;">
                            <span class="input-group-addon">
                                <span class="glyphicon glyphicon-calendar"></span>
                            </span>
                        </div>
                        @if ($errors->has('start_datetime'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('start_datetime') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group col-md-2">
                        <label for="end_datetime">End DateTime: <span class="tx-danger">*</span></label>
                        <div class="input-group">
                            <input id="end_datetime" type="text" class="form-control readonly-datepicker" name="end_datetime" value="{{isset($poll) ? \Carbon::createFromFormat('Y-m-d H:i:s', $poll->end_datetime)->format('Y-m-d H:i') : old('end_datetime') }}" required onkeypress="return false;">
                            <span class="input-group-addon">
                                <span class="glyphicon glyphicon-calendar"></span>
                            </span>
                        </div>
                        @if ($errors->has('end_datetime'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('end_datetime') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="policy">Policy:</label>
                        <textarea rows="3" class="form-control characters-count{{ $errors->has('policy') ? ' is-invalid' : '' }}" id="policy" data-maxlength="400" maxlength="400" placeholder="Policy" name="policy">{{isset($poll) ? $poll->policy : old('policy') }}</textarea>
                        <div class="clearfix mt-2">
                            <div class="float-left mr-3">
                                <span>Characters</span>
                                <span class="current-length">0</span>
                            </div>
                            <div class="float-left">
                                <span>Characters remaining</span>
                                <span class="remaining-length">400</span>
                            </div>
                        </div>
                        @if ($errors->has('policy'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('policy') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="clearfix">
                    <div class="clearfix">
                        <h3 class="float-left">Options</h3><span class="float-left ml-2" style="line-height: 36px;"><a class="toggle-deleted-poll-options" href="javascript:;">(show deleted)</a></span>
                    </div>
                    <div class="poll-options-wrap mt-3">
                        @if(isset($poll))
                            @foreach($poll->all_options as $key => $option)
                                <div class="row mt-3 poll-option @if($option->deleted_at) poll-option-deleted d-none @endif">
                                    <div class="col-md-2">
                                        <input class="form-control poll-option-title-input" type="text" name="poll_option_title__ex__{{ $option->id }}" placeholder="Option Title" value="{{ $option->title }}" required>
                                    </div>
                                    <div class="col-md-2 poll-option-btn-wrap">
                                        @if($option->deleted_at)
                                            <a class="btn btn-primary recover-poll-option mr-2" href="javascript:;"><i class="fa fa-undo-alt"></i></a>
                                        @else
                                            <a class="btn btn-primary add-new-poll-option mr-2" href="javascript:;"><i class="fa fa-plus"></i></a>
                                        @endif
                                        @if(count($poll->all_options) > 2 || $key > 1)
                                            <a class="btn btn-primary remove-poll-option" href="javascript:;"><i class="fa fa-minus"></i></a>
                                        @endif
                                    </div>
                                </div>
                            @endforeach
                        @endif
                    </div>
                </div>
                <div class="clearfix mt-5">
                    <div class="form-group float-left">
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" id="allow_multiple_options" name="allow_multiple_options" value="yes" @if(isset($poll) && $poll->allow_multiple_options == 1) checked @endif>
                            <label class="custom-control-label" for="allow_multiple_options">Allow Multiple Selection</label>
                        </div>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-2">
                        <label for="show_result">Show Result: <span class="tx-danger">*</span></label>
                        <select class="form-control poll-show-result-select{{ $errors->has('show_result') ? ' is-invalid' : '' }}" id="show_result" name="show_result" required>
                            @foreach($show_result_options as $show_result_option)
                                <option value="{{ $show_result_option }}" @if(isset($poll) && $poll->show_result == $show_result_option) selected @elseif(!isset($poll) && $show_result_option == 'Never') selected @else @endif>{{ $show_result_option }}</option>
                            @endforeach
                        </select>
                        @if ($errors->has('show_result'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('show_result') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group col-md-2 result-end-datetime-wrap" @if(isset($poll) && $poll->show_result == 'Never') style="display: none;" @elseif(!isset($poll)) style="display: none;" @else @endif>
                        <label for="result_end_datetime">Result End DateTime: <span class="tx-danger">*</span></label>
                        <div class="input-group">
                            <input id="result_end_datetime" type="text" class="form-control readonly-datepicker" name="result_end_datetime" value="{{isset($poll) && $poll->result_end_datetime !== null ? \Carbon::createFromFormat('Y-m-d H:i:s', $poll->result_end_datetime)->format('Y-m-d H:i') : old('result_end_datetime') }}" onkeypress="return false;">
                            <span class="input-group-addon">
                                <span class="glyphicon glyphicon-calendar"></span>
                            </span>
                        </div>
                        @if ($errors->has('result_end_datetime'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('result_end_datetime') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="clearfix">
                    <div class="form-group float-left">
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" id="only_for_registered_user" name="only_for_registered_user" value="yes" @if(isset($poll) && $poll->only_for_registered_user == 1) checked @endif>
                            <label class="custom-control-label" for="only_for_registered_user">Only for Registered User</label>
                        </div>
                    </div>
                </div>
                <div class="clearfix">
                    <div class="form-group form-inline">
                        <div class="custom-control custom-radio mr-3">
                            <input type="radio" id="customRadio1" name="type" class="custom-control-input poll-news-radio" value="Standalone" @if(isset($poll) && $poll->type == 'Standalone') checked @elseif(!isset($poll)) checked @else @endif>
                            <label class="custom-control-label" for="customRadio1">Stand-Alone</label>
                        </div>

                        <div class="custom-control custom-radio">
                            <input type="radio" id="customRadio2" name="type" class="custom-control-input poll-news-radio" value="News" @if(isset($poll) && $poll->type == 'News') checked @endif>
                            <label class="custom-control-label" for="customRadio2">Bind with News</label>
                        </div>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-2 poll-position-wrap" @if(isset($poll) && $poll->type == 'News') style="display: none;" @endif>
                        <label for="position">Position: <span class="tx-danger">*</span></label>
                        <input type="text" class="form-control poll-position-input only-numeric{{ $errors->has('position') ? ' is-invalid' : '' }}" id="position" placeholder="Position" name="position" value="{{isset($poll) ? $poll->position : old('position') }}" maxlength="2" @if(!isset($poll)) required @elseif(isset($poll) && $poll->type == 'Standalone') required @else @endif>
                        @if ($errors->has('position'))
                            <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('position') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="form-group col-md-6 poll-news-wrap" @if(!isset($poll)) style="display: none;" @elseif(isset($poll) && $poll->type == 'Standalone') style="display: none;" @else @endif>
                        <label for="news_id">News: <span class="tx-danger">*</span></label>
                        <select class="form-control poll-news-select{{ $errors->has('news_id') ? ' is-invalid' : '' }}" id="news_id" name="news_id" style="width: 100%;" @if(isset($poll) && $poll->type == 'News') required @endif>
                            <option disabled selected value>Select News</option>
                            @foreach($news as $news_single)
                                <option value="{{ $news_single->id }}" @if(isset($poll) && $poll->news_id == $news_single->id) selected @endif>{{ $news_single->headline }}</option>
                            @endforeach
                        </select>
                        @if ($errors->has('news_id'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('news_id') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-2">
                        <label for="status">Status: <span class="tx-danger">*</span></label>
                        <select class="form-control poll-status-select{{ $errors->has('status') ? ' is-invalid' : '' }}" id="status" name="status" required>
                            @foreach($statuses as $status)
                                <option value="{{ $status }}" @if(isset($poll) && $poll->status == $status) selected @elseif(!isset($poll) && $status == 'Active') selected @else @endif>{{ $status }}</option>
                            @endforeach
                        </select>
                        @if ($errors->has('status'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('status') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="text-right">
                    <a href="{{ route('polls.index') }}" class="btn btn-warning mg-r-1">Cancel</a>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </form>
        </div>
    </div>
@endsection

@section('last-scripts')
    @if(isset($poll))
    <script type="text/javascript">
        window.poll_options = {{count($poll->all_options)}};
    </script>
    @endif
@endsection