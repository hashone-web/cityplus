@extends('layouts.admin')

@php($title = isset($public_page) ? 'Edit Public Page - ' . $public_page->title: 'Create Public Page')
@push('title', yieldTitle($title))

@section('breadcrumb-title', $title)

@section('breadcrumb-link')
    <li class="breadcrumb-item active" aria-current="page">
        <a href="{{ route('public.pages.index') }}">Public Pages</a>
    </li>
    <li class="breadcrumb-item active" aria-current="page">{{ $title }}</li>
@endsection

@section('breadcrumb')
    @include('components.breadcrumb')
@endsection

@push('content-class', 'content-fixed')
@push('container-class', 'container-fluid')

@section('content')
    <div class="card">
        <div class="card-body">
            <form action="{{ isset($public_page) ? route('public.pages.update', ['id' => $public_page->id]) : route('public.pages.store') }}" method="POST">
                @csrf
                <div class="form-row">
                    <div class="form-group col-md-8">
                        <label for="title">Title: <span class="tx-danger">*</span></label>
                        <input type="text" class="form-control{{ $errors->has('title') ? ' is-invalid' : '' }}" id="title" placeholder="Title" name="title" value="{{isset($public_page) ? $public_page->title : old('title') }}" required>
                        @if ($errors->has('title'))
                            <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('title') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-12 {{ $errors->has('content') ? ' has-error' : '' }}">
                        <label for="content">Content: <span class="tx-danger">*</span></label>
                        <textarea name="content" id="summernote">{{ isset($public_page) ? $public_page->content : old('content') }}</textarea>
                        @if ($errors->has('content'))
                            <span class="help-block">
                            <strong>{{ $errors->first('content') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>
                <div class="text-right">
                    <a href="{{ route('public.pages.index') }}" class="btn btn-warning mg-r-1">Cancel</a>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </form>
        </div>
    </div>
@endsection
