@extends('layouts.admin')

@php($title = isset($quote) ? 'Edit Quote - ' . $quote->name: 'Create Quote')
@push('title', yieldTitle($title))

@section('breadcrumb-title', $title)

@section('breadcrumb-link')
    <li class="breadcrumb-item active" aria-current="page">
        <a href="{{ route('quotes.index') }}">Quotes</a>
    </li>
    <li class="breadcrumb-item active" aria-current="page">{{ $title }}</li>
@endsection

@section('breadcrumb')
    @include('components.breadcrumb')
@endsection

@push('content-class', 'content-fixed')
@push('container-class', 'container-fluid')

@section('content')
    <div class="card">
        <div class="card-body">
            <form id="quote-form" action="{{ isset($quote) ? route('quotes.update', ['id' => $quote->id]) : route('quotes.store') }}" method="POST">
                @csrf
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="text">Quote Text: <span class="tx-danger">*</span></label>
                        <textarea rows="3" class="form-control characters-count{{ $errors->has('text') ? ' is-invalid' : '' }}" id="text" data-maxlength="400" maxlength="400" placeholder="Details" name="text" required="">{{isset($quote) ? $quote->text : old('text') }}</textarea>
                        <div class="clearfix mt-2">
                            <div class="float-left mr-3">
                                <span>Characters</span>
                                <span class="current-length">0</span>
                            </div>
                            <div class="float-left">
                                <span>Characters remaining</span>
                                <span class="remaining-length">400</span>
                            </div>
                        </div>
                        @if ($errors->has('text'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('text') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-2">
                        <label for="date">Quote Date: <span class="tx-danger">*</span></label>
                        <div class="input-group">
                            <input id="date" type="text" class="form-control readonly-datepicker" name="date" value="{{isset($quote) ? $quote->date : old('date') }}" required onkeypress="return false;">
                            <span class="input-group-addon">
                                <span class="glyphicon glyphicon-calendar"></span>
                            </span>
                        </div>
                        @if ($errors->has('date'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('date') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="text-right">
                    <a href="{{ route('quotes.index') }}" class="btn btn-warning mg-r-1">Cancel</a>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </form>
        </div>
    </div>
@endsection