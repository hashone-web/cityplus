@extends('layouts.admin')

@php($title = isset($rejection_reason) ? 'Edit Rejection Reason - ' . $rejection_reason->name: 'Create Rejection Reason')
@push('title', yieldTitle($title))

@section('breadcrumb-title', $title)

@section('breadcrumb-link')
    <li class="breadcrumb-item active" aria-current="page">
        <a href="{{ route('rejection_reasons.index') }}">rejection reasons</a>
    </li>
    <li class="breadcrumb-item active" aria-current="page">{{ $title }}</li>
@endsection

@section('breadcrumb')
    @include('components.breadcrumb')
@endsection

@push('content-class', 'content-fixed')
@push('container-class', 'container-fluid')

@section('content')
    <div class="card">
        <div class="card-body">
            <form action="{{ isset($rejection_reason) ? route('rejection_reasons.update', ['id' => $rejection_reason->id]) : route('rejection_reasons.store') }}" method="POST">
                @csrf
                <div class="form-row">
                    <div class="form-group col-md-3">
                        <label for="title">Title: <span class="tx-danger">*</span></label>
                        <input type="text" class="form-control{{ $errors->has('title') ? ' is-invalid' : '' }}" id="title" placeholder="Title" name="title" maxlength="200" value="{{isset($rejection_reason) ? $rejection_reason->title : old('title') }}" required>
                        @if ($errors->has('title'))
                            <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('title') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>
                <div class="text-right">
                    <a href="{{ route('rejection_reasons.index') }}" class="btn btn-warning mg-r-1">Cancel</a>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </form>
        </div>
    </div>
@endsection