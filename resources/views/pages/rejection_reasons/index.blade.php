@extends('layouts.admin')

@php($title = 'News Rejection Reasons')
@push('title', yieldTitle($title))

@section('breadcrumb-title', $title)

@section('breadcrumb-link')
    <li class="breadcrumb-item active" aria-current="page">{{ $title }}</li>
@endsection

@section('breadcrumb-btn')
    <a class="btn btn-sm pd-x-15 btn-primary btn-uppercase mg-l-5" href="{{ route('rejection_reasons.create') }}"><i data-feather="plus" class="wd-10 mg-r-5"></i>Create News Rejection Reason</a>
@endsection

@section('breadcrumb')
    @include('components.breadcrumb')
@endsection

@push('content-class', 'content-fixed')
@push('container-class', 'container-fluid')

@section('content')
    <div class="card">
        <div class="card-body">
            <div data-label="Example" class="df-example demo-table">
                <div class="table-responsive">
                    <table class="table table-striped mg-b-0">
                        <thead>
                        <tr>
                            <th scope="col">ID</th>
                            <th scope="col">@sortablelink('title')</th>
                            <th scope="col">@sortablelink('created_at', 'Added On')</th>
                            @if(auth()->user()->can('edit settings'))
                                <th scope="col">Actions</th>
                            @endif
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($rejection_reasons as $rejection_reason)
                        <tr>
                            <th scope="row">{{ $rejection_reason->id }}</th>
                            <td>{{ $rejection_reason->title }}</td>
                            <td>{{ getDateFormat($rejection_reason->created_at) }}</td>
                            @if(auth()->user()->can('edit settings'))
                                <td>
                                    @can('edit settings')<a href="{{ route('rejection_reasons.edit', ['id' => $rejection_reason->id ]) }}"><i data-feather="edit-2" width="18" height="18"></i></a>@endcan
                                    @can('edit settings')
                                        <a href="javascript:;" onclick="deleteRecords(event, 'News Rejection Reason', 'rejection_reason-{{ $rejection_reason->id }}-delete')"><i data-feather="trash" width="18" height="18" stroke="red"></i></a>

                                        <form id="rejection_reason-{{ $rejection_reason->id }}-delete" action="{{ route('rejection_reasons.delete', ['id' => $rejection_reason->id]) }}" method="POST" style="display: none;">
                                            @method('delete')
                                            @csrf
                                        </form>
                                    @endcan
                                </td>
                            @endif
                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div><!-- table-responsive -->
            </div><!-- df-example -->
        </div>
    </div>
    <div class="mt-4">
        {!! $rejection_reasons->appends(\Request::except('page'))->render() !!}
    </div>
@endsection
