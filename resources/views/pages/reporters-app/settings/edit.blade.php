@extends('layouts.admin')

@php($title = 'Reporter App Settings')
@push('title', yieldTitle($title))

@section('breadcrumb-title', $title)

@section('breadcrumb-link')
    <li class="breadcrumb-item active" aria-current="page">{{ $title }}</li>
@endsection

@section('breadcrumb')
    @include('components.breadcrumb')
@endsection

@push('content-class', 'content-fixed')
@push('container-class', 'container-fluid')

@section('content')
    <div class="card">
        <div class="card-body">
            <form action="{{ route('reporters-app.settings.update') }}" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="login_not_allowed_message">Login not Allowed Message:</label>
                        <input type="text" class="form-control{{ $errors->has('login_not_allowed_message') ? ' is-invalid' : '' }}" id="login_not_allowed_message" placeholder="Login not Allowed Message" name="login_not_allowed_message" value="{{isset($setting) ? $setting->login_not_allowed_message : old('login_not_allowed_message') }}">
                        @if ($errors->has('login_not_allowed_message'))
                            <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('login_not_allowed_message') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="privacy_policy_page_link">Privacy Policy Page Link:</label>
                        <input type="text" class="form-control{{ $errors->has('privacy_policy_page_link') ? ' is-invalid' : '' }}" id="privacy_policy_page_link" placeholder="Privacy Policy Page Link" name="privacy_policy_page_link" value="{{isset($setting) ? $setting->privacy_policy_page_link : old('privacy_policy_page_link') }}" required>
                        @if ($errors->has('privacy_policy_page_link'))
                            <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('privacy_policy_page_link') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="form-group col-md-6">
                        <label for="terms_of_use_page_link">Terms of Use Page Link:</label>
                        <input type="text" class="form-control{{ $errors->has('terms_of_use_page_link') ? ' is-invalid' : '' }}" id="terms_of_use_page_link" placeholder="Terms of Use Page Link" name="terms_of_use_page_link" value="{{isset($setting) ? $setting->terms_of_use_page_link : old('terms_of_use_page_link') }}" required>
                        @if ($errors->has('terms_of_use_page_link'))
                            <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('terms_of_use_page_link') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>
                <div class="text-right">
                    <a href="{{ route('home') }}" class="btn btn-warning mg-r-1">Cancel</a>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </form>
        </div>
    </div>
@endsection