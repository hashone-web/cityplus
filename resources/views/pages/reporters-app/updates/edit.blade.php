@extends('layouts.admin')

@php($title = 'Reporters App Updates')
@push('title', yieldTitle($title))

@section('breadcrumb-title', $title)

@section('breadcrumb-link')
    <li class="breadcrumb-item active" aria-current="page">{{ $title }}</li>
@endsection

@section('breadcrumb')
    @include('components.breadcrumb')
@endsection

@push('content-class', 'content-fixed')
@push('container-class', 'container-fluid')

@section('content')
    <div class="card">
        <div class="card-body">
            <form action="{{ route('reporters-app.updates.update') }}" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="form-row">
                    <div class="form-group col-md-9">
                        <label for="name">Name: <span class="tx-danger">*</span></label>
                        <input type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" id="name" placeholder="Name" name="name" value="{{isset($update) ? $update->name : old('name') }}">
                        @if ($errors->has('name'))
                            <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('name') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-9">
                        <label for="app_ver">App Ver: <span class="tx-danger">*</span></label>
                        <input type="text" class="form-control{{ $errors->has('app_ver') ? ' is-invalid' : '' }}" id="app_ver" placeholder="Application Version" name="app_ver" value="{{isset($update) ? $update->app_ver : old('app_ver') }}">
                        @if ($errors->has('app_ver'))
                            <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('app_ver') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-9">
                        <label for="link">Link: <span class="tx-danger">*</span></label>
                        <input type="text" class="form-control{{ $errors->has('link') ? ' is-invalid' : '' }}" id="link" placeholder="Link" name="link" value="{{isset($update) ? $update->link : old('link') }}">
                        @if ($errors->has('link'))
                            <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('link') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-9">
                        <label for="link_text">Link Text: <span class="tx-danger">*</span></label>
                        <input type="text" class="form-control{{ $errors->has('link_text') ? ' is-invalid' : '' }}" id="link_text" placeholder="Link Text" name="link_text" value="{{isset($update) ? $update->link_text : old('link_text') }}">
                        @if ($errors->has('link_text'))
                            <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('link_text') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" id="force" name="force" value="yes" @if(isset($update) && $update->force == 1) checked @endif>
                            <label class="custom-control-label" for="force">Force</label>
                        </div>
                    </div>
                </div>
                <div class="text-right">
                    <a href="{{ route('home') }}" class="btn btn-warning mg-r-1">Cancel</a>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </form>
        </div>
    </div>
@endsection