@extends('layouts.admin')

@php($title = 'Reports')
@push('title', yieldTitle($title))

@section('breadcrumb-title', $title)

@section('breadcrumb-link')
    <li class="breadcrumb-item active" aria-current="page">{{ $title }}</li>
@endsection

@section('breadcrumb')
    @include('components.breadcrumb')
@endsection

@push('content-class', 'content-fixed')
@push('container-class', 'container-fluid')

@section('content')
    <div class="card">
        <div class="card-body">
            <div data-label="Example" class="df-example demo-table">
                <div class="table-responsive">
                    <table class="table table-striped mg-b-0">
                        <thead>
                        <tr>
                            <th scope="col">ID</th>
                            <th scope="col">News</th>
                            <th scope="col">Report Type</th>
                            <th scope="col">Message</th>
                            <th scope="col">Date</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($reports as $report)
                            <tr>
                                <th scope="row">{{ $report->id }}</th>
                                <td>{{ $report->news->headline }}</td>
                                <td>{{ $report->type->name }}</td>
                                <td>{{ $report->message }}</td>
                                <td>{{ getDateFormat($report->created_at) }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div><!-- table-responsive -->
            </div><!-- df-example -->
        </div>
    </div>
    <div class="mt-4">
        {!! $reports->links() !!}
    </div>
@endsection
