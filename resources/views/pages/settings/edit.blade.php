@extends('layouts.admin')

@php($title = 'Settings')
@push('title', yieldTitle($title))

@section('breadcrumb-title', $title)

@section('breadcrumb-link')
    <li class="breadcrumb-item active" aria-current="page">{{ $title }}</li>
@endsection

@section('breadcrumb')
    @include('components.breadcrumb')
@endsection

@push('content-class', 'content-fixed')
@push('container-class', 'container-fluid')

@section('content')
    <div class="card">
        <div class="card-body">
            <form action="{{ route('settings.update') }}" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="form-row">
                    <div class="form-group col-md-3">
                        <label for="contact_whatsapp_number">Contact Whatsapp Number:</label>
                        <input type="text" class="form-control{{ $errors->has('contact_whatsapp_number') ? ' is-invalid' : '' }}" id="contact_whatsapp_number" placeholder="+918866553779" name="contact_whatsapp_number" pattern="^[0-9+]+$" maxlength="15" value="{{isset($setting) ? $setting->contact_whatsapp_number : old('contact_whatsapp_number') }}">
                        @if ($errors->has('contact_whatsapp_number'))
                            <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('contact_whatsapp_number') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="form-group col-md-3">
                        <label for="contact_email">Contact Email:</label>
                        <input type="email" class="form-control{{ $errors->has('contact_email') ? ' is-invalid' : '' }}" id="contact_email" placeholder="jhalakjaviya@gmail.com" name="contact_email" value="{{isset($setting) ? $setting->contact_email : old('contact_email') }}">
                        @if ($errors->has('contact_email'))
                            <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('contact_email') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="android_share_app_link">Android Share App Link:</label>
                        <input type="text" class="form-control{{ $errors->has('android_share_app_link') ? ' is-invalid' : '' }}" id="android_share_app_link" name="android_share_app_link" value="{{isset($setting) ? $setting->android_share_app_link : old('android_share_app_link') }}">
                        @if ($errors->has('android_share_app_link'))
                            <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('android_share_app_link') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="form-group col-md-6">
                        <label for="ios_share_app_link">iOS Share App Link:</label>
                        <input type="text" class="form-control{{ $errors->has('ios_share_app_link') ? ' is-invalid' : '' }}" id="ios_share_app_link" name="ios_share_app_link" value="{{isset($setting) ? $setting->ios_share_app_link : old('ios_share_app_link') }}">
                        @if ($errors->has('ios_share_app_link'))
                            <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('ios_share_app_link') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="android_share_description">Android Share Description:</label>
                        <textarea class="form-control{{ $errors->has('android_share_description') ? ' is-invalid' : '' }}" id="android_share_description" name="android_share_description">{{ isset($setting) ? $setting->android_share_description : old('android_share_description') }}</textarea>
                        @if ($errors->has('android_share_description'))
                            <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('android_share_description') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="form-group col-md-6">
                        <label for="ios_share_description">iOS Share Description:</label>
                        <textarea class="form-control{{ $errors->has('ios_share_description') ? ' is-invalid' : '' }}" id="ios_share_description" name="ios_share_description">{{ isset($setting) ? $setting->ios_share_description : old('ios_share_description') }}</textarea>
                        @if ($errors->has('ios_share_description'))
                            <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('ios_share_description') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="android_share_description_gujarati">Android Share Description Gujarati:</label>
                        <textarea class="form-control{{ $errors->has('android_share_description_gujarati') ? ' is-invalid' : '' }}" id="android_share_description_gujarati" name="android_share_description_gujarati">{{ isset($setting) ? $setting->android_share_description_gujarati : old('android_share_description_gujarati') }}</textarea>
                        @if ($errors->has('android_share_description_gujarati'))
                            <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('android_share_description_gujarati') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="form-group col-md-6">
                        <label for="ios_share_description_gujarati">iOS Share Description Gujarati:</label>
                        <textarea class="form-control{{ $errors->has('ios_share_description_gujarati') ? ' is-invalid' : '' }}" id="ios_share_description_gujarati" name="ios_share_description_gujarati">{{ isset($setting) ? $setting->ios_share_description_gujarati : old('ios_share_description_gujarati') }}</textarea>
                        @if ($errors->has('ios_share_description_gujarati'))
                            <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('ios_share_description_gujarati') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="file_photo_description">File Photo Description:</label>
                        <textarea class="form-control{{ $errors->has('file_photo_description') ? ' is-invalid' : '' }}" id="file_photo_description" name="file_photo_description">{{ isset($setting) ? $setting->file_photo_description : old('file_photo_description') }}</textarea>
                        @if ($errors->has('file_photo_description'))
                            <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('file_photo_description') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="form-group col-md-6">
                        <label for="file_photo_description_gujarati">File Photo Description Gujarati:</label>
                        <textarea class="form-control{{ $errors->has('file_photo_description_gujarati') ? ' is-invalid' : '' }}" id="file_photo_description_gujarati" name="file_photo_description_gujarati">{{ isset($setting) ? $setting->file_photo_description_gujarati : old('file_photo_description_gujarati') }}</textarea>
                        @if ($errors->has('file_photo_description_gujarati'))
                            <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('file_photo_description_gujarati') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-3">
                        <label for="news_share_bottom_image">News Share Bottom Image:</label>
                        <div class="custom-file">
                            <input type="file" class="custom-file-input{{ $errors->has('news_share_bottom_image') ? ' is-invalid' : '' }}" id="news_share_bottom_image" name="news_share_bottom_image" accept=".png,.jpg,.jpeg" data-accept="['.png','.jpg','.jpeg','.PNG','.JPG','.JPEG']" multiple>
                            <label class="custom-file-label" for="news_share_bottom_image">Choose file</label>
                        </div>
                        <div class="news_share_bottom_image" class="mt-2" style="color: #0168fa;">
                        @if(isset($setting) && $setting->news_share_bottom_image)
                            <a class="mt-2 d-block" href="{{ $setting->news_share_bottom_image_full_path }}" target="_blank">
                                <img src="{{ $setting->news_share_bottom_image_full_path }}" style="max-height: 25px;">
                            </a>
                        @endif
                        </div>
                        @if ($errors->has('news_share_bottom_image'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('news_share_bottom_image') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group col-md-3">
                        <label for="news_share_bottom_image_gujarati">News Share Bottom Image Gujarati:</label>
                        <div class="custom-file">
                            <input type="file" class="custom-file-input{{ $errors->has('news_share_bottom_image_gujarati') ? ' is-invalid' : '' }}" id="news_share_bottom_image_gujarati" name="news_share_bottom_image_gujarati" accept=".png,.jpg,.jpeg" data-accept="['.png','.jpg','.jpeg','.PNG','.JPG','.JPEG']" multiple>
                            <label class="custom-file-label" for="news_share_bottom_image_gujarati">Choose file</label>
                        </div>
                        <div class="news_share_bottom_image_gujarati" class="mt-2" style="color: #0168fa;">
                        @if(isset($setting) && $setting->news_share_bottom_image_gujarati)
                            <a class="mt-2 d-block" href="{{ $setting->news_share_bottom_image_gujarati_full_path }}" target="_blank">
                                <img src="{{ $setting->news_share_bottom_image_gujarati_full_path }}" style="max-height: 25px;">
                            </a>
                        @endif
                        </div>
                        @if ($errors->has('news_share_bottom_image_gujarati'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('news_share_bottom_image_gujarati') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="android_qr_code_link">Android QR Code Link:</label>
                        <input type="text" class="form-control{{ $errors->has('android_qr_code_link') ? ' is-invalid' : '' }}" id="android_qr_code_link" name="android_qr_code_link" value="{{isset($setting) ? $setting->android_qr_code_link : old('android_qr_code_link') }}">
                        @if ($errors->has('android_qr_code_link'))
                            <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('android_qr_code_link') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="form-group col-md-6">
                        <label for="ios_qr_code_link">iOS QR Code Link:</label>
                        <input type="text" class="form-control{{ $errors->has('ios_qr_code_link') ? ' is-invalid' : '' }}" id="ios_qr_code_link" name="ios_qr_code_link" value="{{isset($setting) ? $setting->ios_qr_code_link : old('ios_qr_code_link') }}">
                        @if ($errors->has('ios_qr_code_link'))
                            <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('ios_qr_code_link') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="privacy_policy_page_link">Privacy Policy Page Link:</label>
                        <input type="text" class="form-control{{ $errors->has('privacy_policy_page_link') ? ' is-invalid' : '' }}" id="privacy_policy_page_link" name="privacy_policy_page_link" value="{{isset($setting) ? $setting->privacy_policy_page_link : old('privacy_policy_page_link') }}" required>
                        @if ($errors->has('privacy_policy_page_link'))
                            <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('privacy_policy_page_link') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="form-group col-md-6">
                        <label for="terms_of_use_page_link">Terms of Use Page Link:</label>
                        <input type="text" class="form-control{{ $errors->has('terms_of_use_page_link') ? ' is-invalid' : '' }}" id="terms_of_use_page_link" name="terms_of_use_page_link" value="{{isset($setting) ? $setting->terms_of_use_page_link : old('terms_of_use_page_link') }}" required>
                        @if ($errors->has('terms_of_use_page_link'))
                            <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('terms_of_use_page_link') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="instagram_page_link">Instagram Page Link:</label>
                        <input type="text" class="form-control{{ $errors->has('instagram_page_link') ? ' is-invalid' : '' }}" id="instagram_page_link" name="instagram_page_link" value="{{isset($setting) ? $setting->instagram_page_link : old('instagram_page_link') }}">
                        @if ($errors->has('instagram_page_link'))
                            <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('instagram_page_link') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="reporter_join_link">Reporter Join Link:</label>
                        <input type="text" class="form-control{{ $errors->has('reporter_join_link') ? ' is-invalid' : '' }}" id="reporter_join_link" name="reporter_join_link" value="{{isset($setting) ? $setting->reporter_join_link : old('reporter_join_link') }}">
                        @if ($errors->has('reporter_join_link'))
                            <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('reporter_join_link') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="form-group col-md-6">
                        <label for="bank_fd_rates_link">Bank FD Rates Link:</label>
                        <input type="text" class="form-control{{ $errors->has('bank_fd_rates_link') ? ' is-invalid' : '' }}" id="bank_fd_rates_link" name="bank_fd_rates_link" value="{{isset($setting) ? $setting->bank_fd_rates_link : old('bank_fd_rates_link') }}">
                        @if ($errors->has('bank_fd_rates_link'))
                            <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('bank_fd_rates_link') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>
                <div class="form-row mt-3">
                    <div class="form-group float-left" style="padding-top: 9px; padding-left: 6px;">
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" id="market_values" name="market_values" value="yes" @if(isset($setting) && $setting->market_values == 1) checked @endif>
                            <label class="custom-control-label" for="market_values">Market Values</label>
                        </div>
                    </div>
                    <div class="form-group float-left ml-5" style="padding-top: 9px;">
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" id="corona_virus_data" name="corona_virus_data" value="yes" @if(isset($setting) && $setting->corona_virus_data == 1) checked @endif>
                            <label class="custom-control-label" for="corona_virus_data">Corona Virus Data</label>
                        </div>
                    </div>
                </div>
                <div class="form-row mt-3">
                    <div class="form-group col-md-3">
                        <div class="mb-2">Ads Type: <span class="tx-danger">*</span></div>
                        <div class="form-inline">
                            <div class="custom-control custom-radio mr-3">
                                <input type="radio" id="customRadio1" name="ads_type" class="custom-control-input setting-type-radio" value="none" @if(isset($setting) && $setting->ads_type == 'none') checked @elseif(!isset($setting)) checked @else @endif>
                                <label class="custom-control-label" for="customRadio1">None</label>
                            </div>
                            <div class="custom-control custom-radio mr-3">
                                <input type="radio" id="customRadio2" name="ads_type" class="custom-control-input setting-type-radio" value="admob" @if(isset($setting) && $setting->ads_type == 'admob') checked @endif>
                                <label class="custom-control-label" for="customRadio2">Admob</label>
                            </div>
                            <div class="custom-control custom-radio mr-3">
                                <input type="radio" id="customRadio3" name="ads_type" class="custom-control-input setting-type-radio" value="facebook" @if(isset($setting) && $setting->ads_type == 'facebook') checked @endif>
                                <label class="custom-control-label" for="customRadio3">Facebook</label>
                            </div>
                            <div class="custom-control custom-radio">
                                <input type="radio" id="customRadio4" name="ads_type" class="custom-control-input setting-type-radio" value="both" @if(isset($setting) && $setting->ads_type == 'both') checked @endif>
                                <label class="custom-control-label" for="customRadio4">Both</label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="poll_policy">Poll Policy:</label>
                        <textarea rows="4" class="form-control{{ $errors->has('poll_policy') ? ' is-invalid' : '' }}" id="poll_policy" name="poll_policy" placeholder="Poll Policy">{{ isset($setting) ? $setting->poll_policy : old('poll_policy') }}</textarea>
                        @if ($errors->has('poll_policy'))
                            <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('poll_policy') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="form-group col-md-6">
                        <label for="corona_virus_data_message">Corona Virus Data Message:</label>
                        <textarea rows="4" class="form-control{{ $errors->has('corona_virus_data_message') ? ' is-invalid' : '' }}" id="corona_virus_data_message" name="corona_virus_data_message" placeholder="Corona Virus Data Message">{{ isset($setting) ? $setting->corona_virus_data_message : old('corona_virus_data_message') }}</textarea>
                        @if ($errors->has('corona_virus_data_message'))
                            <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('corona_virus_data_message') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-3">
                        <label for="watermark_image">Watermark Image:</label>
                        <div class="custom-file">
                            <input type="file" class="custom-file-input{{ $errors->has('watermark_image') ? ' is-invalid' : '' }}" id="watermark_image" name="watermark_image" accept=".png,.jpg,.jpeg" data-accept="['.png','.jpg','.jpeg','.PNG','.JPG','.JPEG']" multiple>
                            <label class="custom-file-label" for="watermark_image">Choose file</label>
                        </div>
                        <div class="watermark_image" class="mt-2" style="color: #0168fa;">
                        @if(isset($setting) && $setting->watermark_image)
                            <a class="mt-2 d-block" href="{{ $setting->watermark_image_full_path }}" target="_blank">
                                <img src="{{ $setting->watermark_image_full_path }}" style="max-height: 25px;">
                            </a>
                        @endif
                        </div>
                        @if ($errors->has('watermark_image'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('watermark_image') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="text-right">
                    <a href="{{ route('home') }}" class="btn btn-warning mg-r-1">Cancel</a>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </form>
        </div>
    </div>
@endsection
