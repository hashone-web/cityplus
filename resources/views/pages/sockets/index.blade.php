<!doctype html>
<html>
    <head>
        <script src='https://cdnjs.cloudflare.com/ajax/libs/socket.io/2.3.0/socket.io.js'></script>
        <script>
            var socket = io('https://test.cityplus.co.in:4000');

            socket.on('connect', function() { 
                socket.emit('new_connection', { data: { cities: [1, 2] } });
            });
            socket.on('disconnect', function() {
                console.log('disconnected');
            });
            socket.on('error', function (e) {
                console.log('System', e ? e : 'A unknown error occurred');
            });
            
            socket.on('data', function(data) {
                console.log('data', data);
            });

            socket.on('featured_news', function(data) {
                console.log('featured_news', data);
            });
        </script>
    </head>
    <body>
        <ul id='messages'></ul>
    </body>
</html>