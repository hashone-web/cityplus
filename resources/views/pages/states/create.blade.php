@extends('layouts.admin')

@php($title = isset($state) ? 'Edit State - ' . $state->name: 'Create State')
@push('title', yieldTitle($title))

@section('breadcrumb-title', $title)

@section('breadcrumb-link')
    <li class="breadcrumb-item active" aria-current="page">
        <a href="{{ route('states.index') }}">States</a>
    </li>
    <li class="breadcrumb-item active" aria-current="page">{{ $title }}</li>
@endsection

@section('breadcrumb')
    @include('components.breadcrumb')
@endsection

@push('content-class', 'content-fixed')
@push('container-class', 'container-fluid')

@section('content')
    <div class="card">
        <div class="card-body">
            <form action="{{ isset($state) ? route('states.update', ['id' => $state->id]) : route('states.store') }}" method="POST">
                @csrf
                <div class="form-row">
                    <div class="form-group col-md-3">
                        <label for="name">Name: <span class="tx-danger">*</span></label>
                        <input type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" id="name" placeholder="Name" name="name" pattern="^[A-Za-z ]+$" maxlength="30" value="{{isset($state) ? $state->name : old('name') }}" required>
                        @if ($errors->has('name'))
                            <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('name') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="form-group col-md-3">
                        <label for="country">Country: <span class="tx-danger">*</span></label>
                        <select class="form-control state-country-select" id="country" name="country" required>
                            <option disabled selected value>Select Country</option>
                            @foreach($countries as $country)
                                <option value="{{ $country->id }}" @if(isset($state) && $state->country_id == $country->id) selected @endif>{{ $country->name }}</option>
                            @endforeach
                        </select>

                    </div>
                </div>
                <div class="text-right">
                    <a href="{{ route('states.index') }}" class="btn btn-warning mg-r-1">Cancel</a>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </form>
        </div>
    </div>
@endsection
