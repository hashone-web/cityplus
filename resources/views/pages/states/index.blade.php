@extends('layouts.admin')

@php($title = 'States')
@push('title', yieldTitle($title))

@section('breadcrumb-title', $title)

@section('breadcrumb-link')
    <li class="breadcrumb-item active" aria-current="page">{{ $title }}</li>
@endsection

@section('breadcrumb-btn')
    <a class="btn btn-sm pd-x-15 btn-primary btn-uppercase mg-l-5" href="{{ route('states.create') }}"><i data-feather="plus" class="wd-10 mg-r-5"></i>Create State</a>
@endsection

@section('breadcrumb')
    @include('components.breadcrumb')
@endsection

@push('content-class', 'content-fixed')
@push('container-class', 'container-fluid')

@section('content')
    <div class="card">
        <div class="card-body">
            <div data-label="Example" class="df-example demo-table">
                <div class="table-responsive">
                    <table class="table table-striped mg-b-0">
                        <thead>
                        <tr>
                            <th scope="col">ID</th>
                            <th scope="col">@sortablelink('name')</th>
                            <th scope="col">Country</th>
                            <th scope="col">@sortablelink('created_at', 'Added On')</th>
                            @if(auth()->user()->can('edit locations') || auth()->user()->can('delete locations'))
                                <th scope="col">Actions</th>
                            @endif
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($states as $state)
                            <tr>
                                <th scope="row">{{ $state->id }}</th>
                                <td>{{ $state->name }}</td>
                                <td>{{ $state->country->name }}</td>
                                <td>{{ getDateFormat($state->created_at) }}</td>
                                @if(auth()->user()->can('edit locations') || auth()->user()->can('delete locations'))
                                    <td>
                                        @can('edit locations')<a href="{{ route('states.edit', ['id' => $state->id ]) }}"><i data-feather="edit-2" width="18" height="18"></i></a>@endcan
                                        @can('delete locations')
                                            <a href="javascript:;" onclick="deleteRecords(event, 'State', 'state-{{ $state->id }}-delete')"><i data-feather="trash" width="18" height="18" stroke="red"></i></a>

                                            <form id="state-{{ $state->id }}-delete" action="{{ route('states.delete', ['id' => $state->id]) }}" method="POST" style="display: none;">
                                                @method('delete')
                                                @csrf
                                            </form>
                                        @endcan
                                    </td>
                                @endif
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div><!-- table-responsive -->
            </div><!-- df-example -->
        </div>
    </div>
    <div class="mt-4">
        {!! $states->appends(\Request::except('page'))->render() !!}
    </div>
@endsection
