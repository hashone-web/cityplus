@extends('layouts.admin')

@php($title = isset($user) ? 'Edit User - ' . $user->name: 'Create User')
@push('title', yieldTitle($title))

@section('breadcrumb-title', $title)

@section('breadcrumb-link')
    <li class="breadcrumb-item active" aria-current="page">
        <a href="{{ route('users.index') }}">Users</a>
    </li>
    <li class="breadcrumb-item active" aria-current="page">{{ $title }}</li>
@endsection

@section('breadcrumb')
    @include('components.breadcrumb')
@endsection

@push('content-class', 'content-fixed')
@push('container-class', 'container-fluid')

@section('content')
    <div class="card">
        <div class="card-body">
            <form id="user_form" action="{{ isset($user) ? route('users.update', ['id' => $user->id]) : route('users.store') }}" method="POST" enctype="multipart/form-data" autocomplete="off">
                @csrf
                <div class="form-row">
                    <div class="form-group col-md-10">
                        @if(!isset($profile_edit))
                        <div class="form-row">
                            @if(isset($user) && $user->getRoleNames()->first() !== 'Super Admin' || !isset($user))
                            <div class="form-group col-md-3">
                                <label for="role">Role: <span class="tx-danger">*</span></label>
                                <select class="form-control {{ $errors->has('role') ? ' is-invalid' : '' }} user-role-select" id="role" name="role" required>
                                    <option disabled selected value>Select Role</option>
                                    @foreach($roles as $role)
                                        <option value="{{ $role->name }}" @if(isset($user) && $user->getRoleNames()->first() == $role->name) selected @endif>{{ $role->name }}</option>
                                    @endforeach
                                </select>
                                @if ($errors->has('role'))
                                    <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('role') }}</strong>
                                </span>
                                @endif
                            </div>
                            @endif
                        </div>
                        @endif
                        <div class="form-row">
                            <div class="form-group col-md-3">
                                <label for="name">Name: <span class="tx-danger">*</span></label>
                                <input type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" id="name" placeholder="Name" name="name" maxlength="30" value="{{isset($user) ? $user->name : old('name') }}" required>
                                @if ($errors->has('name'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </span>
                                @endif
                            </div>
                            <div id="reporter-alternate-name-input-wrap" class="form-group col-md-3" @if((isset($user) && $user->getRoleNames()->first() !== 'Reporter' || !isset($user))) style="display: none;" @endif>
                                <label for="alternate_name">English Name: <span class="tx-danger">*</span></label>
                                <input type="text" class="form-control{{ $errors->has('alternate_name') ? ' is-invalid' : '' }}" id="alternate_name" placeholder="English Name" name="alternate_name" maxlength="30" value="{{isset($user) ? $user->alternate_name : old('alternate_name') }}">
                                @if ($errors->has('alternate_name'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('alternate_name') }}</strong>
                                </span>
                                @endif
                            </div>
                            <div class="form-group col-md-3">
                                <label for="mobile_number">Mobile Number: <span class="tx-danger">*</span></label>
                                <input type="text" class="form-control{{ $errors->has('mobile_number') ? ' is-invalid' : '' }}" id="mobile_number" placeholder="Mobile Number" name="mobile_number" maxlength="10" value="{{isset($user) ? $user->mobile_number : old('mobile_number') }}" required>
                                @if ($errors->has('mobile_number'))
                                    <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('mobile_number') }}</strong>
                                </span>
                                @endif
                            </div>
                            <div class="form-group col-md-3">
                                <label for="profile_picture">Profile Picture</label>
                                <div class="custom-file">
                                    <input type="file" class="form-control{{ $errors->has('profile_picture') ? ' is-invalid' : '' }}" id="profile_picture" name="profile_picture" accept=".png,.jpg,.jpeg" data-accept="['.png','.jpg','.jpeg', '.PNG', '.JPG', '.JPEG']">
                                    <label class="custom-file-label" for="profile_picture">Choose file</label>
                                </div>
                                <input type="hidden" id="profile_picture_data64" name="profile_picture_data64">
                                @if(isset($user) && $user->profile_picture)
                                    <span>{{$user->profile_picture}}</span>
                                @endif
                                @if ($errors->has('profile_picture'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('profile_picture') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                        @if(!isset($profile_edit))
                        <div class="form-row">
                            <div class="form-group col-md-3">
                                <label for="status">Status: <span class="tx-danger">*</span></label>
                                <select class="form-control {{ $errors->has('status') ? ' is-invalid' : '' }} user-status-select" id="status" name="status" required>
                                    <option disabled selected value>Select Status</option>
                                    @foreach($statuses as $status)
                                        <option value="{{ $status }}" @if(isset($user) && $user->status == $status) selected @endif>{{ $status }}</option>
                                    @endforeach
                                </select>
                                @if ($errors->has('status'))
                                    <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('status') }}</strong>
                                </span>
                                @endif
                            </div>
                            <div id="reporter-anonymous-reporting-input-wrap" class="form-group col-md-3" @if(isset($user) && $user->getRoleNames()->first() == 'Reporter') style="padding-top: 38px;" @elseif(!isset($user)) style="padding-top: 38px; display: none;" @else style="padding-top: 38px;" @endif>
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" id="anonymous_reporting" name="anonymous_reporting" value="yes" @if(isset($user) && $user->anonymous_reporting == 1) checked @endif>
                                    <label class="custom-control-label" for="anonymous_reporting">Anonymous Reporting</label>
                                </div>
                            </div>
                            <div class="form-group col-md-3">
                                <label for="reference">Reference: </label>
                                <input type="text" class="form-control{{ $errors->has('reference') ? ' is-invalid' : '' }}" id="reference" placeholder="Reference" name="reference" maxlength="100" value="{{isset($user) ? $user->reference : old('reference') }}" autocomplete="off">
                                @if ($errors->has('reference'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('reference') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                        <div id="reporter-city-select-wrap" class="form-row" @if((isset($user) && $user->getRoleNames()->first() !== 'Reporter' || !isset($user))) style="display: none;" @endif>
                            <div class="form-group col-md-12">
                                <label for="cities">Reporter Cities: <span class="tx-danger">*</span></label>
                                <select class="form-control reporter-cities-select" id="cities" name="cities[]" multiple style="width: 100%;">
                                    @foreach($cities as $city)
                                        <option value="{{ $city->id }}" @if(isset($user) && in_array($city->id, $user->reporting_cities_ids->toArray())) selected @endif>{{ $city->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        @endif
                        <div class="form-row">
                            <div class="form-group col-md-3">
                                <label for="password">Password: @if(!isset($user)) <span class="tx-danger">*</span>@endif</label>
                                <input type="password" class="form-control {{ $errors->has('password') ? ' is-invalid' : '' }}" id="password" placeholder="Password" name="password" @if(!isset($user)) required @endif autocomplete="off">
                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                                @endif
                            </div>
                            <div class="form-group col-md-3">
                                <label for="password-confirm">Confirm Password: @if(!isset($user)) <span class="tx-danger">*</span>@endif</label>
                                <input type="password" class="form-control" id="password-confirm" placeholder="Confirm Password" name="password_confirmation" @if(!isset($user)) required @endif>
                            </div>
                        </div>
                    </div>
                    <div class="form-group col-md-2">
                        <img id="upload-demo-image" src="{{ isset($user) && $user->profile_picture !== null ? $user->original_profile_picture_link : asset('assets/img/image-placeholder.jpg') }}" style="display: none;">
                        <label for="profile_picture" style="margin-left: 24px;">Profile Picture Preview</label>
                        <div id="upload-demo" style="width: 200px; height: 200px;"></div>
                    </div>
                </div>
                <div class="text-right">
                    <a href="{{ route('users.index') }}" class="btn btn-warning mg-r-1">Cancel</a>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </form>
        </div>
    </div>
@endsection
