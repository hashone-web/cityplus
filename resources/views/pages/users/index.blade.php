@extends('layouts.admin')

@php($title = 'Users')
@if($is_reporters_page)
@php($title = 'Reporters')
@endif
@push('title', yieldTitle($title))

@section('breadcrumb-title', $title)

@section('breadcrumb-link')
    <li class="breadcrumb-item active" aria-current="page">{{ $title }}</li>
@endsection

@section('breadcrumb-btn')
    @can('create users')
    <a class="btn btn-sm pd-x-15 btn-primary btn-uppercase mg-l-5" href="{{ route('users.create') }}"><i data-feather="plus" class="wd-10 mg-r-5"></i>Create User</a>
    @endcan
@endsection

@section('content-header')
    <div class="float-left ml-3">
        <a class="btn btn-primary" href="{{ $is_reporters_page? route('users.reporters'): route('users.index') }}"><i class="fa fa-redo"></i></a>
    </div>
    
    <div class="float-left ml-5 form-inline">
        <form method="GET" action="{{ $is_reporters_page? route('users.reporters'): route('users.index') }}" autocomplete="off">
            <div class="float-left mr-2">
                <select class="form-control filter-sort-select card-header__filter-wrap__search-type" name="search_type" style="width: 140px" required="">
                    <option value="" selected disabled hidden>Select</option>
                    <option value="name" @if(isset($query_parameters['search_type']) && $query_parameters['search_type'] == 'name')selected @endif>Name</option>
                    <option value="mobile_number" @if(isset($query_parameters['search_type']) && $query_parameters['search_type'] == 'mobile_number')selected @endif>Mobile Number</option>
                </select>
            </div>
            <input class="form-control float-left card-header__filter-wrap__search-value mr-2" type="text" name="search_value" value="{{isset($query_parameters['search_value']) ? $query_parameters['search_value'] : ''}}" placeholder="Search">
            <button type="submit" class="btn btn-primary btn-addon btn-rounded float-left card-header__filter-wrap__search-btn">Search</button>
        </form>
    </div>

    @if(!$is_reporters_page)
    <div class="float-left ml-5">
        <select class="filter-by-role-select" onchange="location = this.value;" style="width: 200px;">
            <option disabled selected value>Role</option>
            @foreach($roles as $role)
                <option value="{{ route('users.index', array_merge($query_parameters, ['role' => $role->name])) }}" @if(isset($query_parameters['role']) && $query_parameters['role'] == $role->name) selected @endif>{{ $role->name }}</option>
            @endforeach
        </select>
    </div>
    @endif

    <div class="float-left ml-3">
        <select class="filter-by-status-select" onchange="location = this.value;" style="width: 120px;">
            <option disabled selected value>Status</option>
            @foreach($statuses as $status)
                <option value="{{ route('users.index', array_merge($query_parameters, ['status' => $status])) }}" @if(isset($query_parameters['status']) && $query_parameters['status'] == $status) selected @endif>{{ $status }}</option>
            @endforeach
        </select>
    </div>
@endsection

@section('breadcrumb')
    @include('components.breadcrumb')
@endsection

@push('content-class', 'content-fixed')
@push('container-class', 'container-fluid')

@section('content')
    <div class="card">
        <div class="card-body">
            <div data-label="Example" class="df-example demo-table">
                <div class="table-responsive">
                    <table class="table table-striped mg-b-0 table--users">
                        <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">@sortablelink('name')</th>
                            @if($is_reporters_page)
                                <th scope="col">@sortablelink('alternate_name', 'English Name')</th>
                            @endif
                            <th scope="col">@sortablelink('mobile_number', 'Mobile Number')</th>
                            @if($is_reporters_page)
                                <th scope="col">News Count</th>
                            @else
                                <th scope="col">Role</th>
                            @endif
                            @if($is_reporters_page)
                                <th scope="col">Anonymous</th>
                            @endif
                            <th scope="col">@sortablelink('status')</th>
                            <th scope="col">@sortablelink('created_at', 'Added On')</th>
                            @if(auth()->user()->can('edit users') || auth()->user()->can('delete users'))
                                <th scope="col">Actions</th>
                            @endif
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($users as $user)
                            <tr>
                                <td><img class="rounded-circle" style="width:40px;" src="{{ $user->profile_picture_link }}" alt="activity-user"></td>
                                <td>{{ $user->name }}</td>
                                @if($is_reporters_page)
                                    <td>{{ $user->alternate_name }}</td>
                                @endif
                                <td>{{ $user->mobile_number }}</td>
                                @if($is_reporters_page)
                                    <td>{{ $user->news_count }}</td>
                                @else
                                    <td>{{ $user->getRoleNames()->first() }}</td>
                                @endif
                                <td>{{ $user->anonymous_reporting }}</td>
                                <td><span class="{{ getStatusBadgeClass($user->status) }}">{{ $user->status }}</span></td>
                                <td>{{ getDateFormat($user->created_at) }}</td>
                                @if(auth()->user()->can('edit users') || auth()->user()->can('delete users'))
                                    <td>
                                        @can('edit users')<a href="{{ route('users.edit', ['id' => $user->id ]) }}"><i data-feather="edit-2" width="18" height="18"></i></a>@endcan
                                        @can('delete users')
                                            <a href="javascript:;" onclick="deleteRecords(event, 'User', 'user-{{ $user->id }}-delete')"><i data-feather="trash" width="18" height="18" stroke="red"></i></a>

                                            <form id="user-{{ $user->id }}-delete" action="{{ route('users.delete', ['id' => $user->id]) }}" method="POST" style="display: none;">
                                                @method('delete')
                                                @csrf
                                            </form>
                                        @endcan
                                    </td>
                                @endif
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div><!-- table-responsive -->
            </div><!-- df-example -->
        </div>
    </div>
    <div class="mt-4">
        {!! $users->appends(\Request::except('page'))->render() !!}
    </div>

@endsection
