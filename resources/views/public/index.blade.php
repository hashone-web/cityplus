@extends('layouts.public')
@push('content-class', 'home-page-wrap')
@section('content')
<div class="header-banner" >
	<div class="container">
		@include('public.market_values')
	</div>
</div>
<div class="card-stack container">
	@foreach($news as $news_single)
	<div class="news-card">
		<div class="row news__card">
			<div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
				@if(isset($news_single->thumbs) && count($news_single->thumbs))
				<div class="news-media news-thum center-cropped" style="background-image: url('{{ $news_single->thumbs->first()->full_path }}');">
					<img class="news-media-image news-thum__img" src="{{ $news_single->thumbs->first()->full_path }}">
				</div>
				@endif
			</div>
			<div class="col-lg-8 col-md-12 col-sm-12 col-xs-12">
				<div class="news-card-title news-right-box p-all">
					<div class="news-card-author-time news-card-author-time-in-title">
						@if($news_single->is_breaking)<span class="author__card mr-1">BREAKING NEWS</span>@endif
						<span class="author__card-city mr-1">{{ $news_single->city->name }}</span>
						<span class="news__time ml-1">{{ getDateFormat($news_single->datetime) }}</span>
					</div>
					<a href="{{ route('public.home.single', [ 'id' => $news_single->id ]) }}"><h3 class="headline mt-2">{{ $news_single->headline }}</h3></a>
					<div class="articleBody" style="white-space: nowrap; text-overflow: ellipsis;">{!! $news_single->article !!}</div>
				</div>
			</div>
		</div>
	</div>
	@endforeach

	<div class="mt-4" style="margin-bottom: 36px;">
        {!! $news->appends(\Request::except('page'))->render() !!}
    </div>
</div>
<div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 header__price__res header__box">
	<div class="container">
		<div class="header-app__res" >
			<div class="header-app__text">
				For the best experience use our app
			</div>
			<div class="clearfix">
				<a href="https://cityplus.app.link" target="_blank"><img src="{{ asset('images/Play_Store.png') }}"></a></a>
			</div>
		</div>
	</div>
</div>
@endsection