@if(isset($market_values['gold']) && isset($market_values['gold']['is_increased']))
<div class="clearfix">
	<div class="float-left header__price">
		<div class="header__gold">
			<img class="news-gold-image" src="{{ asset('images/gold.png') }}">
			<div class="goldTxt">સોનુ : </div>
			<div class="{{ $market_values['gold']['is_increased'] ? 'specifics': 'earnings' }}">{{ $market_values['gold']['value'] }} <span class="fa {{ $market_values['gold']['is_increased'] ? 'fa-caret-up': 'fa-caret-down' }}"></span></div>
		</div>
		<div class="line"></div>
		<div class="header__gold">
			<img class="news-gold-image" src="{{ asset('images/gold1.png') }}">
			<div class="goldTxt">ચાંદી :</div>
			<div class="{{ $market_values['silver']['is_increased'] ? 'specifics': 'earnings' }}">{{ $market_values['silver']['value'] }} <span class="fa {{ $market_values['silver']['is_increased'] ? 'fa-caret-up': 'fa-caret-down' }}"></span></div>
		</div>
		<div class="line"></div>
		<div class="header__earnings">
			<img class="news-gold-image" src="{{ asset('images/earnings.png') }}">
			<div class="goldTxt">સેનસેકસ :</div>
			<div class="{{ $market_values['sensex']['is_increased'] ? 'specifics': 'earnings' }}">{{ $market_values['sensex']['value'] }} <span class="fa {{ $market_values['sensex']['is_increased'] ? 'fa-caret-up': 'fa-caret-down' }}"></span></div>
		</div>
	</div>
	<div class="float-right header__price1">
		<div class="header-app" >
			<div class="header-app__text">
				For the best experience use our app
			</div>
			<div class="clearfix" style="padding-left: 20px;">
				<a href="https://play.google.com/store/apps/details?id=com.cityplus.local.news.info" target="_blank"><img src="{{ asset('images/Play_Store.png') }}"></a></a>
				<a href="https://apps.apple.com/in/app/cityplus-local-news-and-more/id1497445430" target="_blank"><img src="{{ asset('images/app-store-icon.png') }}"></a></a>
			</div>
		</div>
	</div>
</div>
@endif