@extends('layouts.public')

@section('head')
<meta property="og:title" content="{{ $news->headline }}">
<meta name="twitter:title" content="{{ $news->headline }}">

<meta property="og:description" content="{{ $news->article }}">
<meta name="twitter:description" content="{{ $news->article }}">

@if(isset($news_single->thumbs) && count($news_single->thumbs))
<meta property="og:image" content="{{ $news_single->thumbs->first()->full_path }}">
<meta name="twitter:image" content="{{ $news_single->thumbs->first()->full_path }}">
<meta name="twitter:card" content="{{ $news_single->thumbs->first()->full_path }}">
@endif
<meta property="og:url" content="{{ route('public.home.single', [ 'id' => $news->id ]) }}">
@endsection

@section('content')
    <div class="header-banner" >
        <div class="container">
            @include('public.market_values')
        </div>
    </div>

    <div class="container">
        <div class="card-stack">
            <div class="news-card__slider">
              <div class="row news__card">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="Modern-Slider">
                        @if(isset($news->thumbs) && count($news->thumbs))
                          @foreach($news->thumbs as $thumb)
                          <!-- Item -->
                          <div class="item">
                            <div class="img-fill">
                              <img src="{{ $thumb->full_path }}">
                              <div class="info">
                              </div>
                            </div>
                          </div>
                          @endforeach
                        @endif
                        <!-- // Item -->
                    </div>
                   {{--social-icon--}}
                    <div id="fixed-social">
                      <div>
                        <a href="https://www.facebook.com/sharer/sharer.php?u={{ route('public.home.single', [ 'id' => $news->id ]) }}" class="fixed-facebook" target="_blank"><i class="fa fa-facebook"></i></a>
                      </div>
                      <div>
                        <a href="https://twitter.com/home?status={{ route('public.home.single', [ 'id' => $news->id ]) }}" class="fixed-twitter" target="_blank"><i class="fa fa-twitter icon"></i></a>
                      </div>
                      <div>
                        <a class="fixed-gplus" href="whatsapp://send?text={{ route('public.home.single', [ 'id' => $news->id ]) }}" data-action="share/whatsapp/share" onClick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600');return false;" target="_blank" title="Share on whatsapp"><i class="fa fa-whatsapp icon"></i></a>
                      </div>
                      <div>
                        <a href="https://www.linkedin.com/shareArticle?mini=true&url={{ route('public.home.single', [ 'id' => $news->id ]) }}&title=&summary={{ $news->headline }}" class="fixed-linkedin" target="_blank"><i class="fa fa-linkedin"></i></a>
                      </div>
                    </div>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                  <div class="news-card-title1 news-right-box">
                    <div class="news-card-author-time news-card-author-time-in-title">
                        @if($news->is_breaking)<span class="author__card mr-1">BREAKING NEWS</span>@endif
                        <span class="author__card-city mr-1">{{ $news->city->name }}</span>
                        <span class="author__name mr-10 dsp">By :<span>{{ $news->reporter->name }}</span> </span>
                        <span class="news__time ml-1">{{ getDateFormat($news->datetime) }}<span class="counter dsp" data-count="150"><img src="{{ asset('images/visible.png') }}" style="padding-right:5px"> 100</span></span>
                        
                    </div>
                    <div class="news-card-author-time__set">
                        <span class="author__name">By :<span> Suresh Mehta </span> </span>
                        <span class="counter" data-count="150"><img src="{{ asset('images/visible.png') }}" style="padding-right:5px">100</span>
                    </div>

                    <div class="heading__title">
                    <h3 class="headline">{{ $news->headline }}</h3>
                    </div>
                    <div class="header__artical">
                      <div class="articleBody" style="max-height: initial;">{!! $news->article !!}</div>
                    </div>
                   </div>
                </div>
              </div> 
            </div>
          </div>

      <div class="footer__box">
        <div class="container">
          <div id="fixed-social__res">
            <div class="social__name">
              <p>Share :</p>
            </div>
            <div class="social__icon">
              <a href="https://www.facebook.com/sharer/sharer.php?u={{ route('public.home.single', [ 'id' => $news->id ]) }}" class="fixed-facebook" target="_blank"><i class="fa fa-facebook"></i></a>
           
              <a href="https://twitter.com/home?status={{ route('public.home.single', [ 'id' => $news->id ]) }}" class="fixed-twitter" target="_blank"><i class="fa fa-twitter icon"></i></a>

              <a class="fixed-gplus" href="whatsapp://send?text={{ route('public.home.single', [ 'id' => $news->id ]) }}" data-action="share/whatsapp/share" onClick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600');return false;" target="_blank" title="Share on whatsapp"><i class="fa fa-whatsapp icon"></i></a>
            
              <a href="https://www.linkedin.com/shareArticle?mini=true&url={{ route('public.home.single', [ 'id' => $news->id ]) }}&title=&summary={{ $news->headline }}" class="fixed-linkedin" target="_blank"><i class="fa fa-linkedin"></i></a>
            </div>
          </div>

          <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 header__price__res">
            <div class="header-app__res" >
                <div class="header-app__text">
                  For the best experience use our app
                </div>
                <div class="clearfix">
                  <a href="https://cityplus.app.link" target="_blank"><img src="{{ asset('images/Play_Store.png') }}"></a></a>
                </div>
            </div>
          </div>
        </div>
      </div>
    </div>
@endsection
         