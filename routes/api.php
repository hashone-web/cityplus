<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => 'token'], function () {
    
    // Reporters App API
    Route::group([
        'prefix' => 'reporters-app',
    ], function() {
        Route::group(['middleware' => 'throttle:100'], function () {
            Route::post('login/request', 'Api\AuthController@loginRequest');
            Route::post('login', 'Api\AuthController@login');
        });

        Route::group(['middleware' => 'auth:api'], function () {
            Route::get('user', 'Api\AuthController@user');
            Route::post('logout', 'Api\AuthController@logout');

            Route::get('news', 'Reporter\ApiController@news');
            Route::post('news', 'Reporter\ApiController@createNews');
        });

        Route::get('/cities', 'Api\NewsController@cities');

        Route::get('/settings', 'Reporter\ApiController@settings')->name('api.settings');
        Route::get('/updates', 'Reporter\ApiController@updates')->name('api.updates');
        Route::get('/extras', 'Reporter\ApiController@extras')->name('api.extras');
    });


    // User Application API
    Route::group(['middleware' => 'throttle:100'], function () {
        Route::post('login/request', 'Api\AuthController@loginRequest');
        Route::post('login', 'Api\AuthController@login');
    });

    Route::group(['middleware' => 'auth:api'], function () {
        Route::post('user/update', 'Api\AuthController@update');
        Route::post('logout', 'Api\AuthController@logout');

        Route::get('user', 'Api\AuthController@user');
    });

    Route::get('/test-notification', 'Api\NewsController@testNotification')->name('api.test.notification');

    Route::get('/news', 'Api\NewsController@index')->name('api.news.index');
    Route::get('/news/{id}', 'Api\NewsController@show')->name('api.news.show');
    Route::post('/news/{id}/statistics', 'Api\NewsController@statistics')->name('api.news.statistics');

    Route::get('/featured-news', 'Api\NewsController@featured_news')->name('api.featured.news.index');
    Route::get('/reporters', 'Api\NewsController@reporters')->name('api.news.reporters');
    Route::get('/cities', 'Api\NewsController@cities')->name('api.news.cities');
    Route::get('/districts', 'Api\NewsController@districts')->name('api.news.districts');
    Route::post('/devices/cities', 'Api\NewsController@deviceCities')->name('api.devices.cities');
    Route::get('/report-types', 'Api\NewsController@reportTypes')->name('api.report.types');
    Route::post('/reports', 'Api\NewsController@reports')->name('api.reports');
    Route::get('/settings', 'Api\NewsController@settings')->name('api.settings');
    Route::get('/updates', 'Api\NewsController@updates')->name('api.updates');
    Route::get('/additional-data', 'Api\NewsController@additionalData')->name('api.additional.data');

    Route::get('/devices/bookmarks', 'Api\DeviceController@getBookmarks')->name('api.device.bookmarks.get');
    Route::post('/devices/bookmarks', 'Api\DeviceController@storeBookmarks')->name('api.device.bookmarks.store');

    Route::post('/devices/views', 'Api\DeviceController@storeViews')->name('api.device.views.store');

    Route::post('/devices/reactions', 'Api\DeviceController@storeReactions')->name('api.device.reactions.store');

    Route::post('/devices/shares', 'Api\DeviceController@storeShares')->name('api.device.shares.store');

    Route::post('/devices/news-sensitivities', 'Api\DeviceController@storeSensitivity')->name('api.device.news.sensitivity');
    
    Route::post('/devices/notifications', 'Api\DeviceController@notifications')->name('api.devices.notifications');
    
    Route::post('/devices/locations', 'Api\DeviceController@locations')->name('api.devices.locations');

    Route::post('/corona-virus-data/shares', 'Api\DeviceController@storeCoronaVirusDataShares')->name('api.corona.virus.data.shares.store');

    Route::post('/devices/location-permissions', 'Api\DeviceController@locationPermissions')->name('api.device.location.permissions');

    // Polls
    Route::post('/polls/{id}/votes', 'Poll\PollController@votes')->name('api.poll.votes.store');
    Route::post('/polls/devices/clicks', 'Poll\PollAPIController@storeResultViews')->name('api.poll.result.views.store');

    // StaticFeedbacks
    Route::get('/static-feedbacks/1', 'Api\StaticFeedbackController@index')->name('api.static.feedbacks.index');
    Route::post('/static-feedbacks/1/steps/1', 'Api\StaticFeedbackController@stepOne')->name('api.static.feedbacks.step.one');
    Route::post('/static-feedbacks/1/steps/2', 'Api\StaticFeedbackController@stepTwo')->name('api.static.feedbacks.step.two');
    Route::post('/static-feedbacks/1/close', 'Api\StaticFeedbackController@close')->name('api.static.feedbacks.close');

    Route::post('/devices/notifications/clicks', 'Api\NewsController@notificationClick')->name('api.news.notifications.clicks');
    Route::post('/devices/campaigns/clicks', 'Api\DeviceController@campaignDevices')->name('api.campaign.devices');

    // Memories
    Route::post('/memories/devices/reactions', 'Memory\MemoryAPIController@storeReactions')->name('api.memories.device.reactions.store');
    Route::post('/memories/devices/shares', 'Memory\MemoryAPIController@storeShares')->name('api.memories.device.shares.store');
    Route::post('/memories/devices/clicks', 'Memory\MemoryAPIController@storeClicks')->name('api.memories.device.clicks.store');

    // Quotes
    Route::post('/quotes/devices/reactions', 'Quote\QuoteAPIController@storeReactions')->name('api.quotes.device.reactions.store');
    Route::post('/quotes/devices/shares', 'Quote\QuoteAPIController@storeShares')->name('api.quotes.device.shares.store');

    // Ads
    Route::post('/ads/devices/reactions', 'Ads\AdsAPIController@storeReactions')->name('api.ads.device.reactions.store');
    Route::post('/ads/devices/shares', 'Ads\AdsAPIController@storeShares')->name('api.ads.device.shares.store');
});
