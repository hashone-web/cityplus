<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Http\Request;
use App\Events\NewsPublished;

Auth::routes(['register' => false, 'reset' => false, 'verify' => false]);

Route::get('/pages/{slug}', 'Other\PublicPageController@show')->name('public.pages.show');

Route::group(['middleware' => 'auth'], function () {
    Route::get('/profile', 'User\UserController@profileEdit')->name('profile.edit');
    Route::post('/users/{id}', 'User\UserController@update')->name('users.update');

    Route::post('/users/{id}/fcm-token', 'User\UserController@fcmToken')->name('users.fcm.token.store');

    Route::group(['middleware' => ['permission:view user reports']], function () {
        // ReportTypes
        Route::get('/reports', 'Other\ReportController@index')->name('reports.index');
    });

    // Users CRUD
    Route::group(['middleware' => ['permission:edit settings']], function () {
        Route::get('/settings', 'Other\SettingController@edit')->name('settings.edit');
        Route::post('/settings', 'Other\SettingController@update')->name('settings.update');

        Route::group([
            'prefix' => 'reporters-app',
        ], function() {
            Route::get('/settings', 'Reporter\SettingController@edit')->name('reporters-app.settings.edit');
            Route::post('/settings', 'Reporter\SettingController@update')->name('reporters-app.settings.update');
        
            Route::get('/updates', 'Reporter\UpdateController@edit')->name('reporters-app.updates.edit');
            Route::post('/updates', 'Reporter\UpdateController@update')->name('reporters-app.updates.update');

            Route::get('/news-rejection-reasons', 'News\NewsRejectionReasonController@index')->name('rejection_reasons.index');
            Route::post('/news-rejection-reasons', 'News\NewsRejectionReasonController@store')->name('rejection_reasons.store');
            Route::get('/news-rejection-reasons/create', 'News\NewsRejectionReasonController@create')->name('rejection_reasons.create');
            Route::post('/news-rejection-reasons/{id}', 'News\NewsRejectionReasonController@update')->name('rejection_reasons.update');
            Route::get('/news-rejection-reasons/{id}/edit', 'News\NewsRejectionReasonController@edit')->name('rejection_reasons.edit');
            Route::delete('/news-rejection-reasons/{id}', 'News\NewsRejectionReasonController@destroy')->name('rejection_reasons.delete');
        });

        Route::get('/admin-panel-settings', 'Other\AdminPanelSettingController@edit')->name('admin.panel.settings.edit');
        Route::post('/admin-panel-settings', 'Other\AdminPanelSettingController@update')->name('admin.panel.settings.update');

        Route::get('/updates', 'Other\UpdateController@edit')->name('updates.edit');
        Route::post('/updates', 'Other\UpdateController@update')->name('updates.update');

        Route::get('/application-installs', 'Other\ApplicationInstallController@index')->name('application.installs.index');
        
        Route::get('/devices', 'Other\DeviceController@index')->name('devices.index');
        Route::get('/devices/{id}/locations', 'Other\DeviceController@locations')->name('devices.locations');
    
        Route::get('/devices/static-feedbacks', 'Feedback\StaticFeedbackController@index')->name('devices.feedbacks.static.index');
        
        Route::get('/daily-active-user-by-city-reports', 'Reports\DailyActiveUsersByCitiesReportController@index')->name('daily.active.user.by.city.reports.index');

        Route::get('/news-statistics-reports', 'Statistics\NewsStatisticsReportController@index')->name('news.statistics.reports.index');
        
        Route::get('/ads-statistics-reports', 'Statistics\AdsStatisticsReportController@index')->name('ads.statistics.reports.index');

        Route::get('/custom-notifications/create', 'Notification\CustomNotificationController@index')->name('custom.notifications.index');
        Route::post('/custom-notifications', 'Notification\CustomNotificationController@store')->name('custom.notifications.store');
    });

    Route::group(['middleware' => ['permission:edit corona virus data']], function () {    
        Route::get('/plus/settings', 'Plus\PlusSettingController@edit')->name('plus.settings.edit');
        Route::post('/plus/settings', 'Plus\PlusSettingController@update')->name('plus.settings.update');

        Route::get('/corona-virus-data', 'Other\CoronaVirusDataController@index')->name('corona.virus.data.index');
        Route::post('/corona-virus-data/last-updated-at', 'Other\CoronaVirusDataController@lastUpdatedAt')->name('corona.virus.data.update.last.updated.at');
        Route::post('/corona-virus-data/{id}', 'Other\CoronaVirusDataController@update')->name('corona.virus.data.update');
    });

    // News CRUD
    Route::group(['middleware' => ['permission:view useful info']], function () {
        Route::get('/news/useful', 'News\NewsController@index')->name('news.useful');
    });
    
    Route::group(['middleware' => ['permission:view news']], function () {
        Route::get('/home', 'News\NewsController@index')->name('home');

        Route::group(['middleware' => ['permission:create news']], function () {
            Route::post('/news', 'News\NewsController@store')->name('news.store');
            Route::get('/news/create', 'News\NewsController@create')->name('news.create');
            Route::get('/news/reporting-cities/{reporter_id}', 'News\NewsController@reportingCities')->name('news.reporting.cities');
            Route::post('/news/convert-html-to-text', 'News\NewsController@convertHTMLtoText')->name('news.convert-html-to-text');
        });

        Route::group(['middleware' => ['permission:edit news']], function () {
            Route::get('/news/{id}/edit', 'News\NewsController@edit')->name('news.edit');
            Route::post('/news/{id}', 'News\NewsController@update')->name('news.update');
        });

        Route::group(['middleware' => ['permission:delete news']], function () {
            Route::delete('/news/{id}', 'News\NewsController@destroy')->name('news.delete');
            Route::post('/news/media/{id}/delete', 'News\NewsMediaController@destroy')->name('news.media.delete');
        });
    });


    // Users CRUD
    Route::group(['middleware' => ['permission:view users']], function () {
        Route::get('/users', 'User\UserController@index')->name('users.index');
        Route::get('/users/reporters', 'User\UserController@index')->name('users.reporters');

        Route::group(['middleware' => ['permission:create users']], function () {
            Route::post('/users', 'User\UserController@store')->name('users.store');
            Route::get('/users/create', 'User\UserController@create')->name('users.create');
        });

        Route::group(['middleware' => ['permission:edit users']], function () {
            Route::get('/users/{id}/edit', 'User\UserController@edit')->name('users.edit');
        });

        Route::group(['middleware' => ['permission:delete users']], function () {
            Route::delete('/users/{id}', 'User\UserController@destroy')->name('users.delete');
        });
    });

    
    // Roles CRUD
    Route::group(['middleware' => ['permission:edit roles']], function () {
        Route::get('/roles', 'Permission\RoleController@index')->name('roles.index');
        Route::post('/roles', 'Permission\RoleController@store')->name('roles.store');
        Route::get('/roles/create', 'Permission\RoleController@create')->name('roles.create');
        Route::post('/roles/{id}', 'Permission\RoleController@update')->name('roles.update');
        Route::get('/roles/{id}/edit', 'Permission\RoleController@edit')->name('roles.edit');
    });


    // Permissions CRUD
    Route::group(['middleware' => ['permission:edit permissions']], function () {
        Route::get('/permissions', 'Permission\PermissionController@index')->name('permissions.index');
        Route::post('/permissions', 'Permission\PermissionController@store')->name('permissions.store');
        Route::get('/permissions/create', 'Permission\PermissionController@create')->name('permissions.create');
        Route::post('/permissions/{id}', 'Permission\PermissionController@update')->name('permissions.update');
        Route::get('/permissions/{id}/edit', 'Permission\PermissionController@edit')->name('permissions.edit');
    });


    // Polls CRUD
    Route::group(['middleware' => ['permission:view polls']], function () {
        Route::get('/polls', 'Poll\PollController@index')->name('polls.index');
        
        Route::group(['middleware' => ['permission:create polls']], function () {
            Route::post('/polls', 'Poll\PollController@store')->name('polls.store');
            Route::get('/polls/create', 'Poll\PollController@create')->name('polls.create');
        });

        Route::group(['middleware' => ['permission:edit polls']], function () {
            Route::get('/polls/{id}/edit', 'Poll\PollController@edit')->name('polls.edit');
            Route::post('/polls/{id}', 'Poll\PollController@update')->name('polls.update');
        });

        Route::get('/polls/{id}', 'Poll\PollController@show')->name('polls.show');

        Route::group(['middleware' => ['permission:delete polls']], function () {
            Route::post('/polls/{id}/image/delete', 'Poll\PollController@destroyPollImage')->name('polls.image.delete');
            Route::delete('/polls/{id}', 'Poll\PollController@destroy')->name('polls.delete');
        });
    });


    // Ads CRUD
    Route::group(['middleware' => ['permission:advertisements crud']], function () {
        // Advertisers CRUD
        Route::get('/advertisers', 'Ads\AdvertiserController@index')->name('advertisers.index');
        Route::post('/advertisers', 'Ads\AdvertiserController@store')->name('advertisers.store');
        Route::get('/advertisers/create', 'Ads\AdvertiserController@create')->name('advertisers.create');
        Route::post('/advertisers/{id}', 'Ads\AdvertiserController@update')->name('advertisers.update');
        Route::get('/advertisers/{id}/edit', 'Ads\AdvertiserController@edit')->name('advertisers.edit');
        Route::delete('/advertisers/{id}', 'Ads\AdvertiserController@destroy')->name('advertisers.delete');
        

        // Campaigns CRUD
        Route::get('/campaigns', 'Ads\CampaignController@index')->name('campaigns.index');
        Route::post('/campaigns', 'Ads\CampaignController@store')->name('campaigns.store');
        Route::get('/campaigns/create', 'Ads\CampaignController@create')->name('campaigns.create');
        Route::post('/campaigns/{id}', 'Ads\CampaignController@update')->name('campaigns.update');
        Route::get('/campaigns/{id}/edit', 'Ads\CampaignController@edit')->name('campaigns.edit');
        Route::delete('/campaigns/{id}', 'Ads\CampaignController@destroy')->name('campaigns.delete');
    

        Route::post('/campaigns/banners/{id}/delete', 'Ads\CampaignBannerController@destroy')->name('campaigns.banners.delete');
    });


    // Quotes CRUD
    Route::group(['middleware' => ['permission:quotes crud']], function () {
        Route::get('/quotes', 'Quote\QuoteController@index')->name('quotes.index');
        Route::post('/quotes', 'Quote\QuoteController@store')->name('quotes.store');
        Route::get('/quotes/create', 'Quote\QuoteController@create')->name('quotes.create');
        Route::post('/quotes/{id}', 'Quote\QuoteController@update')->name('quotes.update');
        Route::get('/quotes/{id}/edit', 'Quote\QuoteController@edit')->name('quotes.edit');
        Route::delete('/quotes/{id}', 'Quote\QuoteController@destroy')->name('quotes.delete');
    });


    // Memories CRUD
    Route::group(['middleware' => ['permission:memories crud']], function () {
        // Settings CRUD
        Route::get('/memories/settings', 'Memory\MemorySettingController@edit')->name('memories.settings.edit');
        Route::post('/memories/settings', 'Memory\MemorySettingController@update')->name('memories.settings.update');


        // MemoryFrameTypes CRUD
        Route::get('/frame-types', 'Memory\MemoryFrameTypeController@index')->name('frame_types.index');
        Route::post('/frame-types', 'Memory\MemoryFrameTypeController@store')->name('frame_types.store');
        Route::get('/frame-types/create', 'Memory\MemoryFrameTypeController@create')->name('frame_types.create');
        Route::post('/frame-types/{id}', 'Memory\MemoryFrameTypeController@update')->name('frame_types.update');
        Route::get('/frame-types/{id}/edit', 'Memory\MemoryFrameTypeController@edit')->name('frame_types.edit');
        Route::delete('/frame-types/{id}', 'Memory\MemoryFrameTypeController@destroy')->name('frame_types.delete');


        // Agencies CRUD
        Route::get('/agencies', 'Memory\MemoryAgencyController@index')->name('agencies.index');
        Route::post('/agencies', 'Memory\MemoryAgencyController@store')->name('agencies.store');
        Route::get('/agencies/create', 'Memory\MemoryAgencyController@create')->name('agencies.create');
        Route::post('/agencies/{id}', 'Memory\MemoryAgencyController@update')->name('agencies.update');
        Route::get('/agencies/{id}/edit', 'Memory\MemoryAgencyController@edit')->name('agencies.edit');
        Route::delete('/agencies/{id}', 'Memory\MemoryAgencyController@destroy')->name('agencies.delete');
    

        // Memories CRUD
        Route::get('/memories', 'Memory\MemoryController@index')->name('memories.index');
        Route::post('/memories', 'Memory\MemoryController@store')->name('memories.store');
        Route::get('/memories/create', 'Memory\MemoryController@create')->name('memories.create');
        Route::post('/memories/{id}', 'Memory\MemoryController@update')->name('memories.update');
        Route::get('/memories/{id}/edit', 'Memory\MemoryController@edit')->name('memories.edit');
        Route::delete('/memories/{id}', 'Memory\MemoryController@destroy')->name('memories.delete');
    
        Route::delete('/memories/proofs/{id}', 'Memory\MemoryController@destroyProof')->name('memories.proofs.delete');
    });


    // Locations CRUD
    Route::group(['middleware' => ['permission:view locations']], function () {
        // Countries
        Route::get('/countries', 'Location\CountryController@index')->name('countries.index');

        // Districts
        Route::get('/districts', 'Location\DistrictController@index')->name('districts.index');

        // States
        Route::get('/states', 'Location\StateController@index')->name('states.index');

        // Cities
        Route::get('/cities', 'Location\CityController@index')->name('cities.index');

        // CityGroups
        Route::get('/city-groups', 'Location\CityGroupController@index')->name('city.groups.index');

        // Localities
        Route::get('/localities', 'Location\LocalityController@index')->name('localities.index');


        Route::group(['middleware' => ['permission:create locations']], function () {
            // Countries
            Route::post('/countries', 'Location\CountryController@store')->name('countries.store');
            Route::get('/countries/create', 'Location\CountryController@create')->name('countries.create');

            // Districts
            Route::post('/districts', 'Location\DistrictController@store')->name('districts.store');
            Route::get('/districts/create', 'Location\DistrictController@create')->name('districts.create');

            // States
            Route::post('/states', 'Location\StateController@store')->name('states.store');
            Route::get('/states/create', 'Location\StateController@create')->name('states.create');

            // Cities
            Route::post('/cities', 'Location\CityController@store')->name('cities.store');
            Route::get('/cities/create', 'Location\CityController@create')->name('cities.create');

            // CityGroups
            Route::post('/city-groups', 'Location\CityGroupController@store')->name('city.groups.store');
            Route::get('/city-groups/create', 'Location\CityGroupController@create')->name('city.groups.create');

            // Localities
            Route::post('/localities', 'Location\LocalityController@store')->name('localities.store');
            Route::get('/localities/create', 'Location\LocalityController@create')->name('localities.create');
        });

        Route::group(['middleware' => ['permission:edit locations']], function () {
            // Countries
            Route::post('/countries/{id}', 'Location\CountryController@update')->name('countries.update');
            Route::get('/countries/{id}/edit', 'Location\CountryController@edit')->name('countries.edit');

            // Districts
            Route::post('/districts/{id}', 'Location\DistrictController@update')->name('districts.update');
            Route::get('/districts/{id}/edit', 'Location\DistrictController@edit')->name('districts.edit');
            Route::post('/districts/{id}/toggle-status', 'Location\DistrictController@toggleStatus')->name('districts.toggle.status');

            // States
            Route::post('/states/{id}', 'Location\StateController@update')->name('states.update');
            Route::get('/states/{id}/edit', 'Location\StateController@edit')->name('states.edit');

            // Cities
            Route::post('/cities/{id}', 'Location\CityController@update')->name('cities.update');
            Route::get('/cities/{id}/edit', 'Location\CityController@edit')->name('cities.edit');
            Route::post('/cities/{id}/toggle-status', 'Location\CityController@toggleStatus')->name('cities.toggle.status');

            // CityGroups
            Route::post('/city-groups/{id}', 'Location\CityGroupController@update')->name('city.groups.update');
            Route::get('/city-groups/{id}/edit', 'Location\CityGroupController@edit')->name('city.groups.edit');
            Route::post('/city-groups/{id}/toggle-status', 'Location\CityGroupController@toggleStatus')->name('city.groups.toggle.status');

            // Localities
            Route::post('/localities/{id}', 'Location\LocalityController@update')->name('localities.update');
            Route::get('/localities/{id}/edit', 'Location\LocalityController@edit')->name('localities.edit');
        });

        Route::group(['middleware' => ['permission:delete locations']], function () {
            // Countries
            Route::delete('/countries/{id}', 'Location\CountryController@destroy')->name('countries.delete');

            // Districts
            Route::delete('/districts/{id}', 'Location\DistrictController@destroy')->name('districts.delete');

            // States
            Route::delete('/states/{id}', 'Location\StateController@destroy')->name('states.delete');

            // Cities
            Route::delete('/cities/{id}', 'Location\CityController@destroy')->name('cities.delete');

            // CityGroups
            Route::delete('/city-groups/{id}', 'Location\CityGroupController@destroy')->name('city.groups.delete');

            // Localities
            Route::delete('/localities/{id}', 'Location\LocalityController@destroy')->name('localities.delete');
        });
    });


    // Other CRUD
    Route::group(['middleware' => ['permission:view other cruds']], function () {
        // Categories
        Route::get('/categories', 'Other\CategoryController@index')->name('categories.index');

        // Tags
        Route::get('/tags', 'Other\TagController@index')->name('tags.index');

        // PublicPages
        Route::get('/public-pages', 'Other\PublicPageController@index')->name('public.pages.index');

        // ReportTypes
        Route::get('/report-types', 'Other\ReportTypeController@index')->name('report.types.index');

        Route::group(['middleware' => ['permission:create other cruds']], function () {
            // Categories
            Route::post('/categories', 'Other\CategoryController@store')->name('categories.store');
            Route::get('/categories/create', 'Other\CategoryController@create')->name('categories.create');

            // Tags
            Route::post('/tags', 'Other\TagController@store')->name('tags.store');
            Route::get('/tags/create', 'Other\TagController@create')->name('tags.create');

            // PublicPages
            Route::post('/public-pages', 'Other\PublicPageController@store')->name('public.pages.store');
            Route::get('/public-pages/create', 'Other\PublicPageController@create')->name('public.pages.create');

            // ReportTypes
            Route::post('/report-types', 'Other\ReportTypeController@store')->name('report.types.store');
            Route::get('/report-types/create', 'Other\ReportTypeController@create')->name('report.types.create');
        });

        Route::group(['middleware' => ['permission:edit other cruds']], function () {
            // Categories
            Route::get('/categories/sorting', 'Other\CategoryController@sorting')->name('categories.sorting.edit');
            Route::post('/categories/sorting', 'Other\CategoryController@sortingUpdate')->name('categories.sorting.update');
            Route::post('/categories/{id}', 'Other\CategoryController@update')->name('categories.update');
            Route::get('/categories/{id}/edit', 'Other\CategoryController@edit')->name('categories.edit');
            Route::post('/categories/{id}/toggle-status', 'Other\CategoryController@toggleStatus')->name('categories.toggle.status');

            // Tags
            Route::post('/tags/{id}', 'Other\TagController@update')->name('tags.update');
            Route::get('/tags/{id}/edit', 'Other\TagController@edit')->name('tags.edit');

            // PublicPages
            Route::post('/public-pages/{id}', 'Other\PublicPageController@update')->name('public.pages.update');
            Route::get('/public-pages/{id}/edit', 'Other\PublicPageController@edit')->name('public.pages.edit');

            // ReportTypes
            Route::post('/report-types/{id}', 'Other\ReportTypeController@update')->name('report.types.update');
            Route::get('/report-types/{id}/edit', 'Other\ReportTypeController@edit')->name('report.types.edit');
        });

        Route::group(['middleware' => ['permission:delete other cruds']], function () {
            // Categories
            Route::delete('/categories/{id}', 'Other\CategoryController@destroy')->name('categories.delete');

            // Tags
            Route::delete('/tags/{id}', 'Other\TagController@destroy')->name('tags.delete');

            // PublicPages
            Route::delete('/public-pages/{id}', 'Other\PublicPageController@destroy')->name('public.pages.delete');

            // ReportTypes
            Route::delete('/report-types/{id}', 'Other\ReportTypeController@destroy')->name('report.types.delete');
        });
    });
});

Route::get('/', 'Other\PublicController@index')->name('public.home.index');
Route::get('/news/{id}', 'Other\PublicController@single')->name('public.home.single');

