const dotenv = require('dotenv').config({ path: './.env' });

const PORT = process.env.SOCKET_APP_PORT || 4000;

const fs = require('fs');

let server;

if(process.env.SOCKET_APP_SSL == 'yes') {
	// Certificate
	const privateKey = fs.readFileSync(process.env.SSL_CERT_KEY, 'utf8');
	const certificate = fs.readFileSync(process.env.SSL_CERT, 'utf8');
	const ca = fs.readFileSync(process.env.SSL_CA, 'utf8');

	const credentials = {
		key: privateKey,
		cert: certificate,
		ca: ca
	};

	server = require('https').createServer(credentials);
} else {
	server = require('http').createServer();
}

const io = require('socket.io')(server, {
	origins: '*:*',
	serveClient: false,
	// below are engine.IO options
	pingInterval: 10000,
	pingTimeout: 5000,
	cookie: false
});

var redis = require('redis')
var ioredis = require('socket.io-redis')

// Multi-server socket handling allowing you to scale horizontally 
// or use a load balancer with Redis distributing messages across servers.
io.adapter(ioredis({host: 'localhost', port: 6379}));

/*
 * Redis pub/sub
 */

// Listen to local Redis broadcasts
var sub = redis.createClient()

sub.on('error', function (error) {
    // console.log('ERROR ' + error)
});

sub.psubscribe("cityplus_database_private-news-published", function (err, count) {});
sub.psubscribe("cityplus_database_private-news-featured", function (err, count) {});

// Handle messages from channels we're subscribed to
sub.on('pmessage', function (pattern, channel, message) {
    const event_data = JSON.parse(message);

    // console.log('channel:: ', channel);
    // console.log('event_data:: ', event_data);

    if(channel == 'cityplus_database_private-news-published') {
    	io.emit('data', { data: event_data.data.data });
    }
    if(channel == 'cityplus_database_private-news-featured') {
    	io.emit('featured_news', { data: event_data.data.data });
    }
})

io.on('connection', function (socket) {
	// console.log(socket.id, ' socket connected');

	socket.on('disconnect', function () {
		// console.log(socket.id, ' socket disconnected');
	});

	socket.on('new_connection', function (data) {
		// console.log(data);
	});
});

// // Send current time to all connected clients
// function sendTime() {
//     io.emit('data', { data: { id: 1 } });
// }

// // Send current time every 1 secs
// setInterval(sendTime, 30 * 1000);

server.listen(PORT, function(){
	console.log('listening on *:' + PORT);
});